﻿<%@ Page Title="Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationForm-07.aspx.cs" Inherits="WebApplicationForms.WebForm_A07" %>

<%@ Register Src="~/UserControls/SelfEmployedIncomeGrid.ascx" TagPrefix="uc1" TagName="SelfEmployedIncomeGrid" %>
<%@ Register Src="~/UserControls/MonthlyIncomeGrid.ascx" TagPrefix="uc1" TagName="MonthlyIncomeGrid" %>
<%@ Register Src="~/UserControls/ApplicantAssetLiabilityGrid.ascx" TagPrefix="uc1" TagName="ApplicantAssetLiabilityGrid" %>




<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	<style type="text/css">
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

	<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Your Application</h1>
				<h2>Asset and Liability Details</h2>
			</hgroup>
			<p>Your assets and liabilities.</p>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
		<h3>Tell us what you own, and what you owe...</h3>		
		<table class="splitTable">
			<tr>
				<td class="split">					
					<uc1:ApplicantAssetLiabilityGrid runat="server" id="ApplicantAssetLiabilityGrid" />
				</td>
			</tr>
		</table>

		
		<div style="text-align: right">
			<asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click">go back</asp:LinkButton>
			<asp:Button ID="btnSubmit" Text="next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
		</div>
	</telerik:RadAjaxPanel>
</asp:Content>
