﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;


namespace WebApplicationForms
{
	public partial class WebForm_A06 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{			

			if (!this.IsPostBack)
			{
				//initialising controls...				

				//getting data...
				MMApplication a;
				if ((Session["Application"] == null) && HttpContext.Current.Request.IsLocal)
				{
					a = new MMApplication("CFE209842ECB48F4AD1E47BB40529E1F");
					a.LoadApplicants();
					Session["Application"] = a;
				}
				else
				{ a = (MMApplication)Session["Application"]; }

				MMApplicant ap1 = a.MMApplicant1;

				//Populating controls...				
				SelfEmployedIncomeGridAp1.ApplicantID = ap1.ApplicantID;
				MonthlyIncomeGridAp1.ApplicantID = ap1.ApplicantID;
				litAp1Firstname1.Text = ap1.FirstName;
				litAp1Firstname2.Text = ap1.FirstName;

				try
				{
					if (a.MMApplicant2 != null)
					{
						MMApplicant ap2 = a.MMApplicant2;
						//populating controls...
						SelfEmployedIncomeGridAp2.ApplicantID = ap2.ApplicantID;
						MonthlyIncomeGridAp2.ApplicantID= ap2.ApplicantID;
						litAp2Firstname1.Text = ap2.FirstName;
						litAp2Firstname2.Text = ap2.FirstName;

					}
					else
					{
						tdP2_1.Visible = false;
						tdP2_2.Visible = false;
						NameLabelsVisible(false);
					}
				}
				catch { }
			}
		}


		protected void btnSubmit_Click(object sender, EventArgs e)
		{
      MonthlyIncomeGridAp1.WrapUp();
      if (tdP2_2.Visible) MonthlyIncomeGridAp2.WrapUp();

			if (Page.IsValid)	Response.Redirect("ApplicationForm-07.aspx");
			
		}

		protected void btnBack_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)	Response.Redirect("ApplicationForm-05a.aspx");
		}
		protected void NameLabelsVisible(bool visible)
		{
			litAp1Firstname1.Visible = visible;
			litAp1Firstname2.Visible = visible;
			litAp2Firstname1.Visible = visible;
			litAp2Firstname2.Visible = visible;
		}
	}
}