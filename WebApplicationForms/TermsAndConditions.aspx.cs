﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms
{
	public partial class TermsAndConditions : PageWithFieldReplace
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}
		protected override void Render(HtmlTextWriter writer)
		{
			if (Session["Application"] != null) {
				MMApplication a = (MMApplication)Session["Application"];

				StringWriter output = new StringWriter();
				base.Render(new HtmlTextWriter(output));
				//This is the rendered HTML of your page. Feel free to manipulate it.
        			
				string outputAsString = output.ToString().Replace("[CompanyName]",a.Company.Company);
        outputAsString = outputAsString.Replace("[Phone Number]", a.Company.Phone);
				writer.Write(outputAsString);
			}
			else
			{ Response.Write("Session expired."); }
		}
		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{				
				Response.Redirect("ApplicationForm-01.aspx");
			}
		}

	}
}