﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms.EnquiryForm
{
  public partial class unable_to_assist_20pc_dep : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      MMCompany c = (MMCompany)(Session["Company"] ?? new MMCompany("ALC")) ;
      if (!String.IsNullOrEmpty(c.WebEnquiryFormFailureURL))
      {
        Session["redirect"] = "http://" + c.WebEnquiryFormFailureURL;
        Response.Redirect("redirect.aspx");
      }
      lblPhoneNumber.Text = c.Phone;
      lblFirstname.Text = (Session["Firstname"] ?? "<firstname>").ToString();
      litGoogleAdwordsScript.Text = c.GoogleAdWordsConversionScript;
      litGoogleAnalyticsScript.Text = c.GoogleAnalyticsScript;
    }
  }
}