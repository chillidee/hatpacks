﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms.EnquiryForm
{
  public partial class complete : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      string cid = "";
      cid = Request.QueryString["cid"] ?? "ALC";
      MMCompany c = new MMCompany(cid);
      //MMCompany c = (MMCompany)(Session["Company"] ?? new MMCompany("ALC")) ;
      try
      {
        Int32 CallId = Convert.ToInt32(Request.QueryString["CallID"]);
        EmailAgent ea = new EmailAgent();
        ea.SendThankYouEmail_WebEnquiry(CallId);
      }
      catch { }

      if (!String.IsNullOrEmpty(c.WebEnquiryFormSuccessURL))
      {
        Response.Write("<script>");
        Response.Write("window.top.location.href = '" + "http://" + c.WebEnquiryFormSuccessURL + "'");
        Response.Write("</script>");


        //Session["redirect"] = "http://" + c.WebEnquiryFormSuccessURL;
        //Response.Redirect("redirect.aspx");
      }

      lblPhoneNumber.Text = c.Phone;
      lblFirstname.Text = (Session["Firstname"] ?? "<firstname>").ToString();
      litGoogleAdwordsScript.Text = c.GoogleAdWordsConversionScript;
      litGoogleAnalyticsScript.Text = c.GoogleAnalyticsScript;

    }
  }
}