﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;
using System.Data.SqlClient;
using System.Configuration;
using FiftyOne.Foundation.Mobile.Detection;

namespace WebApplicationForms.EnquiryForm
{
  public partial class _default : System.Web.UI.Page
  {
    private string _parentUrl = "";
    private string _referrerURL = "";
    private string _companyID = "";
    private string _t; //t querystring for Adwords
    private string _k; //k querystring for Adwords
    private string _a; //a querystring for Adwords
    private bool isMobile = false;

    protected void Page_Load(object sender, EventArgs e)
    {
      //detecting if mobile version

      isMobile = Request.Browser["IsMobile"]=="True";
      lblisMobile.Text = Request.Browser["IsMobile"];
      lblProvider.Text = Request.ServerVariables["HTTP_USER_AGENT"];
      
      //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "val", "fnOnUpdateValidators();");
      divTest.Visible = (Request.QueryString["mode"] == "test");
      string css = (Request.QueryString["css"] ?? "").ToString();
      if (css != "")
      {
        litCSS.Text = "<link href=\"" + css + "\" rel=\"stylesheet\" />";
      }
      else
      {
        litCSS.Text = "<link href=\"enquiryForm.css\" rel=\"stylesheet\" />";
      }
      string showValidationSummary = (Request.QueryString["svs"] ?? "").ToString();
      vs.Visible = (showValidationSummary != "0");
      vs.Enabled = (showValidationSummary != "0");

      if (!IsPostBack)
      {
        try
        {
          _parentUrl = Request.UrlReferrer.ToString();
        }
        catch
        {
          _parentUrl = "no parent URL detected";
        }
        _referrerURL = Request.QueryString["ref"] ?? "";
        _companyID = Request.QueryString["cid"] ?? "ALC";
        _t = Request.QueryString["t"] ?? "";
        _k = Request.QueryString["k"] ?? "";
        _a = Request.QueryString["a"] ?? "";

        MMCompany c = null;
        if (_companyID != "")
        {
          c = new MMCompany(_companyID);
          Session["Company"] = c;
          lblCompanyName.Text = c.FullCompanyName;
          lblCompanyIDInternal.Text = c.CompanyID.ToString();
        }
        Title = c.Company + " web enquiry form";
        lblParent.Text = _parentUrl;
        lblReferrerURL.Text = _referrerURL;
        lblSearchTerm.Text = Utils.GetKeyword(_referrerURL);
        lblCompanyID.Text = _companyID;
        lblt.Text = _t;
        lblk.Text = _k;
        lbla.Text = _a;

        //lblIpAddress.Text = Request.ServerVariables["REMOTE_ADDR"];
        lblIpAddress.Text = Request.UserHostAddress + "; " + Request.ServerVariables["HTTP_X_FORWARDED_FOR"] + "; " + Request.ServerVariables["REMOTE_ADDR"];

        if (_parentUrl.Contains("refinance")) ddlLoanType.SelectedValue = "18";
        else if (_parentUrl.Contains("personalloans")) ddlLoanType.SelectedValue = "17";
        else ddlLoanType.SelectedValue = "";

        Boolean isCleanCredit = (_companyID == "CC" || _companyID == "CDR" || _companyID == "JB");
        /*trLoanAmount.Visible = (!isCleanCredit);
        trTypeOfLoan.Visible = (!isCleanCredit);
        trHasProperty.Visible = (!isCleanCredit);
        trComments.Visible = (!isCleanCredit);*/
        if (!isMobile) {
        trLoanAmount.Visible = c.WebEnquiryFormShowLoanAmount;
        trTypeOfLoan.Visible = c.WebEnquiryFormShowLoanType;
        trHasProperty.Visible = c.WebEnquiryFormShowHasProperty;
        trComments.Visible = c.WebEnquiryFormShowComments;
        trLastname.Visible = c.WebEnquiryFormShowLastname;
        trMobile.Visible = c.WebEnquiryFormShowMobile;
        trLandline.Visible = c.WebEnquiryFormShowLandline;
        trEmailAddress.Visible = c.WebEnquiryFormShowEmail;
        trStatePostCode.Visible = c.WebEnquiryFormShowStatePostCode;
        trStatePostCode2.Visible = c.WebEnquiryFormShowStatePostCode;
        trSuburb.Visible = c.WebEnquiryFormShowSuburb;
        trWhereHear.Visible = c.WebEnquiryFormShowWhereDidYouHear;
        trTitle.Visible = c.WebEnquiryFormShowTitle;
        trFirstname.Visible = c.WebEnquiryFormShowFirstname;

        trCC.Visible = (isCleanCredit && c.WebEnquiryFormShowComments);
        trCC1.Visible = (isCleanCredit && c.WebEnquiryFormShowComments);
        }
        else{
          trLoanAmount.Visible = c.WebEnquiryFormShowLoanAmount_M;
          trTypeOfLoan.Visible = c.WebEnquiryFormShowLoanType_M;
          trHasProperty.Visible = c.WebEnquiryFormShowHasProperty_M;
          trComments.Visible = c.WebEnquiryFormShowComments_M;
          trLastname.Visible = c.WebEnquiryFormShowLastname_M;
          trMobile.Visible = c.WebEnquiryFormShowMobile_M;
          trLandline.Visible = c.WebEnquiryFormShowLandline_M;
          trEmailAddress.Visible = c.WebEnquiryFormShowEmail_M;
          trStatePostCode.Visible = c.WebEnquiryFormShowStatePostCode_M;
          trSuburb.Visible = c.WebEnquiryFormShowSuburb_M;
          trWhereHear.Visible = c.WebEnquiryFormShowWhereDidYouHear_M;
          trTitle.Visible = c.WebEnquiryFormShowTitle_M;
          trFirstname.Visible = c.WebEnquiryFormShowFirstname_M;

          trCC.Visible = (isCleanCredit && c.WebEnquiryFormShowComments_M);
          trCC1.Visible = (isCleanCredit && c.WebEnquiryFormShowComments_M);
        }



        string issue_type = "";
        switch (_companyID)
        {
            /*changed 22-Jan-2014 Kelvin, original below
          case "CC":
            issue_type = "Default";
            break;
          case "CDR":
            issue_type = "Debt";
            break;*/
          case "CC":
            issue_type = "Comments";
            break;
          case "CDR":
            issue_type = "Comments";
            break;
          case "JB":
            issue_type = "Comments";
            break;
          case "ALC": case "ACC": case "NMC":            
            trContactHeader.Visible = false;
            break;
        }
        lit1.Text = issue_type;
        lit2.Text = issue_type;
        lit3.Text = issue_type;
        lit4.Text = issue_type;
        /* changed 22-Jan-2014
        btnAddCC1.Text = "add another " + issue_type.ToLower() + " issue";
        btnAddCC2.Text = "add another " + issue_type.ToLower() + " issue";
        btnAddCC3.Text = "add another " + issue_type.ToLower() + " issue";
         */

        btnAddCC1.Visible = false;

      }
      else
      {
        _parentUrl = lblParent.Text;

      }
    }


    protected void rblOwnProperty_SelectedIndexChanged(object sender, EventArgs e)
    {
      refresh_controls();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      if (Page.IsValid)
      {
        /*
        rtbWebsite and txtURL are the anti-spam "honeypot" control on the form. This control is hidden (at the <tr> level). If this field is filled in, this is  SPAM!        
        */
        string AntiSpam1 = rtbWebsite.Text;
        string AntiSpam2 = txtURL.Value;
        if (AntiSpam1 != "" || AntiSpam2 != "") Response.Redirect("complete.aspx?r=1"); //if you see the r=1 when testing, you know it is spam. Nothing is submitted.

        //Collecting data.
        string CompanyID = lblCompanyID.Text;
        string Title = rblTitle.SelectedValue;
        string Surname = rtbLastname.Text.Trim();
        //added 29 Aug 2013, to make Surname non mandatory, thus, putting in an "[UNKNOWN]" if it is blank.
        if (Surname == String.Empty)
        { Surname = "[UNKNOWN]"; }
        string Firstname = rtbFirstname.Text.Trim();

        string TelephoneHome = "";
        if (rtbHomeNumber.Text != "") TelephoneHome = "(" + ddlHomePhoneAreaCode.SelectedValue + ") " + rtbHomeNumber.Text.Trim();
        string TelephoneWork = "";
        if (rmtbWorkNumber.Text != "") TelephoneWork = "(" + ddlWorkPhoneAreaCode.SelectedValue + ") " + rmtbWorkNumber.TextWithLiterals.Trim();
        string Mobile = "";
        if (rtbMobileNumber.Text != "") Mobile = rtbMobileNumber.Text.Trim();
        string Email = rtbEmail.Text.Trim();
        string Notes = rtbComments.Text.Trim();
        string PostCode = rtbPostCode.Text.Trim();
        string Suburb = rtbSuburb.Text.Trim();
        string State = ddlState.SelectedValue;
        string ReferralURL = lblReferrerURL.Text;
        string SearchTerm = "";
        int LoanPurposeID = ddlLoanType.SelectedValue == "" ? 0 : Convert.ToInt32(ddlLoanType.SelectedValue);
        Boolean HasProperty = (rblOwnProperty.SelectedValue == "1");
        Boolean HaveDeposit = (rblHaveDeposit.SelectedValue == "1");
        string LoanAmount = rntbLoanAmount.Text;
        string TotalPropertyValue = rntbRealEstateValue.Text;
        string TotalPropertyOwing = rntbBalanceOwing.Text;
        Int32 CallStatusID = 14;
        string CFIssue01 = rtbCreditIssue01.Text.Trim();
        string CFIssue02 = rtbCreditIssue02.Text.Trim();
        string CFIssue03 = rtbCreditIssue03.Text.Trim();
        string CFIssue04 = rtbCreditIssue04.Text.Trim();
        string ProductID = "";
        string ReferralType = lblt.Text;
        string AdwordsKeyword = lblk.Text;
        string Adgroup = lbla.Text;

        //Check Pass...
        /*19 Aug 2013 instructed by Chris to bypass
        if (ddlLoanType.SelectedItem.Text == "Property Purchase")
        {
          if (!HasProperty && !HaveDeposit)
          { Response.Redirect("unable-to-assist-20pc-dep.aspx"); }
        }
        */


        //Massaging data...
        //'EXPRESS ENQUIRY: ' + tWebAddress.State + '  ' + tWebAddress.Suburb + '  ' + tWebAddress.PostCode + '; ' + tWebLoanApplication.Comments,
        Notes = "EXPRESS ENQUIRY: " + State + "  " + Suburb + "  " + PostCode + "; " + Notes;
        Firstname = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Firstname.ToLower());
        Surname = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Surname.ToLower());
        MMCompany c = new MMCompany(CompanyID);
        if (CompanyID == "") CompanyID = "ALC";
        if (CompanyID == "CC" || CompanyID == "CDR")
        {
          CallStatusID = (c.DefaultCallStatusID ?? 0);
          LoanPurposeID = (c.DefaultLoanPurposeID ?? 0);
          ProductID = "CF";
          Notes += "CLEAN CREDIT: " +
            "\r\n Issue 1:" + CFIssue01 +
            "\r\n Issue 2:" + CFIssue02 +
            "\r\n Issue 3:" + CFIssue03 +
            "\r\n Issue 4:" + CFIssue04;
        }

        Session["Company"] = c;
        Session["Firstname"] = Firstname;
        SearchTerm = Utils.GetKeyword(ReferralURL);

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
        {
          SqlCommand cmd = new SqlCommand("spCreateLeadFromWebApplication", conn);
          cmd.CommandType = System.Data.CommandType.StoredProcedure;
          cmd.Parameters.Add(new SqlParameter("CompanyID", c.CompanyID));
          cmd.Parameters.Add(new SqlParameter("Website", CompanyID));
          cmd.Parameters.Add(new SqlParameter("Title", Title));
          cmd.Parameters.Add(new SqlParameter("Surname", Surname));
          cmd.Parameters.Add(new SqlParameter("Firstname", Firstname));
          cmd.Parameters.Add(new SqlParameter("TelephoneHome", TelephoneHome));
          cmd.Parameters.Add(new SqlParameter("TelephoneWork", TelephoneWork));
          cmd.Parameters.Add(new SqlParameter("Mobile", Mobile));
          cmd.Parameters.Add(new SqlParameter("Email", Email));
          cmd.Parameters.Add(new SqlParameter("Notes", Notes));
          cmd.Parameters.Add(new SqlParameter("PostCode", PostCode));
          cmd.Parameters.Add(new SqlParameter("Suburb", Suburb));
          cmd.Parameters.Add(new SqlParameter("State", State));
          cmd.Parameters.Add(new SqlParameter("ReferralURL", ReferralURL));
          cmd.Parameters.Add(new SqlParameter("LoanPurposeID", (LoanPurposeID == 0 ? (object)DBNull.Value : LoanPurposeID)));
          cmd.Parameters.Add(new SqlParameter("HasProperty", HasProperty.ToString()));
          cmd.Parameters.Add(new SqlParameter("LoanAmount", LoanAmount));
          cmd.Parameters.Add(new SqlParameter("TotalPropertyValue", ((TotalPropertyValue == "") ? 0 : Convert.ToDecimal(TotalPropertyValue))));
          cmd.Parameters.Add(new SqlParameter("TotalPropertyOwing", ((TotalPropertyOwing == "") ? 0 : Convert.ToDecimal(TotalPropertyOwing))));
          cmd.Parameters.Add(new SqlParameter("CallStatusID", CallStatusID));
          cmd.Parameters.Add(new SqlParameter("CFIssue01", CFIssue01));
          cmd.Parameters.Add(new SqlParameter("CFIssue02", CFIssue02));
          cmd.Parameters.Add(new SqlParameter("CFIssue03", CFIssue03));
          cmd.Parameters.Add(new SqlParameter("CFIssue04", CFIssue04));
          cmd.Parameters.Add(new SqlParameter("ProductID", ProductID));
          cmd.Parameters.Add(new SqlParameter("SearchTerm", SearchTerm));
          cmd.Parameters.Add(new SqlParameter("ReferralType", ReferralType));
          //cmd.Parameters.Add(new SqlParameter("AdwordsKeyword", AdwordsKeyword));
          SqlParameter sp = new SqlParameter("AdwordsKeyword", System.Data.SqlDbType.VarChar,50);
          sp.Value = AdwordsKeyword;
          cmd.Parameters.Add(sp);

          cmd.Parameters.Add(new SqlParameter("Adgroup", Adgroup));

          SqlParameter p_rtn = cmd.Parameters.Add("@ReturnValue", System.Data.SqlDbType.Int);
          p_rtn.Direction = System.Data.ParameterDirection.ReturnValue;

          conn.Open();
          cmd.ExecuteNonQuery();

          if (p_rtn.Value.ToString() != "0")
          {
            string CallID = p_rtn.Value.ToString();

            string LoanType = ddlLoanType.SelectedItem.Text;


            //instructed by chris to bypass - 4th Sep 2014
            Response.Redirect("complete.aspx?cid=" + CompanyID + "&CallID=" + CallID);
            if (true) { }

            else if (LoanType == "Business" || HasProperty)
            { Response.Redirect("complete.aspx?cid=" + CompanyID + "&CallID=" + CallID); }
            else if (LoanType == "Property Purchase" /* && HaveDeposit*/)
            { Response.Redirect("complete.aspx?cid=" + CompanyID + "&CallID=" + CallID); }


            else if (LoanType == "Debt Consolidation" || LoanType == "Personal" || LoanType == "Other" || LoanType == "Refinance")
            {
              /*19 Aug 2013 instructed by Chris to bypass
              if (double.Parse(LoanAmount) < 8000)
              {
                //Response.Redirect("https://applicationform.hatpacks.com.au/debtQuestions/debtQuestions_new.aspx?bid=" + curBorrowerID.ToString() + "&lid=" + loanApplicationID.ToString() + "&cid=ALC");
                Session["redirect"] = "http://" + HttpContext.Current.Request.Url.Authority + "/debtQuestions/debtQuestions_new.aspx?callID=" + p_rtn.Value.ToString() + "&cid=ALC";
              }
              else
              {*/
              //Response.Redirect("https://applicationform.hatpacks.com.au/debtQuestions/debtQuestions.aspx?bid=" + curBorrowerID.ToString() + "&lid=" + loanApplicationID.ToString() + "&cid=ALC");

              //Response.Write("<script>");
              //Response.Write("window.top.location.href = '" + "http://" + HttpContext.Current.Request.Url.Authority + "/debtQuestions/debtQuestions.aspx?callID=" + p_rtn.Value.ToString() + "&cid=ALC" + "'");
              //Response.Write("</script>");              

              //Session["redirect"] = "http://" + HttpContext.Current.Request.Url.Authority + "/debtQuestions/debtQuestions.aspx?callID=" + p_rtn.Value.ToString() + "&cid=ALC";
              /*}*/
              string destinationURL = "http://" + HttpContext.Current.Request.Url.Authority + "/debtQuestions/debtQuestions.aspx?callID=" + p_rtn.Value.ToString() + "&cid=ALC";
              Response.Redirect("redirect.aspx?u=" + Server.UrlEncode(destinationURL));
            }
            //this is hardcoded 2 July 2013 - VERY QUICKLY!!!!
            /*19 Aug 2013 instructed by Chris to bypass
            else if (CompanyID == "ALC" && !HasProperty && LoanType != "Property Purchase")
            {
              Session["redirect"] = "http://www.australianlendingcentre.com.au/complete-np/";
              Response.Redirect("redirect.aspx");
            }*/
            else Response.Redirect("complete.aspx?cid=" + CompanyID + "&CallID=" + CallID);
          }
          else
          {
            lblErrorMessage.Visible = true;
            lblErrorMessage.Text = "This is embarrasing, something went wrong.... we were not able to process your enquiry. Please contact us on " + c.Phone + ".";
          }
        }
      }
      else
      {
      }
    }

    protected void cvContactNumbers_ServerValidate(object source, ServerValidateEventArgs args)
    {
      int i = 0;
      if (rtbHomeNumber.Text != "") i += 1;
      if (rmtbWorkNumber.Text != "") i += 1;
      if (rtbMobileNumber.Text != "") i += 1;
      //if (rtbEmail.Text != "") i += 1;
      args.IsValid = (i >= 1);
    }

    protected void cvHomeNumber_ServerValidate(object source, ServerValidateEventArgs args)
    {
      string s = rtbHomeNumber.Text.Replace(" ","");
      Int64 num;
      bool isNumeric = Int64.TryParse(s, out num);
      args.IsValid = (rtbHomeNumber.Text == "" || (s.Length == 8 && isNumeric));
    }

    protected void cvWorkNumber_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = (rmtbWorkNumber.Text == "" || rmtbWorkNumber.Text.Length == 8);
    }

    protected void cvMobile_ServerValidate(object source, ServerValidateEventArgs args)
    {
      //args.IsValid = (rtbMobileNumber.Text == "" || rtbMobileNumber.Text.Length == 8);
      string s = rtbMobileNumber.Text.Replace(" ", "").Replace("-", "");
      Int64 num;
      bool isNumeric = Int64.TryParse(s, out num);
      args.IsValid = (rtbMobileNumber.Text =="" || ((s.Length == 10) && (s.Substring(0,2) == "04") && isNumeric));
    }

    protected void cvPostCode_ServerValidate(object source, ServerValidateEventArgs args)
    {
      //args.IsValid = (rmtbPostCode.Text == "" || rmtbPostCode.Text.Length == 4);
      string s = rtbPostCode.Text.Replace(" ", "").Replace("-", "");
      Int32 num;
      bool isNumeric = Int32.TryParse(s, out num);
      args.IsValid = (s.Length == 4 && isNumeric);
    }

    protected void btnAddCC1_Click(object sender, EventArgs e)
    {
      trCC2.Visible = true;
      btnAddCC1.Visible = false;
    }

    protected void btnAddCC2_Click(object sender, EventArgs e)
    {
      trCC3.Visible = true;
      btnAddCC2.Visible = false;
    }

    protected void btnAddCC3_Click(object sender, EventArgs e)
    {
      trCC4.Visible = true;
      btnAddCC3.Visible = false;
    }

    protected void ddlLoanType_SelectedIndexChanged(object sender, EventArgs e)
    {
      refresh_controls();
    }

    private void refresh_controls()
    {
      bool OwnProperty = (rblOwnProperty.SelectedValue == "1");
      tr1.Visible = OwnProperty;
      tr2.Visible = OwnProperty;
      tr3.Visible = OwnProperty;

      trHaveDeposit.Visible = (ddlLoanType.SelectedItem.Text == "Property Purchase" && rblOwnProperty.SelectedValue == "0");

    }

    protected void sqlLoanPurpose_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
      e.Command.Parameters["@CompanyID"].Value = Convert.ToInt32(lblCompanyIDInternal.Text);
    }


  }
}