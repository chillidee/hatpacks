﻿<%@ Page Title="ttttt" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="test3.aspx.cs" Inherits="WebApplicationForms.test3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
	<telerik:RadComboBox Runat="server"></telerik:RadComboBox>
	<asp:FormView ID="FormView1" runat="server" AllowPaging="True" DataKeyNames="CompanyID" DataMember="DefaultView" DataSourceID="SqlDataSource1">
	<EditItemTemplate>
		CompanyID:
		<asp:Label ID="CompanyIDLabel1" runat="server" Text='<%# Eval("CompanyID") %>' />
		<br />
		Company:
		<asp:TextBox ID="CompanyTextBox" runat="server" Text='<%# Bind("Company") %>' />
		<br />
		FullCompanyName:
		<asp:TextBox ID="FullCompanyNameTextBox" runat="server" Text='<%# Bind("FullCompanyName") %>' />
		<br />
		Acronym:
		<asp:TextBox ID="AcronymTextBox" runat="server" Text='<%# Bind("Acronym") %>' />
		<br />
		ABN:
		<asp:TextBox ID="ABNTextBox" runat="server" Text='<%# Bind("ABN") %>' />
		<br />
		CreditLicNo:
		<asp:TextBox ID="CreditLicNoTextBox" runat="server" Text='<%# Bind("CreditLicNo") %>' />
		<br />
		SortOrder:
		<asp:TextBox ID="SortOrderTextBox" runat="server" Text='<%# Bind("SortOrder") %>' />
		<br />
		Phone:
		<asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' />
		<br />
		Fax:
		<asp:TextBox ID="FaxTextBox" runat="server" Text='<%# Bind("Fax") %>' />
		<br />
		AddressLine1:
		<asp:TextBox ID="AddressLine1TextBox" runat="server" Text='<%# Bind("AddressLine1") %>' />
		<br />
		AddressLine2:
		<asp:TextBox ID="AddressLine2TextBox" runat="server" Text='<%# Bind("AddressLine2") %>' />
		<br />
		Suburb:
		<asp:TextBox ID="SuburbTextBox" runat="server" Text='<%# Bind("Suburb") %>' />
		<br />
		PostCode:
		<asp:TextBox ID="PostCodeTextBox" runat="server" Text='<%# Bind("PostCode") %>' />
		<br />
		State:
		<asp:TextBox ID="StateTextBox" runat="server" Text='<%# Bind("State") %>' />
		<br />
		Email:
		<asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
		<br />
		LoanWriter:
		<asp:TextBox ID="LoanWriterTextBox" runat="server" Text='<%# Bind("LoanWriter") %>' />
		<br />
		AccountBank:
		<asp:TextBox ID="AccountBankTextBox" runat="server" Text='<%# Bind("AccountBank") %>' />
		<br />
		AccountName:
		<asp:TextBox ID="AccountNameTextBox" runat="server" Text='<%# Bind("AccountName") %>' />
		<br />
		AccountBSB:
		<asp:TextBox ID="AccountBSBTextBox" runat="server" Text='<%# Bind("AccountBSB") %>' />
		<br />
		AccountNo:
		<asp:TextBox ID="AccountNoTextBox" runat="server" Text='<%# Bind("AccountNo") %>' />
		<br />
		ACN:
		<asp:TextBox ID="ACNTextBox" runat="server" Text='<%# Bind("ACN") %>' />
		<br />
		IsLead:
		<asp:CheckBox ID="IsLeadCheckBox" runat="server" Checked='<%# Bind("IsLead") %>' />
		<br />
		DummyField:
		<asp:TextBox ID="DummyFieldTextBox" runat="server" Text='<%# Bind("DummyField") %>' />
		<br />
		<asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
		&nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
	</EditItemTemplate>
	<InsertItemTemplate>
		Company:
		<asp:TextBox ID="CompanyTextBox" runat="server" Text='<%# Bind("Company") %>' />
		<br />
		FullCompanyName:
		<asp:TextBox ID="FullCompanyNameTextBox" runat="server" Text='<%# Bind("FullCompanyName") %>' />
		<br />
		Acronym:
		<asp:TextBox ID="AcronymTextBox" runat="server" Text='<%# Bind("Acronym") %>' />
		<br />
		ABN:
		<asp:TextBox ID="ABNTextBox" runat="server" Text='<%# Bind("ABN") %>' />
		<br />
		CreditLicNo:
		<asp:TextBox ID="CreditLicNoTextBox" runat="server" Text='<%# Bind("CreditLicNo") %>' />
		<br />
		SortOrder:
		<asp:TextBox ID="SortOrderTextBox" runat="server" Text='<%# Bind("SortOrder") %>' />
		<br />
		Phone:
		<asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' />
		<br />
		Fax:
		<asp:TextBox ID="FaxTextBox" runat="server" Text='<%# Bind("Fax") %>' />
		<br />
		AddressLine1:
		<asp:TextBox ID="AddressLine1TextBox" runat="server" Text='<%# Bind("AddressLine1") %>' />
		<br />
		AddressLine2:
		<asp:TextBox ID="AddressLine2TextBox" runat="server" Text='<%# Bind("AddressLine2") %>' />
		<br />
		Suburb:
		<asp:TextBox ID="SuburbTextBox" runat="server" Text='<%# Bind("Suburb") %>' />
		<br />
		PostCode:
		<asp:TextBox ID="PostCodeTextBox" runat="server" Text='<%# Bind("PostCode") %>' />
		<br />
		State:
		<asp:TextBox ID="StateTextBox" runat="server" Text='<%# Bind("State") %>' />
		<br />
		Email:
		<asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
		<br />
		LoanWriter:
		<asp:TextBox ID="LoanWriterTextBox" runat="server" Text='<%# Bind("LoanWriter") %>' />
		<br />
		AccountBank:
		<asp:TextBox ID="AccountBankTextBox" runat="server" Text='<%# Bind("AccountBank") %>' />
		<br />
		AccountName:
		<asp:TextBox ID="AccountNameTextBox" runat="server" Text='<%# Bind("AccountName") %>' />
		<br />
		AccountBSB:
		<asp:TextBox ID="AccountBSBTextBox" runat="server" Text='<%# Bind("AccountBSB") %>' />
		<br />
		AccountNo:
		<asp:TextBox ID="AccountNoTextBox" runat="server" Text='<%# Bind("AccountNo") %>' />
		<br />
		ACN:
		<asp:TextBox ID="ACNTextBox" runat="server" Text='<%# Bind("ACN") %>' />
		<br />
		IsLead:
		<asp:CheckBox ID="IsLeadCheckBox" runat="server" Checked='<%# Bind("IsLead") %>' />
		<br />
		DummyField:
		<asp:TextBox ID="DummyFieldTextBox" runat="server" Text='<%# Bind("DummyField") %>' />
		<br />
		<asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
		&nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
	</InsertItemTemplate>
	<ItemTemplate>
		CompanyID:
		<asp:Label ID="CompanyIDLabel" runat="server" Text='<%# Eval("CompanyID") %>' />
		<br />
		Company:
		<asp:Label ID="CompanyLabel" runat="server" Text='<%# Bind("Company") %>' />
		<br />
		FullCompanyName:
		<asp:Label ID="FullCompanyNameLabel" runat="server" Text='<%# Bind("FullCompanyName") %>' />
		<br />
		Acronym:
		<asp:Label ID="AcronymLabel" runat="server" Text='<%# Bind("Acronym") %>' />
		<br />
		ABN:
		<asp:Label ID="ABNLabel" runat="server" Text='<%# Bind("ABN") %>' />
		<br />
		CreditLicNo:
		<asp:Label ID="CreditLicNoLabel" runat="server" Text='<%# Bind("CreditLicNo") %>' />
		<br />
		SortOrder:
		<asp:Label ID="SortOrderLabel" runat="server" Text='<%# Bind("SortOrder") %>' />
		<br />
		Phone:
		<asp:Label ID="PhoneLabel" runat="server" Text='<%# Bind("Phone") %>' />
		<br />
		Fax:
		<asp:Label ID="FaxLabel" runat="server" Text='<%# Bind("Fax") %>' />
		<br />
		AddressLine1:
		<asp:Label ID="AddressLine1Label" runat="server" Text='<%# Bind("AddressLine1") %>' />
		<br />
		AddressLine2:
		<asp:Label ID="AddressLine2Label" runat="server" Text='<%# Bind("AddressLine2") %>' />
		<br />
		Suburb:
		<asp:Label ID="SuburbLabel" runat="server" Text='<%# Bind("Suburb") %>' />
		<br />
		PostCode:
		<asp:Label ID="PostCodeLabel" runat="server" Text='<%# Bind("PostCode") %>' />
		<br />
		State:
		<asp:Label ID="StateLabel" runat="server" Text='<%# Bind("State") %>' />
		<br />
		Email:
		<asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' />
		<br />
		LoanWriter:
		<asp:Label ID="LoanWriterLabel" runat="server" Text='<%# Bind("LoanWriter") %>' />
		<br />
		AccountBank:
		<asp:Label ID="AccountBankLabel" runat="server" Text='<%# Bind("AccountBank") %>' />
		<br />
		AccountName:
		<asp:Label ID="AccountNameLabel" runat="server" Text='<%# Bind("AccountName") %>' />
		<br />
		AccountBSB:
		<asp:Label ID="AccountBSBLabel" runat="server" Text='<%# Bind("AccountBSB") %>' />
		<br />
		AccountNo:
		<asp:Label ID="AccountNoLabel" runat="server" Text='<%# Bind("AccountNo") %>' />
		<br />
		ACN:
		<asp:Label ID="ACNLabel" runat="server" Text='<%# Bind("ACN") %>' />
		<br />
		IsLead:
		<asp:CheckBox ID="IsLeadCheckBox" runat="server" Checked='<%# Bind("IsLead") %>' Enabled="false" />
		<br />
		DummyField:
		<asp:Label ID="DummyFieldLabel" runat="server" Text='<%# Bind("DummyField") %>' />
		<br />

	</ItemTemplate>
</asp:FormView>
	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Data Source=MMSERVER;Initial Catalog=CRISMAR;Persist Security Info=True;User ID=WebApplication_User;Password=happy4burwood" ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM [Company]"></asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

</asp:Content>

