﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RadWindow.Master" AutoEventWireup="true" CodeBehind="SessionExpired.aspx.cs" Inherits="WebApplicationForms.Errors.SessionExpired" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<h1>Session Expired</h1>
	<p>Oooops...</p>
	<p>Perhaps you took too long to complete the loan application. For security purposes, the server will expire session which have not responded for too long. You will need to
		re-start you loan application from the original email you received (again, for security purposes).
	</p>
	<p>
		Don't worry, we've saved as much information as you've progressed, so we don't think you've lost that much (if any) information :-)
	</p>
</asp:Content>
