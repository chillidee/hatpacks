﻿<%@ Page Title="Thank you" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ThankYou.aspx.cs" Inherits="WebApplicationForms.ThankYou" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured">
        <div class="content-wrapper">
            <div>
                <hgroup class="title">
                    <h1>Thank you
                    <asp:Literal ID="litFirstname" runat="server" />, you are AWESOME!</h1>
                </hgroup>
            </div>
            <div>
                <span>
                    <asp:Literal ID="litFirstname2" runat="server" />, thank you for submittting your loan application with
                    <asp:Label runat="server" ID="lblCompanyName" />.</span>
                <span>Remember, you can contact your personal Application Form manager
                    <asp:Label runat="server" ID="lblSalesManager" />, anytime :-) </span>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>
