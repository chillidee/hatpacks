﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplicantAssetLiabilityGrid.ascx.cs" Inherits="WebApplicationForms.UserControls.ApplicantAssetLiabilityGrid" %>
<%@ Register Src="~/UserControls/AssetLiabilityEdit.ascx" TagPrefix="uc1" TagName="AssetLiabilityEdit" %>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
  <script type="text/javascript">
    var popUp;
    function PopUpShowing(sender, eventArgs) {
      popUp = eventArgs.get_popUp();      
      var pageWidth = document.body.offsetWidth;
      var pageHeight = window.innerHeight;
      var gridWidth = sender.get_element().offsetWidth;
      var gridHeight = sender.get_element().offsetHeight;
      var popUpWidth = popUp.style.width.substr(0, popUp.style.width.indexOf("px"));
      var popUpHeight = popUp.style.height.substr(0, popUp.style.height.indexOf("px"));
      //alert('windowHeight ' + pageHeight);
      //alert('popupHeight: ' + popUpHeight);
      popUp.style.left = ((pageWidth - popUpWidth) / 2 + sender.get_element().offsetLeft).toString() + "px";
    	//popUp.style.top = ((pageHeight - popUpHeight) / 2 + sender.get_element().offsetTop).toString() + "px";
      popUp.style.top = "150px";
    } 
  </script>
</telerik:RadCodeBlock>
<telerik:RadGrid ID="rgAssetLiability" runat="server"
	AllowAutomaticDeletes="True"
	AllowAutomaticInserts="true"
	AllowAutomaticUpdates="True"
	CellSpacing="0"
	ClientSettings-AllowKeyboardNavigation="true"
	DataSourceID="sqlAssetLiability"
	EnableLinqExpressions="false"
	GridLines="None"
	HeaderStyle-VerticalAlign="Bottom"
	HeaderStyle-Font-Bold="True"
	HeaderStyle-Font-Size="12px"	
	OnItemCommand="rgAssetLiability_ItemCommand"
	OnUpdateCommand="rgAssetLiability_UpdateCommand"
	OnItemUpdated="rgAssetLiability_ItemUpdated"
	 OnEditCommand="rgAssetLiability_EditCommand"
	 OnPreRender="rgAssetLiability_PreRender"
	AutoGenerateColumns="False"
	 OnItemDataBound="rgAssetLiability_ItemDataBound"
	>
	<MasterTableView
		CommandItemDisplay="Bottom"
		DataKeyNames="FinanceDetailID"
		DataSourceID="sqlAssetLiability"
		EditItemStyle-BackColor="#FFFFCC"
		EditItemStyle-Width="5px"
		EditMode="Popup"
		InsertItemDisplay="Bottom"
		ShowFooter="true">
		<ColumnGroups>
			<telerik:GridColumnGroup HeaderText="ASSETS" Name="Asset" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="#6699ff"></telerik:GridColumnGroup>
			<telerik:GridColumnGroup HeaderText="LIABILITIES" Name="Liability" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="#ff6600"></telerik:GridColumnGroup>
			<telerik:GridColumnGroup HeaderText="NETT POSITION" Name="NettPosition" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="#698820"></telerik:GridColumnGroup>
			<telerik:GridColumnGroup HeaderText="" Name="Misc"></telerik:GridColumnGroup>
		</ColumnGroups>
		<Columns>
			<telerik:GridBoundColumn DataField="FinanceDetailID" ReadOnly="True" HeaderText="FinanceDetailID" SortExpression="FinanceDetailID" UniqueName="FinanceDetailID" DataType="System.Int32" FilterControlAltText="Filter FinanceDetailID column" Visible="false"></telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="ApplicantID" HeaderText="ApplicantID" SortExpression="ApplicantID" UniqueName="ApplicantID" DataType="System.Int32" FilterControlAltText="Filter ApplicantID column" Visible="false"></telerik:GridBoundColumn>

			<telerik:GridDropDownColumn CurrentFilterFunction="NoFilter" DataField="AssetLiabilityID" DataSourceID="sqlAssetLiabilityType" DropDownControlType="DropDownList" FooterStyle-Font-Bold="true" FooterText="" HeaderText="Type" ListTextField="AssetLiability" ListValueField="AssetLiabilityID" UniqueName="AssetLiabilityID" ColumnGroupName="Asset" ItemStyle-VerticalAlign="Top"></telerik:GridDropDownColumn>
			<telerik:GridBoundColumn DataField="Details" HeaderText="Details" SortExpression="Details" UniqueName="Details" FilterControlAltText="Filter Details column" ColumnGroupName="Asset" ItemStyle-VerticalAlign="Top"></telerik:GridBoundColumn>
			<telerik:GridCheckBoxColumn DataField="IsRealEstate" HeaderText="Real Estate?" SortExpression="IsRealEstate" UniqueName="IsRealEstate" DataType="System.Boolean" FilterControlAltText="Filter IsRealEstate column" ColumnGroupName="Asset" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center" DefaultInsertValue="false"></telerik:GridCheckBoxColumn>
			<telerik:GridCheckBoxColumn DataField="UseForSecurity" HeaderText="UseForSecurity" SortExpression="UseForSecurity" UniqueName="UseForSecurity" DataType="System.Boolean" FilterControlAltText="Filter IsRealEstate column" ColumnGroupName="Asset" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center" DefaultInsertValue="false" Visible="false"></telerik:GridCheckBoxColumn>
			<telerik:GridBoundColumn DataField="Value" HeaderText="Value" SortExpression="Value" UniqueName="Value" DataType="System.Decimal" FilterControlAltText="Filter Value column" DataFormatString="{0:C0}" ColumnGroupName="Asset" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Right" FooterAggregateFormatString="{0:C0}" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" Aggregate="Sum"></telerik:GridBoundColumn>
			
			<telerik:GridBoundColumn DataField="CreditCardLimit" HeaderText="Credit Limit" SortExpression="CreditCardLimit" UniqueName="CreditCardLimit" DataType="System.Decimal" FilterControlAltText="Filter CreditCardLimit column" DataFormatString="{0:C0}" ColumnGroupName="Liability" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Right"  FooterAggregateFormatString="{0:C0}" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" Aggregate="Sum"></telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="AmountOwing" HeaderText="Owing" SortExpression="AmountOwing" UniqueName="AmountOwing" DataType="System.Decimal" FilterControlAltText="Filter AmountOwing column" DataFormatString="{0:C0}" ColumnGroupName="Liability" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Right"  FooterAggregateFormatString="{0:C0}" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" Aggregate="Sum"></telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="Lender" HeaderText="Provider" SortExpression="Lender" UniqueName="Lender" FilterControlAltText="Filter Lender column" ColumnGroupName="Liability" ItemStyle-VerticalAlign="Top"></telerik:GridBoundColumn>
			
			<telerik:GridBoundColumn DataField="NettValue" HeaderText="Nett Position" SortExpression="NettValue" UniqueName="NettValue" DataType="System.Decimal" FilterControlAltText="Filter NettValue column" DataFormatString="{0:C0}" ColumnGroupName="NettPosition" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Right"  FooterAggregateFormatString="{0:C0}" FooterStyle-Font-Bold="true" FooterStyle-Font-Size="16px" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" Aggregate="Sum" ItemStyle-BackColor="#FCFBE4" FooterStyle-BackColor="#FCFBE4"></telerik:GridBoundColumn>	
			<telerik:GridBoundColumn DataField="Monthly" HeaderText="Monthly Repayment" SortExpression="Monthly" UniqueName="Monthly" DataType="System.Decimal" FilterControlAltText="Filter Monthly column" DataFormatString="{0:C0}" ColumnGroupName="Misc" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Top"  FooterAggregateFormatString="{0:C0}" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" Aggregate="Sum"></telerik:GridBoundColumn>									
			
			<telerik:GridBoundColumn DataField="FullAddress" HeaderText="FullAddress" SortExpression="FullAddress" UniqueName="FullAddress" FilterControlAltText="Filter FullAddress column" ColumnGroupName="Misc" ItemStyle-VerticalAlign="Top" Visible="false"></telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="UnitNumber" HeaderText="UnitNumber" SortExpression="UnitNumber" UniqueName="UnitNumber" FilterControlAltText="Filter UnitNumber column" ColumnGroupName="Misc" ItemStyle-VerticalAlign="Top" Visible="false"></telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="StreetNumber" HeaderText="StreetNumber" SortExpression="StreetNumber" UniqueName="StreetNumber" FilterControlAltText="Filter StreetNumber column" ColumnGroupName="Misc" ItemStyle-VerticalAlign="Top" Visible="false"></telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="StreetName" HeaderText="StreetName" SortExpression="StreetName" UniqueName="StreetName" FilterControlAltText="Filter StreetName column" ColumnGroupName="Misc" ItemStyle-VerticalAlign="Top" Visible="false"></telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="StreetType" HeaderText="StreetType" SortExpression="StreetType" UniqueName="StreetType" FilterControlAltText="Filter StreetType column" ColumnGroupName="Misc" ItemStyle-VerticalAlign="Top" Visible="false"></telerik:GridBoundColumn>
			<telerik:GridBoundColumn DataField="Suburb" HeaderText="Suburb" SortExpression="Suburb" UniqueName="Suburb" FilterControlAltText="Filter Suburb column" ColumnGroupName="Misc" ItemStyle-VerticalAlign="Top" Visible="false"></telerik:GridBoundColumn>

			<telerik:GridEditCommandColumn CancelText="cancel" EditText="edit" InsertText="save" UpdateText="save" ColumnGroupName="Misc" ItemStyle-VerticalAlign="Top"></telerik:GridEditCommandColumn>
			<telerik:GridButtonColumn CommandName="Delete" ConfirmText="Not recoverable. Delete?" HeaderTooltip="Delete" Text="X" ColumnGroupName="Misc" ItemStyle-VerticalAlign="Top"></telerik:GridButtonColumn>
		</Columns>
		<EditFormSettings EditFormType="Template" PopUpSettings-Width="900px" PopUpSettings-Modal="true" >
			<FormTemplate>
				<uc1:AssetLiabilityEdit runat="server" ID="AssetLiabilityEdit" DataItem="<%# Container %>" ApplicantID="<%# Convert.ToInt32(lblApplicantID.Text) %>" />

			</FormTemplate>
		</EditFormSettings>
		<CommandItemTemplate>
			<div style="padding: 10px">
        <asp:Button ID="btnAdd" runat="server" CommandName ="InitInsert" Text="add item"/>	
			</div>
		</CommandItemTemplate>
	</MasterTableView>
	<ClientSettings>
    <ClientEvents OnPopUpShowing="PopUpShowing" />
    <Selecting AllowRowSelect="true" />
  </ClientSettings>
</telerik:RadGrid>
<asp:SqlDataSource ID="sqlAssetLiability" runat="server" ConnectionString="<%$ ConnectionStrings:CRISMARConnectionString %>"
	OnSelecting="sqlAssetLiability_Selecting"
	SelectCommand="SELECT [FinanceDetailID], [ApplicantID], [AssetLiabilityID], [Value], [Monthly], [AmountOwing], [NettValue], [Details], [CreditCardLimit], [IsRealEstate], [Lender], [FullAddress], [UseForSecurity], [UnitNumber], [StreetNumber], [StreetName], [StreetType], [Suburb], [SecurityID] FROM [AssetLiability] WHERE ApplicantID = @ApplicantID ORDER BY FinanceDetailID"
	DeleteCommand="DELETE FROM [AssetLiability] WHERE [FinanceDetailID] = @FinanceDetailID">
	<SelectParameters>
		<asp:Parameter Name="ApplicantID" />
	</SelectParameters>
	<DeleteParameters>
		<asp:Parameter Name="FinanceDetailID" Type="Int32"></asp:Parameter>
	</DeleteParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqlAssetLiabilityType" runat="server" ConnectionString="<%$ ConnectionStrings:CRISMARConnectionString %>"
	SelectCommand="SELECT null AssetLiabilityID, '' AssetLiability, 0 SortOrder UNION SELECT AssetLiabilityID, AssetLiability, SortOrder FROM [AssetLiabilityType] WHERE DisplayOnWeb = 1 ORDER BY SortOrder, AssetLiability"></asp:SqlDataSource>
<asp:Literal runat="server" ID="litTest" Visible="false"></asp:Literal>
<asp:Label runat="server" Visible="false" Text="" ID="lblApplicantID"></asp:Label>
