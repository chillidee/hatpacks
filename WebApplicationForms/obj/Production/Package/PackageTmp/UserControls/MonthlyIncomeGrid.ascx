﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MonthlyIncomeGrid.ascx.cs" Inherits="WebApplicationForms.UserControls.MonthlyIncomeGrid" %>
						<telerik:RadGrid ID="rgMonthlyIncome" runat="server"
              AllowAutomaticDeletes="True"
              AllowAutomaticInserts="true"
              AllowAutomaticUpdates="True"
              CellSpacing="0"
              ClientSettings-AllowKeyboardNavigation="true"
              DataSourceID="sqlMonthlyIncome"
              EnableHeaderContextAggregatesMenu="true"
              EnableLinqExpressions="false"
              GridLines="None"
              HeaderStyle-VerticalAlign="Bottom"
              HeaderStyle-Font-Bold="True"
              HeaderStyle-Font-Size="12px"
              OnItemCommand="rgMonthlyIncome_ItemCommand"
              OnPreRender="rgMonthlyIncome_PreRender"
              OnUnload="rgMonthlyIncome_Unload">
							<MasterTableView 
                AutoGenerateColumns="False"
                CommandItemDisplay="Bottom"
                DataKeyNames="IncomeID"
                DataSourceID="sqlMonthlyIncome"
                EditItemStyle-BackColor="#FFFFCC"
                EditItemStyle-Width="5px"
                EditMode="InPlace"
                InsertItemDisplay="Bottom"
                ShowFooter="false">
								<CommandItemTemplate>									
								</CommandItemTemplate>
								<Columns>
									<telerik:GridBoundColumn DataField="ApplicantID" UniqueName="ApplicantID" Visible="false">
									</telerik:GridBoundColumn>
									<telerik:GridDropDownColumn CurrentFilterFunction="NoFilter" DataField="IncomeTypeID" DataSourceID="sqlMonthlyIncomeType" DropDownControlType="DropDownList" FooterStyle-Font-Bold="true" FooterText="Total monthly:" HeaderText="Type" ListTextField="IncomeType" ListValueField="IncomeTypeID" UniqueName="IncomeTypeID">
									</telerik:GridDropDownColumn>

									<telerik:GridTemplateColumn DataField="AmountDisplay" DataType="System.Decimal" FooterAggregateFormatString="{0:C0}" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Amount" ItemStyle-HorizontalAlign="Right" UniqueName="gtcAmountDisplay" ItemStyle-Wrap="false">
										<ItemTemplate>
											<asp:Literal ID="litAmountDisplay" runat="server" Text='<%# String.Format("{0:C0}",Eval("AmountDisplay")) %>'></asp:Literal>
										</ItemTemplate>
										<EditItemTemplate>
											<telerik:RadNumericTextBox ID="rtbAmountDisplay" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("AmountDisplay") %>' Type="Currency" Width="70px">
											</telerik:RadNumericTextBox>
											per
										</EditItemTemplate>
									</telerik:GridTemplateColumn>


									<telerik:GridDropDownColumn CurrentFilterFunction="NoFilter" DataField="TermDisplay" DataSourceID="sqlTermDisplay" DropDownControlType="DropDownList" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" FooterText="" HeaderText="Frequency" ListTextField="Term" ListValueField="TermCode" UniqueName="TermDisplay">
									</telerik:GridDropDownColumn>

									<telerik:GridTemplateColumn Visible="true" Aggregate="Sum" DataField="Amount" DataType="System.Decimal" FooterAggregateFormatString="{0:C0}" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Amount /mth" ItemStyle-HorizontalAlign="Right" UniqueName="gtcAmount" DefaultInsertValue="0">
										<ItemTemplate>
											<asp:Literal ID="litAmount" runat="server" Text='<%# String.Format("{0:C0}",Eval("Amount")) %>'></asp:Literal>
										</ItemTemplate>
										<EditItemTemplate>
											<telerik:RadNumericTextBox Visible="false" ID="rtbAmount" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("Amount") %>' Type="Currency" Width="70px">
											</telerik:RadNumericTextBox>
                      <asp:RequiredFieldValidator runat="server" ControlToValidate="rtbAmount" Text="Required." ErrorMessage="Amount is required." Display="Dynamic" />
										</EditItemTemplate>
									</telerik:GridTemplateColumn>

									<telerik:GridEditCommandColumn CancelText="cancel" EditText="edit" InsertText="save" UpdateText="save">
									</telerik:GridEditCommandColumn>
									<telerik:GridButtonColumn CommandName="Delete" ConfirmText="Not recoverable. Delete?" HeaderTooltip="Delete" Text="delete">
									</telerik:GridButtonColumn>
								</Columns>
							</MasterTableView>
						</telerik:RadGrid>
						<asp:SqlDataSource ID="sqlMonthlyIncome" runat="server" ConnectionString="<%$ ConnectionStrings:CRISMARConnectionString %>"
							DeleteCommand="DELETE FROM [MonthlyIncome] WHERE [IncomeID] = @IncomeID"
							InsertCommand="INSERT INTO [MonthlyIncome] ([ApplicantID], [IncomeTypeID], [Amount], [AmountDisplay], [TermDisplay], [Comment]) VALUES (@ApplicantID, @IncomeTypeID, @Amount, @AmountDisplay, @TermDisplay, @Comment)"
							SelectCommand="SELECT * FROM [MonthlyIncome] WHERE [ApplicantID] = @ApplicantID"
							UpdateCommand="UPDATE [MonthlyIncome] SET [ApplicantID] = @ApplicantID, [IncomeTypeID] = @IncomeTypeID, [Amount] = @Amount, [AmountDisplay] = @AmountDisplay, [TermDisplay] = @TermDisplay, [Comment] = @Comment WHERE [IncomeID] = @IncomeID"
							OnSelecting="sqlMonthlyIncome_Selecting">
							<SelectParameters>
									<asp:Parameter Name="ApplicantID" />
							</SelectParameters>
							<DeleteParameters>
								<asp:Parameter Name="IncomeID" Type="Int32" />
							</DeleteParameters>
							<InsertParameters>
								<asp:Parameter Name="ApplicantID" Type="Int32" />
								<asp:Parameter Name="IncomeTypeID" Type="Int32" />
								<asp:Parameter Name="Amount" Type="Decimal" />
								<asp:Parameter Name="AmountDisplay" Type="Decimal" />
								<asp:Parameter Name="TermDisplay" Type="String" />
								<asp:Parameter Name="Comment" Type="String" />
							</InsertParameters>
							<UpdateParameters>
								<asp:Parameter Name="ApplicantID" Type="Int32" />
								<asp:Parameter Name="IncomeTypeID" Type="Int32" />
								<asp:Parameter Name="Amount" Type="Decimal" />
								<asp:Parameter Name="AmountDisplay" Type="Decimal" />
								<asp:Parameter Name="TermDisplay" Type="String" />
								<asp:Parameter Name="Comment" Type="String" />
								<asp:Parameter Name="IncomeID" Type="Int32" />
							</UpdateParameters>
						</asp:SqlDataSource>
						<asp:SqlDataSource ID="sqlMonthlyIncomeType" runat="server" ConnectionString="<%$ ConnectionStrings:CRISMARConnectionString %>"
							SelectCommand="SELECT * FROM [MonthlyIncomeType] WHERE DisplayOnWeb = 1 ORDER BY [IncomeType]"></asp:SqlDataSource>
						<asp:SqlDataSource ID="SqlTermDisplay" runat="server" ConnectionString="<%$ ConnectionStrings:CRISMARConnectionString %>" SelectCommand="SELECT * FROM [Term] ORDER BY [SortOrder]"></asp:SqlDataSource>
						<asp:Label runat="server" Visible="false" Text="" ID="lblApplicantID"></asp:Label>