﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Address_Simple.ascx.cs" Inherits="WebApplicationForms.UserControls.Address_Simple" %>

<table class="fieldset">
	<tr>
		<td class="<%=FieldCssClass %>" style="text-align:right">Address Line 1</td>
		<td>
			<telerik:RadTextBox ID="rtbAddressLine1" runat="server" Width="200px"></telerik:RadTextBox>
		</td>
	</tr>
	<tr>
		<td class="<%=FieldCssClass %>" style="text-align:right">Address Line 2</td>
		<td>
			<telerik:RadTextBox ID="rtbAddressLine2" runat="server" Width="200px"></telerik:RadTextBox>
		</td>
	</tr>
	<tr>
		<td class="<%=FieldCssClass %>" style="text-align:right" nowrap="nowrap">Suburb, State & Post Code</td>
		<td colspan="3">
			<script>
				function rcbSuburb_OnClientItemsRequestingHandler(sender, eventArgs) {
					if (sender.get_text().length < 2) {
						eventArgs.set_cancel(true);
					}
				}
			</script>
			<telerik:RadComboBox ID="rcbSuburb" runat="server"
				DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID"
				ShowMoreResultsBox="True" EnableVirtualScrolling="True"
				EnableAutomaticLoadOnDemand="True" ItemsPerRequest="20" Height="200px" Width="200px"
				DropDownWidth="370px" MarkFirstMatch="True" OffsetY="10" EmptyMessage=""				
				>
			</telerik:RadComboBox>

			<asp:SqlDataSource ID="sqlPCode" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT [PCodeID], [Combo] FROM [vwPCode] ORDER BY [SUBURB], [PCODE] DESC"></asp:SqlDataSource>			
		</td>
	</tr>
</table>