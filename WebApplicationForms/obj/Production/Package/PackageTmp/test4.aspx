﻿<%@ Page Language="C#" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title>RadControls for ASP.NET AJAX</title>
    <style type="text/css">
        /*RadInput with no button and label (1)*/
        .TextBoxClass
        {
            width: 200px;
        }
        /*RadInput with no button and label (2)*/
        .TextBoxParent300 .RadInput .riTextBox
        {
            width: 300px !important;
        }
        /*RadInput with a button and/or a label*/
        .TextBoxParent400 .RadInput, .TextBoxParent400 .RadInput .riTable
        {
            width: 400px !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <h1>
        Ways to remove a RadInput textbox width or customize it with external CSS code</h1>
    <p>
        <strong>In all cases you should set ShouldResetWidthInPixels="false"</strong></p>
    <h2>
        RadInput with no button and label (1)</h2>
    <p>
        This approach involves runtime removal of the control's width on the client with
        Javascript. After this, the control will either apply the default browser textbox
        width, or any width set with a CSS class.</p>
    <telerik:RadTextBox ID="RadTextBox4" runat="server" ShouldResetWidthInPixels="false"
        Text="default browser width">
        <ClientEvents OnLoad="RemoveWidth" />
    </telerik:RadTextBox>
    <br />
    <br />
    <telerik:RadTextBox ID="RadTextBox1" runat="server" ShouldResetWidthInPixels="false"
        CssClass="TextBoxClass" Text="200px set with a CSS rule">
        <ClientEvents OnLoad="RemoveWidth" />
    </telerik:RadTextBox>
    <script type="text/javascript">
    	function RemoveWidth(sender, args) {
    		//remove only the width style from the inline style collection     
    		sender._originalTextBoxCssText = sender._originalTextBoxCssText.replace(/(^|[^-])width\s?:\s?[\w|\.]+\s?;/i, "$1");
    		sender.updateCssClass();
    	}
    </script>
    <h2>
        RadInput with no button and label (2)</h2>
    <p>
        This approach involves overriding the control's inline width style with CSS rules
        using the !important clause.</p>
    <div class="TextBoxParent300">
        <telerik:RadTextBox ID="RadTextBox2" runat="server" ShouldResetWidthInPixels="true"
            Text="300px set with a CSS rule and !important" />
    </div>
    <h2>
        RadInput with a button and/or a label</h2>
    <p>
        This approach involves overriding the control's inline width style with CSS rules
        using the !important clause. Since the HTML rendering of the control in this case
        requires an explicit width, you cannot simply remove the default one without setting
        another.</p>
    <div class="TextBoxParent400">
        <telerik:RadTextBox ID="RadTextBox3" runat="server" ShouldResetWidthInPixels="true"
            ShowButton="true" Label="Label" Text="400px set with a CSS rule and !important" />
    </div>
    </form>
</body>
</html>