﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="unable-to-assist-20pc-dep.aspx.cs" Inherits="WebApplicationForms.EnquiryForm.unable_to_assist_20pc_dep" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="enquiryForm.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
      <h1>Sorry.</h1>
      <h2><asp:Label ID="lblFirstname" runat="server" />,</h2>
      <p>Unfortunately, our assessment has not resulted in a suitable solution for your current situation.</p>
      <p>If you need to speak with a consultant urgently, then call our office line on <asp:Label runat="server" ID="lblPhoneNumber" />.</p>
    </div>
    </form>
    <asp:Literal runat="server" ID="litGoogleAdwordsScript" />
    <asp:Literal runat="server" ID="litGoogleAnalyticsScript" />
</body>
</html>
