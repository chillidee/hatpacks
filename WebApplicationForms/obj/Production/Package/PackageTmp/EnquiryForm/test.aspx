﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="WebApplicationForms.EnquiryForm.test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <link href="test.css" rel="stylesheet" />
</head>

<body>
  
    <form id="form1" runat="server">
    <telerik:RadScriptManager runat="server"></telerik:RadScriptManager>
    <div>
      <telerik:RadTextBox runat="server" ID="rtbTest" Text="test." CssClass="awesome-text-box" Label="Text only" >
      </telerik:RadTextBox>
      <br />&nbsp;<br />
      <telerik:RadNumericTextBox runat="server" ID="rntbTest" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Type="Currency" CssClass="awesome-text-box" Label="dollars"/>
      <br />&nbsp;<br />
      <telerik:RadNumericTextBox runat="server" ID="rntbMobile" NumberFormat-DecimalDigits="0" Type="Number" CssClass="awesome-text-box" Label="mobile" MaxLength="10" />
      <br />&nbsp;<br />
      <telerik:RadMaskedTextBox runat="server" ID="rmtbMobile" CssClass="awesome-text-box" Label="mobile" type="text" value="test." Mask="#### ### ###" >
      </telerik:RadMaskedTextBox>
      <br />&nbsp;<br />
      <input type="text" class="awesome-text-box"/>
      <br />&nbsp;<br />

      <div class="awesome-text-box" style="display:inline;padding:5px 2px 5px 2px"><asp:DropDownList runat="server" ID="ddl" Width="200px" CssClass="awesome-select">
        <asp:ListItem Text="Apple" />
        <asp:ListItem Text="Orange" />
        <asp:ListItem Text="Mango" />
      </asp:DropDownList></div>
      <br />&nbsp;<br />

    </div>
    </form>
</body>
</html>
        <!--
        <focusedstyle cssclass="MyFocusedTextBox"></focusedstyle>
        <hoveredstyle cssclass="hovered-text-box"></hoveredstyle>
        <invalidstyle cssclass="MyInvalidTextBox"></invalidstyle>        -->