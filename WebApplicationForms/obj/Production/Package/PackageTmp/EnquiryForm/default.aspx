﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="WebApplicationForms.EnquiryForm._default" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>enquiry form</title>  
  <asp:Literal runat="server" ID="litCSS" />
</head>
<body>
    <form id="form1" runat="server">    
    <asp:ScriptManager runat="server">
			<Scripts>
				<%--Framework Scripts--%>
				<asp:ScriptReference Name="MsAjaxBundle" />
				<asp:ScriptReference Name="jquery" />
				<asp:ScriptReference Name="jquery.ui.combined" />
				<asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
				<asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
				<asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
				<asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
				<asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
				<asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
				<asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
				<asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
				<asp:ScriptReference Name="WebFormsBundle" />
				<asp:ScriptReference Path="~/Scripts/custom-form-elements.js" />
				<%--Site Scripts--%>
			</Scripts>
    </asp:ScriptManager>
    <div id="RadAjaxPanel1Header"></div>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
      <div runat="server" id="divTest" style="border: solid 1px #444" visible="false">
        <h3 style="color:red">Testing purposes only</h3>
        parentURL: <asp:Label runat="server" ID="lblParent" /><br />
        referrerURL: <asp:Label runat="server" ID="lblReferrerURL" /><br />
        search term: <asp:Label runat="server" ID="lblSearchTerm" /><br />
        companyID: <asp:Label runat="server" ID="lblCompanyID" /><br />
        company: <asp:Label runat="server" ID="lblCompanyName" /><br />
        ip address: <asp:Label runat="server" ID="lblIpAddress" /><br />
        t: <asp:Label runat="server" ID="lblt" /><br />
        k: <asp:Label runat="server" ID="lblk" /><br />
        a: <asp:Label runat="server" ID="lbla" /><br />
        isMobile: <asp:Label runat="server" ID="lblisMobile" /><br />
        provider: <asp:Label runat="server" ID="lblProvider" />
      </div>
    
    <table class="main">
      <tr runat="server" id="trLoanAmount">
        <td class="td-label">
          Loan Amount*
        </td>
        <td class="td-input">
          <telerik:RadNumericTextBox runat="server" ID="rntbLoanAmount" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Type="Currency" CssClass="awesome-text-box" IncrementSettings-InterceptMouseWheel="false" IncrementSettings-InterceptArrowKeys="false">
          </telerik:RadNumericTextBox>
          <asp:RequiredFieldValidator runat="server" ID="rfvLoanAmount" ControlToValidate="rntbLoanAmount" ErrorMessage="Loan amount is required." Text="required" Display="Dynamic" />
        </td>
      </tr>
      <tr runat="server" id="trTypeOfLoan">
        <td class="td-label">Type of Loan*</td>
        <td class="td-input">
          <div class="awesome-text-box" style="display:inline;padding:5px 2px 5px 1px; margin-left:2px"><asp:DropDownList runat="server" ID="ddlLoanType" CssClass="awesome-select" Width="150px" DataSourceID="sqlLoanPurpose" AppendDataBoundItems="true" DataTextField="TypeOfLoan" DataValueField="LoanPurposeID" AutoPostBack="true" OnSelectedIndexChanged="ddlLoanType_SelectedIndexChanged">
            <asp:ListItem Text="[select one]" Value="" />
          </asp:DropDownList></div>
          <asp:RequiredFieldValidator runat="server" ID="rfvLoanPurposeID" ControlToValidate="ddlLoanType" ErrorMessage="Please choose a type of loan you're interested in." Text="required" />
          <asp:SqlDataSource runat="server" id="sqlLoanPurpose" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT clp.LoanPurposeID AS LoanPurposeID, COALESCE(AlternativeLoanPurpose, lp.LoanPurpose) AS TypeOfLoan FROM CompanyLoanPurpose clp INNER JOIN LoanPurpose lp ON clp.LoanPurposeID = lp.LoanPurposeID WHERE clp.CompanyID = @CompanyID AND AppearOnWebEnquiryForm = 1 ORDER BY clp.SortOrder" OnSelecting="sqlLoanPurpose_Selecting">
            <SelectParameters>
              <asp:Parameter Name="CompanyID" Type="Int32" DefaultValue="0" />
            </SelectParameters>
          </asp:SqlDataSource>
          </td>
      </tr>
      <tr runat="server" id="trHasProperty">
        <td colspan="2">
          <table>
            <tr>
              <td class="td-label" style="width:145px">Do you currently own or paying off real estate?*</td>        
              <td class="td-input">
                <asp:RadioButtonList runat="server" ID="rblOwnProperty" AutoPostBack="true" RepeatDirection="Horizontal" RepeatLayout="Flow" OnSelectedIndexChanged="rblOwnProperty_SelectedIndexChanged">
                  <asp:ListItem Text="Yes" Value="1" />
                  <asp:ListItem Text="No" Value="0" />
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator runat="server" ID="rvOwnProperty" ControlToValidate="rblOwnProperty" ErrorMessage="Please tell us if you currently own or paying off real estate." Text="required" Display="Dynamic" />
              </td>
            </tr>
          </table>
        </td>
        <td></td>
      </tr>
      <tr runat="server" id="trHaveDeposit" visible="false">
        <td class="td-label">Do you have a minimum 20% deposit of the purchase price?*</td>
        <td class="td-input">
          <asp:RadioButtonList runat="server" ID="rblHaveDeposit" AutoPostBack="true" RepeatDirection="Horizontal" RepeatLayout="Flow">
            <asp:ListItem Text="Yes" Value="1" />
            <asp:ListItem Text="No" Value="0" />
          </asp:RadioButtonList>
          <asp:RequiredFieldValidator runat="server" ID="rvHaveDeposit" ControlToValidate="rblHaveDeposit" ErrorMessage="Please tell us if you have a minimum of 20% deposit of the purchase price." Text="required" Display="Dynamic" />
        </td>
      </tr>
      <tr runat="server" id="tr1" visible="false">
        <td colspan="2" class="td-break">Tell us a little about your property</td>
      </tr>
      <tr runat="server" id="tr2" visible="false">
        <td class="td-label">Total real estate value</td>
        <td class="td-input">
          <telerik:RadNumericTextBox runat="server" ID="rntbRealEstateValue" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Type="Currency" CssClass="awesome-text-box"/>
          <asp:RequiredFieldValidator runat="server" ID="rfvRealEstateValue" ControlToValidate="rntbRealEstateValue" ErrorMessage="Total real estate value required." Text="required" Display="Dynamic" />
        </td>
      </tr>
      <tr runat="server" id="tr3" visible="false">
        <td class="td-label">Balance Owing</td>
        <td class="td-input">
          <telerik:RadNumericTextBox runat="server" ID="rntbBalanceOwing" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Type="Currency" CssClass="awesome-text-box"/>
          <asp:RequiredFieldValidator runat="server" ID="rfvBalanceOwing" ControlToValidate="rntbBalanceOwing" ErrorMessage="Balance owing required." Text="required" Display="Dynamic" />
        </td>
      </tr>
      <tr runat="server" visible="false">
        <td colspan="2" class="td-break">About you</td>
      </tr>
      <tr runat="server" id="trTitle">
        <td class="td-label">Title</td>
        <td class="td-input">
          <asp:RadioButtonList runat="server" ID="rblTitle" RepeatDirection="Horizontal" RepeatLayout="Flow">
            <asp:ListItem Value="Mr" Text="Mr" />
            <asp:ListItem Value="Mrs" Text="Mrs" />
            <asp:ListItem Value="Ms" Text="Ms" />
            <asp:ListItem Value="Dr" Text="Dr" />
          </asp:RadioButtonList>
        </td>
      </tr>
      <tr class="redNose">
        <td class="td-label">Website</td>
        <td class="td-input">
          <telerik:RadTextBox runat="server" ID="rtbWebsite" CssClass="awesome-text-box" />          
        </td>
      </tr>
      <tr runat="server" id="trFirstname">      
        <td class="td-label">First Name*</td>
        <td class="td-input">
          <telerik:RadTextBox runat="server" ID="rtbFirstname" CssClass="awesome-text-box" />
          <asp:RequiredFieldValidator runat="server" ID="rfvFirstname" ControlToValidate="rtbFirstname" ErrorMessage="First name is required." Text="required" EnableClientScript="true"/>
        </td>
      </tr>
      <tr runat="server" id="trLastname" visible="false">
        <td class="td-label">Last Name*</td>
        <td class="td-input">
          <telerik:RadTextBox runat="server" ID="rtbLastname" CssClass="awesome-text-box" />
          <asp:RequiredFieldValidator runat="server" ID="rfvLastname" ControlToValidate="rtbLastname" ErrorMessage="Last name is required." Text="required" Enabled="false" />
        </td>
      </tr>
      <tr runat="server" id="trContactHeader">
        <td colspan="2" class="td-break">
          Enter at least 2 forms of contact (eg. mobile + email)
          
        </td>
      </tr>
      <tr runat="server" id="trMobile" visible="false">
        <td class="td-label">Mobile number*</td>
        <td class="td-input">
          <telerik:RadTextBox runat="server" ID="rtbMobileNumber" CssClass="awesome-text-box" />          
          <asp:CustomValidator ID="cvMobile" runat="server" ErrorMessage="Mobile number is incomplete, or not in the correct format." Text="incomplete/invalid" ControlToValidate="rtbMobileNumber" OnServerValidate="cvMobile_ServerValidate"/>
          <asp:RequiredFieldValidator runat="server" ID="rfvMobile" ControlToValidate="rtbMobileNumber" ErrorMessage="Mobile is required." Text="required" Enabled="true" />
        </td>
      </tr>
      <tr runat="server" id="trLandline" visible="false">
        <td class="td-label">Landline number</td>
        <td class="td-input">
          <div class="awesome-text-box" style="display:inline;padding:5px 2px 5px 1px; margin-left:2px"><asp:DropDownList runat="server" ID="ddlHomePhoneAreaCode" CssClass="awesome-select">
            <asp:ListItem Value="02" />
            <asp:ListItem Value="03" />
            <asp:ListItem Value="07" />
            <asp:ListItem Value="08" />
          </asp:DropDownList></div>
          <telerik:RadTextBox runat="server" ID="rtbHomeNumber" CssClass="awesome-text-box" width="105px"/>          
          <asp:CustomValidator ID="cvHomeNumber" runat="server" ErrorMessage="Land line number is incomplete or invalid." Text="incomplete/invalid" ControlToValidate="rtbHomeNumber" OnServerValidate="cvHomeNumber_ServerValidate"/>
          <asp:CustomValidator runat="server" ID="cvContactNumbers" ErrorMessage="At least 1 contact number is needed please." OnServerValidate="cvContactNumbers_ServerValidate" />
        </td>
      </tr>
      <tr runat="server" visible="false">
        <td class="td-label">Work number</td>
        <td class="td-input">
          <div class="awesome-text-box" style="display:inline;padding:5px 2px 5px 1px; margin-left:2px"><asp:DropDownList runat="server" ID="ddlWorkPhoneAreaCode" CssClass="awesome-select">
            <asp:ListItem Value="02" />
            <asp:ListItem Value="03" />
            <asp:ListItem Value="07" />
            <asp:ListItem Value="08" />
          </asp:DropDownList></div>
          <telerik:RadMaskedTextBox runat="server" ID="rmtbWorkNumber" CssClass="awesome-text-box" Mask="#### ####" Width="105px"/>
          <asp:CustomValidator ID="cvWorkNumber" runat="server" ErrorMessage="Work number is incomplete." Text="incomplete" ControlToValidate="rmtbWorkNumber" OnServerValidate="cvWorkNumber_ServerValidate"/>
        </td>
      </tr>

      <tr runat="server" id="trEmailAddress" visible="false">
        <td class="td-label">Email*</td>
        <td class="td-input">
          <telerik:RadTextBox runat="server" ID="rtbEmail" CssClass="awesome-text-box" />
          <asp:RequiredFieldValidator runat="server" ID="rfvEmail" ControlToValidate="rtbEmail" ErrorMessage="Email is required." Text="required" />
          <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="rtbEmail" ErrorMessage="Invalid email format" Display="Dynamic"/>
        </td>
      </tr>
      <tr>
        <td colspan="2" class="td-break" runat="server" visible="false">
          Where are you approximately?          
        </td>
      </tr>
      <tr runat="server" id="trSuburb">
        <td class="td-label">Suburb*</td>
        <td class="td-input">
          <telerik:RadTextBox runat="server" ID="rtbSuburb" CssClass="awesome-text-box" />
          <asp:RequiredFieldValidator runat="server" ID="rfvSuburb" ControlToValidate="rtbSuburb" ErrorMessage="Your suburb is required." Text="required" EnableClientScript="true"/>
        </td>
      </tr>
      <tr runat="server" id="trStatePostCode" visible="false">
        <td class="td-label">State*</td>
        <td class="td-input">
          <div class="awesome-text-box" style="display:inline;padding:3px 2px 4px 1px; margin-left:2px; margin-right:5px;">
            <asp:DropDownList runat="server" ID="ddlState" CssClass="awesome-select">
            <asp:ListItem Value="ACT" />
            <asp:ListItem Value="NSW" Selected="True"/>
            <asp:ListItem Value="NT" />
            <asp:ListItem Value="QLD" />
            <asp:ListItem Value="SA" />
            <asp:ListItem Value="TAS" />
            <asp:ListItem Value="VIC" />
            <asp:ListItem Value="WA" />
          </asp:DropDownList></div>
        </td>
      </tr>
      <tr runat="server" id="trStatePostCode2" visible="false">
        <td class="td-label">Post Code*</td>
        <td class="td-input clearfix">
          <telerik:RadTextBox runat="server" ID="rtbPostCode" CssClass="awesome-text-box" Width="50px" />          
          <asp:RequiredFieldValidator runat="server" ID="rfvPostCode" ControlToValidate="rtbPostCode" ErrorMessage="Your PostCode is required." Text="required" EnableClientScript="true"/>
          <asp:CustomValidator ID="cvPostCode" runat="server" ErrorMessage="Postcode is incomplete or invalid." Text="incomplete/invalid" ControlToValidate="rtbPostCode" OnServerValidate="cvPostCode_ServerValidate"/>
        </td>
      </tr>

      <!--<tr>
        <td class="td-label">Postcode*</td>
        <td class="td-input">

        </td>
      </tr>-->
      <tr runat="server" id="trWhereHear">
        <td class="td-label">Where did you hear about us?*</td>
        <td class="td-input">
          <div class="awesome-text-box" style="display:inline;padding:5px 2px 5px 2px; margin-left:2px"><asp:DropDownList runat="server" ID="ddlReferral" CssClass="awesome-select">
            <asp:ListItem Text="[select one]" Value="" Selected="True" />
            <asp:ListItem Value="Yahoo" />
            <asp:ListItem Value="Radio" />
            <asp:ListItem Value="Newspaper" />
            <asp:ListItem Value="Google" />
            <asp:ListItem Value="Television" />
            <asp:ListItem Value="Family/Friend" />
            <asp:ListItem Value="Email" />
            <asp:ListItem Value="Other/Not Sure" />
          </asp:DropDownList></div>
          <asp:RequiredFieldValidator runat="server" ID="rfvLoanType" ControlToValidate="ddlReferral" ErrorMessage="Please tell us how you heard about us." Text="required" />
        </td>
      </tr>
      <tr runat="server" id="trComments">
        <td class="td-label">Comments</td>
        <td class="td-input">
          <telerik:RadTextBox runat="server" ID="rtbComments" CssClass="awesome-text-box" TextMode="MultiLine" Rows="2" />          
        </td>
      </tr>
      <tr class="redNose">
        <td class="td-label">URL:</td>
        <td class="td-input">
          <input type="text" id="txtURL" runat="server" />
        </td>
      </tr>

      <tr runat="server" id="trCC" visible="false">
        <td colspan="2" class="td-break">&nbsp;</td>
      </tr>
      <tr runat="server" id="trCC1" visible="false">
        <td class="td-label"><asp:Literal runat="server" ID="lit1" Text="Default" /></td>
        <td class="td-input">
          <telerik:RadTextBox runat="server" ID="rtbCreditIssue01" CssClass="awesome-text-box" TextMode="MultiLine" Rows="2" />
          <br />
          <asp:LinkButton runat="server" id="btnAddCC1" Text="add another credit issue" OnClick="btnAddCC1_Click" CausesValidation="false"/>
        </td>
      </tr>
      <tr runat="server" id="trCC2" visible="false">
        <td class="td-label"><asp:Literal runat="server" ID="lit2" Text="Default" /></td>
        <td class="td-input">
          <telerik:RadTextBox runat="server" ID="rtbCreditIssue02" CssClass="awesome-text-box" TextMode="MultiLine" Rows="2" />
          <br />
          <asp:LinkButton runat="server" id="btnAddCC2" Text="add another credit issue" OnClick="btnAddCC2_Click" CausesValidation="false"/>
        </td>
      </tr>
      <tr runat="server" id="trCC3" visible="false">
        <td class="td-label"><asp:Literal runat="server" ID="lit3" Text="Default" /></td>
        <td class="td-input">
          <telerik:RadTextBox runat="server" ID="rtbCreditIssue03" CssClass="awesome-text-box" TextMode="MultiLine" Rows="2" />
          <br />
          <asp:LinkButton runat="server" id="btnAddCC3" Text="add another credit issue" OnClick="btnAddCC3_Click" CausesValidation="false" Visible="false"/>
        </td>
      </tr>
      <tr runat="server" id="trCC4" visible="false">
        <td class="td-label"><asp:Literal runat="server" ID="lit4" Text="Default" /></td>
        <td class="td-input">
          <telerik:RadTextBox runat="server" ID="rtbCreditIssue04" CssClass="awesome-text-box" TextMode="MultiLine" Rows="2" />          
        </td>
      </tr>

<tr>
  <td colspan="2" class="td-validationSummary">
    <asp:ValidationSummary runat="server" ID="vs"
      HeaderText="<b>Oops, we've missed something...</b>"
      DisplayMode="BulletList"
      EnableClientScript="true"
      ShowValidationErrors="true"
      CssClass="validationSummary"
      
      />
  </td>
</tr>

      <tr>
        <td>* indicates required information.</td>
        <td class="td-input"><asp:Button runat="server" id="btnSubmit" Text="submit" width="" OnClick="btnSubmit_Click"/></td>
      </tr>
    </table>
      <asp:Label ID="lblErrorMessage" Visible="false" runat="server" Font-Bold="true" ForeColor="Red" />
      
      <script type="text/javascript">
      function fnOnUpdateValidators()
      {
        for (var i = 0; i < Page_Validators.length; i++)
        {
          var val = Page_Validators[i];
          var ctrl = document.getElementById(val.controltovalidate);
          if (ctrl != null && ctrl.style != null)
          {
            if (!val.isvalid)
              ctrl.className = 'InvalidTextBox';
              /*ctrl.style.background = 'green';*/
            else
              ctrl.style.backgroundColor = '';
          }
        }
      }
      </script>
      <asp:Label runat="server" ID="lblCompanyIDInternal" Visible="false" />
    </telerik:RadAjaxPanel>
    <div id="RadAjaxPanel1Footer"></div>
    </form>    
    <script type="text/javascript" src="../Scripts/iframeResizer.contentWindow.min.js"></script> 
</body>
</html>
