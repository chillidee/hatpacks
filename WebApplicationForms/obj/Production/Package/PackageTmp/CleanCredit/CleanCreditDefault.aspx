﻿<%@ Page Title="Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CleanCreditDefault.aspx.cs" Inherits="WebApplicationForms._CleanCreditDefault" %>

<%--<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h2>Welcome to your online form</h2>
				<p>
					Please fill in your details where appropriate. If you need assistance, contact us on <asp:Label runat="server" ID="lblPhone"/>.
				</p>
				<p>Regards, <asp:Label runat="server" ID="lblSalesManager" />.</p>
			</hgroup>			
		</div>
	</section>
</asp:Content>--%>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<h3>Privacy Statement</h3>		
	<script lang="javascript">
		function OpenWindow(filename) {
			window.open(filename, '920', 'menubar=no,location=no,menus=no,scrollbars,resizable=1,status=yes,top=50,left=200,width=920,height=640');
		}
	</script>
		<table>
		<tr>
			<td class="squaredFour" style="vertical-align:top"><input runat="server" type="checkbox" ID="cb1" class=""><label for="MainContent_cb1"></label></td>
			<td runat="server" id="tdAccept1">I/We <asp:Label runat="server" id="aplicate_name1"></asp:Label> have accessed the link provided and have read the <a onclick="OpenWindow('http://www.cleancredit.com.au/index.php/privacy-policy/')">Privacy Policy</a>, <a onclick="OpenWindow('http://www.cleancredit.com.au/index.php/terms-conditions/')">Terms and Conditions</a> and agree to continue.
				<br /><asp:CustomValidator runat="server" ID="CustomValidator1" OnServerValidate="CustomValidator1_ServerValidate" ErrorMessage="Your acceptance is required." Display="Dynamic"></asp:CustomValidator>
			</td>
		</tr>
		<tr>
			<td class="squaredFour" style="vertical-align:top"><input runat="server" type="checkbox" ID="cb2" class="" checked="checked"><label for="MainContent_cb2"></label></td>
			<td id="optionalaccept">(Optional) I <asp:Label runat="server" id="aplicate_name"></asp:Label> authorise [CompanyName] to provide me with information about products and services - which may be supplied by [CompanyName], or any member of the ALC group or its preferred partners - that may be of interest to me.</td>
	  </tr>
	</table>
	<div style="text-align:center">				
        <asp:Button ID="btnGetStartedNow" Text="Next" CssClass="main" runat="server" OnClick="btnGetStartedNow_Click" style="margin:10px 0!important"/>
        <p style="margin: 0;padding-top: 5px;border-top: 1px solid #cacaca;"><img src="../Images/noeffects.png" style="width: 13px;vertical-align: middle;margin-right: 5px;margin-top: -3px;">  <strong>Will NOT affect your credit file</strong></p>		
	</div>
  <div id="adminView" runat="server" visible="false">
    <asp:Label ID="agreed" runat="server"></asp:Label>
  </div>
</asp:Content>
