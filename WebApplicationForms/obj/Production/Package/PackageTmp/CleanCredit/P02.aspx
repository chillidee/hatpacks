﻿<%@ Page Title="Clean Credit Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="P02.aspx.cs" Inherits="WebApplicationForms.CleanCredit.P02" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
  <asp:Label ID="AdminCss" runat="server"></asp:Label>
  <section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Payment Options</h1>
      </hgroup>
		</div>
  </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
    <h3>Application Fee</h3>
    <div style="font-size:24px; color:red; font-weight:700;">Money Back Guarantee*</div>
    <p style="font-size: 18px;"><strong style="font-size: 18px;">In the unlikely event the credit listing cannot be removed or improved we will refund the entire removal fee back to you.</strong></p>
    <p style="font-size: 18px;">Please select a choice of payment below:</p>
    <table class="splitTable">
      <tr>
        <td class="split" style="padding-top:0px">
            <div style="float:left;clear:none">
            <asp:RadioButtonList runat="server" ID="rbl" AutoPostBack="true" OnSelectedIndexChanged="rbl_SelectedIndexChanged">
              <asp:ListItem Text="One off payment via <strong>Credit Card</strong> of <b>[ApplicationFee]</b> including GST." Value="1" />
              <asp:ListItem Text="One off payment via Direct <strong>Debit Authority</strong> for the amount of <b>[ApplicationFee]</b> including GST." Value="2"/>
              <asp:ListItem Text="Periodical payment of <b>[ApplicationFeeWeekly]</b> inc GST over 2 weeks totalling <b>[ApplicationFeeWeeklyTotal]</b> inc GST via <strong>Direct Debit Authority</strong>." Value="3"/>
              <asp:ListItem Text="Periodical payment of <b>[ApplicationFeeWeekly]</b> inc GST over 2 weeks totalling <b>[ApplicationFeeWeeklyTotal]</b> inc GST via <strong>Credit Card</strong>." Value="4"/>
            </asp:RadioButtonList>
            <asp:Panel ID="pnlJudgement" runat="server" Visible="false">

               
            <asp:Label ID="lblCourtCosts" runat="server"><p>JUDGEMENTS require upfront payment of $200.00 (GST Free) per Judgement to commence.</p></asp:Label>
            <p><strong>Reimbursement of costs - Court fees are GST free.</strong></p>
            <asp:RequiredFieldValidator validationgroup="Removelfeerequired" runat="server" ID="Reimcosts" ControlToValidate="rblCourtCost" Display="Dynamic" Text="A selection is required." />
            <asp:RadioButtonList runat="server" ID="rblCourtCost" AutoPostBack="true">
            <asp:ListItem  Text="One off payment via Credit Card of [JudgementFee] (GST Free) i.e. $200.00 per Judgement." Value="1" />
            <asp:ListItem  Text="One off payment via Direct Debit Authority for the amount of [JudgementFee] (GST Free) i.e. $200.00 per Judgement." Value="2"/>
            </asp:RadioButtonList>            
            </asp:Panel>
            </div>
          <asp:RequiredFieldValidator validationgroup="Removelfeerequired" runat="server" ID="rfv" ControlToValidate="rbl" Display="Dynamic" Text="A selection is required." />
          <div runat="server" id="divDayOfBusinessWeekToDebit" style="float:left" visible="false">
          <div runat="server" id="bestdaytoprocess">Which business day is best to process the funds?
          <asp:DropDownList runat="server" ID="ddlDayOfBusinessWeekToDebit">
            <asp:ListItem Value="" Text="Anyday" />
            <asp:ListItem Value="Monday" />
            <asp:ListItem Value="Tuesday" />
            <asp:ListItem Value="Wednesday" />
            <asp:ListItem Value="Thursday" />
            <asp:ListItem Value="Friday" />
          </asp:DropDownList></div>
           
            <div runat="server" id="divFurtherAgreement" visible="false">
              <div runat="server" id="adminifee" style="display:none;"><p>All payments must be met on the due dates.  Please note that there is an administration fee of $25.00 for each time a direct debit payment is dishonoured.</p></div>
              <p style="display:none;">I <asp:Label runat="server" id="aplicate_name"></asp:Label> authorise Clean Credit Pty Ltd, its employees and agents to enter negotiations on my behalf with regard to my credit 
information with a view to facilitating the removal of an inaccurate, false, misleading, unfair or 
contestable credit listing.</p>
             
            </div>
          </div>
        </td>
      </tr>
    </table>

          
  </telerik:RadAjaxPanel>

     <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server">
    <h3>Removal Fee incl GST
<%--    <table style="margin-bottom: 20px;">
      <tr>
        <td><strong>Per removal  (Default. Judgement or Clearout)</strong></td><td><strong>$880.00 inc GST</strong></td>
      </tr>
    </table>--%>
      <asp:GridView ID="grdRemovalFee" runat="server" style="margin-bottom: 20px;" AutoGenerateColumns="False" ShowHeader="False">
          <Columns> 
              <asp:BoundField DataField="RemovalFee" DataFormatString="{0:c}" >
             <ItemStyle Font-Bold="True" Font-Size="Large" />
              </asp:BoundField>
          </Columns>
      </asp:GridView>

        </h3>
    <!--<p><u><b>Removal fee</b></u>: This fee is only payable upon conformation of the successful removal or improvement of your credit item/s.</p>-->
    <div style="background-color: #14497b;padding:30px; margin: 17px;">
      <asp:RadioButtonList runat="server" ID="rblRemovalFeeOption">
        <asp:ListItem Value="1" Text="<h4 style='color: #fff;'>Option 1 - (Lump sump payment)</h4><p>I agree to pay the total of my removal fees inc GST at the time Clean Credit Pty Ltd commences work on my file.</p>"/>
        <asp:ListItem Value="2" Text="<h4 style='color: #fff;'>Option 2 - (Pay in instalments $100 per week)</h4><p>I agree to pay $100.00 towards my removal fees.</p>" />     
        <asp:ListItem Value="4" Text="<h4 style='color: #fff;'>Option 3 - (Pay in instalments $200 per fortnight)</h4><p>I agree to pay $200.00 per fortnight towards my removal fees.</p>" />                
      </asp:RadioButtonList>
        <p style="color:#ffffff;">These payments are to begin following the completed payment of my application fee.</p>
      <!--<h4>Option 2 - (Pay in instalments)</h4><p>I agree to pay YYY including GST per removal, or improvement of any Default, Clearout, or court listing on my credit file if paid by direct debit at a minimum payment of $100 per week.</p>-->
      <!--<asp:ListItem Value="3" Text="<h4>Option 3 - (Decide later)</h4><p>I don't know, I will decide later.</p>" />-->

      <asp:RequiredFieldValidator validationgroup="Removelfeerequired" runat="server" ID="RequiredFieldValidator1" ControlToValidate="rblRemovalFeeOption" Display="Dynamic" Text="<span style='font-size:16px;font-weight:bold'>A selection is required, please select an option</span>" />
    </div>


<p>I <asp:Label runat="server" id="Label1"></asp:Label> authorise Clean Credit Pty Ltd, its employees and agents to enter negotiations on my behalf with regard to my credit 
information with a view to facilitating the removal of an inaccurate, false, misleading, unfair or 
contestable credit listing.</p>
  
<p>I confirm that the information that I have provided to Clean Credit Pty Ltd is true and accurate and accept the terms of the refund policy.</p>


    <div>
         <table style="width:100%"><tr><td class="squaredFour" style="width:50px;height:50px"><input runat="server" type="checkbox" class="checkbox" ID="cb1" checked="checked"/><label style="top: 10px;" for="MainContent_cb1"></label></td>
                <td id="tdaccept-2"><asp:Label runat="server" ID="lblAgreementMessage" Text="I agree to the above conditions." style="color:#333"/>
                 <br /><asp:CustomValidator runat="server" ID="cvAgree" OnServerValidate="cvAgree_ServerValidate" ErrorMessage="Your acceptance is required." Display="Dynamic"></asp:CustomValidator>          
                </td></tr>
               <tr><td></td><td><asp:Label runat="server" ID="lblStamp" Visible="false" /></td></tr>
         </table>

     </div>
  </telerik:RadAjaxPanel>

			<div class="nextbtn-block">
                <asp:Button ID="btnSubmit" validationgroup="Removelfeerequired" Text="Next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
                <p style="margin: 0;padding-top: 5px;border-top: 1px solid #cacaca;"><img src="../Images/noeffects.png" style="width: 13px;vertical-align: middle;margin-right: 5px;margin-top: -3px;">  <strong>Will NOT affect your credit file</strong></p>
		    </div>

             <div class="goback-btn">			
                <asp:LinkButton ID="btnBack" runat="server" CssClass="backbtn" OnClick="btnBack_Click">Go Back</asp:LinkButton>
                <p style="margin: 0;padding-top: 5px;border-top: 1px solid #cacaca;"><a href="https://www.cleancredit.com.au/index.php/terms-conditions/" target="_blank">*Terms & conditions</a> apply</p>
             </div>

      
		
<asp:SqlDataSource ID="sqlState" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT '' State UNION SELECT State FROM State"></asp:SqlDataSource>
    
</asp:Content>
