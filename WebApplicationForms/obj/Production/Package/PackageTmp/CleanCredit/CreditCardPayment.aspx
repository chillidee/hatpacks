﻿<%@ Page Title="Clean Credit Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreditCardPayment.aspx.cs" Inherits="WebApplicationForms.CleanCredit.CreditCardPayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
  <section class="featured">
    <div class="content-wrapper">
      <hgroup class="title">
        <h1>Credit Card Payment</h1>
      </hgroup>
    </div>
  </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">    
    <table class="splitTable">
      <tr>
        <td class="split" style="padding-top: 0px;width:75%;">
          <table class="fieldset">
            <tr>
              <td class="fieldLabel">Select Credit Card</td>
              <td>
                <asp:RadioButtonList runat="server" ID="rblCreditCardType" CssClass="rblCreditCardType" OnSelectedIndexChanged="rblCreditCardType_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                  <asp:ListItem Value="Visa" Text="<img src='/images/Visapayment.png' class='pay-image' style='float:left;'/>" />
                  <asp:ListItem Value="Mastercard" Text="<img src='/images/mastercardpayment.png' class='pay-image' style='float:left;'/>" />
                  <asp:ListItem Value="AMEX" Text="<img src='/images/Amxpayment.png' class='pay-image' style='float:left;'/>" />                  
                </asp:RadioButtonList>
                  <asp:RequiredFieldValidator  validationgroup="Creditcardinforequired" ID="RequiredFieldValidator8" Runat="server" Display="Dynamic" ControlToValidate="rblCreditCardType" ErrorMessage="Please Choose Your Card Type" > </asp:RequiredFieldValidator>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel">Credit Card Number</td>
              <td>
                <telerik:RadMaskedTextBox runat="server" ID="rmtbCreditCardNumber" OnTextChanged="rmtbCreditCardNumber_TextChanged" AutoPostBack="true" Width="100%" />
                <asp:CustomValidator runat="server" ID="cvCreditCardNumber" ControlToValidate="rmtbCreditCardNumber" OnServerValidate="cvCreditCardNumber_ServerValidate" Text="Invalid credit card number, please try again." />
                  <br />
                <asp:RequiredFieldValidator validationgroup="Creditcardinforequired" ID="RequiredFieldValidator6" Runat="server" Display="Dynamic" ControlToValidate="rmtbCreditCardNumber" ErrorMessage="Please Enter Your Card Details" > </asp:RequiredFieldValidator>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel">Expiry (Month/Year)</td>
              <td>
                <asp:DropDownList runat="server" ID="ddlCCExpiryMonth" AutoPostBack="true"></asp:DropDownList>
                <asp:DropDownList runat="server" ID="ddlCCExpiryYear" AutoPostBack="true" OnSelectedIndexChanged="ddlCCExpiryYear_SelectedIndexChanged"></asp:DropDownList>
                <asp:CustomValidator runat="server" ID="cvExpMthYear" ControlToValidate="ddlCCExpiryYear" OnServerValidate="cvExpMthYear_ServerValidate" Text="Invalid month/year. Expiry cannot be any earlier than present time." />
              </td>
            </tr>
            <tr>
              <td class="fieldLabel">Name on Card</td>
              <td>
                <telerik:RadTextBox runat="server" ID="rtbCCNameOnCard" Width="100%"></telerik:RadTextBox>
                <asp:RequiredFieldValidator validationgroup="Creditcardinforequired" ID="RequiredFieldValidatora" Runat="server" Display="Dynamic" ControlToValidate="rtbCCNameOnCard" ErrorMessage="Please Enter Your Name On The Card" > </asp:RequiredFieldValidator>
              </td>
            </tr>
          </table>
          <div style="padding-top: 30px">
            <table>
              <tr>
                <td class="squaredFour" style="width: 50px; height: 50px">
                    <input runat="server" type="checkbox" class="checkbox" id="cb1" visible="true" checked="checked"/><label style="top: 10px;background: #b7b7b7;" for="MainContent_cb1"></label></td>
                <td id="tdaccept-2">
                  <asp:Label runat="server" ID="lblAgreementMessage" Text="" />
                  <br />
                  <asp:CustomValidator runat="server" ID="cvAgree" OnServerValidate="cvAgree_ServerValidate" ErrorMessage="Your acceptance is required." Display="Dynamic"></asp:CustomValidator>
                </td>
              </tr>
            </table>
          </div>
          <div id="adminView" runat="server" visible="false">
            <asp:Label ID="agreed" runat="server"></asp:Label>
          </div>
        </td>

        <td class="block-image-2">
        </td>

      </tr>
    </table>

  </telerik:RadAjaxPanel>
  
    <div class="nextbtn-block">
       <asp:Button ID="btnSubmit" validationgroup="Creditcardinforequired" Text="Next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
       <p style="margin: 0;padding-top: 5px;border-top: 1px solid #cacaca;"><img src="../Images/noeffects.png" style="width: 13px;vertical-align: middle;margin-right: 5px;margin-top: -3px;">  <strong>Will NOT affect your credit file</strong></p>
    </div>

    <div class="goback-btn">
    <asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click">Go Back</asp:LinkButton>
    </div>

</asp:Content>
