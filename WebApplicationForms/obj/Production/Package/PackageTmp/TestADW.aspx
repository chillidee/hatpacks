﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestADW.aspx.cs" Inherits="WebApplicationForms.TestADW" Async="true"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>
        (function ($) {

            var budgetCalculator;

            var id = 'DFAC5CB0-69B9-4F75-A299-DA2F472AEEE0';
            var app = 'true';

            $.get("http://localhost:64756/api/enquiryform/GetBudgetCalculator", { guid: id, app: app })
            .done(function (data) {
                if (data != null) {
                    budgetCalculator = data;

                    /*$.ajax({
                        url: "http://localhost:64756/api/enquiryform/PostBudgetCalculator",
                        type: "post",
                        data: JSON.stringify(budgetCalculator),
                        dataType: 'json'
                    });*/


                    $.post("http://localhost:64756/api/enquiryform/PostBudgetCalculator", JSON.stringify(budgetCalculator))
                    .done(function (data) {
                       if (data != null) {
                           budgetCalculator = data;
                       }
                    })
                    .fail(function () { });
                }
            })
            .fail(function () { });
        })(jQuery);
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <input runat="server" type="checkbox" class="checkbox" id="cb1" visible="true" checked="checked" />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        <asp:TextBox ID="txtInput" runat="server" Width="400px"></asp:TextBox>
    
    </div>
        <asp:Label ID="lblerror" runat="server" Text="Label"></asp:Label>
    </form>
</body>
</html>
