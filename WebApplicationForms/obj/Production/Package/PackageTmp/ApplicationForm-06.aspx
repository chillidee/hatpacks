﻿<%@ Page Title="Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationForm-06.aspx.cs" Inherits="WebApplicationForms.WebForm_A06" %>

<%@ Register Src="~/UserControls/SelfEmployedIncomeGrid.ascx" TagPrefix="uc1" TagName="SelfEmployedIncomeGrid" %>
<%@ Register Src="~/UserControls/MonthlyIncomeGrid.ascx" TagPrefix="uc1" TagName="MonthlyIncomeGrid" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	<style type="text/css">
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

	<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Your Application</h1>
				<h2>Income Details</h2>
			</hgroup>
			<p>&nbsp;</p>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
		<div runat="server" id="divSelfEmployedIncome" visible="false">
		<h3>Self Employed Income</h3>
		If any of you are self employed, please provide your self employed income information below.
		<table class="splitTable">
			<tr>
				<td class="split">
					<h4><asp:Literal ID="litAp1Firstname1" runat="server"></asp:Literal></h4>
					<uc1:SelfEmployedIncomeGrid runat="server" ID="SelfEmployedIncomeGridAp1" />
				</td>
				<td class="split" runat="server" id="tdP2_1">
					<h4>
						<asp:Literal ID="litAp2Firstname1" runat="server"></asp:Literal>
					</h4>
					<uc1:SelfEmployedIncomeGrid runat="server" ID="SelfEmployedIncomeGridAp2" />
				</td>
			</tr>
		</table>
		</div>
		<h3>Tell us about your income...</h3>

		<table class="splitTable">
			<tr>
				<td class="split">
					<h4><asp:Literal ID="litAp1Firstname2" runat="server"></asp:Literal></h4>
					<uc1:MonthlyIncomeGrid runat="server" ID="MonthlyIncomeGridAp1" />
				</td>
				<td class="split" runat="server" id="tdP2_2">
					<h4><asp:Literal ID="litAp2Firstname2" runat="server"></asp:Literal></h4>
					<uc1:MonthlyIncomeGrid runat="server" ID="MonthlyIncomeGridAp2" />					
				</td>
			</tr>
		</table>





		<div style="text-align: right">
			<asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click">go back</asp:LinkButton>
			<asp:Button ID="btnSubmit" Text="next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
		</div>
	</telerik:RadAjaxPanel>
</asp:Content>
