﻿<%@ Page Title="Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplicationForms._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Hello <asp:Literal ID="litFirstname" runat="server" />.</h1>
				<h2>Welcome to your Application Form.</h2>
				<p class="display:none"><asp:Literal ID="litFirstname2" runat="server" /> this is your online application form with <asp:Label runat="server" ID="lblCompanyName" />. Please fill in your details where appropriate.</p>	
                <p>If you need assistance, contact us at <asp:Label runat="server" ID="lblPhone"/>. Your personal Application Form manager is <asp:Label runat="server" ID="lblSalesManager" />.</p>
			</hgroup>			
		</div>
	</section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<h3>The Application Form process are as follows:</h3>
	<ol class="round">
		<li class="one">
			<h5>Your personal details</h5>
			Your personal details are required such as name, address and contact details.
		</li>
		<li class="two">
			<h5>Your financial position</h5>
			We will need details on your financial position, being details about your current assets and liabilities.
		</li>
		<li class="three">
			<h5>Your business details (if applicable)</h5>
			We will need details about your current business.
		</li>
	</ol>

	But first...we need you to agree to a few things.<br />
	<h3>Privacy Statement</h3>		
	<script lang="javascript">
		function OpenWindow(filename) {
			window.open(filename, '920', 'menubar=no,location=no,menus=no,scrollbars,resizable=1,status=yes,top=50,left=200,width=920,height=640');
		}
	</script>
		<table>
		<tr>
			<td style="vertical-align:top"><input runat="server" type="checkbox" ID="cb1" class="styled"></td>
			<td runat="server" id="tdAccept1">I/We have accessed the link provided and have read the <a onclick="OpenWindow('TermsAndConditions.aspx')">Statement and Declaration</a> and agree to continue with my application.
				<br /><asp:CustomValidator runat="server" ID="CustomValidator1" OnServerValidate="CustomValidator1_ServerValidate" ErrorMessage="Your acceptance is required." Display="Dynamic"></asp:CustomValidator>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top"><input runat="server" type="checkbox" ID="cb2" class="styled" checked="checked"></td>
			<td>(Optional) I authorise [CompanyName] to provide me with information about products and services - which may be supplied by [CompanyName], or any member of the ALC group or its preferred partners - that may be of interest to me.</td>
	  </tr>
	</table>
	
	<h2>Lets get started!</h2>
	<br />Regards,<br />
	<asp:Label runat="server" ID="lblSalesManager2" />
	<div style="float:right">		
		<asp:ImageButton runat="server" ID="btnGetStartedNow" OnClick="btnGetStartedNow_Click" ImageUrl="Images/button-getstartednow.jpg" BorderStyle="None" style="background:none"/>		
	</div>
</asp:Content>
