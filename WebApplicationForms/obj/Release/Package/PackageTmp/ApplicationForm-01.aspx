﻿<%@ Page Title="Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationForm-01.aspx.cs" Inherits="WebApplicationForms.WebForm_A01" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
	<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Your Application</h1>
				<h2>Personal Details</h2>
			<p>
				Please fill in your personal details.
			</p>
      </hgroup>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
		<h3>Your names</h3>
		<!--<table style="width: 
            ; border-collapse: separate; border-spacing: 15px;">-->
		<table class="splitTable">
			<tr>
				<!--<td style="width: 50%; background-color: #c7d1d6; padding: 40px;">-->
				<td class="split">
					<h4>You (the main contact)</h4>
					<table class="fieldset">
						<tr>
							<td>
								<asp:HiddenField ID="hfApplicantID" runat="server" />
								Title</td>
							<td>
								<telerik:RadComboBox ID="RadComboBoxTitle" runat="server" OffsetY="10">
									<Items>
                                        <telerik:RadComboBoxItem runat="server" Value="" Text="(none)"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Value="Mr" Text="Mr"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Text="Mrs" Value="Mrs"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Text="Ms" Value="Ms"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Text="Dr" Value="Dr"></telerik:RadComboBoxItem>
									</Items>
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td>Firstname</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxFirstname" runat="server"></telerik:RadTextBox>
								<asp:RequiredFieldValidator ID="rfvFirstname" runat="server" ErrorMessage="Required." ControlToValidate="RadTextBoxFirstName"></asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td>Surname</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxSurname" runat="server"></telerik:RadTextBox>
								<asp:RequiredFieldValidator ID="rfvSurname" runat="server" ErrorMessage="Required." ControlToValidate="RadTextBoxSurname"></asp:RequiredFieldValidator>
							</td>
						</tr>
					</table>
				</td>
				<td class="split">
					<h4>Person 2</h4>
					<asp:Button runat="server" ID="btnAddApplicant2" Text="add person" OnClick="btnCreateApplicant2_Click" visible="false"/>
					<table class="fieldset" runat="server" id="tableApplicant2_1">
						<tr>
							<td>
								<asp:HiddenField ID="hfApplicantID2" runat="server" />
								Title</td>
							<td>
								<telerik:RadComboBox ID="RadComboBoxTitle2" runat="server" OffsetY="10">
									<Items>
                    <telerik:RadComboBoxItem runat="server" Value="" Text="(none)"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Value="Mr" Text="Mr"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Text="Mrs" Value="Mrs"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Text="Ms" Value="Ms"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Text="Dr" Value="Dr"></telerik:RadComboBoxItem>
									</Items>
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td>Firstname</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxFirstname2" runat="server"></telerik:RadTextBox>
								<asp:RequiredFieldValidator ID="rfvRadTextBoxFirstname2" runat="server" ControlToValidate="RadTextBoxFirstname2" Display="Dynamic" ErrorMessage="Required."></asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td>Surname</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxSurname2" runat="server"></telerik:RadTextBox>
								<asp:RequiredFieldValidator ID="rfvRadTextBoxSurname2" runat="server" ControlToValidate="RadTextBoxSurname2" Display="Dynamic" ErrorMessage="Required."></asp:RequiredFieldValidator>
							</td>
						</tr>
					</table>

				</td>
			</tr>
		</table>

		<h3>How we can best contact you</h3>
		<table class="splitTable">
			<tr>
				<td class="split">
					<h4>You (the main contact)</h4>
					<table class="fieldset">
						<tr>
							<td>Mobile Phone</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxMobile" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td>Home Phone
							</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxTelephoneHome" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td>Work Phone</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxTelephoneWork" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td>Fax</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxFax" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td>Primary email</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxEmail" runat="server"></telerik:RadTextBox>
								<asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Invalid email." ControlToValidate="RadTextBoxEmail" EnableClientScript="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
								<asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Required." ControlToValidate="RadTextBoxEmail"></asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td>Secondary email</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxEmailSecond" runat="server"></telerik:RadTextBox>
								<asp:RegularExpressionValidator ID="revEmailSecond" runat="server" ErrorMessage="Invalid email." ControlToValidate="RadTextBoxEmailSecond" EnableClientScript="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
							</td>
						</tr>
					</table>
				</td>
				<td class="split">
					<h4>Person 2 </h4>
					<table class="fieldset" runat="server" id="tableApplicant2_2">
						<tr><td></td><td><asp:Button runat="server" ID="btnCopyContactDetails" Text="same as main contact" CausesValidation="false" OnClick="btnCopyContactDetails_Click" /></td></tr>
						<tr>
							<td>Mobile Phone</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxMobile2" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td>Home Phone
							</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxTelephoneHome2" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td>Work Phone</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxTelephoneWork2" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td>Fax</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxFax2" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td>Primary email</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxEmail2" runat="server"></telerik:RadTextBox>
								<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid email address." ControlToValidate="RadTextBoxEmail2" EnableClientScript="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>								
							</td>
						</tr>
						<tr>
							<td>Secondary email</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxEmailSecond2" runat="server"></telerik:RadTextBox>
								<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Invalid email address." ControlToValidate="RadTextBoxEmailSecond2" EnableClientScript="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</telerik:RadAjaxPanel>
			<div style="text-align: right">			
			<asp:Button ID="btnSubmit" Text="next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
		</div>
</asp:Content>
