﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="complete.aspx.cs" Inherits="WebApplicationForms.EnquiryForm.complete" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="enquiryForm.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
      <h1>Done!</h1>
      <h2>Thanks <asp:Label ID="lblFirstname" runat="server" />,</h2>
      <p>For taking the time to fill out an enquiry form. One of our consultants will contact you shortly.</p>
      <p>If you need to speak with a consultant urgently, please wait 5 minutes for us to receive your enquiry then call our office line on <asp:Label runat="server" ID="lblPhoneNumber" />.</p>
    </div>
      <asp:Literal runat="server" ID="litGoogleAdwordsScript" />
      <asp:Literal runat="server" ID="litGoogleAnalyticsScript" />
    </form>
</body>
</html>
