﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelfEmployedIncomeGrid.ascx.cs" Inherits="WebApplicationForms.UserControls.SelfEmployedIncomeGrid" %>
					<telerik:RadGrid
						ID="rgSelfEmployed"
						runat="server"
						CellSpacing="0"
						DataSourceID="sqlApplicantSelfEmployedDtl"
						GridLines="None"
						EnableLinqExpressions="false"
						AllowAutomaticUpdates="True"
						AllowAutomaticDeletes="True"
						AllowAutomaticInserts="true"
						OnItemInserted="rgSelfEmployed_ItemInserted"
						HeaderStyle-Font-Bold="True"
						HeaderStyle-Font-Size="12px"
						EnableHeaderContextAggregatesMenu="true"
						ClientSettings-AllowKeyboardNavigation="true">

						<MasterTableView
							AutoGenerateColumns="False"
							DataKeyNames="ApplicantSelfEmployedDtlID"
							DataSourceID="sqlApplicantSelfEmployedDtl"
							EditMode="InPlace"
							CommandItemDisplay="Bottom"
							InsertItemDisplay="Bottom"
							EditItemStyle-Width="5px" EditItemStyle-BackColor="#FFFFCC"
							ShowFooter="true">
							<CommandItemTemplate>
								<div style="padding: 10px">
									<asp:LinkButton runat="server" ID="btnAdd" Text="+ add" CommandName="InitInsert" ForeColor="#338FB3" />
								</div>
							</CommandItemTemplate>
							<Columns>
								<telerik:GridBoundColumn
									UniqueName="ApplicantID" Visible="false"
									DataField="ApplicantID">
								</telerik:GridBoundColumn>

								<telerik:GridDropDownColumn
									UniqueName="ApplicantSelfEmployedDtlTypeID"
									ListTextField="ApplicantSelfEmployedDtlType"
									DataSourceID="sqlApplicantSelfEmployedDtlType"
									HeaderText="Type"
									DataField="ApplicantSelfEmployedDtlTypeID"
									DropDownControlType="DropDownList"
									ListValueField="ApplicantSelfEmployedDtlTypeID"
									CurrentFilterFunction="NoFilter"
									FooterText="Totals:"
									FooterStyle-Font-Bold="true">
								</telerik:GridDropDownColumn>

								<telerik:GridTemplateColumn
									HeaderText="Current Year"
									UniqueName="gtcAmountCurYr"
									Aggregate="Sum"
									DataField="AmountCurYr"
									DataType="System.Decimal"
									ItemStyle-HorizontalAlign="Right"
									HeaderStyle-HorizontalAlign="Right"
									FooterAggregateFormatString="{0:C0}"
									FooterStyle-HorizontalAlign="Right"
									FooterStyle-Font-Bold="true">
									<ItemTemplate>
										<asp:Literal runat="server" ID="litAmountCurYr" Text='<%# String.Format("{0:C0}",Eval("AmountCurYr")) %>'></asp:Literal>
									</ItemTemplate>
									<EditItemTemplate>
										<telerik:RadNumericTextBox runat="server" ID="rtbAmountCurYr" Text='<%# Bind("AmountCurYr") %>' Width="70px" DataType="System.Decimal" Type="Currency" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
									</EditItemTemplate>
								</telerik:GridTemplateColumn>

								<telerik:GridTemplateColumn
									HeaderText="Previous Year"
									UniqueName="gtcAmountPreYr"
									Aggregate="Sum"
									DataField="AmountPreYr"
									DataType="System.Decimal"
									ItemStyle-HorizontalAlign="Right"
									HeaderStyle-HorizontalAlign="Right"
									FooterAggregateFormatString="{0:C0}"
									FooterStyle-HorizontalAlign="Right"
									FooterStyle-Font-Bold="true">								
									<ItemTemplate>
										<asp:Literal runat="server" ID="litAmountPreYr" Text='<%# String.Format("{0:C0}",Eval("AmountPreYr")) %>'></asp:Literal>
									</ItemTemplate>
									<EditItemTemplate>
										<telerik:RadNumericTextBox runat="server" ID="rtbAmountPreYr" Text='<%# Bind("AmountPreYr") %>' Width="70px" DataType="System.Decimal" Type="Currency" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
									</EditItemTemplate>
								</telerik:GridTemplateColumn>

								<telerik:GridEditCommandColumn CancelText="cancel" InsertText="save" EditText="edit" UpdateText="save"></telerik:GridEditCommandColumn>

								<telerik:GridButtonColumn CommandName="Delete" Text="X" HeaderTooltip="Delete" ConfirmText="Not recoverable. Delete?"></telerik:GridButtonColumn>

							</Columns>
						</MasterTableView>
					</telerik:RadGrid>

					<asp:SqlDataSource ID="sqlApplicantSelfEmployedDtl" runat="server" ConnectionString="<%$ ConnectionStrings:CRISMARConnectionString %>"
						SelectCommand="SELECT * FROM [ApplicantSelfEmployedDtl]"
						DeleteCommand="DELETE FROM [ApplicantSelfEmployedDtl] WHERE [ApplicantSelfEmployedDtlID] = @ApplicantSelfEmployedDtlID"
						UpdateCommand="UPDATE [ApplicantSelfEmployedDtl] SET [ApplicantID] = @ApplicantID, [ApplicantSelfEmployedDtlTypeID] = @ApplicantSelfEmployedDtlTypeID, [AmountCurYr] = @AmountCurYr, [AmountPreYr] = @AmountPreYr, [AmountDisplay] = @AmountDisplay, [TermDisplay] = @TermDisplay, [Comment] = @Comment WHERE [ApplicantSelfEmployedDtlID] = @ApplicantSelfEmployedDtlID"
						InsertCommand="INSERT INTO [ApplicantSelfEmployedDtl] ([ApplicantID], [ApplicantSelfEmployedDtlTypeID], [AmountCurYr], [AmountPreYr]) VALUES (@ApplicantID, @ApplicantSelfEmployedDtlTypeID, @AmountCurYr, @AmountPreYr)">
						<DeleteParameters>
							<asp:Parameter Name="ApplicantSelfEmployedDtlID" Type="Int32" />
						</DeleteParameters>
						<InsertParameters>
							<asp:Parameter Name="ApplicantID" Type="Int32" />
							<asp:Parameter Name="ApplicantSelfEmployedDtlID" Type="Int32" />
							<asp:Parameter Name="AmountCurYr" Type="Decimal" />
							<asp:Parameter Name="AmountPreYr" Type="Decimal" />
						</InsertParameters>
						<UpdateParameters>
							<asp:Parameter Name="ApplicantID" Type="Int32" />
							<asp:Parameter Name="ApplicantSelfEmployedDtlTypeID" Type="Int32" />
							<asp:Parameter Name="AmountCurYr" Type="Decimal" />
							<asp:Parameter Name="AmountPreYr" Type="Decimal" />
							<asp:Parameter Name="AmountDisplay" Type="Decimal" />
							<asp:Parameter Name="TermDisplay" Type="String" />
							<asp:Parameter Name="Comment" Type="String" />
							<asp:Parameter Name="ApplicantSelfEmployedDtlID" Type="Int32" />
						</UpdateParameters>
					</asp:SqlDataSource>
					<asp:SqlDataSource ID="sqlApplicantSelfEmployedDtlType" runat="server" ConnectionString="<%$ ConnectionStrings:CRISMARConnectionString %>" SelectCommand="SELECT * FROM [ApplicantSelfEmployedDtlType]"></asp:SqlDataSource>