﻿<%@ Page Title="Thank you" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ThankYou.aspx.cs" Inherits="WebApplicationForms.ThankYou" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
		<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Thank you <asp:Literal ID="litFirstname" runat="server" />, you are AWESOME!</h1>
				
				<p><asp:Literal ID="litFirstname2" runat="server" />, thank you for submittting your loan application with <asp:Label runat="server" ID="lblCompanyName" />.</p>
				<p>Remember, you can contact your personal Application Form manager <asp:Label runat="server" ID="lblSalesManager" />, anytime :-) </p>
			</hgroup>
			<p>
			</p>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	
	
</asp:Content>
