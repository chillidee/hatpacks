﻿<%@ Page Title="Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationForm-05a.aspx.cs" Inherits="WebApplicationForms.WebForm_A05a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	<style type="text/css">
.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}
		</style>
	</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

	<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Your Application</h1>
				<h2>Personal Details</h2>
			</hgroup>
			<p>Your employment details.</p>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">		
		<asp:SqlDataSource ID="sqlPCode" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT [PCodeID], [Combo] FROM [vwPCode] ORDER BY [SUBURB], [PCODE] DESC"></asp:SqlDataSource>		
		<asp:SqlDataSource ID="sqlEmploymentBasis" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="(SELECT '' EmploymentBasis, 0 IsSelfEmployed) UNION (SELECT EmploymentBasis, IsSelfEmployed FROM EmploymentBasis) ORDER BY EmploymentBasis"></asp:SqlDataSource>

		<h3>Who was your former employer?</h3>

		<table class="splitTable">
			<tr>
				<td class="split">
					<h4><asp:Literal ID="litAp1Firstname2" runat="server"></asp:Literal></h4>					
					<asp:label runat="server" ID="lblAp1Msg" Text="Not required." />
					<table class="fieldset" runat="server" id="tblAp1">
						<tr>
							<td class="fieldLabel">Employment Basis</td>
							<td>
								<telerik:RadComboBox ID="rcbPreviousEmploymentBasis" runat="server" AllowCustomText="False" DataSourceID="sqlEmploymentBasis" DataTextField="EmploymentBasis" DataValueField="EmploymentBasis" OffsetY="10" Width="160px" AppendDataBoundItems="True"></telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Position/Title</td>
							<td>
								<telerik:RadTextBox ID="rtbPreviousOccupation" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Employer</td>
							<td>

								<telerik:RadTextBox ID="rtbPreviousEmployer" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Contact Phone Number</td>
							<td>

								<telerik:RadTextBox ID="rtbPreviousEmployerPhone" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 1</td>
							<td>
								<telerik:RadTextBox ID="rtbPrevEmplAddLine1" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 2</td>
							<td>
								<telerik:RadTextBox ID="rtbPrevEmplAddLine2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State &amp; Post Code</td>
							<td>
								<telerik:RadComboBox ID="rcbPrevEmplSuburb" runat="server" DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID" DropDownWidth="370px" EmptyMessage="" EnableAutomaticLoadOnDemand="True" EnableVirtualScrolling="True" Height="200px" ItemsPerRequest="20" MarkFirstMatch="True" OffsetY="10" OnClientItemsRequesting="RadComboBoxHomeSuburb_OnClientItemsRequestingHandler" ShowMoreResultsBox="True" Width="160px">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Since</td>
							<td>
								<telerik:RadMonthYearPicker ID="rmypPrevEmplSince" runat="server" ShowPopupOnFocus="True" ZIndex="30001">
								</telerik:RadMonthYearPicker>
							</td>
						</tr>
					</table>					
				</td>
				<td class="split" runat="server" id="tdP2_2">
					<h4>
						<asp:Literal ID="litAp2Firstname2" runat="server"></asp:Literal>
					</h4>
					<asp:label runat="server" ID="lblAp2Msg" Text="Not required." />
					<table class="fieldset" runat="server" id="tblAp2">
						<tr>
							<td></td>
							<td colspan="3">
                                <asp:Button runat="server" ID="btnSameAs12" Text="same as main contact" CausesValidation="false" OnClick="btnSameAs12_Click" />
								<asp:CheckBox ID="cbSameAs12" runat="server" CausesValidation="false" Text="same as main contact" AutoPostBack="True" OnCheckedChanged="cbSameAs12_CheckedChanged" Visible="false"/></td>
						</tr>
						<tr>
							<td class="fieldLabel">Employment Basis</td>
							<td>
								<telerik:RadComboBox ID="rcbPreviousEmploymentBasis2" runat="server" AllowCustomText="False" DataSourceID="sqlEmploymentBasis" DataTextField="EmploymentBasis" DataValueField="EmploymentBasis" OffsetY="10" Width="160px">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Position/Title</td>
							<td>
								<telerik:RadTextBox ID="rtbPreviousOccupation2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Employer</td>
							<td>
								<telerik:RadTextBox ID="rtbPreviousEmployer2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Contact Phone Number</td>
							<td>
								<telerik:RadTextBox ID="rtbPreviousEmployerPhone2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 1</td>
							<td>
								<telerik:RadTextBox ID="rtbPrevEmplAddLine12" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 2</td>
							<td>
								<telerik:RadTextBox ID="rtbPrevEmplAddLine22" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State &amp; Post Code</td>
							<td>
								<telerik:RadComboBox ID="rcbPrevEmplSuburb2" runat="server" DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID" DropDownWidth="370px" EmptyMessage="" EnableAutomaticLoadOnDemand="True" EnableVirtualScrolling="True" Height="200px" ItemsPerRequest="20" MarkFirstMatch="True" OffsetY="10" OnClientItemsRequesting="RadComboBoxHomeSuburb_OnClientItemsRequestingHandler" ShowMoreResultsBox="True" Width="160px">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Since</td>
							<td>
								<telerik:RadMonthYearPicker ID="rmypPrevEmplSince2" runat="server" ShowPopupOnFocus="True" ZIndex="40001">
								</telerik:RadMonthYearPicker>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<div style="text-align: right">
			<asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click">go back</asp:LinkButton>
			<asp:Button ID="btnSubmit" Text="next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
		</div>
	    <script>
		    function RadComboBoxHomeSuburb_OnClientItemsRequestingHandler(sender, eventArgs) {
			    if (sender.get_text().length < 2) {
				    eventArgs.set_cancel(true);
			    }
		    }
	    </script>
	</telerik:RadAjaxPanel>
</asp:Content>
