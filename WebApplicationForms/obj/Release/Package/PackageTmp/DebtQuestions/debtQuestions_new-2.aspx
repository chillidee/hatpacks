﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="debtQuestions_new-2.aspx.cs" Inherits="WebApplicationForms.DebtQuestions.debtQuestions_new_2" %>
<%@ MasterType virtualpath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Please add your list of debt items</h2>

<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
  <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"></telerik:RadWindowManager>
    <telerik:RadGrid ID="rgQualifierDADebt" runat="server"
      AllowAutomaticDeletes="True"
      AllowAutomaticInserts="True"
      AllowAutomaticUpdates="True"
      EnableLinqExpressions="false"
      ClientSettings-AllowKeyboardNavigation="true"
      EnableHeaderContextAggregatesMenu="true"
      CellSpacing="0"
      DataSourceID="SqlDataSource1"
      GridLines="None"      
      OnItemCommand="rgQualifierDADebt_ItemCommand"
      OnPreRender="rgQualifierDADebt_PreRender">
      <MasterTableView
        AutoGenerateColumns="False"
        CommandItemDisplay="Bottom"
        DataKeyNames="QualifierID"
        DataSourceID="SqlDataSource1"
        EditItemStyle-BackColor="#FFFFCC"
        EditItemStyle-Width="5px"
        EditMode="InPlace"
        InsertItemDisplay="Bottom"
        ShowFooter="false"
        >
<CommandItemTemplate>
</CommandItemTemplate>
  <Columns>
  
    <telerik:GridBoundColumn DataField="QualifierID" UniqueName="QualifierID" Visible="false">
    </telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="CallID" UniqueName="CallID" Visible="false">
    </telerik:GridBoundColumn>
    
    <telerik:GridDropDownColumn DataField="NatureOfDebt" DataSourceID="sqlrefNatureOfDebt" DropDownControlType="DropDownList" HeaderText="Type of Debt" ListTextField="NatureOfDebt" ListValueField="NatureOfDebt" UniqueName="NatureOfDebt" HeaderStyle-Font-Bold="true">
		</telerik:GridDropDownColumn>

    <telerik:GridCheckBoxColumn DataField="IsSecured" DataType="System.Boolean" HeaderText="IsSecured" UniqueName="IsSecured" Display="False" DefaultInsertValue="False">
    </telerik:GridCheckBoxColumn>

		<telerik:GridTemplateColumn DataField="CreditLimit" DataType="System.Decimal" FooterAggregateFormatString="{0:C0}" HeaderText="What is the credit limit?" UniqueName="CreditLimit" ItemStyle-Wrap="false" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Right" DefaultInsertValue="0">
			<ItemTemplate>
				<asp:Literal ID="litCreditLimit" runat="server" Text='<%# String.Format("{0:C0}",Convert.ToDecimal(Eval("CreditLimit"))) %>'></asp:Literal>
			</ItemTemplate>
			<EditItemTemplate>
				<telerik:RadNumericTextBox ID="rtbCreditLimit" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("CreditLimit") %>' Type="Currency" Width="70px">
				</telerik:RadNumericTextBox>				
        <asp:RequiredFieldValidator runat="server" ControlToValidate="rtbCreditLimit" Text="Required." ErrorMessage="" />
			</EditItemTemplate>
		</telerik:GridTemplateColumn>



    <telerik:GridTemplateColumn DataField="TotalAmountOwing" DataType="System.Decimal" FooterAggregateFormatString="{0:C0}" HeaderText="Balance Owing" UniqueName="TotalAmountOwing" ItemStyle-Wrap="false" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Right" DefaultInsertValue="0">
			<ItemTemplate>
				<asp:Literal ID="litTotalAmountOwing" runat="server" Text='<%# String.Format("{0:C0}",Convert.ToDecimal(Eval("TotalAmountOwing"))) %>'></asp:Literal>
			</ItemTemplate>
			<EditItemTemplate>
				<telerik:RadNumericTextBox ID="rtbTotalAmountOwing" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("TotalAmountOwing") %>' Type="Currency" Width="70px">
				</telerik:RadNumericTextBox>	
        <asp:RequiredFieldValidator runat="server" ControlToValidate="rtbTotalAmountOwing" Text="Required." ErrorMessage="" />			
			</EditItemTemplate>
		</telerik:GridTemplateColumn>

    <telerik:GridDropDownColumn DataField="Joint" UniqueName="Joint" DataSourceID="sqlrefJoint" DropDownControlType="DropDownList" HeaderText="Whose debt is this?" ListTextField="Joint" ListValueField="Joint" HeaderStyle-Font-Bold="true" DefaultInsertValue="Mine"></telerik:GridDropDownColumn>

    <telerik:GridBoundColumn DataField="CreditorName" HeaderText="Lender/Credit Provider (e.g. ANZ, Telstra etc)" UniqueName="CreditorName" HeaderStyle-Font-Bold="true">
    </telerik:GridBoundColumn>
		<telerik:GridEditCommandColumn CancelText="cancel" EditText="edit" InsertText="add" UpdateText="save">
		</telerik:GridEditCommandColumn>
		<telerik:GridButtonColumn CommandName="Delete" HeaderTooltip="Delete" Text="delete">
		</telerik:GridButtonColumn>
  </Columns>

</MasterTableView>
</telerik:RadGrid>
    <asp:SqlDataSource ID="sqlrefNatureOfDebt" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT [NatureOfDebt] FROM [refNatureOfDebt]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlrefJoint" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT [Joint] FROM [refJoint]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CRISMARConnectionString %>"
      SelectCommand="SELECT [QualifierID], [CallID], [NatureOfDebt], [IsSecured], [CreditLimit], [TotalAmountOwing], [Joint], [CreditorName] FROM [QualifierDADebt] WHERE [CallID] = @CallID"
      InsertCommand="INSERT INTO QualifierDADebt(CallID, NatureOfDebt, IsSecured, CreditLimit, TotalAmountOwing, Joint, CreditorName) VALUES (@CallID, @NatureOfDebt, @IsSecured, @CreditLimit, @TotalAmountOwing, @Joint, @CreditorName)"
      UpdateCommand="UPDATE QualifierDADebt SET NatureOfDebt = @NatureOfDebt, IsSecured = @IsSecured, CreditLimit = @CreditLimit, TotalAmountOwing = @TotalAmountOwing, Joint = @Joint, CreditorName = @CreditorName WHERE QualifierID = @QualifierID"
      DeleteCommand="DELETE FROM [QualifierDADebt] WHERE [QualifierID] = @QualifierID"
      OnSelecting="SqlDataSource1_Selecting"
      OnInserting="SqlDataSource1_Inserting">
      <InsertParameters>
        <asp:Parameter Name="CallID" Type="Int32"></asp:Parameter>
        <asp:Parameter Name="NatureOfDebt" Type="String"></asp:Parameter>
        <asp:Parameter Name="IsSecured" Type="Boolean"></asp:Parameter>
        <asp:Parameter Name="CreditLimit" Type="Decimal"></asp:Parameter>
        <asp:Parameter Name="TotalAmountOwing" Type="Decimal"></asp:Parameter>
        <asp:Parameter Name="Joint" Type="String"></asp:Parameter>
        <asp:Parameter Name="CreditorName" Type="String"></asp:Parameter>
      </InsertParameters>
      <SelectParameters>
        <asp:Parameter Name="CallID" Type="Int32" />
      </SelectParameters>
      <UpdateParameters>        
        <asp:Parameter Name="QualifierID" Type="Int32"></asp:Parameter>
        <asp:Parameter Name="CallID" Type="Int32"></asp:Parameter>
        <asp:Parameter Name="NatureOfDebt" Type="String"></asp:Parameter>
        <asp:Parameter Name="IsSecured" Type="Boolean"></asp:Parameter>
        <asp:Parameter Name="CreditLimit" Type="Decimal"></asp:Parameter>
        <asp:Parameter Name="TotalAmountOwing" Type="Decimal"></asp:Parameter>
        <asp:Parameter Name="Joint" Type="String"></asp:Parameter>
        <asp:Parameter Name="CreditorName" Type="String"></asp:Parameter>
      </UpdateParameters>
      <DeleteParameters>
        <asp:Parameter Name="QualifierID" Type="Int32" />
      </DeleteParameters>
    </asp:SqlDataSource>

    <table style="width:100%; background-color:#e1e1e1">
      <tr>
        <td>
          <img src="/Images/arror.png" style="float:left" />
          <div style="padding-top:50px;" class="NoteHelp">
            Start here. Fill in the appropriate information, <br />then click on "add" to add your item the list.
          </div>
        </td>        
        <td style="text-align:right">
  	      <div style="text-align: right">
            <a href="javascript:window.history.back()">back</a>
			      <asp:Button ID="btnSubmit" Text="next" CssClass="main" runat="server" onclick="btnSubmit_Click"/><br />
            <asp:Label runat="server" ID="lblMsg" ForeColor="Red" Visible="false" />
		      </div>
        </td>
      </tr>
    </table>
</telerik:RadAjaxPanel>
 

</asp:Content>
