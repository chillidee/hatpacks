﻿<%@ Page Title="Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationForm-08.aspx.cs" Inherits="WebApplicationForms.WebForm_A08
	" %>

<%@ Register Src="~/UserControls/SelfEmployedIncomeGrid.ascx" TagPrefix="uc1" TagName="SelfEmployedIncomeGrid" %>
<%@ Register Src="~/UserControls/MonthlyIncomeGrid.ascx" TagPrefix="uc1" TagName="MonthlyIncomeGrid" %>
<%@ Register Src="~/UserControls/ApplicantAssetLiabilityGrid.ascx" TagPrefix="uc1" TagName="ApplicantAssetLiabilityGrid" %>
<%@ Register Src="~/UserControls/Address_Simple.ascx" TagPrefix="uc1" TagName="Address_Simple" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	<style type="text/css">
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

	<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Your Application</h1>
				<h2>Professional Contact Details</h2>
			</hgroup>
			<p>Details about your Accountant and Solicitor.</p>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	
		<h3>Accountant&#39;s Contact Details (optional)</h3>		

			<table class="splitTable">
			<tr>
				<td class="split">
					<table class="fieldset">
						<tr>
							<td class="fieldLabel">Firm</td>
							<td><telerik:RadTextBox ID="rtbAccountantsFirm" Runat="server"/></td>							
						</tr>
						<tr>
							<td class="fieldLabel">Name</td>
							<td><telerik:RadTextBox ID="rtbAccountantsName" Runat="server"/></td>
						</tr>
						<tr>
							<td class="fieldLabel">Phone</td>
							<td><telerik:RadTextBox ID="rtbAccountantsPhone" Runat="server"/></td>
						</tr>
						<tr>
							<td class="fieldLabel">Mobile</td>
							<td><telerik:RadTextBox ID="rtbAccountantsMobile" Runat="server"/></td>
						</tr>
						<tr>
							<td class="fieldLabel">Fax</td>
							<td><telerik:RadTextBox ID="rtbAccountantsFax" Runat="server"/></td>
						</tr>
						<tr>
							<td class="fieldLabel">Email</td>
							<td><telerik:RadTextBox ID="rtbAccountantsEmail" Runat="server"/>
								<asp:RegularExpressionValidator ID="revAccountantsEmail" runat="server" ErrorMessage="Invalid email." ControlToValidate="rtbAccountantsEmail" EnableClientScript="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
							</td>
						</tr>
					</table>
			</td>		
			<td class="split">
				<uc1:Address_Simple runat="server" ID="AccountantsAddress" FieldCssClass="fieldLabel" />
			</td>
		</tr>
		</table>

				<h3>Solicitor&#39;s Contact Details (optional)</h3>		

			<table class="splitTable">
			<tr>
				<td class="split">
					<table class="fieldset">
						<tr>
							<td class="fieldLabel">Firm</td>
							<td><telerik:RadTextBox ID="rtbSolicitorsFirm" Runat="server"/></td>							
						</tr>
						<tr>
							<td class="fieldLabel">Name</td>
							<td><telerik:RadTextBox ID="rtbSolicitorsName" Runat="server"/></td>
						</tr>
						<tr>
							<td class="fieldLabel">Phone</td>
							<td><telerik:RadTextBox ID="rtbSolicitorsPhone" Runat="server"/></td>
						</tr>
						<tr>
							<td class="fieldLabel">Mobile</td>
							<td><telerik:RadTextBox ID="rtbSolicitorsMobile" Runat="server"/></td>
						</tr>
						<tr>
							<td class="fieldLabel">Fax</td>
							<td><telerik:RadTextBox ID="rtbSolicitorsFax" Runat="server"/></td>
						</tr>
						<tr>
							<td class="fieldLabel">Email</td>
							<td><telerik:RadTextBox ID="rtbSolicitorsEmail" Runat="server"/>
								<asp:RegularExpressionValidator ID="revSolicitorsEmail" runat="server" ErrorMessage="Invalid email." ControlToValidate="rtbSolicitorsEmail" EnableClientScript="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">DX</td>
							<td><telerik:RadTextBox ID="rtbDX" Runat="server"/>								
							</td>
						</tr>
					</table>
			</td>		
			<td class="split">
				<uc1:Address_Simple runat="server" ID="SolicitorsAddress" FieldCssClass="fieldLabel" />
			</td>
		</tr>
		</table>

		<div style="text-align: right">
			<asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click">go back</asp:LinkButton>
			<asp:Button ID="btnSubmit" Text="finish!" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
		</div>

</asp:Content>
