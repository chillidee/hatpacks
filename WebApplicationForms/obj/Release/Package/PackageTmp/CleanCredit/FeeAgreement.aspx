﻿    <%@ Page Title="Clean Credit Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FeeAgreement.aspx.cs" Inherits="WebApplicationForms.CleanCredit.FeeAgreement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .hiddencol
         {
             display: none;
         }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
  <section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Removal Fee Agreement</h1>				
      </hgroup>
		</div>
  </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">    
    <p><strong>Removal Fees incl GST</strong></p>
<%--    <table style="margin-bottom: 20px;">
      <tr>
        <td><strong>Per removal  (Default. Judgement or Clearout)</strong></td><td><strong>$880.00 inc GST</strong></td>
      </tr>
    </table>--%>
      <asp:GridView ID="grdRemovalFee" runat="server" style="margin-bottom: 20px;" AutoGenerateColumns="False" ShowHeader="False">
          <Columns>
              <asp:BoundField DataField="CreditProvider">
              <ItemStyle Font-Bold="True" />
              </asp:BoundField>
              <asp:BoundField DataField="RemovalFee" DataFormatString="{0:c}" >
              <ItemStyle Font-Bold="True" />
              </asp:BoundField>
          </Columns>
      </asp:GridView>
    <!--<p><u><b>Removal fee</b></u>: This fee is only payable upon conformation of the successful removal or improvement of your credit item/s.</p>-->
    <div style="background-color: #14497b;padding:30px">
      <asp:RadioButtonList runat="server" ID="rblRemovalFeeOption">
        <asp:ListItem Value="1" Text="<h4 style='color: #fff;'>Option 1 - (Lump sump payment)</h4><p>I agree to pay the total of my removal fees inc GST at the time Clean Credit Pty Ltd commences work on my file.</p>"/>
        <asp:ListItem Value="2" Text="<h4 style='color: #fff;'>Option 2 - (Pay in instalments $100 per week)</h4><p>I agree to pay $100.00 towards my removal fees. These payments are to begin following the completed payment of my application fee. I acknowledge that any balances will be payable upon confirmation of removal or improvement of a negative credit listing from my credit file.</p>" />     
        <asp:ListItem Value="4" Text="<h4 style='color: #fff;'>Option 3 - (Pay in instalments $200 per fortnight)</h4><p>I agree to pay $200.00 per fortnight towards my removal fees. These payments are to begin following the completed payment of my application fee. I acknowledge that any balances will be payable upon confirmation of removal or improvement of a negative credit listing from my credit file.</p>" />                
      </asp:RadioButtonList>
      <!--<h4>Option 2 - (Pay in instalments)</h4><p>I agree to pay YYY including GST per removal, or improvement of any Default, Clearout, or court listing on my credit file if paid by direct debit at a minimum payment of $100 per week.</p>-->
      <!--<asp:ListItem Value="3" Text="<h4>Option 3 - (Decide later)</h4><p>I don't know, I will decide later.</p>" />-->
      <asp:RequiredFieldValidator runat="server" ID="rfv" ControlToValidate="rblRemovalFeeOption" Display="Dynamic" Text="A selection is required." />
    </div>

<p>All payments must be met on the due dates.  Please note that there is an administration fee of $25.00 for each time a direct debit payment is dishonoured.</p>

<p>I <asp:Label runat="server" id="aplicate_name"></asp:Label> authorise Clean Credit Pty Ltd, its employees and agents to enter negotiations on my behalf with regard to my credit 
information with a view to facilitating the removal of an inaccurate, false, misleading, unfair or 
contestable credit listing.</p>
  
<p>I confirm that the information that I have provided to Clean Credit Pty Ltd is true and accurate and except the terms of the refund policy.</p>


    <div>
      <table style="width:100%"><tr><td class="squaredFour" style="width:50px;height:50px"><input runat="server" class="checkbox" type="checkbox" ID="cb1"/><label for="MainContent_cb1" style="top:10px"></label></td>
        <td id="tdaccept-2"><asp:Label runat="server" ID="lblAgreementMessage" Text="<span style='color:#333'>I agree to the above conditions.</span>"/>
          <br /><asp:CustomValidator runat="server" ID="cvAgree" OnServerValidate="cvAgree_ServerValidate" ErrorMessage="Your acceptance is required." Display="Dynamic"></asp:CustomValidator>          
      </td></tr>
        <tr><td></td><td><asp:Label runat="server" ID="lblStamp" Visible="false" /></td></tr>
      </table>
     </div>
  </telerik:RadAjaxPanel>
			<div class="goback-btn">			
            <asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click" CausesValidation="false">go back</asp:LinkButton>
                </div>
		<div class="nextbtn-block">
            <asp:Button ID="btnSubmit" Text="Next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
            <p style="margin: 0;padding-top: 5px;border-top: 1px solid #cacaca;"><img src="../Images/noeffects.png" style="width: 13px;vertical-align: middle;margin-right: 5px;margin-top: -3px;">  <strong>Will NOT affect your credit file</strong></p>
		  </div>
</asp:Content>
