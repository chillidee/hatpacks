﻿<%@ Page Title="Clean Credit Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DirectDebitAuthority.aspx.cs" Inherits="WebApplicationForms.CleanCredit.DirectDebitAuthority" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
  <section class="featured">
    <div class="content-wrapper">
      <hgroup class="title">
        <h1>Direct Debit Payment</h1>
      </hgroup>
    </div>
  </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">    
    <table class="splitTable">
     <tr>
        <td class="split" style="padding: 10px 20px 20px 20px;width:75%">
          <div class="form-block">
            
            <div class="label-block">Financial Institution</div>
            <div class="input-block">
                <telerik:RadTextBox runat="server" ID="rtbBank" Width="100%"/>
                <asp:RequiredFieldValidator validationgroup="directdebitauthorityrequired" ID="TextBoxRequiredFieldValidator1" Runat="server" Display="Dynamic" ControlToValidate="rtbBank" ErrorMessage="Please Complete Your Financial Institution Details" > </asp:RequiredFieldValidator>
            </div>

              <div class="label-block">Branch</div>
              <div class="input-block">
                <telerik:RadTextBox runat="server" ID="rtbBankBranch" Width="100%" />
                <asp:RequiredFieldValidator validationgroup="directdebitauthorityrequired" ID="RequiredFieldValidator2" Runat="server" Display="Dynamic" ControlToValidate="rtbBankBranch" ErrorMessage="Please Complete Your Branch Details" > </asp:RequiredFieldValidator>
              </div>

              <div class="label-block">Account Name</div>
              <div class="input-block">
                <telerik:RadTextBox runat="server" ID="rtbAccountName" Width="100%" />
                <asp:RequiredFieldValidator validationgroup="directdebitauthorityrequired" ID="RequiredFieldValidator3" Runat="server" Display="Dynamic" ControlToValidate="rtbAccountName" ErrorMessage="Please Complete Your Account Name Details" > </asp:RequiredFieldValidator>
              </div>
            
              <div class="label-block">BSB</div>
              <div class="input-block">
                <telerik:RadMaskedTextBox runat="server" ID="rmtbAccountBSB" Mask="######" Width="100%" />               
                <asp:RequiredFieldValidator validationgroup="directdebitauthorityrequired" ID="RequiredFieldValidator4" Runat="server" Display="Dynamic" ControlToValidate="rmtbAccountBSB" ErrorMessage="Please Complete Your BSB Details" > </asp:RequiredFieldValidator>
              </div>

              <div class="label-block">Account Number</div>
              <div class="input-block">
                <telerik:RadTextBox runat="server" ID="rtbAccountNo" Width="100%" />
                <asp:RequiredFieldValidator validationgroup="directdebitauthorityrequired" ID="RequiredFieldValidator5" Runat="server" Display="Dynamic" ControlToValidate="rtbAccountNo" ErrorMessage="Please Complete Your Account Number Details" > </asp:RequiredFieldValidator>
              </div>
              </div>
            </td>
         
         <td class="block-image-2">
         </td>  
            
         </tr>
       </table>

    <h3>Direct Debit Request Service Agreement Terms and Conditions</h3>

    <p>I/We hereby authorise and request the debt user detailed below to debit payments from my/our nominated account, specified below, at intervals and amounts as directed by Clean Credit Pty Ltd as per the Terms and Conditions of the Clean Credit Pty Ltd Agreement and any subsequent agreements through the Bulk Electronic Clearing System.</p>
    <p>I/We authorise PayGate, as the Debit User, User ID number 314753, to make withdrawals from my/our nominated account as determined by the centre named above. PayGate, as the Debit User, acts as the billing agent for the centre named above and the service is administrative only and does not extend to the provision of any service or benefits provided by the centre named above. An administration fee of 99¢ is applied to each transaction.</p>
    <ol>
      <li>FFA PayGate (Debit User) will debit the Account nominated in the Schedule of the DDR as specified. Clean Credit Pty Ltd may, by prior arrangement, vary the amount and frequency of future debits.</li>
      <li>Should the original terms & conditions of this arrangement need to be varied a minimum of seven days notice will be provided. Queries arising as a result of any such variation must be notified to Clean Credit Pty Ltd two working days prior to the first debit date the variation would apply to.</li>
      <li>Deferment or alteration to the schedule is subject to the terms & conditions of any agreement between you and Clean Credit Pty Ltd whom FFA PayGate (Debit User) acts on behalf of.</li>
      <li>Debits will be identified on your bank statement by the use of the name FFA PayGate usually followed by a reference number.</li>
      <li>All payments must be met on the due dates. Please note that there is an administration fee of $25.00 for each time a direct debit payment is dishonoured.</li>
      <li>Clean Credit Pty Ltd may, in its absolute discretion, at any time by notice in writing to PayGate (Debit User) terminate this request as to future debits.</li>
      <li>If a debit item is disputed, Clean Credit Pty Ltd must be notified immediately. PayGate (Debit User) will endeavour to resolve this matter within industry agreed timeframes.</li>
      <li>Direct debiting is not available on the full range of accounts and as such you must check with your financial institution.</li>
      <li>It is your responsibility to have sufficient funds in your nominated account to permit a successful debit to be made. Direct debits usually occur overnight, however transactions can take up to three days, depending on your financial institution.</li>
      <li>If a debit is returned unpaid by the financial institution you will be responsible for payment of the debit plus any return fees and administrative costs incurred.</li>
      <li>To stop or cancel a direct debit, the terms & conditions of any agreement between you and Clean Credit Pty Ltd for whom PayGate (Debit User) acts on behalf of must be complied with.</li>
      <li>FFA PayGate is a fully owned division of PaySmart, a licensed commercial corporation regulated by the Queensland Dept. of Equity & Fair Trading and collected funds are held in trust until disbursement. In event of fraud where PayGate is not at fault, PayGate will be free of any legal liability.</li>
      <li>No account records or account details will be disclosed to any persons except where such information is required in connection with any claim relating to an alleged incorrect or wrongful debit.</li>
      <li>For your payment frequency/frequencies, please refer to the Clean Credit Fee agreement.</li>
    </ol>
    <p>I/We have been provided with and have read the Service Agreement and acknowledge and agree to same. I/We request this arrangement remain in force with the Schedule described above and in compliance with the Service Agreement. I/We authorise the Financial Institution to release information allowing the Debit User to verify my/our account details.</p>

    <div>
      <table>
        <tr>
          <td class="squaredFour" style="width: 50px; height: 50px">
            <input runat="server" type="checkbox" class="checkbox" id="cb1" visible="true" /><label style="top: 10px;" for="MainContent_cb1"></label></td>
          <td id="tdaccept-2">
            <asp:Label runat="server" ID="lblAgreementMessage" Text="" style="color:#333"/>
            <br />
            <asp:CustomValidator runat="server" ID="cvAgree" OnServerValidate="cvAgree_ServerValidate" ErrorMessage="Your acceptance is required." Display="Dynamic"></asp:CustomValidator>
          </td>
        </tr>
      </table>
      <div id="adminView" runat="server" visible="false">
        <asp:Label ID="agreed" runat="server"></asp:Label>
      </div>
    </div>
  </telerik:RadAjaxPanel>

  <div class="nextbtn-block">
    <asp:Button ID="btnSubmit" validationgroup="directdebitauthorityrequired" Text="Next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
    <p style="margin: 0;padding-top: 5px;border-top: 1px solid #cacaca;"><img src="../Images/noeffects.png" style="width: 13px;vertical-align: middle;margin-right: 5px;margin-top: -3px;">  <strong>Will NOT affect your credit file</strong></p>
  </div>

  <div class="goback-btn">
    <asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click">Go back</asp:LinkButton>
  </div>

</asp:Content>
