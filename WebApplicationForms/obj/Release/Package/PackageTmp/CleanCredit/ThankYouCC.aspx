﻿<%@ Page Title="Thank you" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ThankYouCC.aspx.cs" Inherits="WebApplicationForms.ThankYouCC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
		<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1><asp:Literal ID="litFirstname" runat="server" /></h1>
				<p>Thank you for submittting your form, we will be back to you shortly<span style="display:none"><asp:Label runat="server" ID="lblCompanyName" /></span>.</p>
				<p>Regards, <asp:Label runat="server" ID="lblSalesManager" />, <asp:label runat="server" ID="lblPhone" /></p>
			</hgroup>
			<p></p>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	
	
</asp:Content>
