﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="IDRequired.aspx.cs" Inherits="WebApplicationForms.Errors.IDRequired" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<h1>ID required.</h1>
	<p>You need to commence your loan application with the link that was provided to you via email.</p>
</asp:Content>
