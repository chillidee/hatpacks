﻿<%@ Page Title="Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationForm-03.aspx.cs" Inherits="WebApplicationForms.WebForm_A03" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

	<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Your Application</h1>
				<h2>Personal Details</h2>
			</hgroup>
			<p>
				Your previous residential addresses.</p>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
		<asp:SqlDataSource ID="sqlStreetType" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT '' StreetType, '' StreetTypeDisplay UNION SELECT * FROM [StreetType]"></asp:SqlDataSource>
		<asp:SqlDataSource ID="sqlPCode" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT [PCodeID], [Combo] FROM [vwPCode] ORDER BY [SUBURB], [PCODE] DESC"></asp:SqlDataSource>		
		<asp:SqlDataSource ID="sqlResidentialStatus" runat="server" ConnectionString="<%$ ConnectionStrings:CRISMARConnectionString %>" SelectCommand="SELECT '' [Value], '' [Status] UNION SELECT [Value], [Status] FROM [ResidentialStatus] WHERE DisplayOnWeb = 1"></asp:SqlDataSource>

		<h3>Where did you live? (previously...)</h3>

		<table class="splitTable">
			<tr>
				<td class="split">
					<h4><asp:Literal ID="litAp1Firstname1" runat="server"></asp:Literal></h4>
					<asp:Label runat="server" ID="lblAp1Msg" Text="Not required." />
					<table class="fieldset" runat="server" id="tblAp1">
						<tr>
							<td class="fieldLabel">Unit No</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxPrevUnitNumber" runat="server" Width="60px"></telerik:RadTextBox>
							</td>
							<td class="fieldLabel">Street No</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxPrevStreetNumber" runat="server" Width="60px"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Street Name</td>
							<td colspan="3">
								<telerik:RadTextBox ID="RadTextBoxPrevStreetName" runat="server" Width="200px"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Street Type</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxPrevStreetType" runat="server" DataSourceID="sqlStreetType" Width="200px" DataTextField="StreetType" DataValueField="StreetType" OffsetY="10" AllowCustomText="False" EnableAutomaticLoadOnDemand="True"></telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State & Post Code</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxPrevSuburb" runat="server"
									DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID"
									ShowMoreResultsBox="True" EnableVirtualScrolling="True"
									EnableAutomaticLoadOnDemand="True" ItemsPerRequest="20" Height="200px" Width="200px"
									DropDownWidth="370px" MarkFirstMatch="True" OffsetY="10" EmptyMessage=""
									OnClientItemsRequesting="RadComboBoxHomeSuburb_OnClientItemsRequestingHandler">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">There Since</td>
							<td colspan="3">
								<telerik:RadMonthYearPicker ID="RadMonthYearPickerPrevSince" runat="server" ZIndex="30001" ShowPopupOnFocus="True"></telerik:RadMonthYearPicker>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Residential Status</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxPrevResidentialStatus" runat="server" AllowCustomText="False" DataSourceID="sqlResidentialStatus" DataTextField="Status" DataValueField="Value" OffsetY="10" Width="200px" AppendDataBoundItems="true">
								</telerik:RadComboBox>
							</td>
						</tr>
					</table>
				</td>
				<td class="split" runat="server" id="tdP2_1">
					<h4>
						<asp:Literal ID="litAp2Firstname1" runat="server"></asp:Literal>
					</h4>
					<asp:Label runat="server" ID="lblAp2Msg" Text="Not required." />
					<table class="fieldset" runat="server" id="tblAp2">					
						<tr runat="server" id="trSameAsMainContact">
							<td></td>
							<td colspan="3">
                                <asp:Button runat="server" ID="btnPrevAddressSameAs1" Text="same as main contact" CausesValidation="false" OnClick="btnPrevAddressSameAs1_Click" />
								<asp:CheckBox ID="cbPrevAddressSameAs1" runat="server" CausesValidation="false" Text="same as main contact" AutoPostBack="True" OnCheckedChanged="cbPrevAddressSameAs1_CheckedChanged" Visible="false"/></td>
						</tr>
						<tr>
							<td class="fieldLabel">Unit No</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxPrevUnitNumber2" runat="server" Width="60px"></telerik:RadTextBox>
							</td>
							<td class="fieldLabel">Street No</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxPrevStreetNumber2" runat="server" Width="60px"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Street Name</td>
							<td colspan="3">
								<telerik:RadTextBox ID="RadTextBoxPrevStreetName2" runat="server" Width="200px"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Street Type</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxPrevStreetType2" runat="server" DataSourceID="sqlStreetType" Width="200px" DataTextField="StreetType" DataValueField="StreetType" OffsetY="10" AllowCustomText="False" EnableAutomaticLoadOnDemand="True"></telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State & Post Code</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxPrevSuburb2" runat="server"
									DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID"
									ShowMoreResultsBox="True" EnableVirtualScrolling="True"
									EnableAutomaticLoadOnDemand="True" ItemsPerRequest="20" Height="200px" Width="200px"
									DropDownWidth="370px" MarkFirstMatch="True" OffsetY="10" EmptyMessage=""
									OnClientItemsRequesting="RadComboBoxHomeSuburb2_OnClientItemsRequestingHandler"
									AllowCustomText="False">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">There Since</td>
							<td colspan="3">
								<telerik:RadMonthYearPicker ID="RadMonthYearPickerPrevSince2" runat="server" ShowPopupOnFocus="True" ZIndex="30001">
								</telerik:RadMonthYearPicker>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Residential Status</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxPrevResidentialStatus2" runat="server" AllowCustomText="False" DataSourceID="sqlResidentialStatus" DataTextField="Status" DataValueField="Value" OffsetY="10" Width="200px" AppendDataBoundItems="true">
								</telerik:RadComboBox>
							</td>
						</tr>
					</table>


				</td>
			</tr>
		</table>

		<div style="text-align: right">
			<asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click">go back</asp:LinkButton>
            &nbsp;<asp:Button ID="btnSubmit" Text="next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
		</div>
		<script>
			function RadComboBoxHomeSuburb_OnClientItemsRequestingHandler(sender, eventArgs) {
				if (sender.get_text().length < 2) {
					eventArgs.set_cancel(true);
				}
			}
			function RadComboBoxHomeSuburb2_OnClientItemsRequestingHandler(sender, eventArgs) {
			    if (sender.get_text().length < 2) {
			        eventArgs.set_cancel(true);
			    }
			}
		</script>
	</telerik:RadAjaxPanel>
</asp:Content>
