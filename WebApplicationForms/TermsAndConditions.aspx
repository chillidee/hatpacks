﻿<%@ Page Title="Terms and Conditions" Language="C#" MasterPageFile="RadWindow.Master" AutoEventWireup="true" CodeBehind="TermsAndConditions.aspx.cs" Inherits="WebApplicationForms.TermsAndConditions"%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<h1>Privacy Statement and Declaration</h1>
<p>Please take a moment to read the Privacy Statement and Declaration below carefully before you (being the applicant) close this window, return to this application and agree to this statement by ticking the declaration box. If there are two applicants, both must agree to this application, before it can be submitted to [CompanyName] "we", "us" or "our") for processing.</p>

<h2>Privacy Statement</h2>

<h3>What this statement is about</h3>

<p>This statement explains your privacy right as regulated by the Privacy Act and our rights and obligations in relation to your personal information. You need not give us any of the personal information requested in the application form, however, without this information, we may not be able to process the application or provide you an appropriate level of service.</p>

<p>You may request access at any time to personal information held by us about you and ask us to correct it if you believe it is incorrect or out of date by contacting us on [PhoneNumber], Monday to Friday between 8.30 am & 5.30 pm. Please contact us on this number for any question or concern you may have about how we handle your personal information.</p>

<h2>How we may use your personal information</h2>

<h3>We use your personal information to:</h3>
<ul>
	<li>process this application (including performing identity checks and determining if the National Consumer Credit Protection Act applies);</li>
	<li>facilitate our internal business operations, including systems maintenance and testing.</li>
</ul>

<h2>Our right to disclose your personal information</h2>

<h3>We may disclose your personal information in the following circumstances:</h3>
<ul>
	<li>to any referee nominated by you;</li>
	<li>to our external service providers that provide services for the purposes only of our business, on a confidential basis, for example debt collection agents, and data processors;</li>
	<li>to other persons who have an interest in any property offered to us as security;</li>
	<li>to any party acquiring an interest in any business or in the overdraft and any related securities provided by you or any other person (including mortgages);</li>
	<li>to any persons acting on your behalf, including your financial advisor, broker, solicitor or accountant, unless you tell us not to;</li>
	<li>to government agencies in connection with your loan, for example to stamp and register mortgages; and</li>
	<li>if you request us to do so or if you consent (for example for a direct debit) or where our anti-money laundering policies or the law requires or permits us to do so.</li>
</ul>

<h2>Use by the Australian Lending Centre</h2>

<p>We may also use your personal information or give access to personal information about you to any member of the [CompanyName]  including to:
inform you of products and services provided by us, any member of the [CompanyName] or by preferred providers, which we consider may be of value or interest to you, unless you tell us not to.
In this policy, [CompanyName] and its related bodies corporate. If you have any questions, concerns or complaints about our privacy policy or practices please contact us on [PhoneNumber] Monday to Friday between 8.30 am & 5.30 pm<p>

<h3>You authorise us to:</h3>

<p>Obtain information about your credit worthiness or a credit report containing personal or commercial credit information about you from a credit reporting agency or other business that provides it; and
exchange the information listed above about you with any credit reporting agency;</p>

<h3>These exchanges can be made:</h3>
<ul>
	<li>to assess this application for consumer or commercial credit, or to assess your credit worthiness;</li>
	<li>if you are in default under a credit agreement, notifying, and exchanging information with, other credit providers and any collection agent of ours; and</li>
	<li>for the various purposes permitted under the Privacy Act.</li>
</ul>

<p>You acknowledge that information, including but not limited to a credit report or any other information having a bearing on your credit worthiness, credit standing, credit history or credit capacity may be exchanged or disclosed to other persons as listed below, and that we may:</p>

<ul>
	<li>exchange that information described in the paragraph above about you to with all credit providers named in this application or that may be named in credit reports issued by a credit reporting agency, any introducer referred to in the loan application, give to and receive from a credit provider, a banker's opinion of the purposes connected with your business, trade or profession; and confirm:</li>
</ul>

<p>We may need to confirm your employment and income details with any employer, accountant or tax agent named in this application; or</p>

<ul>
	<li>your income received on an investment property with any nominated real estate agent; or</li>
	<li>your payment history from the landlord or managing agent nominated in this application.</li>
</ul>

<h3>Email address and Phone number</h3>

<p>You agree for us to use my nominated email address or any phone number provided to inform you of the progress of your application and, if approved, provide you with information about administrative matters (including security issues) relating to the accounts you have applied for. You confirm and represent to us that the email address and phone numbers that you provide in this application and at any other subsequent time, is a secure and appropriate way for us to communicate with you.</p>

<div style="text-align:right;margin-right:20px"><input type="button" value="close" onclick="window.close()" /></div>

</asp:Content>
