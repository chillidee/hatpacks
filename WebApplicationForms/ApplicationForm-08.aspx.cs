﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;


namespace WebApplicationForms
{
	public partial class WebForm_A08 : System.Web.UI.Page
	{
		public int ApplicantID { get; set; }
		protected void Page_Load(object sender, EventArgs e)
		{

			if (!this.IsPostBack)
			{
				//getting data...
				MMApplication a;
				if ((Session["Application"] == null) && HttpContext.Current.Request.IsLocal)
				{
					a = new MMApplication("CFE209842ECB48F4AD1E47BB40529E1F");
					a.LoadApplicants();
					Session["Application"] = a;
				}
				else
				{ a = (MMApplication)Session["Application"]; }
				MMApplicant ap1 = a.MMApplicant1;

				//populating...
				rtbAccountantsFirm.Text = a.AccountantsFirm;
				rtbAccountantsName.Text = a.AccountantsName;
				rtbAccountantsPhone.Text = a.AccountantsPhone;
				rtbAccountantsMobile.Text = a.AccountantsMobile;
				rtbAccountantsFax.Text = a.AccountantsFax;
				rtbAccountantsEmail.Text = a.AccountantsEmail;
				AccountantsAddress.AddressLine1 = a.AccountantsAddLine1;
				AccountantsAddress.AddressLine2 = a.AccountantsAddLine2;
				AccountantsAddress.SuburbID = a.AccountantsSuburb;

				rtbSolicitorsFirm.Text = a.SolicitorsFirm;
				rtbSolicitorsName.Text = a.SolicitorsName;
				rtbSolicitorsPhone.Text = a.SolicitorsPhone;
				rtbSolicitorsMobile.Text = a.SolicitorsMobile;
				rtbSolicitorsFax.Text = a.SolicitorsFax;
				rtbSolicitorsEmail.Text = a.Email;
				rtbDX.Text = a.DX;
				SolicitorsAddress.AddressLine1 = a.SolicitorsAddLine1;
				SolicitorsAddress.AddressLine2 = a.SolicitorsAddLine2;
				SolicitorsAddress.SuburbID = a.SolicitorsAddSuburb;
				}
			}
		


		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				Save(true);
        if (!(bool)Session["internal"]) SendEmails();
				Response.Redirect("ThankYou.aspx");
			}
		}
		protected void Save(Boolean IsFinish)
		{
			MMApplication a = new MMApplication();
			if (Session["Application"] != null)
			{
				a = (MMApplication)Session["Application"];
			}
			else
			{
				throw new Exception("No Application Session, or timed out");
			}

			if (a.MMApplicant1 != null)
			{

			}
			else
			{
				//most likely, the session variable has expired. Lets re-load from the Database...
				throw new Exception("Unable to load Applicant object");
			}
			//Collecting ap1 data from the form...

			a.AccountantsFirm = rtbAccountantsFirm.Text;
			a.AccountantsName = rtbAccountantsName.Text;
			a.AccountantsPhone = rtbAccountantsPhone.Text;
			a.AccountantsMobile = rtbAccountantsMobile.Text;
			a.AccountantsFax = rtbAccountantsFax.Text;
			a.AccountantsEmail = rtbAccountantsEmail.Text;
			a.AccountantsAddLine1 = AccountantsAddress.AddressLine1.ToString();
			a.AccountantsAddLine2 = AccountantsAddress.AddressLine2.ToString();
			a.AccountantsSuburb = Convert.ToString(AccountantsAddress.SuburbID);

			a.SolicitorsFirm = rtbSolicitorsFirm.Text;
			a.SolicitorsName = rtbSolicitorsName.Text;
			a.SolicitorsPhone = rtbSolicitorsPhone.Text;
			a.SolicitorsMobile = rtbSolicitorsMobile.Text;
			a.SolicitorsFax = rtbSolicitorsFax.Text;
			a.Email = rtbSolicitorsEmail.Text;
			a.DX = rtbDX.Text;
			a.SolicitorsAddLine1 = SolicitorsAddress.AddressLine1.ToString();
			a.SolicitorsAddLine2 = SolicitorsAddress.AddressLine2.ToString();
			a.SolicitorsAddSuburb = Convert.ToInt32(SolicitorsAddress.SuburbID);

      if (IsFinish)
      {        
        
        a.Web_ApplicationFormFinished = DateTime.Now;
        a.StatusID = 104; //Web Application Complete
        a.StatusStartDate = DateTime.Now;
        a.StatusDetails = null;
        a.StatusEnteredBy = "Web Application";
        a.StatusDate = a.StatusStartDate; //now
        a.StatusExpiryDate = a.StatusStartDate; //now

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
        {
          SqlCommand cmd = new SqlCommand();
          cmd.CommandType = System.Data.CommandType.StoredProcedure;
          cmd.CommandText = "spInsertStatusHistory";
          cmd.Connection = conn;
          SqlParameter p = new SqlParameter();
          p.ParameterName = "@ApplicationNo";
          p.SqlDbType = System.Data.SqlDbType.Int;
          p.Value = a.ApplicationNo;
          cmd.Parameters.Add(p);
          conn.Open();
          cmd.ExecuteNonQuery();
        }
      }
			a.Save();
			Session["Application"] = a;
		}
		protected void btnBack_Click(object sender, EventArgs e)
		{
			Save(false);
			Response.Redirect("ApplicationForm-07.aspx");
		}

		protected void SendEmails()
		{			
			EmailAgent ea = new EmailAgent();
			ea.SendThankYouEmail((MMApplication)Session["Application"]);		
		}
	}
}