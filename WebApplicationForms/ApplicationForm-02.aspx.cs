﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;


namespace WebApplicationForms
{
	public partial class WebForm_A02 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				//initialising controls...
				MMApplication a;
				if ((Session["Application"] == null) && HttpContext.Current.Request.IsLocal)
				{
					a = new MMApplication("CFE209842ECB48F4AD1E47BB40529E1F");
					a.LoadApplicants();
					Session["Application"] = a;
				}
				else
				{ a = (MMApplication)Session["Application"]; }
				//getting data...				
				MMApplicant ap1 = a.MMApplicant1;

				//Populating controls...				
				litAp1Firstname1.Text = ap1.FirstName;
				litAp1Firstname2.Text = ap1.FirstName;
				RadTextBoxHomeUnitNumber.Text = ap1.HomeUnitNumber;
				RadTextBoxHomeStreetNumber.Text = ap1.HomeStreetNumber;
				RadTextBoxHomeStreetName.Text = ap1.HomeStreetName;
				RadComboBoxHomeStreetType.SelectedValue = ap1.HomeStreetType;
				RadComboBoxHomeStreetType.Text = ap1.HomeStreetType;
				RadComboBoxHomeSuburb.SelectedValue = ap1.HomeSuburb.ToString();
				RadComboBoxHomeSuburb.Text = ap1.HomeSuburbText;
				RadMonthYearPickerHereSince.SelectedDate = ap1.HereSince;
				RadComboBoxResidentialStatus.SelectedValue = ap1.ResidentialStatus;

				RadTextBoxPostalAddressLine1.Text = ap1.PostalAddressLine1;
				RadTextBoxPostalAddressLine2.Text = ap1.PostalAddressLine2;
				RadComboBoxPostalSuburb.SelectedValue = ap1.PostalHomeSuburb.ToString();
				RadComboBoxPostalSuburb.Text = ap1.PostalHomeSuburbText;


				try
				{
					if (a.MMApplicant2 != null)
					{
						MMApplicant ap2 = a.MMApplicant2;
						//populating controls...
						litAp2Firstname1.Text = ap2.FirstName;
						litAp2Firstname2.Text = ap2.FirstName;
						RadTextBoxHomeUnitNumber2.Text = ap2.HomeUnitNumber;
						RadTextBoxHomeStreetNumber2.Text = ap2.HomeStreetNumber;
						RadTextBoxHomeStreetName2.Text = ap2.HomeStreetName;
						RadComboBoxHomeStreetType2.SelectedValue = ap2.HomeStreetType;
						RadComboBoxHomeStreetType2.Text = ap2.HomeStreetType;
						RadComboBoxHomeSuburb2.SelectedValue = ap2.HomeSuburb.ToString();
						RadComboBoxHomeSuburb2.Text = ap2.HomeSuburbText;
						RadMonthYearPickerHereSince2.SelectedDate = ap2.HereSince;
						RadComboBoxResidentialStatus2.SelectedValue = ap2.ResidentialStatus;

						RadTextBoxPostalAddressLine12.Text = ap2.PostalAddressLine1;
						RadTextBoxPostalAddressLine22.Text = ap2.PostalAddressLine2;
						RadComboBoxPostalSuburb2.SelectedValue = ap2.PostalHomeSuburb.ToString();
						RadComboBoxPostalSuburb2.Text = ap2.PostalHomeSuburbText;
					}
					else
					{
						tdP2_1.Visible = false;
						tdP2_2.Visible = false;
						NameLabelsVisible(false);
					}
				}
				catch { }
			}
		}
		protected void cbPostalAddressSameAs_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (cb.Checked)
			{
				string line1 = "";

				if (RadTextBoxHomeUnitNumber.Text.Length > 0)
				{
					line1 = "Unit " + RadTextBoxHomeUnitNumber.Text.Trim() + ", ";
				}
				line1 += (RadTextBoxHomeStreetNumber.Text.Trim() + " "
					+ RadTextBoxHomeStreetName.Text.Trim() + " "
					+ RadComboBoxHomeStreetType.Text.Trim()).Trim();

				RadTextBoxPostalAddressLine1.Text = line1;

				RadComboBoxPostalSuburb.SelectedValue = RadComboBoxHomeSuburb.SelectedValue;
				RadComboBoxPostalSuburb.Text = RadComboBoxHomeSuburb.Text;

				//Person 2
				line1 = "";
				if (RadTextBoxHomeUnitNumber2.Text.Length > 0)
				{
					line1 = "Unit " + RadTextBoxHomeUnitNumber2.Text.Trim() + ", ";
				}
				line1 += (RadTextBoxHomeStreetNumber2.Text.Trim() + " "
					+ RadTextBoxHomeStreetName2.Text.Trim() + " "
					+ RadComboBoxHomeStreetType2.Text.Trim()).Trim();

				RadTextBoxPostalAddressLine12.Text = line1;

				RadComboBoxPostalSuburb2.SelectedValue = RadComboBoxHomeSuburb2.SelectedValue;
				RadComboBoxPostalSuburb2.Text = RadComboBoxHomeSuburb2.Text;
			}

			else
			{
				RadTextBoxPostalAddressLine1.Text = "";
				RadTextBoxPostalAddressLine2.Text = "";
				RadComboBoxPostalSuburb.Text = "";
				RadComboBoxPostalSuburb.ClearSelection();
				RadTextBoxPostalAddressLine12.Text = "";
				RadTextBoxPostalAddressLine22.Text = "";
				RadComboBoxPostalSuburb2.Text = "";
				RadComboBoxPostalSuburb2.ClearSelection();
			}
		}

		

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				Save();
				Response.Redirect("ApplicationForm-03.aspx");
			}
		}

		protected void Save()
		{
			MMApplication a = new MMApplication();
			if (Session["Application"] != null)
			{
				a = (MMApplication)Session["Application"];
			}
			else
			{
				throw new Exception("No Application Session, or timed out");
			}

			if (a.MMApplicant1 != null)
			{

			}
			else
			{
				//most likely, the session variable has expired. Lets re-load from the Database...
				throw new Exception("Unable to load Applicant object");
			}
			//Collecting ap1 data from the form...

			a.MMApplicant1.HomeUnitNumber = RadTextBoxHomeUnitNumber.Text;
			a.MMApplicant1.HomeStreetNumber = RadTextBoxHomeStreetNumber.Text;
			a.MMApplicant1.HomeStreetName = RadTextBoxHomeStreetName.Text;
			a.MMApplicant1.HomeStreetType = RadComboBoxHomeStreetType.Text;
			a.MMApplicant1.HomeSuburb = Convert.ToInt32(RadComboBoxHomeSuburb.SelectedValue);
			a.MMApplicant1.HereSince = RadMonthYearPickerHereSince.SelectedDate;
      if (a.MMApplicant1.HereSince != null)
      {
        TimeSpan ts = DateTime.Now - (DateTime)a.MMApplicant1.HereSince;
        a.MMApplicant1.DurationYears = (Int32)ts.Days / 365;
        a.MMApplicant1.DurationMonths = (Int32)((ts.Days % 365) / 31);
      }
			a.MMApplicant1.ResidentialStatus = RadComboBoxResidentialStatus.SelectedValue;

			a.MMApplicant1.PostalAddressLine1 = RadTextBoxPostalAddressLine1.Text;
			a.MMApplicant1.PostalAddressLine2 = RadTextBoxPostalAddressLine2.Text;
			a.MMApplicant1.PostalHomeSuburb = Convert.ToInt32(RadComboBoxPostalSuburb.SelectedValue);


			a.MMApplicant1.Save();

			if (tdP2_1.Visible)
			{
				if (a.MMApplicant2 != null)
				{
				}
				else
				{
				}

				//Collecting ap2 data from form...
				a.MMApplicant2.HomeUnitNumber = RadTextBoxHomeUnitNumber2.Text;
				a.MMApplicant2.HomeStreetNumber = RadTextBoxHomeStreetNumber2.Text;
				a.MMApplicant2.HomeStreetName = RadTextBoxHomeStreetName2.Text;
				a.MMApplicant2.HomeStreetType = RadComboBoxHomeStreetType2.Text;
				a.MMApplicant2.HomeSuburb = Convert.ToInt32(RadComboBoxHomeSuburb2.SelectedValue);
				a.MMApplicant2.HereSince = RadMonthYearPickerHereSince2.SelectedDate;
        if (a.MMApplicant2.HereSince != null)
        {
          TimeSpan ts = DateTime.Now - (DateTime)a.MMApplicant2.HereSince;
          a.MMApplicant2.DurationYears = (Int32)ts.Days / 365;
          a.MMApplicant2.DurationMonths = (Int32)((ts.Days % 365) / 31);
        }

				a.MMApplicant2.ResidentialStatus = RadComboBoxResidentialStatus2.SelectedValue;

				a.MMApplicant2.PostalAddressLine1 = RadTextBoxPostalAddressLine12.Text;
				a.MMApplicant2.PostalAddressLine2 = RadTextBoxPostalAddressLine22.Text;
				a.MMApplicant2.PostalHomeSuburb = Convert.ToInt32(RadComboBoxPostalSuburb2.SelectedValue);

				a.MMApplicant2.Save();
			}

			Session["Application"] = a;
		}

		protected void cbResidentialAddressSameAs1_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (cb.Checked)
			{
				RadTextBoxHomeUnitNumber2.Text = RadTextBoxHomeUnitNumber.Text;
				RadTextBoxHomeStreetNumber2.Text = RadTextBoxHomeStreetNumber.Text;
				RadTextBoxHomeStreetName2.Text = RadTextBoxHomeStreetName.Text;
				RadComboBoxHomeStreetType2.SelectedValue = RadComboBoxHomeStreetType.SelectedValue;
				RadComboBoxHomeStreetType2.Text = RadComboBoxHomeStreetType.Text;
				RadComboBoxHomeSuburb2.SelectedValue = RadComboBoxHomeSuburb.SelectedValue;
				RadComboBoxHomeSuburb2.Text = RadComboBoxHomeSuburb.Text;
				RadMonthYearPickerHereSince2.SelectedDate = RadMonthYearPickerHereSince.SelectedDate;
				RadComboBoxResidentialStatus2.SelectedValue = RadComboBoxResidentialStatus.SelectedValue;
				RadComboBoxResidentialStatus2.Text = RadComboBoxResidentialStatus.Text;
			}
			else
			{
				RadTextBoxHomeUnitNumber2.Text = "";
				RadTextBoxHomeStreetNumber2.Text = "";
				RadTextBoxHomeStreetName2.Text = "";
				RadComboBoxHomeStreetType2.ClearSelection();
				RadComboBoxHomeStreetType2.Text = "";
				RadComboBoxHomeSuburb2.Text = "";
				RadComboBoxHomeSuburb2.ClearSelection();
				RadMonthYearPickerHereSince2.Clear();
				RadComboBoxResidentialStatus2.Text = "";
				RadComboBoxResidentialStatus2.ClearSelection();
			}
		}

		protected void cbPostalAddressSameAs1_CheckedChanged(object sender, EventArgs e)
		{
						CheckBox cb = (CheckBox)sender;
						if (cb.Checked)
						{
							RadTextBoxPostalAddressLine12.Text = RadTextBoxPostalAddressLine1.Text;
							RadTextBoxPostalAddressLine22.Text = RadTextBoxPostalAddressLine2.Text;
							RadComboBoxPostalSuburb2.SelectedValue = RadComboBoxPostalSuburb.SelectedValue;
							RadComboBoxPostalSuburb2.Text = RadComboBoxPostalSuburb.Text;
						}
						else
						{
							RadTextBoxPostalAddressLine12.Text = "";
							RadTextBoxPostalAddressLine22.Text = "";
							RadComboBoxPostalSuburb2.Text = "";
							RadComboBoxPostalSuburb2.ClearSelection();
						}
		}

		protected void btnBack_Click(object sender, EventArgs e)
		{
			Save();
			Response.Redirect("ApplicationForm-01.aspx");
		}

		protected void NameLabelsVisible(bool visible)
		{
			litAp1Firstname1.Visible = visible;
			litAp1Firstname2.Visible = visible;
			litAp2Firstname1.Visible = visible;
			litAp2Firstname2.Visible = visible;
		}

        protected void btnResidentialAddressSameAs1_Click(object sender, EventArgs e)
        {
            RadTextBoxHomeUnitNumber2.Text = RadTextBoxHomeUnitNumber.Text;
            RadTextBoxHomeStreetNumber2.Text = RadTextBoxHomeStreetNumber.Text;
            RadTextBoxHomeStreetName2.Text = RadTextBoxHomeStreetName.Text;
            RadComboBoxHomeStreetType2.SelectedValue = RadComboBoxHomeStreetType.SelectedValue;
            RadComboBoxHomeStreetType2.Text = RadComboBoxHomeStreetType.Text;
            RadComboBoxHomeSuburb2.SelectedValue = RadComboBoxHomeSuburb.SelectedValue;
            RadComboBoxHomeSuburb2.Text = RadComboBoxHomeSuburb.Text;
            RadMonthYearPickerHereSince2.SelectedDate = RadMonthYearPickerHereSince.SelectedDate;
            RadComboBoxResidentialStatus2.SelectedValue = RadComboBoxResidentialStatus.SelectedValue;
            RadComboBoxResidentialStatus2.Text = RadComboBoxResidentialStatus.Text;
        }

        protected void btnPostalAddressSameAs1_Click(object sender, EventArgs e)
        {
            RadTextBoxPostalAddressLine12.Text = RadTextBoxPostalAddressLine1.Text;
            RadTextBoxPostalAddressLine22.Text = RadTextBoxPostalAddressLine2.Text;
            RadComboBoxPostalSuburb2.SelectedValue = RadComboBoxPostalSuburb.SelectedValue;
            RadComboBoxPostalSuburb2.Text = RadComboBoxPostalSuburb.Text;
        }

        protected void btnPostalAddressSameAs_Click(object sender, EventArgs e)
        {
            string line1 = "";

            if (RadTextBoxHomeUnitNumber.Text.Length > 0)
            {
                line1 = "Unit " + RadTextBoxHomeUnitNumber.Text.Trim() + ", ";
            }
            line1 += (RadTextBoxHomeStreetNumber.Text.Trim() + " "
                + RadTextBoxHomeStreetName.Text.Trim() + " "
                + RadComboBoxHomeStreetType.Text.Trim()).Trim();

            RadTextBoxPostalAddressLine1.Text = line1;

            RadComboBoxPostalSuburb.SelectedValue = RadComboBoxHomeSuburb.SelectedValue;
            RadComboBoxPostalSuburb.Text = RadComboBoxHomeSuburb.Text;

            //Person 2
            line1 = "";
            if (RadTextBoxHomeUnitNumber2.Text.Length > 0)
            {
                line1 = "Unit " + RadTextBoxHomeUnitNumber2.Text.Trim() + ", ";
            }
            line1 += (RadTextBoxHomeStreetNumber2.Text.Trim() + " "
                + RadTextBoxHomeStreetName2.Text.Trim() + " "
                + RadComboBoxHomeStreetType2.Text.Trim()).Trim();

            RadTextBoxPostalAddressLine12.Text = line1;

            RadComboBoxPostalSuburb2.SelectedValue = RadComboBoxHomeSuburb2.SelectedValue;
            RadComboBoxPostalSuburb2.Text = RadComboBoxHomeSuburb2.Text;
        }
	}
}