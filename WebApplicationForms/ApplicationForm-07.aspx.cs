﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;


namespace WebApplicationForms
{
	public partial class WebForm_A07 : System.Web.UI.Page
	{
		public int ApplicantID { get; set; }
		protected void Page_Load(object sender, EventArgs e)
		{

			if (!this.IsPostBack)
			{
				//initialising controls...				        

				//getting data...
				MMApplication a;
				if ((Session["Application"] == null) && HttpContext.Current.Request.IsLocal)
				{
					a = new MMApplication("CFE209842ECB48F4AD1E47BB40529E1F");
					a.LoadApplicants();
					Session["Application"] = a;
				}
				else
				{ a = (MMApplication)Session["Application"]; }
				MMApplicant ap1 = a.MMApplicant1;
        ap1.InitialiseAssetLiabilityItems();

				//Populating controls...			
        
				ApplicantAssetLiabilityGrid.ApplicantID = ap1.ApplicantID;
				//litAp1Firstname1.Text = ap1.FirstName;


				try
				{
					if (a.MMApplicant2 != null)
					{
						MMApplicant ap2 = a.MMApplicant2;
						//populating controls...


					}
					else
					{
						//tdP2_1.Visible = false;
					}
				}
				catch { }
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (Page.IsValid) Response.Redirect("ApplicationForm-08.aspx");
		}
		protected void btnBack_Click(object sender, EventArgs e)
		{
			if (Page.IsValid) Response.Redirect("ApplicationForm-06.aspx");
		}

	}
}