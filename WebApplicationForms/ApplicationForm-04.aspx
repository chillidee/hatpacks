﻿<%@ Page Title="Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationForm-04.aspx.cs" Inherits="WebApplicationForms.WebForm_A04" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	<style type="text/css">
.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}
  </style>
	</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

	<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Your Application</h1>
				<h2>Personal Details</h2>
			</hgroup>
			<p>More about your personal details.</p>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">		
		<asp:SqlDataSource ID="sqlPCode" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT [PCodeID], [Combo] FROM [vwPCode] ORDER BY [SUBURB], [PCODE] DESC"></asp:SqlDataSource>
		<asp:SqlDataSource ID="sqlMaritalStatus" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT '' MaritalStatus UNION SELECT * FROM MaritalStatus "></asp:SqlDataSource>
		<asp:SqlDataSource ID="sqlResidencyStatus" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT '' ResidencyStatus UNION SELECT * FROM ResidencyStatus"></asp:SqlDataSource>
		<asp:SqlDataSource ID="sqlState" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT '' State UNION SELECT State FROM State"></asp:SqlDataSource>
		
		<h3>Tell us more about you...</h3>

		<table class="splitTable">
			<tr>
				<td class="split">
					<h4><asp:Literal ID="litAp1Firstname1" runat="server"></asp:Literal></h4>
					<table class="fieldset">
						<tr>
							<td class="fieldLabel">Date Of Birth</td>
							<td colspan="3">
								<telerik:RadDatePicker ID="RadDatePickerDateOfBirth" Runat="server" Culture="en-AU">
									<Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
									</Calendar>
									<DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText="" LabelWidth="40%" type="text" value="">
									</DateInput>
									<DatePopupButton HoverImageUrl="" ImageUrl="" />
								</telerik:RadDatePicker>
								<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadDatePickerDateOfBirth"></asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Marital Status</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxMaritalStatus" runat="server" AllowCustomText="false" DataSourceID="sqlMaritalStatus" DataTextField="MaritalStatus" DataValueField="MaritalStatus" OffsetY="10" Width="160px">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Number of Dependants</td>
							<td style="width:30px">

								<telerik:RadNumericTextBox ID="RadNumericNoOfDependents" Runat="server" DataType="System.Int32" DisplayText="" MaxValue="12" MinValue="0" Width="30px">
									<NumberFormat DecimalDigits="0" ZeroPattern="n" />
								</telerik:RadNumericTextBox>
							</td>
							<td class="fieldLabel">
								Ages
							</td>
							<td>

								<telerik:RadTextBox ID="RadTextBoxAgesOfDependents" Runat="server" Width="80px" ToolTip="List their ages, seperated by commas. Eg: 7, 10, 12">
								</telerik:RadTextBox>

							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Residency Status</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxResidencyStatus" runat="server" AllowCustomText="false" DataSourceID="sqlResidencyStatus" DataTextField="ResidencyStatus" DataValueField="ResidencyStatus" OffsetY="10" Width="160px">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Mother&#39;s Maiden Name (for security purposes)</td>
							<td colspan="3">
								<script>
									function RadComboBoxHomeSuburb_OnClientItemsRequestingHandler(sender, eventArgs) {
										if (sender.get_text().length < 2) {
											eventArgs.set_cancel(true);
										}
									}
								</script>
								<telerik:RadTextBox ID="RadTextBoxMothersMaidenName" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Driver&#39;s License Number</td>
							<td colspan="3">
								<telerik:RadTextBox ID="RadTextBoxDriversLicenceNo" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Driver&#39;s License Expiry</td>
							<td colspan="3">
								<telerik:RadDatePicker ID="RadDatePickerDriversLicenceExpiry" Runat="server" Culture="en-AU">
									<Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
									</Calendar>
									<DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText="" LabelWidth="40%" type="text" value="">
									</DateInput>
									<DatePopupButton HoverImageUrl="" ImageUrl="" />
								</telerik:RadDatePicker>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">State of Issue</td>
							<td colspan="3">
								<telerik:RadComboBox ID="rcbDriversLicenceState" runat="server" DataSourceID="sqlState" DataTextField="State" DataValueField="State" AllowCustomText="false" OffsetY="10" Width="160px"></telerik:RadComboBox>
							</td>
						</tr>
					</table>
				</td>
				<td class="split" runat="server" id="tdP2_1">
					<h4>
						<asp:Literal ID="litAp2Firstname1" runat="server"></asp:Literal>
					</h4>
					<table class="fieldset" runat="server" id="tableApplicant2_1">
						<tr>
							<td></td>
							<td colspan="3">
								<asp:CheckBox ID="cbSameAs1" runat="server" CausesValidation="false" Text="same as main contact" AutoPostBack="True" OnCheckedChanged="cbSameAs1_CheckedChanged" visible="false"/></td>
						</tr>
						<tr>
							<td class="fieldLabel">Date Of Birth</td>
							<td colspan="3">
								<telerik:RadDatePicker ID="RadDatePickerDateOfBirth2" Runat="server" Culture="en-AU">
									<Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
									</Calendar>
									<DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText="" LabelWidth="40%" type="text" value="">
									</DateInput>
									<DatePopupButton HoverImageUrl="" ImageUrl="" />
								</telerik:RadDatePicker>
								<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="RadDatePickerDateOfBirth2"></asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Marital Status</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxMaritalStatus2" runat="server" AllowCustomText="false" DataSourceID="sqlMaritalStatus" DataTextField="MaritalStatus" DataValueField="MaritalStatus" OffsetY="10" Width="160px">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Number of Dependants</td>
							<td style="width:30px">

								<telerik:RadNumericTextBox ID="RadNumericNoOfDependents2" Runat="server" DataType="System.Int32" DisplayText="" MaxValue="12" MinValue="0" Width="30px">
									<NumberFormat DecimalDigits="0" ZeroPattern="n" />
								</telerik:RadNumericTextBox>
							</td>
							<td class="fieldLabel">
								Ages
							</td>
							<td>

								<telerik:RadTextBox ID="RadTextBoxAgesOfDependents2" Runat="server" Width="80px" ToolTip="List their ages, seperated by commas. Eg: 7, 10, 12">
								</telerik:RadTextBox>

							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Residency Status</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxResidencyStatus2" runat="server" AllowCustomText="false" DataSourceID="sqlResidencyStatus" DataTextField="ResidencyStatus" DataValueField="ResidencyStatus" OffsetY="10" Width="160px">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Mother&#39;s Maiden Name (for security purposes)</td>
							<td colspan="3">
								<script>
									function RadComboBoxHomeSuburb_OnClientItemsRequestingHandler(sender, eventArgs) {
										if (sender.get_text().length < 2) {
											eventArgs.set_cancel(true);
										}
									}
								</script>
								<telerik:RadTextBox ID="RadTextBoxMothersMaidenName2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Driver&#39;s License Number</td>
							<td colspan="3">
								<telerik:RadTextBox ID="RadTextBoxDriversLicenceNo2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Driver&#39;s License Expiry</td>
							<td colspan="3">
								<telerik:RadDatePicker ID="RadDatePickerDriversLicenceExpiry2" Runat="server" Culture="en-AU">
									<Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
									</Calendar>
									<DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText="" LabelWidth="40%" type="text" value="">
									</DateInput>
									<DatePopupButton HoverImageUrl="" ImageUrl="" />
								</telerik:RadDatePicker>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">State of Issue</td>
							<td colspan="3">
								<telerik:RadComboBox ID="rcbDriversLicenceState2" runat="server" DataSourceID="sqlState" DataTextField="State" DataValueField="State" AllowCustomText="false" OffsetY="10" Width="160px"></telerik:RadComboBox>
							</td>
						</tr>
					</table>


				</td>
			</tr>
		</table>

		<h3>Tell us about a personal reference who does not live with you (over the age of 18).</h3>
		

		<table class="splitTable">
			<tr>
				<td class="split">
					<h4><asp:Literal ID="litAp1Firstname2" runat="server"></asp:Literal></h4>
					<table class="fieldset">
						<tr>
							<td class="fieldLabel">Relationship</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefRelationship" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Title</td>
							<td>
								<telerik:RadComboBox ID="rcbPersonalRefTitle" runat="server" OffsetY="10">
									<Items>
                                        <telerik:RadComboBoxItem runat="server" Text="(none)" Value="" />
										<telerik:RadComboBoxItem runat="server" Text="Mr" Value="Mr" />
										<telerik:RadComboBoxItem runat="server" Text="Mrs" Value="Mrs" />
										<telerik:RadComboBoxItem runat="server" Text="Ms" Value="Ms" />
										<telerik:RadComboBoxItem runat="server" Text="Dr" Value="Dr" />
									</Items>
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Firstname</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefName" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Surname</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefSurname" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 1</td>
							<td>
								<script>
									function RadComboBoxHomeSuburb_OnClientItemsRequestingHandler(sender, eventArgs) {
										if (sender.get_text().length < 2) {
											eventArgs.set_cancel(true);
										}
									}
								</script>
								<telerik:RadTextBox ID="rtbPersonalRefAddLine1" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 2</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefAddLine2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State &amp; Post Code</td>
							<td>
								<telerik:RadComboBox ID="rcbPersonalRefSuburb" runat="server" DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID" DropDownWidth="370px" EmptyMessage="" EnableAutomaticLoadOnDemand="True" EnableVirtualScrolling="True" Height="200px" ItemsPerRequest="20" MarkFirstMatch="True" OffsetY="10" OnClientItemsRequesting="RadComboBoxHomeSuburb_OnClientItemsRequestingHandler" ShowMoreResultsBox="True" Width="160px">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Mobile Number</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefWorkPhone" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Home Phone Number</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefHomePhone" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
					</table>
				</td>
				<td class="split" runat="server" id="tdP2_2">
					<h4>
						<asp:Literal ID="litAp2Firstname2" runat="server"></asp:Literal>
					</h4>
					<table class="fieldset" runat="server" id="table1">
						<tr>
							<td></td>
							<td>
                                <asp:Button runat="server" ID="btnSameAs12" Text="same as main contact" CausesValidation="false" OnClick="btnSameAs12_Click" />
								<asp:CheckBox ID="cbSameAs12" runat="server" CausesValidation="false" Text="same as main contact" AutoPostBack="True" OnCheckedChanged="cbSameAs12_CheckedChanged" Visible="false" /></td>
						</tr>
						<tr>
							<td class="fieldLabel">Relationship</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefRelationship2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Title</td>
							<td>
								<telerik:RadComboBox ID="rcbPersonalRefTitle2" runat="server" OffsetY="10">
									<Items>
                                        <telerik:RadComboBoxItem runat="server" Text="(none)" Value="" />
										<telerik:RadComboBoxItem runat="server" Text="Mr" Value="Mr" />
										<telerik:RadComboBoxItem runat="server" Text="Mrs" Value="Mrs" />
										<telerik:RadComboBoxItem runat="server" Text="Ms" Value="Ms" />
										<telerik:RadComboBoxItem runat="server" Text="Dr" Value="Dr" />
									</Items>
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Firstname</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefName2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Surname</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefSurname2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 1</td>
							<td>
								<script>
									function RadComboBoxHomeSuburb_OnClientItemsRequestingHandler(sender, eventArgs) {
										if (sender.get_text().length < 2) {
											eventArgs.set_cancel(true);
										}
									}
								</script>
								<telerik:RadTextBox ID="rtbPersonalRefAddLine12" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 2</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefAddLine22" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State &amp; Post Code</td>
							<td>
								<telerik:RadComboBox ID="rcbPersonalRefSuburb2" runat="server" DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID" DropDownWidth="370px" EmptyMessage="" EnableAutomaticLoadOnDemand="True" EnableVirtualScrolling="True" Height="200px" ItemsPerRequest="20" MarkFirstMatch="True" OffsetY="10" OnClientItemsRequesting="RadComboBoxHomeSuburb_OnClientItemsRequestingHandler" ShowMoreResultsBox="True" Width="160px">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Mobile Number</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefWorkPhone2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Home Phone Number</td>
							<td>
								<telerik:RadTextBox ID="rtbPersonalRefHomePhone2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
					</table>


				</td>
			</tr>
		</table>

		
		<div style="text-align: right">
			<asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click">go back</asp:LinkButton>
			<asp:Button ID="btnSubmit" Text="next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
		</div>
	</telerik:RadAjaxPanel>
</asp:Content>
