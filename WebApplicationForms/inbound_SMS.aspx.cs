﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace WebApplicationForms
{
  public partial class inbound_SMS : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      string _to = Request.QueryString["to"];
      string _from = Request.QueryString["from"];
      string _message = Request.QueryString["message"];
      string _ref = Request.QueryString["ref"];

      string ProductID = Utils.Left(_ref, 2);
      string RefID = Utils.Mid(_ref, 2);
      string Message = HttpUtility.UrlDecode(_message);

      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        string sql = "INSERT INTO SMS (ProductID, RefID, ReceiveDate, ReceiveMessage, ReceiveFrom) VALUES (@1, @2, GETDATE(), @3, @4)";
        SqlCommand cmd = new SqlCommand(sql, conn);
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.Parameters.Add(new SqlParameter("@1", ProductID));
        cmd.Parameters.Add(new SqlParameter("@2", RefID));
        cmd.Parameters.Add(new SqlParameter("@3", Message));
        cmd.Parameters.Add(new SqlParameter("@4", _from));

        conn.Open();
        cmd.ExecuteNonQuery();

        switch (ProductID)
        {

          case "BM":
            sql = "SELECT u.Email, u.Firstname, u.Lastname, b.Firstname Client_Firstname, b.Surname Client_Lastname FROM BMApplicant b INNER JOIN [User] u ON b.ApplicationManager = u.Username WHERE b.ApplicantID = " + RefID;
            break;
          case "CF":
            sql = "SELECT u.Email, u.Firstname, u.Lastname, c.Firstname Client_Firstname, c.Surname Client_Lastname FROM CF.dbo.Applicant c INNER JOIN [User] u ON c.ApplicationManager = u.Username WHERE c.ApplicantID = " + RefID;
            break;
          case "LA":
            sql = "SELECT u.Email, u.Firstname, u.Lastname, m.Firstname Client_Firstname, m.Surname Client_Lastname " +
              "FROM [Application] a INNER JOIN [User] u ON a.SalesManager = u.Username " +
              "INNER JOIN dbo.LoanApplicationMainContact(" + RefID + ") m ON m.ApplicationNo = a.ApplicationNo";
            break;
          case "DA":
            sql = "SELECT u.Email, u.Firstname, u.Lastname, d.Firstname Client_Firstname, d.Surname Client_Lastname FROM DSApplicant d INNER JOIN [User] u ON d.ApplicationManager = u.Username WHERE d.ApplicantID = " + RefID;
            break;
          case "LD":
            sql = "SELECT u.Email, u.Firstname, u.Lastname, c.Firstname Client_Firstname, c.Surname Client_Lastname FROM Call c INNER JOIN [User] u ON c.Operator = u.Username WHERE c.CallID = " + RefID;
            break;
          case "":
            sql = "";
            break;
        }


        string emailAddress = "";
        string Firstname = "";
        string Lastname = "";
        string Client_Firstname = "";
        string Client_Lastname = "";

        SqlCommand cmd2 = new SqlCommand(sql, conn);
        SqlDataReader dr = cmd2.ExecuteReader();

        while (dr.Read())
        {
          emailAddress = dr["Email"].ToString();
          Lastname = dr["Lastname"].ToString();
          Firstname = dr["Firstname"].ToString();
          Client_Firstname = dr["Client_Firstname"].ToString();
          Client_Lastname = dr["Client_Lastname"].ToString();
        }

        MailAddress emailFrom = new MailAddress("no-reply@hatpacks.com.au");
        MailMessage m = new MailMessage();

        m.From = emailFrom;

        m.To.Add(new MailAddress(emailAddress, Firstname + " " + Lastname));
        m.Subject = "Reply SMS from " + Client_Firstname + " " + Client_Lastname;
        m.IsBodyHtml = false;
        m.Body = "SMS reply received from " + Client_Firstname + " " + Client_Lastname + "\r\n" + "Mobile number: " + _from + "\r\n" + "Reference ID: " + RefID + "\r\n" + "\r\n" + Message;

        SmtpClient s = new SmtpClient("localhost");

        s.Send(m);

        /*try
        {
          s.Send(m);
          Sent = true;
          //a.Web_InvitationEmailSent = DateTime.Now;

        }
        catch (Exception ex)
        {
          Sent = false;
          throw ex;
        }*/

      }

    }
  }
}