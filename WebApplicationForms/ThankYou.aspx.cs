﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms
{
	public partial class ThankYou : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			MMApplication a = (MMApplication)Session["Application"];
			Session["Company"] = a.Company;

			litFirstname.Text = a.MMApplicant1.FirstName;
			litFirstname2.Text = a.MMApplicant1.FirstName;
			lblCompanyName.Text = a.Company.Company;
			//lblPhone.Text = a.Company.Phone;
			lblSalesManager.Text = a.SalesManager;
			//lblSalesManager2.Text = a.SalesManager;
		}
	}
}