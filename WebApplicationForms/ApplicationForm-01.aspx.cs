﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;


namespace WebApplicationForms
{
	public partial class WebForm_A01 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				//getting data...
				MMApplication a = (MMApplication)Session["Application"];
				//logging the start time...
				a.Web_ApplicationFormStarted = DateTime.Now; a.Save();
				MMApplicant ap1 = a.MMApplicant1;

				//populating controls...
				hfApplicantID.Value = ap1.ApplicantID.ToString();
				RadComboBoxTitle.SelectedValue = ap1.Title;
				RadTextBoxFirstname.Text = ap1.FirstName;
				RadTextBoxSurname.Text = ap1.Surname;

				RadTextBoxMobile.Text = ap1.Mobile;
				RadTextBoxTelephoneHome.Text = ap1.TelephoneHome;
				RadTextBoxTelephoneWork.Text = ap1.TelephoneWork;
				RadTextBoxFax.Text = ap1.Fax;
				RadTextBoxEmail.Text = ap1.Email;
				RadTextBoxEmailSecond.Text = ap1.EmailSecond;

				try
				{
					if (a.MMApplicant2 != null)
					{
						MMApplicant ap2 = a.MMApplicant2;
						//populating controls...
						hfApplicantID2.Value = ap2.ApplicantID.ToString();
                        RadComboBoxTitle2.SelectedValue = ap2.Title;
						RadTextBoxFirstname2.Text = ap2.FirstName;
						RadTextBoxSurname2.Text = ap2.Surname;

						RadTextBoxMobile2.Text = ap2.Mobile;
						RadTextBoxTelephoneHome2.Text = ap2.TelephoneHome;
						RadTextBoxTelephoneWork2.Text = ap2.TelephoneWork;
						RadTextBoxFax2.Text = ap2.Fax;
						RadTextBoxEmail2.Text = ap2.Email;
						RadTextBoxEmailSecond2.Text = ap2.EmailSecond;
					}
					else
					{ throw new Exception("Applicant 2 does not exist"); }
				}
				catch
				{
					//this means there is no 2nd Applicant.
					hfApplicantID2.Value = "NEW";
					Applicant2Controls(false);
				}
			}
			else
			{ }
		}

		protected void btnCopyContactDetails_Click(object sender, EventArgs e)
		{
			RadTextBoxFax2.Text = RadTextBoxFax.Text;
			RadTextBoxTelephoneHome2.Text = RadTextBoxTelephoneHome.Text;
			RadTextBoxTelephoneWork2.Text = RadTextBoxTelephoneWork.Text;
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				MMApplication a = new MMApplication();
				if (Session["Application"] != null)
				{
					a = (MMApplication)Session["Application"];
				}
				else
				{
					throw new Exception("No Application Session, or timed out");
				}


				if (a.MMApplicant1 != null)
				{

				}
				else
				{
					//most likely, the session variable has expired. Lets re-load from the Database...
					throw new Exception("Unable to load Applicant object");
				}
				//Collecting ap1 data from the form...
				a.MMApplicant1.Title = RadComboBoxTitle.SelectedValue;
				a.MMApplicant1.FirstName = RadTextBoxFirstname.Text;
				a.MMApplicant1.Surname = RadTextBoxSurname.Text;
				a.MMApplicant1.Mobile = RadTextBoxMobile.Text;
				a.MMApplicant1.TelephoneHome = RadTextBoxTelephoneHome.Text;
				a.MMApplicant1.TelephoneWork = RadTextBoxTelephoneWork.Text;
				a.MMApplicant1.Fax = RadTextBoxFax.Text;
				a.MMApplicant1.Email = RadTextBoxEmail.Text;
				a.MMApplicant1.EmailSecond = RadTextBoxEmailSecond.Text;

				a.MMApplicant1.Save();

				if (tableApplicant2_1.Visible)
				{
					if (hfApplicantID2.Value == "NEW")
					{
						a.MMApplicant2 = new MMApplicant();
					}
					else
					{
						if (a.MMApplicant2 != null)
						{ }
						else
						{ }
					}
					//Collecting ap2 data from form...
					a.MMApplicant2.Title = RadComboBoxTitle2.SelectedValue;
					a.MMApplicant2.FirstName = RadTextBoxFirstname2.Text;
					a.MMApplicant2.Surname = RadTextBoxSurname2.Text;
					a.MMApplicant2.Mobile = RadTextBoxMobile2.Text;
					a.MMApplicant2.TelephoneHome = RadTextBoxTelephoneHome2.Text;
					a.MMApplicant2.TelephoneWork = RadTextBoxTelephoneWork2.Text;
					a.MMApplicant2.Fax = RadTextBoxFax2.Text;
					a.MMApplicant2.Email = RadTextBoxEmail2.Text;
					a.MMApplicant2.EmailSecond = RadTextBoxEmailSecond2.Text;

					if (hfApplicantID2.Value == "NEW")
					{
						a.MMApplicant2.ApplicationNo = a.ApplicationNo;
						a.MMApplicant2.Create();
					}
					else { a.MMApplicant2.Save(); }
				}

				Session["Application"] = a;

				Response.Redirect("ApplicationForm-02.aspx");
			}
		}

		protected void Validate2ndApplicant(object source, ServerValidateEventArgs args)
		{
			//args.IsValid = (RadTextBoxFirstname2.Text == "" & RadTextBoxSurname2.Text == "") || (RadTextBoxFirstname2.Text != "" & RadTextBoxSurname2.Text != "");									

		}

		protected void Applicant2Controls(bool enabled)
		{
			tableApplicant2_1.Visible = enabled;
			tableApplicant2_2.Visible = enabled;
			btnAddApplicant2.Visible = !enabled;
		}

		protected void btnCreateApplicant2_Click(object sender, EventArgs e)
		{
			Applicant2Controls(true);
			hfApplicantID2.Value = "NEW";
			RadComboBoxTitle2.SelectedValue = "Mrs";
			RadTextBoxSurname2.Text = RadTextBoxSurname.Text;
		}

	}
}