﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;


namespace WebApplicationForms
{
	public partial class WebForm_A05 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				//initialising controls...


				//getting data...
				MMApplication a;
				if ((Session["Application"] == null) && HttpContext.Current.Request.IsLocal)
				{
					a = new MMApplication("CFE209842ECB48F4AD1E47BB40529E1F");
					a.LoadApplicants();
					Session["Application"] = a;
				}
				else
				{ a = (MMApplication)Session["Application"]; }

				MMApplicant ap1 = a.MMApplicant1;

				//Populating controls...				
				litAp1Firstname1.Text = ap1.FirstName;
				
				rcbCurrentEmploymentBasis.SelectedValue = ap1.CurrentEmploymentBasis;
				rtbOccupation.Text = ap1.Occupation;
				rtbCurrentEmployer.Text = ap1.CurrentEmployer;
				rtbCurrentEmployerPhone.Text = ap1.CurrentEmployerPhone;
				rtbEmplAddressLine1.Text = ap1.EmplAddressLine1;
				rtbEmplAddressLine2.Text = ap1.EmplAddressLine2;
				rcbEmplSuburb.SelectedValue = ap1.EmplSuburb.ToString();
				rcbEmplSuburb.Text = ap1.EmplSuburbText;
				rmypEmplSince.SelectedDate = ap1.EmplSince;


				try
				{
					if (a.MMApplicant2 != null)
					{
						MMApplicant ap2 = a.MMApplicant2;
						//populating controls...
						litAp2Firstname1.Text = ap2.FirstName;

						rcbCurrentEmploymentBasis2.SelectedValue = ap2.CurrentEmploymentBasis;
						rtbOccupation2.Text = ap2.Occupation;
						rtbCurrentEmployer2.Text = ap2.CurrentEmployer;
						rtbCurrentEmployerPhone2.Text = ap2.CurrentEmployerPhone;
						rtbEmplAddressLine12.Text = ap2.EmplAddressLine1;
						rtbEmplAddressLine22.Text = ap2.EmplAddressLine2;
						rcbEmplSuburb2.SelectedValue = ap2.EmplSuburb.ToString();
						rcbEmplSuburb2.Text = ap2.EmplSuburbText;
						rmypEmplSince2.SelectedDate = ap2.EmplSince;

					}
					else
					{
						tdP2_1.Visible = false;
						NameLabelsVisible(false);
					}
				}
				catch { }
				RefreshControls();
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				Save();
				Response.Redirect("ApplicationForm-05a.aspx");
			}
		}

		protected void Save()
		{
			MMApplication a = new MMApplication();
			if (Session["Application"] != null)
			{
				a = (MMApplication)Session["Application"];
			}
			else
			{
				throw new Exception("No Application Session, or timed out");
			}

			if (a.MMApplicant1 != null)
			{

			}
			else
			{
				//most likely, the session variable has expired. Lets re-load from the Database...
				throw new Exception("Unable to load Applicant object");
			}
			//Collecting ap1 data from the form...

			a.MMApplicant1.CurrentEmploymentBasis = rcbCurrentEmploymentBasis.SelectedValue;
			a.MMApplicant1.Occupation = rtbOccupation.Text;
			a.MMApplicant1.CurrentEmployer = rtbCurrentEmployer.Text;
			a.MMApplicant1.CurrentEmployerPhone = rtbCurrentEmployerPhone.Text;
			a.MMApplicant1.EmplAddressLine1 = rtbEmplAddressLine1.Text;
			a.MMApplicant1.EmplAddressLine2 = rtbEmplAddressLine2.Text;
			a.MMApplicant1.EmplSuburb = Utils.EmptyToNull(rcbEmplSuburb.SelectedValue);
			a.MMApplicant1.EmplSince = rmypEmplSince.SelectedDate;
      if (a.MMApplicant1.EmplSince != null)
      {
        TimeSpan ts = DateTime.Now - (DateTime)a.MMApplicant1.EmplSince;
        a.MMApplicant1.EmplDurationYears = (Int32)ts.Days / 365;
        a.MMApplicant1.EmplDurationMonths = (Int32)((ts.Days % 365) / 31);
      }

			a.MMApplicant1.Save();

			if (tdP2_1.Visible)
			{
				if (a.MMApplicant2 != null)
				{
				}
				else
				{
				}

				//Collecting ap2 data from form...
				a.MMApplicant2.CurrentEmploymentBasis = rcbCurrentEmploymentBasis2.SelectedValue;
				a.MMApplicant2.Occupation = rtbOccupation2.Text;
				a.MMApplicant2.CurrentEmployer = rtbCurrentEmployer2.Text;
				a.MMApplicant2.CurrentEmployerPhone = rtbCurrentEmployerPhone2.Text;
				a.MMApplicant2.EmplAddressLine1 = rtbEmplAddressLine12.Text;
				a.MMApplicant2.EmplAddressLine2 = rtbEmplAddressLine22.Text;
				//if (String.IsNullOrEmpty(rcbEmplSuburb2.SelectedValue)){a.MMApplicant2.EmplSuburb = null;} else {a.MMApplicant2.EmplSuburb = Convert.ToInt32(rcbEmplSuburb2.SelectedValue);}
				a.MMApplicant2.EmplSuburb = Utils.EmptyToNull(rcbEmplSuburb2.SelectedValue);
				a.MMApplicant2.EmplSince = rmypEmplSince2.SelectedDate;
        if (a.MMApplicant2.EmplSince != null)
        {
          TimeSpan ts = DateTime.Now - (DateTime)a.MMApplicant2.EmplSince;
          a.MMApplicant2.EmplDurationYears = (Int32)ts.Days / 365;
          a.MMApplicant2.EmplDurationMonths = (Int32)((ts.Days % 365) / 31);
        }

				a.MMApplicant2.Save();
			}

			Session["Application"] = a;
		}


		protected void cbSameAs1_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (cb.Checked)
			{

				rcbCurrentEmploymentBasis2.SelectedValue = rcbCurrentEmploymentBasis.SelectedValue;
				rtbOccupation2.Text = rtbOccupation.Text;
				rtbCurrentEmployer2.Text = rtbCurrentEmployer.Text;
				rtbCurrentEmployerPhone2.Text = rtbCurrentEmployerPhone.Text;
				rtbEmplAddressLine12.Text = rtbEmplAddressLine1.Text;
				rtbEmplAddressLine22.Text = rtbEmplAddressLine2.Text;
				rcbEmplSuburb2.SelectedValue = rcbEmplSuburb.SelectedValue;
				rcbEmplSuburb2.Text = rcbEmplSuburb.Text;
				rmypEmplSince2.SelectedDate = rmypEmplSince.SelectedDate;
			}
			else
			{
				rcbCurrentEmploymentBasis2.ClearSelection();
				rtbOccupation2.Text = String.Empty;
				rtbCurrentEmployer2.Text = String.Empty;
				rtbCurrentEmployerPhone2.Text = String.Empty;
				rtbEmplAddressLine12.Text = String.Empty;
				rtbEmplAddressLine22.Text = String.Empty;
				rcbEmplSuburb2.ClearSelection();
				rcbEmplSuburb2.Text = String.Empty;
				rmypEmplSince2.Clear();
			}

		}
		protected void btnBack_Click(object sender, EventArgs e)
		{
			Save();
			Response.Redirect("ApplicationForm-04.aspx");
		}
		protected void NameLabelsVisible(bool visible)
		{
			litAp1Firstname1.Visible = visible;			
			litAp2Firstname1.Visible = visible;			
		}

		protected void rcbCurrentEmploymentBasis_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
		{
			RefreshControls();
		}

		protected void rcbCurrentEmploymentBasis2_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
		{
			RefreshControls();
		}

		protected void RefreshControls()
		{
			bool isUnemployed = (rcbCurrentEmploymentBasis.SelectedValue == "Unemployed");
			tBodyEmploymentDetails.Visible = !isUnemployed;
			bool isUnemployed2 = (rcbCurrentEmploymentBasis2.SelectedValue == "Unemployed");
			tBodyEmploymentDetails2.Visible = !isUnemployed2;

		}

        protected void btnSameAs1_Click(object sender, EventArgs e)
        {
            rcbCurrentEmploymentBasis2.SelectedValue = rcbCurrentEmploymentBasis.SelectedValue;
            rtbOccupation2.Text = rtbOccupation.Text;
            rtbCurrentEmployer2.Text = rtbCurrentEmployer.Text;
            rtbCurrentEmployerPhone2.Text = rtbCurrentEmployerPhone.Text;
            rtbEmplAddressLine12.Text = rtbEmplAddressLine1.Text;
            rtbEmplAddressLine22.Text = rtbEmplAddressLine2.Text;
            rcbEmplSuburb2.SelectedValue = rcbEmplSuburb.SelectedValue;
            rcbEmplSuburb2.Text = rcbEmplSuburb.Text;
            rmypEmplSince2.SelectedDate = rmypEmplSince.SelectedDate;
        }
	}
}