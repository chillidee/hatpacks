﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;
using System.Net.Mail;

namespace WebApplicationForms
{
  public partial class SendWebApplicationFormEmail : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      string GUID = Request.QueryString["GUID"];
      MMApplication a = new MMApplication(GUID);
      a.LoadApplicants();
      Session["Application"] = a;
      //checking client's email...
      if (string.IsNullOrEmpty(a.MMApplicant1.Email))
      {
        literal.Text = "Recipient's email is empty. Unable to send.";
      }
      else
      {
        EmailAgent ea = new EmailAgent();
        ea.SendWebApplicationFormEmail(a);
        //Response.Write(ea.Body);
        if (ea.Sent)
        {          
          literal.Text = "Email sent to " + a.MMApplicant1.FirstName + " " + a.MMApplicant1.Surname +
            ".<br/>Email address: " + a.MMApplicant1.Email + "<br/><br/>" +
            "A copy (by CC) was also sent to the Sales Manager " + a.SalesManager + ".";
        }
      }
    }
  }
}