﻿<%@ Page Title="Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationForm-02.aspx.cs" Inherits="WebApplicationForms.WebForm_A02" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

	<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Your Application</h1>
				<h2>Personal Details</h2>
			</hgroup>
			<p>
				Your current residential and postal addresses.</p>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
		<asp:SqlDataSource ID="sqlStreetType" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT '' StreetType, '' StreetTypeDisplay UNION SELECT * FROM [StreetType]"></asp:SqlDataSource>
		<asp:SqlDataSource ID="sqlPCode" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT [PCodeID], [Combo] FROM [vwPCode] ORDER BY [SUBURB], [PCODE] DESC"></asp:SqlDataSource>
		<asp:SqlDataSource ID="sqlResidentialStatus" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT '' [Value], '' [Status] UNION SELECT [Value], [Status] FROM [ResidentialStatus] WHERE DisplayOnWeb = 1"></asp:SqlDataSource>

		<h3>Where do you live now?</h3>

		<table class="splitTable">
			<tr>
				<td class="split">
					<h4><asp:Literal ID="litAp1Firstname1" runat="server"></asp:Literal></h4>
					<table class="fieldset">
						<tr>
							<td class="fieldLabel">Unit No</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxHomeUnitNumber" runat="server" Width="60px"></telerik:RadTextBox>
							</td>
							<td class="fieldLabel">Street No</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxHomeStreetNumber" runat="server" Width="60px"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Street Name</td>
							<td colspan="3">
								<telerik:RadTextBox ID="RadTextBoxHomeStreetName" runat="server" Width="200px"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Street Type</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxHomeStreetType" runat="server" DataSourceID="sqlStreetType" Width="200px" DataTextField="StreetType" DataValueField="StreetType" OffsetY="10" AllowCustomText="True" EnableAutomaticLoadOnDemand="True"></telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State & Post Code</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxHomeSuburb" runat="server"
									DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID"
									ShowMoreResultsBox="True" EnableVirtualScrolling="True"
									EnableAutomaticLoadOnDemand="True" ItemsPerRequest="20" Height="200px" Width="200px"
									DropDownWidth="370px" MarkFirstMatch="True" OffsetY="10" EmptyMessage=""
									OnClientItemsRequesting="RadComboBoxHomeSuburb_OnClientItemsRequestingHandler">
								</telerik:RadComboBox>
								<script>
									function RadComboBoxHomeSuburb_OnClientItemsRequestingHandler(sender, eventArgs) {
										if (sender.get_text().length < 2) {
											eventArgs.set_cancel(true);
										}
									}
								</script>
								<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required." ControlToValidate="RadComboBoxHomeSuburb"></asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">There Since</td>
							<td colspan="3">
								<telerik:RadMonthYearPicker ID="RadMonthYearPickerHereSince" runat="server" ZIndex="30001" ShowPopupOnFocus="True"></telerik:RadMonthYearPicker>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Residential Status</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxResidentialStatus" runat="server" AllowCustomText="false" DataSourceID="sqlResidentialStatus" DataTextField="Status" DataValueField="Value" OffsetY="10" Width="200px">
								</telerik:RadComboBox>
							</td>
						</tr>
					</table>
				</td>
				<td class="split" runat="server" id="tdP2_1">
					<h4>
						<asp:Literal ID="litAp2Firstname1" runat="server"></asp:Literal>
					</h4>
					<table class="fieldset" runat="server" id="tableApplicant2_1">
						<tr>
							<td></td>
							<td colspan="3">
                                <asp:Button runat="server" ID="btnResidentialAddressSameAs1" Text="same as main contact" CausesValidation="false" OnClick="btnResidentialAddressSameAs1_Click" />
								<asp:CheckBox ID="cbResidentialAddressSameAs1" runat="server" CausesValidation="false" Text="same as main contact" AutoPostBack="True" OnCheckedChanged="cbResidentialAddressSameAs1_CheckedChanged" Visible="false" />
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Unit No</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxHomeUnitNumber2" runat="server" Width="60px"></telerik:RadTextBox>
							</td>
							<td class="fieldLabel">Street No</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxHomeStreetNumber2" runat="server" Width="60px"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Street Name</td>
							<td colspan="3">
								<telerik:RadTextBox ID="RadTextBoxHomeStreetName2" runat="server" Width="200px"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Street Type</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxHomeStreetType2" runat="server" DataSourceID="sqlStreetType" Width="200px" DataTextField="StreetType" DataValueField="StreetType" OffsetY="10" AllowCustomText="True" EnableAutomaticLoadOnDemand="True"></telerik:RadComboBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State & Post Code</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxHomeSuburb2" runat="server"
									DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID"
									ShowMoreResultsBox="True" EnableVirtualScrolling="True"
									EnableAutomaticLoadOnDemand="True" ItemsPerRequest="20" Height="200px" Width="200px"
									DropDownWidth="370px" MarkFirstMatch="True" OffsetY="10" EmptyMessage=""
									OnClientItemsRequesting="RadComboBoxHomeSuburb2_OnClientItemsRequestingHandler">
								</telerik:RadComboBox>
								<script>
									function RadComboBoxHomeSuburb2_OnClientItemsRequestingHandler(sender, eventArgs) {
										if (sender.get_text().length < 2) {
											eventArgs.set_cancel(true);
										}
									}
								</script>
								<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required." ControlToValidate="RadComboBoxHomeSuburb2"></asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">There Since</td>
							<td colspan="3">
								<telerik:RadMonthYearPicker ID="RadMonthYearPickerHereSince2" runat="server" ShowPopupOnFocus="True" ZIndex="30001">
								</telerik:RadMonthYearPicker>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Residential Status</td>
							<td colspan="3">
								<telerik:RadComboBox ID="RadComboBoxResidentialStatus2" runat="server" DataSourceID="sqlResidentialStatus" DataTextField="Status" DataValueField="Value" OffsetY="10" Width="200px">
								</telerik:RadComboBox>
							</td>
						</tr>
					</table>


				</td>
			</tr>
		</table>

		<h3>How do you receive mail? (postal address)</h3>
        <asp:Button runat="server" ID="btnPostalAddressSameAs" Text="same as residential address" CausesValidation="false" OnClick="btnPostalAddressSameAs_Click" />
		<asp:CheckBox ID="cbPostalAddressSameAs" runat="server" CausesValidation="false" Text="same as residential address" AutoPostBack="True" OnCheckedChanged="cbPostalAddressSameAs_CheckedChanged" Visible="false"/>

		<table class="splitTable">
			<tr>
				<td class="split">

										<h4><asp:Literal ID="litAp1Firstname2" runat="server"></asp:Literal></h4>

					<table class="fieldset">
						<tr>
							<td class="fieldLabel">Postal address line 1</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxPostalAddressLine1" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Postal address line 2</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxPostalAddressLine2" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State & Post Code</td>
							<td>
								<telerik:RadComboBox ID="RadComboBoxPostalSuburb" runat="server"
									DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID"
									ShowMoreResultsBox="True" EnableVirtualScrolling="True"
									EnableAutomaticLoadOnDemand="True" ItemsPerRequest="20" Height="200px" Width="200px"
									DropDownWidth="370px" MarkFirstMatch="True" OffsetY="10" EmptyMessage=""
									OnClientItemsRequesting="RadComboBoxPostalSuburb_OnClientItemsRequestingHandler">
								</telerik:RadComboBox>
								<script>
									function RadComboBoxPostalSuburb_OnClientItemsRequestingHandler(sender, eventArgs) {
										if (sender.get_text().length < 2) {
											eventArgs.set_cancel(true);
										}
									}
								</script>
								<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required." ControlToValidate="RadComboBoxPostalSuburb"></asp:RequiredFieldValidator>
							</td>
						</tr>
					</table>

				</td>
				<td class="split" runat="server" id="tdP2_2">
					<h4>
						<asp:Literal ID="litAp2Firstname2" runat="server"></asp:Literal>
					</h4>
					<table class="fieldset" runat="server" id="tableApplicant2_2">
						<tr>
							<td></td>
							<td colspan="3">
                                <asp:Button runat="server" ID="btnPostalAddressSameAs1" Text="same as main contact" CausesValidation="false" OnClick="btnPostalAddressSameAs1_Click" />
								<asp:CheckBox ID="cbPostalAddressSameAs1" runat="server" CausesValidation="false" Text="same as main contact" AutoPostBack="True" OnCheckedChanged="cbPostalAddressSameAs1_CheckedChanged" Visible="false"/>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Postal address line 1</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxPostalAddressLine12" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Postal address line 2</td>
							<td>
								<telerik:RadTextBox ID="RadTextBoxPostalAddressLine22" runat="server"></telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State & Post Code</td>
							<td>
								<telerik:RadComboBox ID="RadComboBoxPostalSuburb2" runat="server"
									DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID"
									ShowMoreResultsBox="True" EnableVirtualScrolling="True"
									EnableAutomaticLoadOnDemand="True" ItemsPerRequest="20" Height="200px" Width="200px"
									DropDownWidth="370px" MarkFirstMatch="True" OffsetY="10" EmptyMessage=""
									OnClientItemsRequesting="RadComboBoxPostalSuburb2_OnClientItemsRequestingHandler">
								</telerik:RadComboBox>
								<script>
									function RadComboBoxPostalSuburb2_OnClientItemsRequestingHandler(sender, eventArgs) {
										if (sender.get_text().length < 2) {
											eventArgs.set_cancel(true);
										}
									}
								</script>
								<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required." ControlToValidate="RadComboBoxPostalSuburb"></asp:RequiredFieldValidator>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>






		
		<div style="text-align: right">
			<asp:LinkButton runat="server" ID="btnBack" OnClick="btnBack_Click">go back</asp:LinkButton>
			<asp:Button ID="btnSubmit" Text="next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
		</div>
	</telerik:RadAjaxPanel>
</asp:Content>
