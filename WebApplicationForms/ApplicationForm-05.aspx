﻿<%@ Page Title="Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationForm-05.aspx.cs" Inherits="WebApplicationForms.WebForm_A05" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	<style type="text/css">
.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox_Default{font:12px "Segoe UI",Arial,sans-serif;color:#333}.RadComboBox{vertical-align:middle;display:-moz-inline-stack;display:inline-block}.RadComboBox{text-align:left}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}.RadComboBox *{margin:0;padding:0}
		</style>
	</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

	<section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Your Application</h1>
				<h2>Personal Details (Part 5)</h2>
			</hgroup>
			<p>Your employment details.</p>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">		
		<asp:SqlDataSource ID="sqlPCode" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT [PCodeID], [Combo] FROM [vwPCode] ORDER BY [SUBURB], [PCODE] DESC"></asp:SqlDataSource>		
		<asp:SqlDataSource ID="sqlEmploymentBasis" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="(SELECT '' EmploymentBasis, 0 IsSelfEmployed) UNION (SELECT EmploymentBasis, IsSelfEmployed FROM EmploymentBasis) ORDER BY EmploymentBasis"></asp:SqlDataSource>
		<h3>Who is your current employer?</h3>

		<table class="splitTable">
			<tr>
				<td class="split">
					<h4><asp:Literal ID="litAp1Firstname1" runat="server"></asp:Literal></h4>
					<table class="fieldset">
						<tr>
							<td class="fieldLabel">Employment Basis</td>
							<td>
								<telerik:RadComboBox ID="rcbCurrentEmploymentBasis" runat="server" AllowCustomText="False" DataSourceID="sqlEmploymentBasis" DataTextField="EmploymentBasis" DataValueField="EmploymentBasis" OffsetY="10" Width="160px" AutoPostBack="True" OnSelectedIndexChanged="rcbCurrentEmploymentBasis_SelectedIndexChanged">
								</telerik:RadComboBox>
							</td>
						</tr>
						<tbody runat="server" id="tBodyEmploymentDetails">
						<tr>
							<td class="fieldLabel">Position/Title</td>
							<td>
								<telerik:RadTextBox ID="rtbOccupation" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Employer</td>
							<td>

								<telerik:RadTextBox ID="rtbCurrentEmployer" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Contact Phone Number</td>
							<td>

								<telerik:RadTextBox ID="rtbCurrentEmployerPhone" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 1</td>
							<td>
								<telerik:RadTextBox ID="rtbEmplAddressLine1" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 2</td>
							<td>
								<script>
									function RadComboBoxHomeSuburb_OnClientItemsRequestingHandler(sender, eventArgs) {
										if (sender.get_text().length < 2) {
											eventArgs.set_cancel(true);
										}
									}
								</script>
								<telerik:RadTextBox ID="rtbEmplAddressLine2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State &amp; Post Code</td>
							<td>
								<telerik:RadComboBox ID="rcbEmplSuburb" runat="server" DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID" DropDownWidth="370px" EmptyMessage="" EnableAutomaticLoadOnDemand="True" EnableVirtualScrolling="True" Height="200px" ItemsPerRequest="20" MarkFirstMatch="True" OffsetY="10" OnClientItemsRequesting="RadComboBoxHomeSuburb_OnClientItemsRequestingHandler" ShowMoreResultsBox="True" Width="160px">
								</telerik:RadComboBox>
							</td>
						</tr>
						</tbody>
						<tr>
							<td class="fieldLabel">Since</td>
							<td>
								<telerik:RadMonthYearPicker ID="rmypEmplSince" runat="server" ShowPopupOnFocus="True" ZIndex="30001"></telerik:RadMonthYearPicker>
							</td>
						</tr>
					</table>
				</td>
				<td class="split" runat="server" id="tdP2_1">
					<h4>
						<asp:Literal ID="litAp2Firstname1" runat="server"></asp:Literal>
					</h4>
					<table class="fieldset">
						<tr>
							<td></td>
							<td colspan="3">
                                <asp:Button runat="server" ID="btnSameAs1" Text="same as main contact" CausesValidation="false" OnClick="btnSameAs1_Click" />
								<asp:CheckBox ID="cbSameAs1" runat="server" CausesValidation="false" Text="same as main contact" AutoPostBack="True" OnCheckedChanged="cbSameAs1_CheckedChanged" Visible="false" /></td>
						</tr>						
						<tr>
							<td class="fieldLabel">Employment Basis</td>
							<td>
								<telerik:RadComboBox ID="rcbCurrentEmploymentBasis2" runat="server" AllowCustomText="False" DataSourceID="sqlEmploymentBasis" DataTextField="EmploymentBasis" DataValueField="EmploymentBasis" OffsetY="10" Width="160px" AutoPostBack="true" OnSelectedIndexChanged="rcbCurrentEmploymentBasis2_SelectedIndexChanged">
								</telerik:RadComboBox>
							</td>
						</tr>										
						<tbody runat="server" id="tBodyEmploymentDetails2">
						<tr>
							<td class="fieldLabel">Position/Title</td>
							<td>
								<telerik:RadTextBox ID="rtbOccupation2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>						
						<tr>
							<td class="fieldLabel">Employer</td>
							<td>

								<telerik:RadTextBox ID="rtbCurrentEmployer2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Contact Phone Number</td>
							<td>

								<telerik:RadTextBox ID="rtbCurrentEmployerPhone2" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 1</td>
							<td>
								<telerik:RadTextBox ID="rtbEmplAddressLine12" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Address Line 2</td>
							<td>
								<telerik:RadTextBox ID="rtbEmplAddressLine22" Runat="server">
								</telerik:RadTextBox>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel">Suburb, State &amp; Post Code</td>
							<td>
								<telerik:RadComboBox ID="rcbEmplSuburb2" runat="server" DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID" DropDownWidth="370px" EmptyMessage="" EnableAutomaticLoadOnDemand="True" EnableVirtualScrolling="True" Height="200px" ItemsPerRequest="20" MarkFirstMatch="True" OffsetY="10" OnClientItemsRequesting="RadComboBoxHomeSuburb_OnClientItemsRequestingHandler" ShowMoreResultsBox="True" Width="160px">
								</telerik:RadComboBox>
							</td>
						</tr>		
						</tbody>
						<tr>
							<td class="fieldLabel">Since</td>
							<td>
								<telerik:RadMonthYearPicker ID="rmypEmplSince2" runat="server" ShowPopupOnFocus="True" ZIndex="30001">
								</telerik:RadMonthYearPicker>
							</td>
						</tr>
					</table>


				</td>
			</tr>
		</table>


		
		<div style="text-align: right">
			<asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click">go back</asp:LinkButton>
			<asp:Button ID="btnSubmit" Text="next" CssClass="main" runat="server" OnClick="btnSubmit_Click" />
		</div>
	</telerik:RadAjaxPanel>
</asp:Content>
