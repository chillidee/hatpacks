﻿using EchosignRESTClient.Models;
using EchosignRESTClient.Parameters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Web;
using WebApplicationForms.Models;
using System.Linq;

namespace WebApplicationForms
{
    public partial class SendWebApplicationFormEmailBM : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AdobeEchoSign();
        }

        private void AdobeEchoSign()
        {
            string GUID = Request.QueryString["GUID"];
            BMApplicant bm = new BMApplicant(GUID);

            Session["BMApplicant"] = bm;
            //checking client's email...
            if (string.IsNullOrEmpty(bm.Email))
            {
                literal.Text = "Recipient's email is empty. Unable to send.";
            }
            else
            {
                MMUser ApplicationManager = new MMUser(bm.ApplicationManager);

                // ClientApp and JsonData location
                String clientAppPath = Path.Combine(HttpRuntime.AppDomainAppPath, "AdobeEchoSignClientJB");
                String jsonDataPath = Path.Combine(clientAppPath, "JsonDataParameter");

                //Prepare data
                JsonData jsonData = new JsonData();
                jsonData.companyID = bm.CompanyID.Value;
                jsonData.ccs = new List<string> { ApplicationManager.Email };
                jsonData.memberInfos = new List<ParticipantInfo>() 
                { 
                    new ParticipantInfo() { email = bm.Email },                    
                };
                jsonData.mergeFieldInfo = new List<MergefieldInfo>()
                {
                    new MergefieldInfo() { defaultValue = bm.FirstName + " " + bm.Surname, fieldName = "customerName" },
                    new MergefieldInfo() { defaultValue = bm.ApplicantID.ToString(), fieldName = "customerReference" },
                    new MergefieldInfo() { defaultValue = bm.Surname, fieldName = "customerSurname" },
                    new MergefieldInfo() { defaultValue = bm.FirstName, fieldName = "customerGivenName" },
                    new MergefieldInfo() { defaultValue = bm.Mobile, fieldName = "customerMobile" },
                    new MergefieldInfo() { defaultValue = bm.FirstName + " " + bm.Surname, fieldName = "accountHolderName" }               
                };

                //Just in case of not existing the json param repository
                if (!Directory.Exists(jsonDataPath))
                    Directory.CreateDirectory(jsonDataPath);

                //Create file named in this format "JsonDataParameter/yyyyMMdd_HHmmss_ApplicantID_GUID.json"
                String jsonDataFilePath = Path.Combine(jsonDataPath, string.Format("{0}_{1}_{2}.json", DateTime.Now.ToString("yyyyMMdd_HHmmss"), bm.ApplicantID, GUID));

                using (StreamWriter sw = new StreamWriter(jsonDataFilePath))
                {
                    sw.Write(JsonConvert.SerializeObject(jsonData));
                }

                //Call the ClientApp that consumes Adobe Echo Sign API - necessary to inform action & json param file
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = Path.Combine(clientAppPath, "ClientApp.exe"); //The source code of this app is in Bitbucket with the others websites
                startInfo.Arguments = string.Format("action={0} json=\"{1}\"", "SendAgreement", jsonDataFilePath);
                Process.Start(startInfo);

                if (true) //The ClientApp has to return something to confirm that those emails were sent
                {
                    literal.Text = "Email sent to " + bm.FirstName + " " + bm.Surname + ".<br/>Email address: " + bm.Email + "<br/><br/>" +
                                    "A copy (by CC) was also sent to the Application Manager " + bm.ApplicationManager + " (" + ApplicationManager.Email + ").";
                }
            }
        }
    }
}