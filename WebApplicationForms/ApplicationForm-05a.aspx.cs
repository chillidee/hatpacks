﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;


namespace WebApplicationForms
{
	public partial class WebForm_A05a : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				bool Ap1NeedPreviousEmployer = false;
				bool Ap2NeedPreviousEmployer = false;

				//initialising controls...

				//getting data...
				MMApplication a;
				if ((Session["Application"] == null) && HttpContext.Current.Request.IsLocal)
				{
					a = new MMApplication("CFE209842ECB48F4AD1E47BB40529E1F");
					a.LoadApplicants();
					Session["Application"] = a;
				}
				else
				{ a = (MMApplication)Session["Application"]; }

				MMApplicant ap1 = a.MMApplicant1;

				//checking to see if the applicant has resided for greater than MinimumMonthsToRequirePreviousAddress
				if (ap1.EmplSince != null)
				{
					int months = Convert.ToInt32(Microsoft.VisualBasic.DateAndTime.DateDiff("m", (DateTime)ap1.EmplSince, DateTime.Now));
					Ap1NeedPreviousEmployer = months <= Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MinimumMonthsToRequirePreviousEmployment"]);
				}

				//Populating controls...				
				
				litAp1Firstname2.Text = ap1.FirstName;

				rcbPreviousEmploymentBasis.SelectedValue = ap1.PreviousEmploymentBasis;
				rtbPreviousOccupation.Text = ap1.PreviousOccupation;
				rtbPreviousEmployer.Text = ap1.PreviousEmployer;
				rtbPreviousEmployerPhone.Text = ap1.PreviousEmployerPhone;
				rtbPrevEmplAddLine1.Text = ap1.PrevEmplAddLine1;
				rtbPrevEmplAddLine2.Text = ap1.PrevEmplAddLine2;
				rcbPrevEmplSuburb.SelectedValue = ap1.PrevEmplSuburb.ToString();
				rcbPrevEmplSuburb.Text = ap1.PrevEmplSuburbText;
				rmypPrevEmplSince.SelectedDate = ap1.PrevEmplSince;				

				try
				{
					if (a.MMApplicant2 != null)
					{
						MMApplicant ap2 = a.MMApplicant2;

						//checking to see if the applicant has resided for greater than MinimumMonthsToRequirePreviousAddress
						if (ap2.EmplSince != null)
						{
							int months = Convert.ToInt32(Microsoft.VisualBasic.DateAndTime.DateDiff("m", (DateTime)ap2.EmplSince, DateTime.Now));
							Ap2NeedPreviousEmployer = months <= Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MinimumMonthsToRequirePreviousEmployment"]);
						}

						//populating controls...						
						litAp2Firstname2.Text = ap2.FirstName;

						rcbPreviousEmploymentBasis2.SelectedValue = ap2.PreviousEmploymentBasis;
						rtbPreviousOccupation2.Text = ap2.PreviousOccupation;
						rtbPreviousEmployer2.Text = ap2.PreviousEmployer;
						rtbPreviousEmployerPhone2.Text = ap2.PreviousEmployerPhone;
						rtbPrevEmplAddLine12.Text = ap2.PrevEmplAddLine1;
						rtbPrevEmplAddLine22.Text = ap2.PrevEmplAddLine2;
						rcbPrevEmplSuburb2.SelectedValue = ap2.PrevEmplSuburb.ToString();
						rcbPrevEmplSuburb2.Text = ap2.PrevEmplSuburbText;
						rmypPrevEmplSince2.SelectedDate = ap2.PrevEmplSince;
					}
					else
					{						
						tdP2_2.Visible = false;
						NameLabelsVisible(false);
					}
				}
				catch { }

				if (!Ap1NeedPreviousEmployer && !Ap2NeedPreviousEmployer)
				{
					string previousPage = Request.UrlReferrer.AbsolutePath;
					string navigateToPage = "";
					switch (previousPage)
					{
						case "/ApplicationForm-05.aspx": navigateToPage = "/ApplicationForm-06.aspx"; break;
						case "/ApplicationForm-06.aspx": navigateToPage = "/ApplicationForm-05.aspx"; break;
						default: navigateToPage = "/Application-06.aspx"; break;
					}
					Response.Redirect(navigateToPage);
				}

                rcbPreviousEmploymentBasis.Visible = Ap1NeedPreviousEmployer;
                rcbPrevEmplSuburb.Visible = Ap1NeedPreviousEmployer;
                rmypPrevEmplSince.Visible = Ap1NeedPreviousEmployer;
                lblAp1Msg.Visible = !Ap1NeedPreviousEmployer;
				tblAp1.Visible = Ap1NeedPreviousEmployer;


				lblAp2Msg.Visible = !Ap2NeedPreviousEmployer;
				tblAp2.Visible = Ap2NeedPreviousEmployer;
				//cbSameAs12.Visible = Ap1NeedPreviousEmployer;
				

				
			}
		}


		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				Save();
				Response.Redirect("ApplicationForm-06.aspx");
			}
		}

		protected void Save()
		{
			MMApplication a = new MMApplication();
			if (Session["Application"] != null)
			{
				a = (MMApplication)Session["Application"];
			}
			else
			{
				throw new Exception("No Application Session, or timed out");
			}

			if (a.MMApplicant1 != null)
			{

			}
			else
			{
				//most likely, the session variable has expired. Lets re-load from the Database...
				throw new Exception("Unable to load Applicant object");
			}
			//Collecting ap1 data from the form...

			a.MMApplicant1.PreviousEmploymentBasis = rcbPreviousEmploymentBasis.Text;
			a.MMApplicant1.PreviousOccupation = rtbPreviousOccupation.Text;
			a.MMApplicant1.PreviousEmployer = rtbPreviousEmployer.Text;
			a.MMApplicant1.PreviousEmployerPhone = rtbPreviousEmployerPhone.Text;
			a.MMApplicant1.PrevEmplAddLine1 = rtbPrevEmplAddLine1.Text;
			a.MMApplicant1.PrevEmplAddLine2 = rtbPrevEmplAddLine2.Text;
			a.MMApplicant1.PrevEmplSuburb = Utils.EmptyToNull(rcbPrevEmplSuburb.SelectedValue);
			a.MMApplicant1.PrevEmplSince = rmypPrevEmplSince.SelectedDate;
      if (a.MMApplicant1.PrevEmplSince != null)
      {
        TimeSpan ts = DateTime.Now - (DateTime)a.MMApplicant1.PrevEmplSince;
        a.MMApplicant1.PrevEmplDurationYears = (Int32)ts.Days / 365;
        a.MMApplicant1.PrevEmplDurationMnths = (Int32)((ts.Days % 365) / 31);
      }
			a.MMApplicant1.Save();

			if (tdP2_2.Visible)
			{
				if (a.MMApplicant2 != null)
				{
				}
				else
				{
				}

				//Collecting ap2 data from form...

				a.MMApplicant2.PreviousEmploymentBasis = rcbPreviousEmploymentBasis2.Text;
				a.MMApplicant2.PreviousOccupation = rtbPreviousOccupation2.Text;
				a.MMApplicant2.PreviousEmployer = rtbPreviousEmployer2.Text;
				a.MMApplicant2.PreviousEmployerPhone = rtbPreviousEmployerPhone2.Text;
				a.MMApplicant2.PrevEmplAddLine1 = rtbPrevEmplAddLine12.Text;
				a.MMApplicant2.PrevEmplAddLine2 = rtbPrevEmplAddLine22.Text;
				a.MMApplicant2.PrevEmplSuburb = Utils.EmptyToNull(rcbPrevEmplSuburb2.SelectedValue);
				a.MMApplicant2.PrevEmplSince = rmypPrevEmplSince2.SelectedDate;
        if (a.MMApplicant2.PrevEmplSince != null)
        {
          TimeSpan ts = DateTime.Now - (DateTime)a.MMApplicant2.PrevEmplSince;
          a.MMApplicant2.PrevEmplDurationYears = (Int32)ts.Days / 365;
          a.MMApplicant2.PrevEmplDurationMnths = (Int32)((ts.Days % 365) / 31);
        }
				a.MMApplicant2.Save();
			}

			Session["Application"] = a;
		}




		protected void cbSameAs12_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (cb.Checked)
			{
				rcbPreviousEmploymentBasis2.SelectedValue = rcbPreviousEmploymentBasis.SelectedValue;
				rtbPreviousOccupation2.Text = rtbPreviousOccupation.Text;
				rtbPreviousEmployer2.Text = rtbPreviousEmployer.Text;
				rtbPreviousEmployerPhone2.Text = rtbPreviousEmployerPhone.Text;
				rtbPrevEmplAddLine12.Text = rtbPrevEmplAddLine1.Text;
				rtbPrevEmplAddLine22.Text = rtbPrevEmplAddLine2.Text;
				rcbPrevEmplSuburb2.SelectedValue = rcbPrevEmplSuburb.SelectedValue;
				rcbPrevEmplSuburb2.Text = rcbPrevEmplSuburb.Text;
				rmypPrevEmplSince2.SelectedDate = rmypPrevEmplSince.SelectedDate;
			}
			else
			{
				rcbPreviousEmploymentBasis2.ClearSelection();
				rtbPreviousOccupation2.Text = String.Empty;
				rtbPreviousEmployer2.Text = String.Empty;
				rtbPreviousEmployerPhone2.Text = String.Empty;
				rtbPrevEmplAddLine12.Text = String.Empty;
				rtbPrevEmplAddLine22.Text = String.Empty;
				rcbPrevEmplSuburb2.ClearSelection();
				rcbPrevEmplSuburb2.Text = String.Empty;
				rmypPrevEmplSince2.Clear();

			}

		}
		protected void btnBack_Click(object sender, EventArgs e)
		{
			Save();
			Response.Redirect("ApplicationForm-05.aspx");
		}
		protected void NameLabelsVisible(bool visible)
		{			
			litAp1Firstname2.Visible = visible;			
			litAp2Firstname2.Visible = visible;
		}

        protected void btnSameAs12_Click(object sender, EventArgs e)
        {
            rcbPreviousEmploymentBasis2.SelectedValue = rcbPreviousEmploymentBasis.SelectedValue;
            rtbPreviousOccupation2.Text = rtbPreviousOccupation.Text;
            rtbPreviousEmployer2.Text = rtbPreviousEmployer.Text;
            rtbPreviousEmployerPhone2.Text = rtbPreviousEmployerPhone.Text;
            rtbPrevEmplAddLine12.Text = rtbPrevEmplAddLine1.Text;
            rtbPrevEmplAddLine22.Text = rtbPrevEmplAddLine2.Text;
            rcbPrevEmplSuburb2.SelectedValue = rcbPrevEmplSuburb.SelectedValue;
            rcbPrevEmplSuburb2.Text = rcbPrevEmplSuburb.Text;
            rmypPrevEmplSince2.SelectedDate = rmypPrevEmplSince.SelectedDate;
        }

	}
}