﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplicationForms.Models
{
  public class MMCompany
  {
    public int CompanyID { get; set; }
    public string Company { get; set; }
    public string FullCompanyName { get; set; }
    public string Acronym { get; set; }
    public string ABN { get; set; }
    public string CreditLicNo { get; set; }
    public Nullable<byte> SortOrder { get; set; }
    public string Phone { get; set; }
    public string Fax { get; set; }
    public string AddressLine1 { get; set; }
    public string AddressLine2 { get; set; }
    public string Suburb { get; set; }
    public string PostCode { get; set; }
    public string State { get; set; }
    public string Email { get; set; }
    public string LoanWriter { get; set; }
    public string AccountBank { get; set; }
    public string AccountName { get; set; }
    public string AccountBSB { get; set; }
    public string AccountNo { get; set; }
    public string ACN { get; set; }
    public Nullable<bool> IsLead { get; set; }
    public string WebApplicationLogoImageFilename { get; set; }
    public string WebsiteURL { get; set; }
    public string FacebookURL { get; set; }
    public string TwitterURL { get; set; }
    public string YouTubeURL { get; set; }
    public string AboutUsURL { get; set; }
    public string ContactUsURL { get; set; }
    public string LinkedInURL { get; set; }
    public Nullable<int> DefaultCallStatusID { get; set; }
    public Nullable<int> DefaultLoanPurposeID { get; set; }
    public string GoogleAdWordsConversionScript { get; set; }
    public string GoogleAnalyticsScript { get; set; }
    public string WebEnquiryFormSuccessURL { get; set; }
    public string WebEnquiryFormFailureURL { get; set; }
    public Boolean WebEnquiryFormShowLoanAmount { get; set; }
    public Boolean WebEnquiryFormShowLoanType { get; set; }
    public Boolean WebEnquiryFormShowHasProperty { get; set; }
    public Boolean WebEnquiryFormShowComments { get; set; }
    public Boolean WebEnquiryFormShowLastname { get; set; }
    public Boolean WebEnquiryFormShowMobile { get; set; }
    public Boolean WebEnquiryFormShowLandline { get; set; }
    public Boolean WebEnquiryFormShowEmail { get; set; }
    public Boolean WebEnquiryFormShowStatePostCode { get; set; }
    public Boolean WebEnquiryFormShowSuburb { get; set; }
    public Boolean WebEnquiryFormShowTitle { get; set; }
    public Boolean WebEnquiryFormShowFirstname { get; set; }
    public Boolean WebEnquiryFormShowWhereDidYouHear { get; set; }

    public Boolean WebEnquiryFormShowLoanAmount_M { get; set; }
    public Boolean WebEnquiryFormShowLoanType_M { get; set; }
    public Boolean WebEnquiryFormShowHasProperty_M { get; set; }
    public Boolean WebEnquiryFormShowComments_M { get; set; }
    public Boolean WebEnquiryFormShowLastname_M { get; set; }
    public Boolean WebEnquiryFormShowMobile_M { get; set; }
    public Boolean WebEnquiryFormShowLandline_M { get; set; }
    public Boolean WebEnquiryFormShowEmail_M { get; set; }
    public Boolean WebEnquiryFormShowStatePostCode_M { get; set; }
    public Boolean WebEnquiryFormShowSuburb_M { get; set; }
    public Boolean WebEnquiryFormShowTitle_M { get; set; }
    public Boolean WebEnquiryFormShowFirstname_M { get; set; }
    public Boolean WebEnquiryFormShowWhereDidYouHear_M { get; set; }
    public Nullable<int> DefaultErrorCallStatusID { get; set; }

    public MMCompany(Int32 ID)
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand("SELECT * FROM Company WHERE CompanyID = " + ID.ToString(), conn);
        conn.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        _load(dr);
      }
    }

    public MMCompany(string CompanyCode)
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand("SELECT * FROM Company WHERE Acronym = '" + CompanyCode + "'", conn);
        conn.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        _load(dr);
      }
    }

    private void _load(SqlDataReader dr)
    {
      while (dr.Read())
      {
        CompanyID = (dr["CompanyID"] as Int32?) ?? 0;
        Company = Convert.ToString(dr["Company"]);
        FullCompanyName = Convert.ToString(dr["FullCompanyName"]);
        Acronym = Convert.ToString(dr["Acronym"]);
        ABN = Convert.ToString(dr["ABN"]);
        CreditLicNo = Convert.ToString(dr["CreditLicNo"]);
        SortOrder = (dr["SortOrder"] as Byte?) ?? null;
        Phone = Convert.ToString(dr["Phone"]);
        Fax = Convert.ToString(dr["Fax"]);
        AddressLine1 = Convert.ToString(dr["AddressLine1"]);
        AddressLine2 = Convert.ToString(dr["AddressLine2"]);
        Suburb = Convert.ToString(dr["Suburb"]);
        PostCode = Convert.ToString(dr["PostCode"]);
        State = Convert.ToString(dr["State"]);
        Email = Convert.ToString(dr["Email"]);
        LoanWriter = Convert.ToString(dr["LoanWriter"]);
        AccountBank = Convert.ToString(dr["AccountBank"]);
        AccountName = Convert.ToString(dr["AccountName"]);
        AccountBSB = Convert.ToString(dr["AccountBSB"]);
        AccountNo = Convert.ToString(dr["AccountNo"]);
        ACN = Convert.ToString(dr["ACN"]);
        IsLead = (dr["IsLead"] as Boolean?) ?? null;
        WebApplicationLogoImageFilename = Convert.ToString(dr["WebApplicationLogoImageFilename"]);
        WebsiteURL = Convert.ToString(dr["WebsiteURL"]);
        FacebookURL = Convert.ToString(dr["FacebookURL"]);
        TwitterURL = Convert.ToString(dr["TwitterURL"]);
        YouTubeURL = Convert.ToString(dr["YouTubeURL"]);
        AboutUsURL = Convert.ToString(dr["AboutUsURL"]);
        ContactUsURL = Convert.ToString(dr["ContactUsURL"]);
        LinkedInURL = Convert.ToString(dr["LinkedInURL"]);
        DefaultCallStatusID = (dr["DefaultCallStatusID"] as Int32?) ?? null;
        DefaultLoanPurposeID = (dr["DefaultLoanPurposeID"] as Int32?) ?? null;
        GoogleAdWordsConversionScript = Convert.ToString(dr["GoogleAdWordsConversionScript"]);
        GoogleAnalyticsScript = Convert.ToString(dr["GoogleAnalyticsScript"]);
        WebEnquiryFormSuccessURL = Convert.ToString(dr["WebEnquiryFormSuccessURL"]);
        WebEnquiryFormFailureURL = Convert.ToString(dr["WebEnquiryFormFailureURL"]);
        WebEnquiryFormShowLoanAmount = Convert.ToBoolean(dr["WebEnquiryFormShowLoanAmount"]);
        WebEnquiryFormShowLoanType = Convert.ToBoolean(dr["WebEnquiryFormShowLoanType"]);
        WebEnquiryFormShowHasProperty = Convert.ToBoolean(dr["WebEnquiryFormShowHasProperty"]);
        WebEnquiryFormShowComments = Convert.ToBoolean(dr["WebEnquiryFormShowComments"]);
        WebEnquiryFormShowLastname = Convert.ToBoolean(dr["WebEnquiryFormShowLastname"]);
        WebEnquiryFormShowMobile = Convert.ToBoolean(dr["WebEnquiryFormShowMobile"]);
        WebEnquiryFormShowLandline = Convert.ToBoolean(dr["WebEnquiryFormShowLandline"]);
        WebEnquiryFormShowEmail = Convert.ToBoolean(dr["WebEnquiryFormShowEmail"]);
        WebEnquiryFormShowStatePostCode = Convert.ToBoolean(dr["WebEnquiryFormShowStatePostCode"]);
        WebEnquiryFormShowSuburb = Convert.ToBoolean(dr["WebEnquiryFormShowSuburb"]);
        WebEnquiryFormShowTitle = Convert.ToBoolean(dr["WebEnquiryFormShowTitle"]);
        WebEnquiryFormShowFirstname = Convert.ToBoolean(dr["WebEnquiryFormShowFirstname"]);
        WebEnquiryFormShowWhereDidYouHear = Convert.ToBoolean(dr["WebEnquiryFormShowWhereDidYouHear"]);

        WebEnquiryFormShowLoanAmount_M = Convert.ToBoolean(dr["WebEnquiryFormShowLoanAmount_M"]);
        WebEnquiryFormShowLoanType_M = Convert.ToBoolean(dr["WebEnquiryFormShowLoanType_M"]);
        WebEnquiryFormShowHasProperty_M = Convert.ToBoolean(dr["WebEnquiryFormShowHasProperty_M"]);
        WebEnquiryFormShowComments_M = Convert.ToBoolean(dr["WebEnquiryFormShowComments_M"]);
        WebEnquiryFormShowLastname_M = Convert.ToBoolean(dr["WebEnquiryFormShowLastname_M"]);
        WebEnquiryFormShowMobile_M = Convert.ToBoolean(dr["WebEnquiryFormShowMobile_M"]);
        WebEnquiryFormShowLandline_M = Convert.ToBoolean(dr["WebEnquiryFormShowLandline_M"]);
        WebEnquiryFormShowEmail_M = Convert.ToBoolean(dr["WebEnquiryFormShowEmail_M"]);
        WebEnquiryFormShowStatePostCode_M = Convert.ToBoolean(dr["WebEnquiryFormShowStatePostCode_M"]);
        WebEnquiryFormShowSuburb_M = Convert.ToBoolean(dr["WebEnquiryFormShowSuburb_M"]);
        WebEnquiryFormShowTitle_M = Convert.ToBoolean(dr["WebEnquiryFormShowTitle_M"]);
        WebEnquiryFormShowFirstname_M = Convert.ToBoolean(dr["WebEnquiryFormShowFirstname_M"]);
        WebEnquiryFormShowWhereDidYouHear_M = Convert.ToBoolean(dr["WebEnquiryFormShowWhereDidYouHear_M"]);
    

      }
    }
  }
}