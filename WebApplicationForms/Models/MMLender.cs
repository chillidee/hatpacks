﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplicationForms.Models
{
    public class MMLender
    {
        public int BankID    { get; set; }
        public string Bank { get; set; }
        public string Code { get; set; }
        public int DefaultCallStatusID { get; set; }



        public MMLender(string lenderCode)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select BankID,Bank,Code,DefaultCallStatusID from banks where code ='" + lenderCode + "'", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                _load(dr);
            }
        }

        private void _load(SqlDataReader dr)
        {
            while (dr.Read())
            {
                BankID = (dr["BankID"] as Int32?) ?? 0;
                Bank = Convert.ToString(dr["Bank"]);
                Code = Convert.ToString(dr["Code"]);
                DefaultCallStatusID = (dr["DefaultCallStatusID"] as Int32?) ?? 0;


            }
        }
    }
}