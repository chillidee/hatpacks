﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationForms.Models
{
    public class EnquiryFormInit
    {
        public List<string> fields {get;set;}
        public Dictionary<int, string> typeOfLoan{get;set;}
        public string sucessURL { get; set; }
        public string failureURL { get; set; }

        public EnquiryFormInit()
        {
          fields = new List<string> { }; 
          typeOfLoan = new Dictionary<int, string> { };
          sucessURL = string.Empty;
          failureURL = string.Empty;
        }

    }
}