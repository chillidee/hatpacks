﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WebApplicationForms.Models;

namespace WebApplicationForms.Models
{
  public class CCApplicant
  {
    public int ApplicantID { get; set; }
    public Nullable<int> CFStatusID { get; set; }
    public string Title { get; set; }
    public string Surname { get; set; }
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
    public string TelephoneHome { get; set; }
    public string TelephoneWork { get; set; }
    public string Mobile { get; set; }
    public string Fax { get; set; }
    public string Email { get; set; }
    public string PersonalRefName { get; set; }
    public string PersonalRefAddress { get; set; }
    public string PersonalRefRelationship { get; set; }
    public string PersonalRefPhone { get; set; }
    public Nullable<bool> HavingSpouse { get; set; }
    public string SpousesName { get; set; }
    public string Sex { get; set; }
    public Nullable<System.DateTime> DateOfBirth { get; set; }
    public string HomeUnitNumber { get; set; }
    public string HomeStreetNumber { get; set; }
    public string HomeStreetName { get; set; }
    public string HomeStreetType { get; set; }
    public Nullable<int> HomeSuburb { get; set; }
    public string HomeState { get; set; }
    public string PostalUnitNumber { get; set; }
    public string PostalStreetNumber { get; set; }
    public string PostalStreetName { get; set; }
    public string PostalStreetType { get; set; }
    public Nullable<int> PostalSuburb { get; set; }
    public string PostalState { get; set; }
    public string PrevUnitNumber { get; set; }
    public string PrevStreetNumber { get; set; }
    public string PrevStreetName { get; set; }
    public string PrevStreetType { get; set; }
    public Nullable<int> PrevSuburb { get; set; }
    public string PrevState { get; set; }
    public Nullable<bool> HavingDriverLicense { get; set; }
    public string DriversLicenceNo { get; set; }
    public string LicenseStateOfIssue { get; set; }
    public Nullable<int> NoOfDependents { get; set; }
    public Nullable<bool> Employed { get; set; }
    public Nullable<int> UnEmployedYear { get; set; }
    public Nullable<int> UnEmployedMonth { get; set; }
    public string Occupation { get; set; }
    public string JobType { get; set; }
    public Nullable<bool> IsASoleTraderOrInPartnership { get; set; }
    public string BusinessName { get; set; }
    public string BusinessPartnerAddress { get; set; }
    public string BusinessPartnerName { get; set; }
    public string BusinessNature { get; set; }
    public Nullable<System.DateTime> BusinessStartDate { get; set; }
    public Nullable<bool> IsDirectorOrManagementRole { get; set; }
    public Nullable<int> CallID { get; set; }
    public Nullable<int> CompanyID { get; set; }
    public string ApplicationManager { get; set; }
    public Nullable<System.DateTime> CreateDate { get; set; }
    public Nullable<System.DateTime> StatusStartDate { get; set; }
    public Nullable<System.DateTime> StatusExpiryDate { get; set; }
    public string StatusEnteredBy { get; set; }
    public Nullable<System.DateTime> StatusDate { get; set; }
    public string Notes { get; set; }
    public string AccountName { get; set; }
    public string AccountBSB { get; set; }
    public string AccountNo { get; set; }
    public string Bank { get; set; }
    public string BankBranch { get; set; }
    public string CCNumber { get; set; }
    public string CCProvider { get; set; }
    public string CCExpiryMonth { get; set; }
    public string CCExpiryYear { get; set; }
    public string CCNameOnCard { get; set; }
    public Nullable<System.DateTime> CreditHealthCheckSentDate { get; set; }
    public string CreditHealthCheckSentBy { get; set; }
    public Nullable<System.DateTime> ApplicationPackageSentDate { get; set; }
    public string ApplicationPackageSentBy { get; set; }
    public Nullable<System.DateTime> ApplicationPackageReceivedDate { get; set; }
    public string ApplicationPackageReceivedBy { get; set; }
    public string GUID { get; set; }
    public Nullable<System.DateTime> WebApplicationSent { get; set; }
    public Nullable<System.DateTime> WebApplicationCompleted { get; set; }
    public string IPAddress { get; set; }
    public bool AgreedToTermsAndConditions { get; set; }
    public Nullable<System.DateTime> AgreedToTermsAndConditionsDate { get; set; }
    public bool AgreedToDirectDebitRequest { get; set; }
    public Nullable<System.DateTime> AgreedToDirectDebitDate { get; set; }
    public bool AgreedToAuthorityToAct { get; set; }
    public Nullable<System.DateTime> AgreedToAuthorityToActDate { get; set; }
    public bool ReceiveFurtherInformationTicked { get; set; }
    public Nullable<decimal> ApplicationFee { get; set; }
    public Nullable<decimal> AdditionalFee { get; set; }
    public Nullable<decimal> ApplicationFeePaid { get; set; }
    public Nullable<int> ApplicationFeeInvoiceNo { get; set; }
    public Nullable<System.DateTime> ApplicationFeeInvoiceDate { get; set; }
    public string ApplicationFeePaymentMethod { get; set; }
    public Nullable<int> PaymentOptionID { get; set; }
    public Nullable<int> JudgementPaymentOptionID { get; set; }
    public string DayOfBusinessWeekToDebit { get; set; }
    public bool AgreedToFeeStructure { get; set; }
    public Nullable<System.DateTime> AgreedToFeeStructureDate { get; set; }
    public Nullable<int> RemovalFeeOption { get; set; }
    public bool AgreedToFeePaymentOptions { get; set; }
    public Nullable<System.DateTime> AgreedToFeePaymentOptionsDate { get; set; }
    public bool AgreedToPayment { get; set; }
    public Nullable<System.DateTime> AgreedToPaymentDate { get; set; }
    public string AgreedToPaymentType { get; set; }
    public Nullable<decimal> JudgementFee { get; set; }
    public Nullable<decimal> JudgemenFeePaid { get; set; }
    //***only freakin temporary!!!
    public string ReferralDX { get; set; }
    //***what a hack!

    public MMCompany Company { get; set; }

    public CCApplicant() { }

    public CCApplicant(string ID)
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand("SELECT * FROM CF..Applicant WHERE GUID = '" + ID.ToString() + "'", conn);
        conn.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {
          GUID = ID;

          ApplicantID = Convert.ToInt32(dr["ApplicantID"]);
          CFStatusID = (dr["CFStatusID"] as Int32?) ?? null;
          Title = Convert.ToString(dr["Title"]);
          Surname = Convert.ToString(dr["Surname"]);
          FirstName = Convert.ToString(dr["FirstName"]);
          MiddleName = Convert.ToString(dr["MiddleName"]);
          TelephoneHome = Convert.ToString(dr["TelephoneHome"]);
          TelephoneWork = Convert.ToString(dr["TelephoneWork"]);
          Mobile = Convert.ToString(dr["Mobile"]);
          Fax = Convert.ToString(dr["Fax"]);
          Email = Convert.ToString(dr["Email"]);
          PersonalRefName = Convert.ToString(dr["PersonalRefName"]);
          PersonalRefAddress = Convert.ToString(dr["PersonalRefAddress"]);
          PersonalRefRelationship = Convert.ToString(dr["PersonalRefRelationship"]);
          PersonalRefPhone = Convert.ToString(dr["PersonalRefPhone"]);
          HavingSpouse = (dr["HavingSpouse"] as Boolean?) ?? null;
          SpousesName = Convert.ToString(dr["SpousesName"]);
          Sex = Convert.ToString(dr["Sex"]);
          DateOfBirth = (dr["DateOfBirth"] as DateTime?) ?? null;
          HomeUnitNumber = Convert.ToString(dr["HomeUnitNumber"]);
          HomeStreetNumber = Convert.ToString(dr["HomeStreetNumber"]);
          HomeStreetName = Convert.ToString(dr["HomeStreetName"]);
          HomeStreetType = Convert.ToString(dr["HomeStreetType"]);
          HomeSuburb = (dr["HomeSuburb"] as Int32?) ?? null;
          HomeState = Convert.ToString(dr["HomeState"]);
          PostalUnitNumber = Convert.ToString(dr["PostalUnitNumber"]);
          PostalStreetNumber = Convert.ToString(dr["PostalStreetNumber"]);
          PostalStreetName = Convert.ToString(dr["PostalStreetName"]);
          PostalStreetType = Convert.ToString(dr["PostalStreetType"]);
          PostalSuburb = (dr["PostalSuburb"] as Int32?) ?? null;
          PostalState = Convert.ToString(dr["PostalState"]);
          PrevUnitNumber = Convert.ToString(dr["PrevUnitNumber"]);
          PrevStreetNumber = Convert.ToString(dr["PrevStreetNumber"]);
          PrevStreetName = Convert.ToString(dr["PrevStreetName"]);
          PrevStreetType = Convert.ToString(dr["PrevStreetType"]);
          PrevSuburb = (dr["PrevSuburb"] as Int32?) ?? null;
          PrevState = Convert.ToString(dr["PrevState"]);
          HavingDriverLicense = (dr["HavingDriverLicense"] as Boolean?) ?? null;
          DriversLicenceNo = Convert.ToString(dr["DriversLicenceNo"]);
          LicenseStateOfIssue = Convert.ToString(dr["LicenseStateOfIssue"]);
          NoOfDependents = (dr["NoOfDependents"] as Int32?) ?? null;
          Employed = (dr["Employed"] as Boolean?) ?? null;
          UnEmployedYear = (dr["UnEmployedYear"] as Int32?) ?? null;
          UnEmployedMonth = (dr["UnEmployedMonth"] as Int32?) ?? null;
          Occupation = Convert.ToString(dr["Occupation"]);
          JobType = Convert.ToString(dr["JobType"]);
          IsASoleTraderOrInPartnership = (dr["IsASoleTraderOrInPartnership"] as Boolean?) ?? null;
          BusinessName = Convert.ToString(dr["BusinessName"]);
          BusinessPartnerAddress = Convert.ToString(dr["BusinessPartnerAddress"]);
          BusinessPartnerName = Convert.ToString(dr["BusinessPartnerName"]);
          BusinessNature = Convert.ToString(dr["BusinessNature"]);
          BusinessStartDate = (dr["BusinessStartDate"] as DateTime?) ?? null;
          IsDirectorOrManagementRole = (dr["IsDirectorOrManagementRole"] as Boolean?) ?? null;
          CallID = (dr["CallID"] as Int32?) ?? null;
          CompanyID = (dr["CompanyID"] as Int32?) ?? null;
          ApplicationManager = Convert.ToString(dr["ApplicationManager"]);
          CreateDate = (dr["CreateDate"] as DateTime?) ?? null;
          StatusStartDate = (dr["StatusStartDate"] as DateTime?) ?? null;
          StatusExpiryDate = (dr["StatusExpiryDate"] as DateTime?) ?? null;
          StatusEnteredBy = Convert.ToString(dr["StatusEnteredBy"]);
          StatusDate = (dr["StatusDate"] as DateTime?) ?? null;
          Notes = Convert.ToString(dr["Notes"]);
          AccountName = Convert.ToString(dr["AccountName"]);
          AccountBSB = Convert.ToString(dr["AccountBSB"]);
          AccountNo = Convert.ToString(dr["AccountNo"]);
          Bank = Convert.ToString(dr["Bank"]);
          BankBranch = Convert.ToString(dr["BankBranch"]);
          CCNumber = Convert.ToString(dr["CCNumber"]);
          CCProvider = Convert.ToString(dr["CCProvider"]);
          CCExpiryMonth = Convert.ToString(dr["CCExpiryMonth"]);
          CCExpiryYear = Convert.ToString(dr["CCExpiryYear"]);
          CCNameOnCard = Convert.ToString(dr["CCNameOnCard"]);
          CreditHealthCheckSentDate = (dr["CreditHealthCheckSentDate"] as DateTime?) ?? null;
          CreditHealthCheckSentBy = Convert.ToString(dr["CreditHealthCheckSentBy"]);
          ApplicationPackageSentDate = (dr["ApplicationPackageSentDate"] as DateTime?) ?? null;
          ApplicationPackageSentBy = Convert.ToString(dr["ApplicationPackageSentBy"]);
          ApplicationPackageReceivedDate = (dr["ApplicationPackageReceivedDate"] as DateTime?) ?? null;
          ApplicationPackageReceivedBy = Convert.ToString(dr["ApplicationPackageReceivedBy"]);
          WebApplicationSent = (dr["WebApplicationSent"] as DateTime?) ?? null;
          WebApplicationCompleted = (dr["WebApplicationCompleted"] as DateTime?) ?? null;
          IPAddress = Convert.ToString(dr["IPAddress"]);
          AgreedToTermsAndConditions = (Boolean)dr["AgreedToTermsAndConditions"];
          AgreedToTermsAndConditionsDate = (dr["AgreedToTermsAndConditionsDate"] as DateTime?) ?? null;
          AgreedToDirectDebitRequest = (Boolean)dr["AgreedToDirectDebitRequest"];
          AgreedToDirectDebitDate = (dr["AgreedToDirectDebitDate"] as DateTime?) ?? null;
          AgreedToAuthorityToAct = (Boolean)dr["AgreedToAuthorityToAct"];
          AgreedToAuthorityToActDate = (dr["AgreedToAuthorityToActDate"] as DateTime?) ?? null;
          ReceiveFurtherInformationTicked = (Boolean)dr["ReceiveFurtherInformationTicked"];
          ApplicationFee = (dr["ApplicationFee"] as Decimal?) ?? null;
          AdditionalFee = (dr["AdditionalFee"] as Decimal?) ?? null;
          ApplicationFeePaid = (dr["ApplicationFeePaid"] as Decimal?) ?? null;
          ApplicationFeeInvoiceNo = (dr["ApplicationFeeInvoiceNo"] as Int32?) ?? null;
          ApplicationFeeInvoiceDate = (dr["ApplicationFeeInvoiceDate"] as DateTime?) ?? null;
          ApplicationFeePaymentMethod = Convert.ToString(dr["ApplicationFeePaymentMethod"]);
          PaymentOptionID = (dr["PaymentOptionID"] as Int32?) ?? null;
          JudgementPaymentOptionID = (dr["JudgementPaymentOptionID"] as Int32?) ?? null;
          DayOfBusinessWeekToDebit = Convert.ToString(dr["DayOfBusinessWeekToDebit"]);
          AgreedToFeeStructure = (Boolean)dr["AgreedToFeeStructure"];
          AgreedToFeeStructureDate = (dr["AgreedToFeeStructureDate"] as DateTime?) ?? null;
          RemovalFeeOption = (dr["RemovalFeeOption"] as Int32?) ?? null;
          AgreedToFeePaymentOptions = (Boolean)dr["AgreedToFeePaymentOptions"];
          AgreedToFeePaymentOptionsDate = (dr["AgreedToFeePaymentOptionsDate"] as DateTime?) ?? null;
          AgreedToPayment = (dr["AgreedToPayment"] as Boolean?) ?? false;
          AgreedToPaymentDate = (dr["AgreedToPaymentDate"] as DateTime?) ?? null;
          AgreedToPaymentType = Convert.ToString(dr["AgreedToPaymentType"]);
          JudgementFee = (dr["OtherFee02"] as Decimal?) ?? null;
          JudgemenFeePaid=  (dr["OtherFeePaid02"] as Decimal?) ?? null;
    
            
          //***only freakin temporary!!!
          ReferralDX = Convert.ToString(dr["ReferralDX"]);
          //***what a hack!
        }
      }
    }

    public void Save()
    {
      //building the UPDATE string...
      StringBuilder sql = new StringBuilder();
      sql.Append("UPDATE CF..Applicant SET ");
      sql.Append("CFStatusID = @CFStatusID, ");
      sql.Append("Title = @Title, ");
      sql.Append("Surname = @Surname, ");
      sql.Append("FirstName = @FirstName, ");
      sql.Append("MiddleName = @MiddleName, ");
      sql.Append("TelephoneHome = @TelephoneHome, ");
      sql.Append("TelephoneWork = @TelephoneWork, ");
      sql.Append("Mobile = @Mobile, ");
      sql.Append("Fax = @Fax, ");
      sql.Append("Email = @Email, ");
      sql.Append("PersonalRefName = @PersonalRefName, ");
      sql.Append("PersonalRefAddress = @PersonalRefAddress, ");
      sql.Append("PersonalRefRelationship = @PersonalRefRelationship, ");
      sql.Append("PersonalRefPhone = @PersonalRefPhone, ");
      sql.Append("HavingSpouse = @HavingSpouse, ");
      sql.Append("SpousesName = @SpousesName, ");
      sql.Append("Sex = @Sex, ");
      sql.Append("DateOfBirth = @DateOfBirth, ");
      sql.Append("HomeUnitNumber = @HomeUnitNumber, ");
      sql.Append("HomeStreetNumber = @HomeStreetNumber, ");
      sql.Append("HomeStreetName = @HomeStreetName, ");
      sql.Append("HomeStreetType = @HomeStreetType, ");
      sql.Append("HomeSuburb = @HomeSuburb, ");
      sql.Append("HomeState = @HomeState, ");
      sql.Append("PostalUnitNumber = @PostalUnitNumber, ");
      sql.Append("PostalStreetNumber = @PostalStreetNumber, ");
      sql.Append("PostalStreetName = @PostalStreetName, ");
      sql.Append("PostalStreetType = @PostalStreetType, ");
      sql.Append("PostalSuburb = @PostalSuburb, ");
      sql.Append("PostalState = @PostalState, ");
      sql.Append("PrevUnitNumber = @PrevUnitNumber, ");
      sql.Append("PrevStreetNumber = @PrevStreetNumber, ");
      sql.Append("PrevStreetName = @PrevStreetName, ");
      sql.Append("PrevStreetType = @PrevStreetType, ");
      sql.Append("PrevSuburb = @PrevSuburb, ");
      sql.Append("PrevState = @PrevState, ");
      sql.Append("HavingDriverLicense = @HavingDriverLicense, ");
      sql.Append("DriversLicenceNo = @DriversLicenceNo, ");
      sql.Append("LicenseStateOfIssue = @LicenseStateOfIssue, ");
      sql.Append("NoOfDependents = @NoOfDependents, ");
      sql.Append("Employed = @Employed, ");
      sql.Append("UnEmployedYear = @UnEmployedYear, ");
      sql.Append("UnEmployedMonth = @UnEmployedMonth, ");
      sql.Append("Occupation = @Occupation, ");
      sql.Append("JobType = @JobType, ");
      sql.Append("IsASoleTraderOrInPartnership = @IsASoleTraderOrInPartnership, ");
      sql.Append("BusinessName = @BusinessName, ");
      sql.Append("BusinessPartnerAddress = @BusinessPartnerAddress, ");
      sql.Append("BusinessPartnerName = @BusinessPartnerName, ");
      sql.Append("BusinessNature = @BusinessNature, ");
      sql.Append("BusinessStartDate = @BusinessStartDate, ");
      sql.Append("IsDirectorOrManagementRole = @IsDirectorOrManagementRole, ");
      sql.Append("CallID = @CallID, ");
      sql.Append("CompanyID = @CompanyID, ");
      sql.Append("ApplicationManager = @ApplicationManager, ");
      sql.Append("CreateDate = @CreateDate, ");
      sql.Append("StatusStartDate = @StatusStartDate, ");
      sql.Append("StatusExpiryDate = @StatusExpiryDate, ");
      sql.Append("StatusEnteredBy = @StatusEnteredBy, ");
      sql.Append("StatusDate = @StatusDate, ");
      sql.Append("Notes = @Notes, ");
      sql.Append("AccountName = @AccountName, ");
      sql.Append("AccountBSB = @AccountBSB, ");
      sql.Append("AccountNo = @AccountNo, ");
      sql.Append("Bank = @Bank, ");
      sql.Append("BankBranch = @BankBranch, ");
      sql.Append("CCNumber = @CCNumber, ");
      sql.Append("CCProvider = @CCProvider, ");
      sql.Append("CCExpiryMonth = @CCExpiryMonth, ");
      sql.Append("CCExpiryYear = @CCExpiryYear, ");
      sql.Append("CCNameOnCard = @CCNameOnCard, ");
      sql.Append("CreditHealthCheckSentDate = @CreditHealthCheckSentDate, ");
      sql.Append("CreditHealthCheckSentBy = @CreditHealthCheckSentBy, ");
      sql.Append("ApplicationPackageSentDate = @ApplicationPackageSentDate, ");
      sql.Append("ApplicationPackageSentBy = @ApplicationPackageSentBy, ");
      sql.Append("ApplicationPackageReceivedDate = @ApplicationPackageReceivedDate, ");
      sql.Append("ApplicationPackageReceivedBy = @ApplicationPackageReceivedBy, ");
      sql.Append("WebApplicationSent = @WebApplicationSent, ");
      sql.Append("WebApplicationCompleted = @WebApplicationCompleted, ");
      sql.Append("IPAddress = @IPAddress, ");
      sql.Append("AgreedToTermsAndConditions = @AgreedToTermsAndConditions, ");
      sql.Append("AgreedToTermsAndConditionsDate = @AgreedToTermsAndConditionsDate, ");
      sql.Append("AgreedToDirectDebitRequest = @AgreedToDirectDebitRequest, ");
      sql.Append("AgreedToDirectDebitDate = @AgreedToDirectDebitDate, ");
      sql.Append("AgreedToAuthorityToAct = @AgreedToAuthorityToAct, ");
      sql.Append("AgreedToAuthorityToActDate = @AgreedToAuthorityToActDate, ");
      sql.Append("ReceiveFurtherInformationTicked = @ReceiveFurtherInformationTicked, ");
      sql.Append("ApplicationFee = @ApplicationFee, ");
      sql.Append("AdditionalFee = @AdditionalFee, ");
      sql.Append("ApplicationFeePaid = @ApplicationFeePaid, ");
      sql.Append("ApplicationFeeInvoiceNo = @ApplicationFeeInvoiceNo, ");
      sql.Append("ApplicationFeeInvoiceDate = @ApplicationFeeInvoiceDate, ");
      sql.Append("ApplicationFeePaymentMethod = @ApplicationFeePaymentMethod, ");
      sql.Append("PaymentOptionID = @PaymentOptionID, ");
      sql.Append("DayOfBusinessWeekToDebit = @DayOfBusinessWeekToDebit, ");
      sql.Append("AgreedToFeeStructure = @AgreedToFeeStructure, ");
      sql.Append("AgreedToFeeStructureDate = @AgreedToFeeStructureDate,");
      sql.Append("RemovalFeeOption = @RemovalFeeOption, ");
      sql.Append("AgreedToFeePaymentOptions = @AgreedToFeePaymentOptions, ");
      sql.Append("AgreedToFeePaymentOptionsDate = @AgreedToFeePaymentOptionsDate, ");
      sql.Append("AgreedToPaymentType = @AgreedToPaymentType, ");
      sql.Append("AgreedToPayment = @AgreedToPayment, ");
      sql.Append("AgreedToPaymentDate = @AgreedToPaymentDate, ");
      sql.Append("JudgementPaymentOptionID = @JudgementPaymentOptionID ");

        

      sql.Append("WHERE ApplicantID = @ApplicantID");

      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

        cmd.Parameters.Add(new SqlParameter("ApplicantID", ApplicantID));
        cmd.Parameters.Add(new SqlParameter("CFStatusID", ((object)CFStatusID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Title", ((object)Title ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Surname", ((object)Surname ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("FirstName", ((object)FirstName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("MiddleName", ((object)MiddleName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TelephoneHome", ((object)TelephoneHome ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TelephoneWork", ((object)TelephoneWork ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Mobile", ((object)Mobile ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Fax", ((object)Fax ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Email", ((object)Email ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefName", ((object)PersonalRefName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefAddress", ((object)PersonalRefAddress ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefRelationship", ((object)PersonalRefRelationship ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefPhone", ((object)PersonalRefPhone ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HavingSpouse", ((object)HavingSpouse ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("SpousesName", ((object)SpousesName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Sex", ((object)Sex ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("DateOfBirth", ((object)DateOfBirth ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeUnitNumber", ((object)HomeUnitNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeStreetNumber", ((object)HomeStreetNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeStreetName", ((object)HomeStreetName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeStreetType", ((object)HomeStreetType ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeSuburb", ((object)HomeSuburb ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeState", ((object)HomeState ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalUnitNumber", ((object)PostalUnitNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalStreetNumber", ((object)PostalStreetNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalStreetName", ((object)PostalStreetName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalStreetType", ((object)PostalStreetType ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalSuburb", ((object)PostalSuburb ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalState", ((object)PostalState ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevUnitNumber", ((object)PrevUnitNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevStreetNumber", ((object)PrevStreetNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevStreetName", ((object)PrevStreetName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevStreetType", ((object)PrevStreetType ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevSuburb", ((object)PrevSuburb ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevState", ((object)PrevState ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HavingDriverLicense", ((object)HavingDriverLicense ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("DriversLicenceNo", ((object)DriversLicenceNo ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("LicenseStateOfIssue", ((object)LicenseStateOfIssue ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("NoOfDependents", ((object)NoOfDependents ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Employed", ((object)Employed ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("UnEmployedYear", ((object)UnEmployedYear ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("UnEmployedMonth", ((object)UnEmployedMonth ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Occupation", ((object)Occupation ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("JobType", ((object)JobType ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IsASoleTraderOrInPartnership", ((object)IsASoleTraderOrInPartnership ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("BusinessName", ((object)BusinessName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("BusinessPartnerAddress", ((object)BusinessPartnerAddress ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("BusinessPartnerName", ((object)BusinessPartnerName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("BusinessNature", ((object)BusinessNature ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("BusinessStartDate", ((object)BusinessStartDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IsDirectorOrManagementRole", ((object)IsDirectorOrManagementRole ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CallID", ((object)CallID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CompanyID", ((object)CompanyID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicationManager", ((object)ApplicationManager ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CreateDate", ((object)CreateDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("StatusStartDate", ((object)StatusStartDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("StatusExpiryDate", ((object)StatusExpiryDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("StatusEnteredBy", ((object)StatusEnteredBy ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("StatusDate", ((object)StatusDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Notes", ((object)Notes ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AccountName", ((object)AccountName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AccountBSB", ((object)AccountBSB ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AccountNo", ((object)AccountNo ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Bank", ((object)Bank ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("BankBranch", ((object)BankBranch ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CCNumber", ((object)CCNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CCProvider", ((object)CCProvider ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CCExpiryMonth", ((object)CCExpiryMonth ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CCExpiryYear", ((object)CCExpiryYear ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CCNameOnCard", ((object)CCNameOnCard ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CreditHealthCheckSentDate", ((object)CreditHealthCheckSentDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CreditHealthCheckSentBy", ((object)CreditHealthCheckSentBy ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicationPackageSentDate", ((object)ApplicationPackageSentDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicationPackageSentBy", ((object)ApplicationPackageSentBy ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicationPackageReceivedDate", ((object)ApplicationPackageReceivedDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicationPackageReceivedBy", ((object)ApplicationPackageReceivedBy ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("WebApplicationSent", ((object)WebApplicationSent ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("WebApplicationCompleted", ((object)WebApplicationCompleted ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IPAddress", ((object)IPAddress ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToTermsAndConditions", ((object)AgreedToTermsAndConditions ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToTermsAndConditionsDate", ((object)AgreedToTermsAndConditionsDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToDirectDebitRequest", ((object)AgreedToDirectDebitRequest ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToDirectDebitDate", ((object)AgreedToDirectDebitDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToAuthorityToAct", ((object)AgreedToAuthorityToAct ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToAuthorityToActDate", ((object)AgreedToAuthorityToActDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ReceiveFurtherInformationTicked", ((object)ReceiveFurtherInformationTicked ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicationFee", ((object)ApplicationFee ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AdditionalFee", ((object)AdditionalFee ?? DBNull.Value)));          
        cmd.Parameters.Add(new SqlParameter("ApplicationFeePaid", ((object)ApplicationFeePaid ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicationFeeInvoiceNo", ((object)ApplicationFeeInvoiceNo ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicationFeeInvoiceDate", ((object)ApplicationFeeInvoiceDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicationFeePaymentMethod", ((object)ApplicationFeePaymentMethod ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PaymentOptionID", ((object)PaymentOptionID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("DayOfBusinessWeekToDebit", ((object)DayOfBusinessWeekToDebit ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToFeeStructure", ((object)AgreedToFeeStructure ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToFeeStructureDate", ((object)AgreedToFeeStructureDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("RemovalFeeOption", ((object)RemovalFeeOption ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToFeePaymentOptions", ((object)AgreedToFeePaymentOptions ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToFeePaymentOptionsDate", ((object)AgreedToFeePaymentOptionsDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToPayment", ((object)AgreedToPayment ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToPaymentDate", ((object)AgreedToPaymentDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedToPaymentType", ((object)AgreedToPaymentType ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("JudgementPaymentOptionID", ((object)JudgementPaymentOptionID ?? DBNull.Value)));
          
        conn.Open();
        cmd.ExecuteNonQuery();
      }
    }
    public void LoadCompany()
    {
      Company = new MMCompany(Convert.ToInt32(CompanyID));
    }

    public List<RemovalFees> GetRemovalFees(string ID)
    {
        
        var result = new List<RemovalFees>();

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
        {
            SqlCommand cmd = new SqlCommand("sp_CFGetRemovalFees", conn);
            cmd.Parameters.AddWithValue("@GUID", ID);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                var removal = new RemovalFees();
                removal.CreditProvider = GetString(dr["CreditorProviderName"]);
                removal.RemovalFee = GetDecimal(dr["removalfee"]);
                result.Add(removal);
            }
        }
        //
        return result;
    }
      private string GetString(object data, string defaultValue = "")
      {
          if (data == DBNull.Value)
              return defaultValue;
          else 
              return data.ToString();
      }
      private decimal GetDecimal(object data, decimal defaultValue = 0)
      {
          if (data == DBNull.Value)
              return defaultValue;
          else
          {
              decimal result = 0;
              decimal.TryParse(data.ToString(), out result);
              return result;
          }
      }
  }
    
  public class RemovalFees
  {
      public string CreditProvider { get; set; }
      public decimal RemovalFee { get; set; }
  }
}