﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplicationForms.Models
{
	public class MMPCode
	{
		public int PCodeID { get; set; }
		public string SUBURB { get; set; }
		public string PCODE { get; set; }
		public string STATE { get; set; }
		public string TVSubMarket { get; set; }
		public string SubMarket { get; set; }
		public Nullable<double> Lat { get; set; }
		public Nullable<double> Lon { get; set; }

		public string SuburbStatePostCode { get; set; } //derived.

		public MMPCode(Nullable<int> ID)
		{
			if (ID != null)
			{
				using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
				{
					SqlCommand cmd = new SqlCommand("SELECT * FROM PCodeLL WHERE PCodeID = " + ID.ToString(), conn);
					conn.Open();
					SqlDataReader dr = cmd.ExecuteReader();
					while (dr.Read())
					{
						SUBURB = Convert.ToString(dr["SUBURB"]);
						PCODE = Convert.ToString(dr["PCODE"]);
						STATE = Convert.ToString(dr["STATE"]);
						SuburbStatePostCode = SUBURB + " " + STATE + " " + PCODE;
					}
				}
			}
			else { }
		}
	}

}
