﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplicationForms.Models
{
	public class MMApplication
	{
		public int ApplicationNo { get; set; }
		public int CompanyID { get; set; }
		public System.DateTime AppDate { get; set; }
		public string EnteredBy { get; set; }
		public string LoanType { get; set; }
		public Nullable<int> StatusID { get; set; }
		public Nullable<System.DateTime> StatusStartDate { get; set; }
		public Nullable<System.DateTime> StatusExpiryDate { get; set; }
		public string StatusDetails { get; set; }
		public Nullable<decimal> RequestedAmount { get; set; }
		public Nullable<double> InterestRate1 { get; set; }
		public Nullable<decimal> SecondMortgage { get; set; }
		public Nullable<double> InterestRate2 { get; set; }
		public Nullable<decimal> Valuation { get; set; }
		public Nullable<int> LoanPurposeID { get; set; }
		public string Description { get; set; }
		public string RepaymentOption { get; set; }
		public Nullable<int> BankID { get; set; }
		public Nullable<decimal> Revenue { get; set; }
		public bool RevenuePaid { get; set; }
		public Nullable<decimal> Commission { get; set; }
		public bool CommissionPaid { get; set; }
		public bool Approved { get; set; }
		public bool Declined { get; set; }
		public Nullable<System.DateTime> ApprovalDate { get; set; }
		public string TradingName { get; set; }
		public string SolicitorsFirm { get; set; }
		public string SolicitorsName { get; set; }
		public string SolicitorsPhone { get; set; }
		public string SolicitorsMobile { get; set; }
		public string SolicitorsFax { get; set; }
		public string SolicitorsAddLine1 { get; set; }
		public string SolicitorsAddLine2 { get; set; }
		public Nullable<int> SolicitorsAddSuburb { get; set; }
		public string Email { get; set; }
		public string DX { get; set; }
		public string Details { get; set; }
		public string CompanyName { get; set; }
		public string CompanyACN { get; set; }
		public string CompanyRegAddLine1 { get; set; }
		public string CompanyRegAddLine2 { get; set; }
		public Nullable<int> CompanyRegSuburb { get; set; }
		public string CompanyTradAddLine1 { get; set; }
		public string CompanyTradAddLine2 { get; set; }
		public Nullable<int> CompanyTradeSuburb { get; set; }
		public string CompanyIndustry { get; set; }
		public string CompanyEstablished { get; set; }
		public Nullable<decimal> CostPrice { get; set; }
		public Nullable<decimal> DepositCash { get; set; }
		public Nullable<decimal> DepositTrade { get; set; }
		public Nullable<decimal> InsuranceComp { get; set; }
		public Nullable<decimal> InsuranceOther { get; set; }
		public Nullable<decimal> Amount { get; set; }
		public Nullable<short> Residual { get; set; }
		public Nullable<double> InterestRate { get; set; }
		public Nullable<decimal> Term { get; set; }
		public string Frequency { get; set; }
		public Nullable<decimal> StampDuty { get; set; }
		public Nullable<decimal> FID { get; set; }
		public Nullable<decimal> Total { get; set; }
		public Nullable<decimal> ChargeAmount { get; set; }
		public Nullable<double> ChargePercentage { get; set; }
		public Nullable<decimal> DefaultAmount { get; set; }
		public Nullable<decimal> LenderChargeAmount { get; set; }
		public Nullable<System.DateTime> LastUpdated { get; set; }
		public string LastUpdatedBy { get; set; }
		public bool Remind { get; set; }
		public bool TaskDone { get; set; }
		public string TaskDoneBy { get; set; }
		public string TaskDetails { get; set; }
		public Nullable<System.DateTime> ReminderDate { get; set; }
		public Nullable<short> ReminderTimeToRemind { get; set; }
		public string ReminderInterval { get; set; }
		public string BusinessWonFromSuburb { get; set; }
		public bool Combined { get; set; }
		public string StatusEnteredBy { get; set; }
		public Nullable<System.DateTime> StatusDate { get; set; }
		public Nullable<decimal> ReferralCommision { get; set; }
		public bool ReferralCommisionPaid { get; set; }
		public string Referral { get; set; }
		public string ReferralGrouped { get; set; }
		public Nullable<decimal> EstMonthlyRepay { get; set; }
		public Nullable<int> MortgageRequest { get; set; }
		public string LenderReferenceNo { get; set; }
		public Nullable<decimal> LegalCost { get; set; }
		public Nullable<decimal> LegalCostDeposit { get; set; }
		public Nullable<decimal> ValuationFees { get; set; }
		public Nullable<decimal> EstablishmentFees { get; set; }
		public Nullable<int> EarlyRedemption { get; set; }
		public Nullable<double> DefaultPercentageFirst { get; set; }
		public Nullable<double> DefaultPercentageSecond { get; set; }
		public string TermDisplay { get; set; }
		public Nullable<int> PrivateFundingApplicationNo { get; set; }
		public Nullable<System.DateTime> LoanFromDate { get; set; }
		public Nullable<System.DateTime> LoanToDate { get; set; }
		public Nullable<System.DateTime> FinalisedDate { get; set; }
		public Nullable<System.DateTime> ApprovedDate { get; set; }
		public string LSolicitorsFirm { get; set; }
		public string LSolicitorsName { get; set; }
		public string LSolicitorsPhone { get; set; }
		public string LSolicitorsMobile { get; set; }
		public string LSolicitorsFax { get; set; }
		public string LSolicitorsAddLine1 { get; set; }
		public string LSolicitorsAddLine2 { get; set; }
		public Nullable<int> LSolicitorsAddSuburb { get; set; }
		public string LSolicitorEmail { get; set; }
		public string LSolicitorDX { get; set; }
		public string ValuersFirm { get; set; }
		public string ValuersName { get; set; }
		public string ValuersPhone { get; set; }
		public string ValuersMobile { get; set; }
		public string ValuersFax { get; set; }
		public string ValuersAddLine1 { get; set; }
		public string ValuersAddLine2 { get; set; }
		public Nullable<int> ValuersAddSuburb { get; set; }
		public string ValuerEmail { get; set; }
		public Nullable<decimal> ThirdMortgage { get; set; }
		public string FileLocation { get; set; }
		public Nullable<double> InterestRate3 { get; set; }
		public Nullable<double> DefaultPercentageThird { get; set; }
		public string ApplicationManager { get; set; }
		public string InterestInterval1 { get; set; }
		public string InterestInterval2 { get; set; }
		public string InterestInterval3 { get; set; }
		public string InterestType { get; set; }
		public string InterestPaymentType { get; set; }
		public Nullable<int> InterestPaymentTypeYear { get; set; }
		public bool MainIncome_PAYG { get; set; }
		public bool MainIncome_Self_Employed { get; set; }
		public bool MainIncome_Other { get; set; }
		public bool SecurityType_Residential { get; set; }
		public bool SecurityType_Commercial { get; set; }
		public bool SecurityType_Land { get; set; }
		public Nullable<System.DateTime> WithdrawnDate { get; set; }
		public bool LoanPurpose_Purchase { get; set; }
		public bool LoanPurpose_Refinance { get; set; }
		public bool LoanPurpose_Other { get; set; }
		public Nullable<System.DateTime> FinanceRequiredBy { get; set; }
		public Nullable<int> InterestTermID { get; set; }
		public Nullable<decimal> InterestTerm { get; set; }
		public string InterestTermDisplay { get; set; }
		public string Product { get; set; }
		public string RepaymentFrequency { get; set; }
		public Nullable<int> DischargeStatusID { get; set; }
		public Nullable<System.DateTime> DischargeStatusStartDate { get; set; }
		public Nullable<System.DateTime> DischargeStatusExpiryDate { get; set; }
		public string DischargeStatusDetails { get; set; }
		public string DischargeStatusEnteredBy { get; set; }
		public Nullable<System.DateTime> DischargeStatusDate { get; set; }
		public string SalesManager { get; set; }
		public string LoanTerm { get; set; }
		public Nullable<int> NoOfApplicant { get; set; }
		public string LoanPurpose { get; set; }
		public Nullable<decimal> MortgageInsurance { get; set; }
		public Nullable<decimal> LenderFee { get; set; }
		public Nullable<decimal> EstSaving { get; set; }
		public Nullable<bool> InsufficientIncome { get; set; }
		public Nullable<bool> SecurityUnacceptable { get; set; }
		public Nullable<bool> NoBenefit { get; set; }
		public Nullable<bool> NoEquity { get; set; }
		public Nullable<bool> OutsideLenderPolicy { get; set; }
		public Nullable<bool> UnDisclosedDebt { get; set; }
		public string STLSubType { get; set; }
		public string STLSecuredType { get; set; }
		public string ValuerDX { get; set; }
		public string AccountantsFirm { get; set; }
		public string AccountantsName { get; set; }
		public string AccountantsPhone { get; set; }
		public string AccountantsFax { get; set; }
		public string AccountantsAddLine1 { get; set; }
		public string AccountantsAddLine2 { get; set; }
		public string AccountantsSuburb { get; set; }
		public string AccountantsMobile { get; set; }
		public string AccountantsEmail { get; set; }
		public string AgentName { get; set; }
		public string AgentContact { get; set; }
		public string AgentPhone { get; set; }
		public string AgentMobile { get; set; }
		public string AgentFax { get; set; }
		public string AgentAddLine1 { get; set; }
		public string AgentAddLine2 { get; set; }
		public string AgentAddSuburb { get; set; }
		public string AgentEmail { get; set; }
		public string AgentOpinionProperty { get; set; }
		public Nullable<decimal> AgentOpinionHowmuch { get; set; }
		public string AgentOpinionHowQuick { get; set; }
		public string Agent2Name { get; set; }
		public string Agent2Contact { get; set; }
		public string Agent2Phone { get; set; }
		public string Agent2Mobile { get; set; }
		public string Agent2Fax { get; set; }
		public string Agent2AddLine1 { get; set; }
		public string Agent2AddLine2 { get; set; }
		public string Agent2AddSuburb { get; set; }
		public string Agent2Email { get; set; }
		public string Agent2OpinionProperty { get; set; }
		public Nullable<decimal> Agent2OpinionHowmuch { get; set; }
		public string Agent2OpinionHowQuick { get; set; }
		public string CompoundPeriod { get; set; }
		public Nullable<System.DateTime> LoanDate { get; set; }
		public Nullable<System.DateTime> FirstPaymentDate { get; set; }
		public Nullable<System.DateTime> OfferDate { get; set; }
		public string LoanAmountInWord { get; set; }
		public Nullable<decimal> OriRequestedAmount { get; set; }
		public Nullable<bool> AddFeeToLoan { get; set; }
		public Nullable<bool> CLazyNoTime { get; set; }
		public Nullable<bool> CNotGetPaperwork { get; set; }
		public Nullable<bool> CWentElsewhere { get; set; }
		public Nullable<bool> CPartnerSayNo { get; set; }
		public Nullable<bool> CSellingProperty { get; set; }
		public Nullable<bool> CFamilyFriendAssisted { get; set; }
		public Nullable<bool> CFeeTooExpensive { get; set; }
		public Nullable<bool> CNotUsePropertyAsSecurity { get; set; }
		public Nullable<bool> CCircumstancesChanged { get; set; }
		public Nullable<System.DateTime> PayOutDate { get; set; }
		public Nullable<decimal> SettlementFee { get; set; }
		public Nullable<decimal> EarlyPayoutFee { get; set; }
		public Nullable<decimal> PayOutInterest { get; set; }
		public Nullable<short> STLPaymentOption { get; set; }
		public Nullable<bool> SentToASTL { get; set; }
		public string Last6MthHomeLoanPaymentUptodate { get; set; }
		public Nullable<short> Last6MthHomeLoanPaymentLateMth { get; set; }
		public string PastHomeLoanPaymentUptodate { get; set; }
		public string CouncilPaymentUptodate { get; set; }
		public Nullable<decimal> CouncilPaymentArrearAmt { get; set; }
		public string LenderAssessSummary { get; set; }
		public Nullable<bool> OnHold { get; set; }
		public Nullable<int> ClientID { get; set; }
		public Nullable<decimal> RetainerAmt { get; set; }
		public string RetainerFrequency { get; set; }
		public Nullable<decimal> ApplicationFee { get; set; }
		public Nullable<decimal> TotalLoanAmount { get; set; }
		public string LoanWriter { get; set; }
		public Nullable<System.DateTime> InterviewDate { get; set; }
		public Nullable<System.DateTime> FinanceRequiredByDate { get; set; }
		public string ConversionBy { get; set; }
		public Nullable<System.DateTime> SigningCostAgreementDate { get; set; }
		public Nullable<System.DateTime> SigningLetterOfOfferDate { get; set; }
		public Nullable<byte> CreditFeeType { get; set; }
		public Nullable<decimal> CreditFeeOverwrite { get; set; }
		public string CreditFeeOverwriteNote { get; set; }
		public Nullable<double> ChargePercentageProposal { get; set; }
		public Nullable<double> UpfrontRate { get; set; }
		public Nullable<double> TrailRate { get; set; }
		public Nullable<bool> OutstandingFeeSettled { get; set; }
		public string SettlementManager { get; set; }
		public Nullable<decimal> ActualFeesToCharge { get; set; }
		public string AssessingComments { get; set; }
		public string GUID { get; set; }
		public Nullable<System.DateTime> Web_InvitationEmailSent { get; set; }
		public Nullable<System.DateTime> Web_ApplicationFormStarted { get; set; }
		public Nullable<System.DateTime> Web_ApplicationFormFinished { get; set; }
		public bool ReceiveFurtherInformationTicked { get; set; }
		public bool AgreedToTermsAndConditions { get; set; }
		public Nullable<System.DateTime> AgreedToTermsAndConditionsDate { get; set; }
		public string IPAddress { get; set; }

		//2 Applicants!
		public MMApplicant MMApplicant1 { get; set; }
		public MMApplicant MMApplicant2 { get; set; }

		public MMCompany Company { get; set; }

		public string LoanPurposeSelected { get; set; }

		public MMApplication() { }

		public MMApplication(string strGUID)
		{
			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT * FROM Application WHERE GUID = '" + strGUID + "'", conn);
				conn.Open();
				SqlDataReader dr = cmd.ExecuteReader();
				if (dr.Read())
				{

					ApplicationNo = (dr["ApplicationNo"] as Int32?) ?? 0;
					CompanyID = (dr["CompanyID"] as Int32?) ?? 0;
					AppDate = (DateTime)dr["AppDate"];
					EnteredBy = Convert.ToString(dr["EnteredBy"]);
					LoanType = Convert.ToString(dr["LoanType"]);
					StatusID = (dr["StatusID"] as Int32?) ?? null;
					StatusStartDate = (dr["StatusStartDate"] as DateTime?) ?? null;
					StatusExpiryDate = (dr["StatusExpiryDate"] as DateTime?) ?? null;
					StatusDetails = Convert.ToString(dr["StatusDetails"]);
					RequestedAmount = (dr["RequestedAmount"] as Decimal?) ?? null;
					InterestRate1 = (dr["InterestRate1"] as Double?) ?? null;
					SecondMortgage = (dr["SecondMortgage"] as Decimal?) ?? null;
					InterestRate2 = (dr["InterestRate2"] as Double?) ?? null;
					Valuation = (dr["Valuation"] as Decimal?) ?? null;
					LoanPurposeID = (dr["LoanPurposeID"] as Int32?) ?? null;
					Description = Convert.ToString(dr["Description"]);
					RepaymentOption = Convert.ToString(dr["RepaymentOption"]);
					BankID = (dr["BankID"] as Int32?) ?? null;
					Revenue = (dr["Revenue"] as Decimal?) ?? null;
					RevenuePaid = (Boolean)dr["RevenuePaid"];
					Commission = (dr["Commission"] as Decimal?) ?? null;
					CommissionPaid = (Boolean)dr["CommissionPaid"];
					Approved = (Boolean)dr["Approved"];
					Declined = (Boolean)dr["Declined"];
					ApprovalDate = (dr["ApprovalDate"] as DateTime?) ?? null;
					TradingName = Convert.ToString(dr["TradingName"]);
					SolicitorsFirm = Convert.ToString(dr["SolicitorsFirm"]);
					SolicitorsName = Convert.ToString(dr["SolicitorsName"]);
					SolicitorsPhone = Convert.ToString(dr["SolicitorsPhone"]);
					SolicitorsMobile = Convert.ToString(dr["SolicitorsMobile"]);
					SolicitorsFax = Convert.ToString(dr["SolicitorsFax"]);
					SolicitorsAddLine1 = Convert.ToString(dr["SolicitorsAddLine1"]);
					SolicitorsAddLine2 = Convert.ToString(dr["SolicitorsAddLine2"]);
					SolicitorsAddSuburb = (dr["SolicitorsAddSuburb"] as Int32?) ?? null;
					Email = Convert.ToString(dr["Email"]);
					DX = Convert.ToString(dr["DX"]);
					Details = Convert.ToString(dr["Details"]);
					CompanyName = Convert.ToString(dr["CompanyName"]);
					CompanyACN = Convert.ToString(dr["CompanyACN"]);
					CompanyRegAddLine1 = Convert.ToString(dr["CompanyRegAddLine1"]);
					CompanyRegAddLine2 = Convert.ToString(dr["CompanyRegAddLine2"]);
					CompanyRegSuburb = (dr["CompanyRegSuburb"] as Int32?) ?? null;
					CompanyTradAddLine1 = Convert.ToString(dr["CompanyTradAddLine1"]);
					CompanyTradAddLine2 = Convert.ToString(dr["CompanyTradAddLine2"]);
					CompanyTradeSuburb = (dr["CompanyTradeSuburb"] as Int32?) ?? null;
					CompanyIndustry = Convert.ToString(dr["CompanyIndustry"]);
					CompanyEstablished = Convert.ToString(dr["CompanyEstablished"]);
					CostPrice = (dr["CostPrice"] as Decimal?) ?? null;
					DepositCash = (dr["DepositCash"] as Decimal?) ?? null;
					DepositTrade = (dr["DepositTrade"] as Decimal?) ?? null;
					InsuranceComp = (dr["InsuranceComp"] as Decimal?) ?? null;
					InsuranceOther = (dr["InsuranceOther"] as Decimal?) ?? null;
					Amount = (dr["Amount"] as Decimal?) ?? null;
					Residual = (dr["Residual"] as Int16?) ?? null;
					InterestRate = (dr["InterestRate"] as Double?) ?? null;
					Term = (dr["Term"] as Decimal?) ?? null;
					Frequency = Convert.ToString(dr["Frequency"]);
					StampDuty = (dr["StampDuty"] as Decimal?) ?? null;
					FID = (dr["FID"] as Decimal?) ?? null;
					Total = (dr["Total"] as Decimal?) ?? null;
					ChargeAmount = (dr["ChargeAmount"] as Decimal?) ?? null;
					ChargePercentage = (dr["ChargePercentage"] as Double?) ?? null;
					DefaultAmount = (dr["DefaultAmount"] as Decimal?) ?? null;
					LenderChargeAmount = (dr["LenderChargeAmount"] as Decimal?) ?? null;
					LastUpdated = (dr["LastUpdated"] as DateTime?) ?? null;
					LastUpdatedBy = Convert.ToString(dr["LastUpdatedBy"]);
					Remind = (Boolean)dr["Remind"];
					TaskDone = (Boolean)dr["TaskDone"];
					TaskDoneBy = Convert.ToString(dr["TaskDoneBy"]);
					TaskDetails = Convert.ToString(dr["TaskDetails"]);
					ReminderDate = (dr["ReminderDate"] as DateTime?) ?? null;
					ReminderTimeToRemind = (dr["ReminderTimeToRemind"] as Int16?) ?? null;
					ReminderInterval = Convert.ToString(dr["ReminderInterval"]);
					BusinessWonFromSuburb = Convert.ToString(dr["BusinessWonFromSuburb"]);
					Combined = (Boolean)dr["Combined"];
					StatusEnteredBy = Convert.ToString(dr["StatusEnteredBy"]);
					StatusDate = (dr["StatusDate"] as DateTime?) ?? null;
					ReferralCommision = (dr["ReferralCommision"] as Decimal?) ?? null;
					ReferralCommisionPaid = (Boolean)dr["ReferralCommisionPaid"];
					Referral = Convert.ToString(dr["Referral"]);
					ReferralGrouped = Convert.ToString(dr["ReferralGrouped"]);
					EstMonthlyRepay = (dr["EstMonthlyRepay"] as Decimal?) ?? null;
					MortgageRequest = (dr["MortgageRequest"] as Int32?) ?? null;
					LenderReferenceNo = Convert.ToString(dr["LenderReferenceNo"]);
					LegalCost = (dr["LegalCost"] as Decimal?) ?? null;
					LegalCostDeposit = (dr["LegalCostDeposit"] as Decimal?) ?? null;
					ValuationFees = (dr["ValuationFees"] as Decimal?) ?? null;
					EstablishmentFees = (dr["EstablishmentFees"] as Decimal?) ?? null;
					EarlyRedemption = (dr["EarlyRedemption"] as Int32?) ?? null;
					DefaultPercentageFirst = (dr["DefaultPercentageFirst"] as Double?) ?? null;
					DefaultPercentageSecond = (dr["DefaultPercentageSecond"] as Double?) ?? null;
					TermDisplay = Convert.ToString(dr["TermDisplay"]);
					PrivateFundingApplicationNo = (dr["PrivateFundingApplicationNo"] as Int32?) ?? null;
					LoanFromDate = (dr["LoanFromDate"] as DateTime?) ?? null;
					LoanToDate = (dr["LoanToDate"] as DateTime?) ?? null;
					FinalisedDate = (dr["FinalisedDate"] as DateTime?) ?? null;
					ApprovedDate = (dr["ApprovedDate"] as DateTime?) ?? null;
					LSolicitorsFirm = Convert.ToString(dr["LSolicitorsFirm"]);
					LSolicitorsName = Convert.ToString(dr["LSolicitorsName"]);
					LSolicitorsPhone = Convert.ToString(dr["LSolicitorsPhone"]);
					LSolicitorsMobile = Convert.ToString(dr["LSolicitorsMobile"]);
					LSolicitorsFax = Convert.ToString(dr["LSolicitorsFax"]);
					LSolicitorsAddLine1 = Convert.ToString(dr["LSolicitorsAddLine1"]);
					LSolicitorsAddLine2 = Convert.ToString(dr["LSolicitorsAddLine2"]);
					LSolicitorsAddSuburb = (dr["LSolicitorsAddSuburb"] as Int32?) ?? null;
					LSolicitorEmail = Convert.ToString(dr["LSolicitorEmail"]);
					LSolicitorDX = Convert.ToString(dr["LSolicitorDX"]);
					ValuersFirm = Convert.ToString(dr["ValuersFirm"]);
					ValuersName = Convert.ToString(dr["ValuersName"]);
					ValuersPhone = Convert.ToString(dr["ValuersPhone"]);
					ValuersMobile = Convert.ToString(dr["ValuersMobile"]);
					ValuersFax = Convert.ToString(dr["ValuersFax"]);
					ValuersAddLine1 = Convert.ToString(dr["ValuersAddLine1"]);
					ValuersAddLine2 = Convert.ToString(dr["ValuersAddLine2"]);
					ValuersAddSuburb = (dr["ValuersAddSuburb"] as Int32?) ?? null;
					ValuerEmail = Convert.ToString(dr["ValuerEmail"]);
					ThirdMortgage = (dr["ThirdMortgage"] as Decimal?) ?? null;
					FileLocation = Convert.ToString(dr["FileLocation"]);
					InterestRate3 = (dr["InterestRate3"] as Double?) ?? null;
					DefaultPercentageThird = (dr["DefaultPercentageThird"] as Double?) ?? null;
					ApplicationManager = Convert.ToString(dr["ApplicationManager"]);
					InterestInterval1 = Convert.ToString(dr["InterestInterval1"]);
					InterestInterval2 = Convert.ToString(dr["InterestInterval2"]);
					InterestInterval3 = Convert.ToString(dr["InterestInterval3"]);
					InterestType = Convert.ToString(dr["InterestType"]);
					InterestPaymentType = Convert.ToString(dr["InterestPaymentType"]);
					InterestPaymentTypeYear = (dr["InterestPaymentTypeYear"] as Int32?) ?? null;
					MainIncome_PAYG = (Boolean)dr["MainIncome_PAYG"];
					MainIncome_Self_Employed = (Boolean)dr["MainIncome_Self_Employed"];
					MainIncome_Other = (Boolean)dr["MainIncome_Other"];
					SecurityType_Residential = (Boolean)dr["SecurityType_Residential"];
					SecurityType_Commercial = (Boolean)dr["SecurityType_Commercial"];
					SecurityType_Land = (Boolean)dr["SecurityType_Land"];
					WithdrawnDate = (dr["WithdrawnDate"] as DateTime?) ?? null;
					LoanPurpose_Purchase = (Boolean)dr["LoanPurpose_Purchase"];
					LoanPurpose_Refinance = (Boolean)dr["LoanPurpose_Refinance"];
					LoanPurpose_Other = (Boolean)dr["LoanPurpose_Other"];
					FinanceRequiredBy = (dr["FinanceRequiredBy"] as DateTime?) ?? null;
					InterestTermID = (dr["InterestTermID"] as Int32?) ?? null;
					InterestTerm = (dr["InterestTerm"] as Decimal?) ?? null;
					InterestTermDisplay = Convert.ToString(dr["InterestTermDisplay"]);
					Product = Convert.ToString(dr["Product"]);
					RepaymentFrequency = Convert.ToString(dr["RepaymentFrequency"]);
					DischargeStatusID = (dr["DischargeStatusID"] as Int32?) ?? null;
					DischargeStatusStartDate = (dr["DischargeStatusStartDate"] as DateTime?) ?? null;
					DischargeStatusExpiryDate = (dr["DischargeStatusExpiryDate"] as DateTime?) ?? null;
					DischargeStatusDetails = Convert.ToString(dr["DischargeStatusDetails"]);
					DischargeStatusEnteredBy = Convert.ToString(dr["DischargeStatusEnteredBy"]);
					DischargeStatusDate = (dr["DischargeStatusDate"] as DateTime?) ?? null;
					SalesManager = Convert.ToString(dr["SalesManager"]);
					LoanTerm = Convert.ToString(dr["LoanTerm"]);
					NoOfApplicant = (dr["NoOfApplicant"] as Int32?) ?? null;
					LoanPurpose = Convert.ToString(dr["LoanPurpose"]);
					MortgageInsurance = (dr["MortgageInsurance"] as Decimal?) ?? null;
					LenderFee = (dr["LenderFee"] as Decimal?) ?? null;
					EstSaving = (dr["EstSaving"] as Decimal?) ?? null;
					InsufficientIncome = (dr["InsufficientIncome"] as Boolean?) ?? null;
					SecurityUnacceptable = (dr["SecurityUnacceptable"] as Boolean?) ?? null;
					NoBenefit = (dr["NoBenefit"] as Boolean?) ?? null;
					NoEquity = (dr["NoEquity"] as Boolean?) ?? null;
					OutsideLenderPolicy = (dr["OutsideLenderPolicy"] as Boolean?) ?? null;
					UnDisclosedDebt = (dr["UnDisclosedDebt"] as Boolean?) ?? null;
					STLSubType = Convert.ToString(dr["STLSubType"]);
					STLSecuredType = Convert.ToString(dr["STLSecuredType"]);
					ValuerDX = Convert.ToString(dr["ValuerDX"]);
					AccountantsFirm = Convert.ToString(dr["AccountantsFirm"]);
					AccountantsName = Convert.ToString(dr["AccountantsName"]);
					AccountantsPhone = Convert.ToString(dr["AccountantsPhone"]);
					AccountantsFax = Convert.ToString(dr["AccountantsFax"]);
					AccountantsAddLine1 = Convert.ToString(dr["AccountantsAddLine1"]);
					AccountantsAddLine2 = Convert.ToString(dr["AccountantsAddLine2"]);
					AccountantsSuburb = Convert.ToString(dr["AccountantsSuburb"]);
					AccountantsMobile = Convert.ToString(dr["AccountantsMobile"]);
					AccountantsEmail = Convert.ToString(dr["AccountantsEmail"]);
					AgentName = Convert.ToString(dr["AgentName"]);
					AgentContact = Convert.ToString(dr["AgentContact"]);
					AgentPhone = Convert.ToString(dr["AgentPhone"]);
					AgentMobile = Convert.ToString(dr["AgentMobile"]);
					AgentFax = Convert.ToString(dr["AgentFax"]);
					AgentAddLine1 = Convert.ToString(dr["AgentAddLine1"]);
					AgentAddLine2 = Convert.ToString(dr["AgentAddLine2"]);
					AgentAddSuburb = Convert.ToString(dr["AgentAddSuburb"]);
					AgentEmail = Convert.ToString(dr["AgentEmail"]);
					AgentOpinionProperty = Convert.ToString(dr["AgentOpinionProperty"]);
					AgentOpinionHowmuch = (dr["AgentOpinionHowmuch"] as Decimal?) ?? null;
					AgentOpinionHowQuick = Convert.ToString(dr["AgentOpinionHowQuick"]);
					Agent2Name = Convert.ToString(dr["Agent2Name"]);
					Agent2Contact = Convert.ToString(dr["Agent2Contact"]);
					Agent2Phone = Convert.ToString(dr["Agent2Phone"]);
					Agent2Mobile = Convert.ToString(dr["Agent2Mobile"]);
					Agent2Fax = Convert.ToString(dr["Agent2Fax"]);
					Agent2AddLine1 = Convert.ToString(dr["Agent2AddLine1"]);
					Agent2AddLine2 = Convert.ToString(dr["Agent2AddLine2"]);
					Agent2AddSuburb = Convert.ToString(dr["Agent2AddSuburb"]);
					Agent2Email = Convert.ToString(dr["Agent2Email"]);
					Agent2OpinionProperty = Convert.ToString(dr["Agent2OpinionProperty"]);
					Agent2OpinionHowmuch = (dr["Agent2OpinionHowmuch"] as Decimal?) ?? null;
					Agent2OpinionHowQuick = Convert.ToString(dr["Agent2OpinionHowQuick"]);
					CompoundPeriod = Convert.ToString(dr["CompoundPeriod"]);
					LoanDate = (dr["LoanDate"] as DateTime?) ?? null;
					FirstPaymentDate = (dr["FirstPaymentDate"] as DateTime?) ?? null;
					OfferDate = (dr["OfferDate"] as DateTime?) ?? null;
					LoanAmountInWord = Convert.ToString(dr["LoanAmountInWord"]);
					OriRequestedAmount = (dr["OriRequestedAmount"] as Decimal?) ?? null;
					AddFeeToLoan = (dr["AddFeeToLoan"] as Boolean?) ?? null;
					CLazyNoTime = (dr["CLazyNoTime"] as Boolean?) ?? null;
					CNotGetPaperwork = (dr["CNotGetPaperwork"] as Boolean?) ?? null;
					CWentElsewhere = (dr["CWentElsewhere"] as Boolean?) ?? null;
					CPartnerSayNo = (dr["CPartnerSayNo"] as Boolean?) ?? null;
					CSellingProperty = (dr["CSellingProperty"] as Boolean?) ?? null;
					CFamilyFriendAssisted = (dr["CFamilyFriendAssisted"] as Boolean?) ?? null;
					CFeeTooExpensive = (dr["CFeeTooExpensive"] as Boolean?) ?? null;
					CNotUsePropertyAsSecurity = (dr["CNotUsePropertyAsSecurity"] as Boolean?) ?? null;
					CCircumstancesChanged = (dr["CCircumstancesChanged"] as Boolean?) ?? null;
					PayOutDate = (dr["PayOutDate"] as DateTime?) ?? null;
					SettlementFee = (dr["SettlementFee"] as Decimal?) ?? null;
					EarlyPayoutFee = (dr["EarlyPayoutFee"] as Decimal?) ?? null;
					PayOutInterest = (dr["PayOutInterest"] as Decimal?) ?? null;
					STLPaymentOption = (dr["STLPaymentOption"] as Int16?) ?? null;
					SentToASTL = (dr["SentToASTL"] as Boolean?) ?? null;
					Last6MthHomeLoanPaymentUptodate = Convert.ToString(dr["Last6MthHomeLoanPaymentUptodate"]);
					Last6MthHomeLoanPaymentLateMth = (dr["Last6MthHomeLoanPaymentLateMth"] as Int16?) ?? null;
					PastHomeLoanPaymentUptodate = Convert.ToString(dr["PastHomeLoanPaymentUptodate"]);
					CouncilPaymentUptodate = Convert.ToString(dr["CouncilPaymentUptodate"]);
					CouncilPaymentArrearAmt = (dr["CouncilPaymentArrearAmt"] as Decimal?) ?? null;
					LenderAssessSummary = Convert.ToString(dr["LenderAssessSummary"]);
					OnHold = (dr["OnHold"] as Boolean?) ?? null;
					ClientID = (dr["ClientID"] as Int32?) ?? null;
					RetainerAmt = (dr["RetainerAmt"] as Decimal?) ?? null;
					RetainerFrequency = Convert.ToString(dr["RetainerFrequency"]);
					ApplicationFee = (dr["ApplicationFee"] as Decimal?) ?? null;
					TotalLoanAmount = (dr["TotalLoanAmount"] as Decimal?) ?? null;
					LoanWriter = Convert.ToString(dr["LoanWriter"]);
					InterviewDate = (dr["InterviewDate"] as DateTime?) ?? null;
					FinanceRequiredByDate = (dr["FinanceRequiredByDate"] as DateTime?) ?? null;
					ConversionBy = Convert.ToString(dr["ConversionBy"]);
					SigningCostAgreementDate = (dr["SigningCostAgreementDate"] as DateTime?) ?? null;
					SigningLetterOfOfferDate = (dr["SigningLetterOfOfferDate"] as DateTime?) ?? null;
					CreditFeeType = (dr["CreditFeeType"] as Byte?) ?? null;
					CreditFeeOverwrite = (dr["CreditFeeOverwrite"] as Decimal?) ?? null;
					CreditFeeOverwriteNote = Convert.ToString(dr["CreditFeeOverwriteNote"]);
					ChargePercentageProposal = (dr["ChargePercentageProposal"] as Double?) ?? null;
					UpfrontRate = (dr["UpfrontRate"] as Double?) ?? null;
					TrailRate = (dr["TrailRate"] as Double?) ?? null;
					OutstandingFeeSettled = (dr["OutstandingFeeSettled"] as Boolean?) ?? null;
					SettlementManager = Convert.ToString(dr["SettlementManager"]);
					ActualFeesToCharge = (dr["ActualFeesToCharge"] as Decimal?) ?? null;
					AssessingComments = Convert.ToString(dr["AssessingComments"]);
					GUID = Convert.ToString(dr["GUID"]);

					dr.Close();

					if (LoanPurposeID != null)
					{ 
						SqlCommand cmd2 = new SqlCommand("SELECT * FROM LoanPurpose WHERE LoanPurposeID = " + LoanPurposeID , conn);						
						SqlDataReader dr2 = cmd2.ExecuteReader();
						if (dr2.Read()) LoanPurposeSelected = Convert.ToString(dr2["LoanPurpose"]);
						dr2.Close();
					}

					SqlCommand cmd3 = new SqlCommand("SELECT * FROM ApplicationExtWebApp WHERE ApplicationNo = " + ApplicationNo, conn);
					SqlDataReader dr3 = cmd3.ExecuteReader();
					if (dr3.Read())
					{
						Web_InvitationEmailSent = (dr3["Web_InvitationEmailSent"] as DateTime?) ?? null;
						Web_ApplicationFormStarted = (dr3["Web_ApplicationFormStarted"] as DateTime?) ?? null;
						Web_ApplicationFormFinished = (dr3["Web_ApplicationFormFinished"] as DateTime?) ?? null;
						ReceiveFurtherInformationTicked = (Boolean)dr3["ReceiveFurtherInformationTicked"];
						AgreedToTermsAndConditions = (Boolean)dr3["AgreedToTermsAndConditions"];
						AgreedToTermsAndConditionsDate = (dr3["AgreedToTermsAndConditionsDate"] as DateTime?) ?? null;
						IPAddress = Convert.ToString(dr3["IPAddress"]);

						dr3.Close();
					}
				}
				else { throw new Exception("Invalid GUID"); }
			}
		}

		public void LoadCompany()
		{
			Company = new MMCompany(CompanyID);
		}

		public void LoadApplicants()
		{
			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT * FROM ApplicantApplication WHERE ApplicationNo = " + ApplicationNo.ToString() + " ORDER BY MainContact DESC", conn);
				conn.Open();
				SqlDataReader dr = cmd.ExecuteReader();
				if (dr.Read())
				{
					MMApplicant1 = new MMApplicant(Convert.ToInt32(dr["ApplicantID"]));
					try
					{
						dr.Read();
						MMApplicant2 = new MMApplicant(Convert.ToInt32(dr["ApplicantID"]));
					}
					catch
					{
						//means there was only 1 applicant... 
						MMApplicant2 = null;

					}
				}
			}
		}

		public void Save()
		{
			//building the UPDATE string...
			StringBuilder sql = new StringBuilder();
			sql.Append("UPDATE Application SET ");
			//sql.Append("Title = @Title, ");			
			sql.Append("CompanyID = @CompanyID, ");
			sql.Append("AppDate = @AppDate, ");
			sql.Append("EnteredBy = @EnteredBy, ");
			sql.Append("LoanType = @LoanType, ");
			sql.Append("StatusID = @StatusID, ");
			sql.Append("StatusStartDate = @StatusStartDate, ");
			sql.Append("StatusExpiryDate = @StatusExpiryDate, ");
			sql.Append("StatusDetails = @StatusDetails, ");
			sql.Append("RequestedAmount = @RequestedAmount, ");
			sql.Append("InterestRate1 = @InterestRate1, ");
			sql.Append("SecondMortgage = @SecondMortgage, ");
			sql.Append("InterestRate2 = @InterestRate2, ");
			sql.Append("Valuation = @Valuation, ");
			sql.Append("LoanPurposeID = @LoanPurposeID, ");
			sql.Append("Description = @Description, ");
			sql.Append("RepaymentOption = @RepaymentOption, ");
			sql.Append("BankID = @BankID, ");
			sql.Append("Revenue = @Revenue, ");
			sql.Append("RevenuePaid = @RevenuePaid, ");
			sql.Append("Commission = @Commission, ");
			sql.Append("CommissionPaid = @CommissionPaid, ");
			sql.Append("Approved = @Approved, ");
			sql.Append("Declined = @Declined, ");
			sql.Append("ApprovalDate = @ApprovalDate, ");
			sql.Append("TradingName = @TradingName, ");
			sql.Append("SolicitorsFirm = @SolicitorsFirm, ");
			sql.Append("SolicitorsName = @SolicitorsName, ");
			sql.Append("SolicitorsPhone = @SolicitorsPhone, ");
			sql.Append("SolicitorsMobile = @SolicitorsMobile, ");
			sql.Append("SolicitorsFax = @SolicitorsFax, ");
			sql.Append("SolicitorsAddLine1 = @SolicitorsAddLine1, ");
			sql.Append("SolicitorsAddLine2 = @SolicitorsAddLine2, ");
			sql.Append("SolicitorsAddSuburb = @SolicitorsAddSuburb, ");
			sql.Append("Email = @Email, ");
			sql.Append("DX = @DX, ");
			sql.Append("Details = @Details, ");
			sql.Append("CompanyName = @CompanyName, ");
			sql.Append("CompanyACN = @CompanyACN, ");
			sql.Append("CompanyRegAddLine1 = @CompanyRegAddLine1, ");
			sql.Append("CompanyRegAddLine2 = @CompanyRegAddLine2, ");
			sql.Append("CompanyRegSuburb = @CompanyRegSuburb, ");
			sql.Append("CompanyTradAddLine1 = @CompanyTradAddLine1, ");
			sql.Append("CompanyTradAddLine2 = @CompanyTradAddLine2, ");
			sql.Append("CompanyTradeSuburb = @CompanyTradeSuburb, ");
			sql.Append("CompanyIndustry = @CompanyIndustry, ");
			sql.Append("CompanyEstablished = @CompanyEstablished, ");
			sql.Append("CostPrice = @CostPrice, ");
			sql.Append("DepositCash = @DepositCash, ");
			sql.Append("DepositTrade = @DepositTrade, ");
			sql.Append("InsuranceComp = @InsuranceComp, ");
			sql.Append("InsuranceOther = @InsuranceOther, ");
			sql.Append("Amount = @Amount, ");
			sql.Append("Residual = @Residual, ");
			sql.Append("InterestRate = @InterestRate, ");
			sql.Append("Term = @Term, ");
			sql.Append("Frequency = @Frequency, ");
			sql.Append("StampDuty = @StampDuty, ");
			sql.Append("FID = @FID, ");
			sql.Append("Total = @Total, ");
			sql.Append("ChargeAmount = @ChargeAmount, ");
			sql.Append("ChargePercentage = @ChargePercentage, ");
			sql.Append("DefaultAmount = @DefaultAmount, ");
			sql.Append("LenderChargeAmount = @LenderChargeAmount, ");
			sql.Append("LastUpdated = @LastUpdated, ");
			sql.Append("LastUpdatedBy = @LastUpdatedBy, ");
			sql.Append("Remind = @Remind, ");
			sql.Append("TaskDone = @TaskDone, ");
			sql.Append("TaskDoneBy = @TaskDoneBy, ");
			sql.Append("TaskDetails = @TaskDetails, ");
			sql.Append("ReminderDate = @ReminderDate, ");
			sql.Append("ReminderTimeToRemind = @ReminderTimeToRemind, ");
			sql.Append("ReminderInterval = @ReminderInterval, ");
			sql.Append("BusinessWonFromSuburb = @BusinessWonFromSuburb, ");
			sql.Append("Combined = @Combined, ");
			sql.Append("StatusEnteredBy = @StatusEnteredBy, ");
			sql.Append("StatusDate = @StatusDate, ");
			sql.Append("ReferralCommision = @ReferralCommision, ");
			sql.Append("ReferralCommisionPaid = @ReferralCommisionPaid, ");
			sql.Append("Referral = @Referral, ");
			sql.Append("ReferralGrouped = @ReferralGrouped, ");
			sql.Append("EstMonthlyRepay = @EstMonthlyRepay, ");
			sql.Append("MortgageRequest = @MortgageRequest, ");
			sql.Append("LenderReferenceNo = @LenderReferenceNo, ");
			sql.Append("LegalCost = @LegalCost, ");
			sql.Append("LegalCostDeposit = @LegalCostDeposit, ");
			sql.Append("ValuationFees = @ValuationFees, ");
			sql.Append("EstablishmentFees = @EstablishmentFees, ");
			sql.Append("EarlyRedemption = @EarlyRedemption, ");
			sql.Append("DefaultPercentageFirst = @DefaultPercentageFirst, ");
			sql.Append("DefaultPercentageSecond = @DefaultPercentageSecond, ");
			sql.Append("TermDisplay = @TermDisplay, ");
			sql.Append("PrivateFundingApplicationNo = @PrivateFundingApplicationNo, ");
			sql.Append("LoanFromDate = @LoanFromDate, ");
			sql.Append("LoanToDate = @LoanToDate, ");
			sql.Append("FinalisedDate = @FinalisedDate, ");
			sql.Append("ApprovedDate = @ApprovedDate, ");
			sql.Append("LSolicitorsFirm = @LSolicitorsFirm, ");
			sql.Append("LSolicitorsName = @LSolicitorsName, ");
			sql.Append("LSolicitorsPhone = @LSolicitorsPhone, ");
			sql.Append("LSolicitorsMobile = @LSolicitorsMobile, ");
			sql.Append("LSolicitorsFax = @LSolicitorsFax, ");
			sql.Append("LSolicitorsAddLine1 = @LSolicitorsAddLine1, ");
			sql.Append("LSolicitorsAddLine2 = @LSolicitorsAddLine2, ");
			sql.Append("LSolicitorsAddSuburb = @LSolicitorsAddSuburb, ");
			sql.Append("LSolicitorEmail = @LSolicitorEmail, ");
			sql.Append("LSolicitorDX = @LSolicitorDX, ");
			sql.Append("ValuersFirm = @ValuersFirm, ");
			sql.Append("ValuersName = @ValuersName, ");
			sql.Append("ValuersPhone = @ValuersPhone, ");
			sql.Append("ValuersMobile = @ValuersMobile, ");
			sql.Append("ValuersFax = @ValuersFax, ");
			sql.Append("ValuersAddLine1 = @ValuersAddLine1, ");
			sql.Append("ValuersAddLine2 = @ValuersAddLine2, ");
			sql.Append("ValuersAddSuburb = @ValuersAddSuburb, ");
			sql.Append("ValuerEmail = @ValuerEmail, ");
			sql.Append("ThirdMortgage = @ThirdMortgage, ");
			sql.Append("FileLocation = @FileLocation, ");
			sql.Append("InterestRate3 = @InterestRate3, ");
			sql.Append("DefaultPercentageThird = @DefaultPercentageThird, ");
			sql.Append("ApplicationManager = @ApplicationManager, ");
			sql.Append("InterestInterval1 = @InterestInterval1, ");
			sql.Append("InterestInterval2 = @InterestInterval2, ");
			sql.Append("InterestInterval3 = @InterestInterval3, ");
			sql.Append("InterestType = @InterestType, ");
			sql.Append("InterestPaymentType = @InterestPaymentType, ");
			sql.Append("InterestPaymentTypeYear = @InterestPaymentTypeYear, ");
			sql.Append("MainIncome_PAYG = @MainIncome_PAYG, ");
			sql.Append("MainIncome_Self_Employed = @MainIncome_Self_Employed, ");
			sql.Append("MainIncome_Other = @MainIncome_Other, ");
			sql.Append("SecurityType_Residential = @SecurityType_Residential, ");
			sql.Append("SecurityType_Commercial = @SecurityType_Commercial, ");
			sql.Append("SecurityType_Land = @SecurityType_Land, ");
			sql.Append("WithdrawnDate = @WithdrawnDate, ");
			sql.Append("LoanPurpose_Purchase = @LoanPurpose_Purchase, ");
			sql.Append("LoanPurpose_Refinance = @LoanPurpose_Refinance, ");
			sql.Append("LoanPurpose_Other = @LoanPurpose_Other, ");
			sql.Append("FinanceRequiredBy = @FinanceRequiredBy, ");
			sql.Append("InterestTermID = @InterestTermID, ");
			sql.Append("InterestTerm = @InterestTerm, ");
			sql.Append("InterestTermDisplay = @InterestTermDisplay, ");
			sql.Append("Product = @Product, ");
			sql.Append("RepaymentFrequency = @RepaymentFrequency, ");
			sql.Append("DischargeStatusID = @DischargeStatusID, ");
			sql.Append("DischargeStatusStartDate = @DischargeStatusStartDate, ");
			sql.Append("DischargeStatusExpiryDate = @DischargeStatusExpiryDate, ");
			sql.Append("DischargeStatusDetails = @DischargeStatusDetails, ");
			sql.Append("DischargeStatusEnteredBy = @DischargeStatusEnteredBy, ");
			sql.Append("DischargeStatusDate = @DischargeStatusDate, ");
			sql.Append("SalesManager = @SalesManager, ");
			sql.Append("LoanTerm = @LoanTerm, ");
			sql.Append("NoOfApplicant = @NoOfApplicant, ");
			sql.Append("LoanPurpose = @LoanPurpose, ");
			sql.Append("MortgageInsurance = @MortgageInsurance, ");
			sql.Append("LenderFee = @LenderFee, ");
			sql.Append("EstSaving = @EstSaving, ");
			sql.Append("InsufficientIncome = @InsufficientIncome, ");
			sql.Append("SecurityUnacceptable = @SecurityUnacceptable, ");
			sql.Append("NoBenefit = @NoBenefit, ");
			sql.Append("NoEquity = @NoEquity, ");
			sql.Append("OutsideLenderPolicy = @OutsideLenderPolicy, ");
			sql.Append("UnDisclosedDebt = @UnDisclosedDebt, ");
			sql.Append("STLSubType = @STLSubType, ");
			sql.Append("STLSecuredType = @STLSecuredType, ");
			sql.Append("ValuerDX = @ValuerDX, ");
			sql.Append("AccountantsFirm = @AccountantsFirm, ");
			sql.Append("AccountantsName = @AccountantsName, ");
			sql.Append("AccountantsPhone = @AccountantsPhone, ");
			sql.Append("AccountantsFax = @AccountantsFax, ");
			sql.Append("AccountantsAddLine1 = @AccountantsAddLine1, ");
			sql.Append("AccountantsAddLine2 = @AccountantsAddLine2, ");
			sql.Append("AccountantsSuburb = @AccountantsSuburb, ");
			sql.Append("AccountantsMobile = @AccountantsMobile, ");
			sql.Append("AccountantsEmail = @AccountantsEmail, ");
			sql.Append("AgentName = @AgentName, ");
			sql.Append("AgentContact = @AgentContact, ");
			sql.Append("AgentPhone = @AgentPhone, ");
			sql.Append("AgentMobile = @AgentMobile, ");
			sql.Append("AgentFax = @AgentFax, ");
			sql.Append("AgentAddLine1 = @AgentAddLine1, ");
			sql.Append("AgentAddLine2 = @AgentAddLine2, ");
			sql.Append("AgentAddSuburb = @AgentAddSuburb, ");
			sql.Append("AgentEmail = @AgentEmail, ");
			sql.Append("AgentOpinionProperty = @AgentOpinionProperty, ");
			sql.Append("AgentOpinionHowmuch = @AgentOpinionHowmuch, ");
			sql.Append("AgentOpinionHowQuick = @AgentOpinionHowQuick, ");
			sql.Append("Agent2Name = @Agent2Name, ");
			sql.Append("Agent2Contact = @Agent2Contact, ");
			sql.Append("Agent2Phone = @Agent2Phone, ");
			sql.Append("Agent2Mobile = @Agent2Mobile, ");
			sql.Append("Agent2Fax = @Agent2Fax, ");
			sql.Append("Agent2AddLine1 = @Agent2AddLine1, ");
			sql.Append("Agent2AddLine2 = @Agent2AddLine2, ");
			sql.Append("Agent2AddSuburb = @Agent2AddSuburb, ");
			sql.Append("Agent2Email = @Agent2Email, ");
			sql.Append("Agent2OpinionProperty = @Agent2OpinionProperty, ");
			sql.Append("Agent2OpinionHowmuch = @Agent2OpinionHowmuch, ");
			sql.Append("Agent2OpinionHowQuick = @Agent2OpinionHowQuick, ");
			sql.Append("CompoundPeriod = @CompoundPeriod, ");
			sql.Append("LoanDate = @LoanDate, ");
			sql.Append("FirstPaymentDate = @FirstPaymentDate, ");
			sql.Append("OfferDate = @OfferDate, ");
			sql.Append("LoanAmountInWord = @LoanAmountInWord, ");
			sql.Append("OriRequestedAmount = @OriRequestedAmount, ");
			sql.Append("AddFeeToLoan = @AddFeeToLoan, ");
			sql.Append("CLazyNoTime = @CLazyNoTime, ");
			sql.Append("CNotGetPaperwork = @CNotGetPaperwork, ");
			sql.Append("CWentElsewhere = @CWentElsewhere, ");
			sql.Append("CPartnerSayNo = @CPartnerSayNo, ");
			sql.Append("CSellingProperty = @CSellingProperty, ");
			sql.Append("CFamilyFriendAssisted = @CFamilyFriendAssisted, ");
			sql.Append("CFeeTooExpensive = @CFeeTooExpensive, ");
			sql.Append("CNotUsePropertyAsSecurity = @CNotUsePropertyAsSecurity, ");
			sql.Append("CCircumstancesChanged = @CCircumstancesChanged, ");
			sql.Append("PayOutDate = @PayOutDate, ");
			sql.Append("SettlementFee = @SettlementFee, ");
			sql.Append("EarlyPayoutFee = @EarlyPayoutFee, ");
			sql.Append("PayOutInterest = @PayOutInterest, ");
			sql.Append("STLPaymentOption = @STLPaymentOption, ");
			sql.Append("SentToASTL = @SentToASTL, ");
			sql.Append("Last6MthHomeLoanPaymentUptodate = @Last6MthHomeLoanPaymentUptodate, ");
			sql.Append("Last6MthHomeLoanPaymentLateMth = @Last6MthHomeLoanPaymentLateMth, ");
			sql.Append("PastHomeLoanPaymentUptodate = @PastHomeLoanPaymentUptodate, ");
			sql.Append("CouncilPaymentUptodate = @CouncilPaymentUptodate, ");
			sql.Append("CouncilPaymentArrearAmt = @CouncilPaymentArrearAmt, ");
			sql.Append("LenderAssessSummary = @LenderAssessSummary, ");
			sql.Append("OnHold = @OnHold, ");
			sql.Append("ClientID = @ClientID, ");
			sql.Append("RetainerAmt = @RetainerAmt, ");
			sql.Append("RetainerFrequency = @RetainerFrequency, ");
			sql.Append("ApplicationFee = @ApplicationFee, ");
			sql.Append("TotalLoanAmount = @TotalLoanAmount, ");
			sql.Append("LoanWriter = @LoanWriter, ");
			sql.Append("InterviewDate = @InterviewDate, ");
			sql.Append("FinanceRequiredByDate = @FinanceRequiredByDate, ");
			sql.Append("ConversionBy = @ConversionBy, ");
			sql.Append("SigningCostAgreementDate = @SigningCostAgreementDate, ");
			sql.Append("SigningLetterOfOfferDate = @SigningLetterOfOfferDate, ");
			sql.Append("CreditFeeType = @CreditFeeType, ");
			sql.Append("CreditFeeOverwrite = @CreditFeeOverwrite, ");
			sql.Append("CreditFeeOverwriteNote = @CreditFeeOverwriteNote, ");
			sql.Append("ChargePercentageProposal = @ChargePercentageProposal, ");
			sql.Append("UpfrontRate = @UpfrontRate, ");
			sql.Append("TrailRate = @TrailRate, ");
			sql.Append("OutstandingFeeSettled = @OutstandingFeeSettled, ");
			sql.Append("SettlementManager = @SettlementManager, ");
			sql.Append("ActualFeesToCharge = @ActualFeesToCharge, ");
			sql.Append("AssessingComments = @AssessingComments ");

			sql.Append("WHERE ApplicationNo = @ApplicationNo");
			sql.Append("; UPDATE ApplicationExtWebApp SET ");
			sql.Append("Web_InvitationEmailSent = @Web_InvitationEmailSent, ");
			sql.Append("Web_ApplicationFormStarted = @Web_ApplicationFormStarted, ");
			sql.Append("Web_ApplicationFormFinished = @Web_ApplicationFormFinished, ");
			sql.Append("ReceiveFurtherInformationTicked = @ReceiveFurtherInformationTicked, ");
			sql.Append("AgreedToTermsAndConditions = @AgreedToTermsAndConditions, ");
			sql.Append("AgreedToTermsAndConditionsDate = @AgreedToTermsAndConditionsDate, ");
			sql.Append("IPAddress = @IPAddress ");


			sql.Append("WHERE ApplicationNo = @ApplicationNo");

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
			{
				SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

				cmd.Parameters.Add(new SqlParameter("ApplicationNo", ((object)ApplicationNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompanyID", ((object)CompanyID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AppDate", ((object)AppDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EnteredBy", ((object)EnteredBy ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanType", ((object)LoanType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("StatusID", ((object)StatusID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("StatusStartDate", ((object)StatusStartDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("StatusExpiryDate", ((object)StatusExpiryDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("StatusDetails", ((object)StatusDetails ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("RequestedAmount", ((object)RequestedAmount ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestRate1", ((object)InterestRate1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecondMortgage", ((object)SecondMortgage ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestRate2", ((object)InterestRate2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Valuation", ((object)Valuation ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanPurposeID", ((object)LoanPurposeID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Description", ((object)Description ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("RepaymentOption", ((object)RepaymentOption ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("BankID", ((object)BankID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Revenue", ((object)Revenue ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("RevenuePaid", ((object)RevenuePaid ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Commission", ((object)Commission ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CommissionPaid", ((object)CommissionPaid ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Approved", ((object)Approved ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Declined", ((object)Declined ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ApprovalDate", ((object)ApprovalDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TradingName", ((object)TradingName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SolicitorsFirm", ((object)SolicitorsFirm ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SolicitorsName", ((object)SolicitorsName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SolicitorsPhone", ((object)SolicitorsPhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SolicitorsMobile", ((object)SolicitorsMobile ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SolicitorsFax", ((object)SolicitorsFax ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SolicitorsAddLine1", ((object)SolicitorsAddLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SolicitorsAddLine2", ((object)SolicitorsAddLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SolicitorsAddSuburb", ((object)SolicitorsAddSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Email", ((object)Email ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DX", ((object)DX ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Details", ((object)Details ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompanyName", ((object)CompanyName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompanyACN", ((object)CompanyACN ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompanyRegAddLine1", ((object)CompanyRegAddLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompanyRegAddLine2", ((object)CompanyRegAddLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompanyRegSuburb", ((object)CompanyRegSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompanyTradAddLine1", ((object)CompanyTradAddLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompanyTradAddLine2", ((object)CompanyTradAddLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompanyTradeSuburb", ((object)CompanyTradeSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompanyIndustry", ((object)CompanyIndustry ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompanyEstablished", ((object)CompanyEstablished ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CostPrice", ((object)CostPrice ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DepositCash", ((object)DepositCash ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DepositTrade", ((object)DepositTrade ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InsuranceComp", ((object)InsuranceComp ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InsuranceOther", ((object)InsuranceOther ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Amount", ((object)Amount ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Residual", ((object)Residual ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestRate", ((object)InterestRate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Term", ((object)Term ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Frequency", ((object)Frequency ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("StampDuty", ((object)StampDuty ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FID", ((object)FID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Total", ((object)Total ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ChargeAmount", ((object)ChargeAmount ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ChargePercentage", ((object)ChargePercentage ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DefaultAmount", ((object)DefaultAmount ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LenderChargeAmount", ((object)LenderChargeAmount ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LastUpdated", ((object)LastUpdated ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LastUpdatedBy", ((object)LastUpdatedBy ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Remind", ((object)Remind ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TaskDone", ((object)TaskDone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TaskDoneBy", ((object)TaskDoneBy ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TaskDetails", ((object)TaskDetails ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ReminderDate", ((object)ReminderDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ReminderTimeToRemind", ((object)ReminderTimeToRemind ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ReminderInterval", ((object)ReminderInterval ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("BusinessWonFromSuburb", ((object)BusinessWonFromSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Combined", ((object)Combined ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("StatusEnteredBy", ((object)StatusEnteredBy ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("StatusDate", ((object)StatusDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ReferralCommision", ((object)ReferralCommision ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ReferralCommisionPaid", ((object)ReferralCommisionPaid ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Referral", ((object)Referral ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ReferralGrouped", ((object)ReferralGrouped ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EstMonthlyRepay", ((object)EstMonthlyRepay ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("MortgageRequest", ((object)MortgageRequest ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LenderReferenceNo", ((object)LenderReferenceNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LegalCost", ((object)LegalCost ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LegalCostDeposit", ((object)LegalCostDeposit ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ValuationFees", ((object)ValuationFees ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EstablishmentFees", ((object)EstablishmentFees ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EarlyRedemption", ((object)EarlyRedemption ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DefaultPercentageFirst", ((object)DefaultPercentageFirst ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DefaultPercentageSecond", ((object)DefaultPercentageSecond ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TermDisplay", ((object)TermDisplay ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrivateFundingApplicationNo", ((object)PrivateFundingApplicationNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanFromDate", ((object)LoanFromDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanToDate", ((object)LoanToDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FinalisedDate", ((object)FinalisedDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ApprovedDate", ((object)ApprovedDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LSolicitorsFirm", ((object)LSolicitorsFirm ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LSolicitorsName", ((object)LSolicitorsName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LSolicitorsPhone", ((object)LSolicitorsPhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LSolicitorsMobile", ((object)LSolicitorsMobile ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LSolicitorsFax", ((object)LSolicitorsFax ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LSolicitorsAddLine1", ((object)LSolicitorsAddLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LSolicitorsAddLine2", ((object)LSolicitorsAddLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LSolicitorsAddSuburb", ((object)LSolicitorsAddSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LSolicitorEmail", ((object)LSolicitorEmail ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LSolicitorDX", ((object)LSolicitorDX ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ValuersFirm", ((object)ValuersFirm ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ValuersName", ((object)ValuersName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ValuersPhone", ((object)ValuersPhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ValuersMobile", ((object)ValuersMobile ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ValuersFax", ((object)ValuersFax ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ValuersAddLine1", ((object)ValuersAddLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ValuersAddLine2", ((object)ValuersAddLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ValuersAddSuburb", ((object)ValuersAddSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ValuerEmail", ((object)ValuerEmail ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ThirdMortgage", ((object)ThirdMortgage ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FileLocation", ((object)FileLocation ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestRate3", ((object)InterestRate3 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DefaultPercentageThird", ((object)DefaultPercentageThird ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ApplicationManager", ((object)ApplicationManager ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestInterval1", ((object)InterestInterval1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestInterval2", ((object)InterestInterval2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestInterval3", ((object)InterestInterval3 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestType", ((object)InterestType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestPaymentType", ((object)InterestPaymentType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestPaymentTypeYear", ((object)InterestPaymentTypeYear ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("MainIncome_PAYG", ((object)MainIncome_PAYG ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("MainIncome_Self_Employed", ((object)MainIncome_Self_Employed ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("MainIncome_Other", ((object)MainIncome_Other ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecurityType_Residential", ((object)SecurityType_Residential ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecurityType_Commercial", ((object)SecurityType_Commercial ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecurityType_Land", ((object)SecurityType_Land ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("WithdrawnDate", ((object)WithdrawnDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanPurpose_Purchase", ((object)LoanPurpose_Purchase ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanPurpose_Refinance", ((object)LoanPurpose_Refinance ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanPurpose_Other", ((object)LoanPurpose_Other ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FinanceRequiredBy", ((object)FinanceRequiredBy ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestTermID", ((object)InterestTermID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestTerm", ((object)InterestTerm ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterestTermDisplay", ((object)InterestTermDisplay ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Product", ((object)Product ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("RepaymentFrequency", ((object)RepaymentFrequency ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DischargeStatusID", ((object)DischargeStatusID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DischargeStatusStartDate", ((object)DischargeStatusStartDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DischargeStatusExpiryDate", ((object)DischargeStatusExpiryDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DischargeStatusDetails", ((object)DischargeStatusDetails ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DischargeStatusEnteredBy", ((object)DischargeStatusEnteredBy ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DischargeStatusDate", ((object)DischargeStatusDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SalesManager", ((object)SalesManager ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanTerm", ((object)LoanTerm ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("NoOfApplicant", ((object)NoOfApplicant ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanPurpose", ((object)LoanPurpose ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("MortgageInsurance", ((object)MortgageInsurance ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LenderFee", ((object)LenderFee ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EstSaving", ((object)EstSaving ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InsufficientIncome", ((object)InsufficientIncome ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecurityUnacceptable", ((object)SecurityUnacceptable ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("NoBenefit", ((object)NoBenefit ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("NoEquity", ((object)NoEquity ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OutsideLenderPolicy", ((object)OutsideLenderPolicy ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("UnDisclosedDebt", ((object)UnDisclosedDebt ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("STLSubType", ((object)STLSubType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("STLSecuredType", ((object)STLSecuredType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ValuerDX", ((object)ValuerDX ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantsFirm", ((object)AccountantsFirm ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantsName", ((object)AccountantsName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantsPhone", ((object)AccountantsPhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantsFax", ((object)AccountantsFax ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantsAddLine1", ((object)AccountantsAddLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantsAddLine2", ((object)AccountantsAddLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantsSuburb", ((object)AccountantsSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantsMobile", ((object)AccountantsMobile ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantsEmail", ((object)AccountantsEmail ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentName", ((object)AgentName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentContact", ((object)AgentContact ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentPhone", ((object)AgentPhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentMobile", ((object)AgentMobile ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentFax", ((object)AgentFax ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentAddLine1", ((object)AgentAddLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentAddLine2", ((object)AgentAddLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentAddSuburb", ((object)AgentAddSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentEmail", ((object)AgentEmail ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentOpinionProperty", ((object)AgentOpinionProperty ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentOpinionHowmuch", ((object)AgentOpinionHowmuch ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgentOpinionHowQuick", ((object)AgentOpinionHowQuick ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2Name", ((object)Agent2Name ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2Contact", ((object)Agent2Contact ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2Phone", ((object)Agent2Phone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2Mobile", ((object)Agent2Mobile ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2Fax", ((object)Agent2Fax ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2AddLine1", ((object)Agent2AddLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2AddLine2", ((object)Agent2AddLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2AddSuburb", ((object)Agent2AddSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2Email", ((object)Agent2Email ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2OpinionProperty", ((object)Agent2OpinionProperty ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2OpinionHowmuch", ((object)Agent2OpinionHowmuch ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Agent2OpinionHowQuick", ((object)Agent2OpinionHowQuick ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CompoundPeriod", ((object)CompoundPeriod ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanDate", ((object)LoanDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FirstPaymentDate", ((object)FirstPaymentDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OfferDate", ((object)OfferDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanAmountInWord", ((object)LoanAmountInWord ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OriRequestedAmount", ((object)OriRequestedAmount ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AddFeeToLoan", ((object)AddFeeToLoan ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CLazyNoTime", ((object)CLazyNoTime ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CNotGetPaperwork", ((object)CNotGetPaperwork ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CWentElsewhere", ((object)CWentElsewhere ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CPartnerSayNo", ((object)CPartnerSayNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CSellingProperty", ((object)CSellingProperty ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CFamilyFriendAssisted", ((object)CFamilyFriendAssisted ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CFeeTooExpensive", ((object)CFeeTooExpensive ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CNotUsePropertyAsSecurity", ((object)CNotUsePropertyAsSecurity ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CCircumstancesChanged", ((object)CCircumstancesChanged ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PayOutDate", ((object)PayOutDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SettlementFee", ((object)SettlementFee ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EarlyPayoutFee", ((object)EarlyPayoutFee ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PayOutInterest", ((object)PayOutInterest ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("STLPaymentOption", ((object)STLPaymentOption ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SentToASTL", ((object)SentToASTL ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Last6MthHomeLoanPaymentUptodate", ((object)Last6MthHomeLoanPaymentUptodate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Last6MthHomeLoanPaymentLateMth", ((object)Last6MthHomeLoanPaymentLateMth ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PastHomeLoanPaymentUptodate", ((object)PastHomeLoanPaymentUptodate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CouncilPaymentUptodate", ((object)CouncilPaymentUptodate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CouncilPaymentArrearAmt", ((object)CouncilPaymentArrearAmt ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LenderAssessSummary", ((object)LenderAssessSummary ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OnHold", ((object)OnHold ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ClientID", ((object)ClientID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("RetainerAmt", ((object)RetainerAmt ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("RetainerFrequency", ((object)RetainerFrequency ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ApplicationFee", ((object)ApplicationFee ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TotalLoanAmount", ((object)TotalLoanAmount ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LoanWriter", ((object)LoanWriter ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InterviewDate", ((object)InterviewDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FinanceRequiredByDate", ((object)FinanceRequiredByDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ConversionBy", ((object)ConversionBy ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SigningCostAgreementDate", ((object)SigningCostAgreementDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SigningLetterOfOfferDate", ((object)SigningLetterOfOfferDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CreditFeeType", ((object)CreditFeeType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CreditFeeOverwrite", ((object)CreditFeeOverwrite ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CreditFeeOverwriteNote", ((object)CreditFeeOverwriteNote ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ChargePercentageProposal", ((object)ChargePercentageProposal ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("UpfrontRate", ((object)UpfrontRate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TrailRate", ((object)TrailRate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OutstandingFeeSettled", ((object)OutstandingFeeSettled ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SettlementManager", ((object)SettlementManager ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ActualFeesToCharge", ((object)ActualFeesToCharge ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AssessingComments", ((object)AssessingComments ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Web_InvitationEmailSent", ((object)Web_InvitationEmailSent ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Web_ApplicationFormStarted", ((object)Web_ApplicationFormStarted ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Web_ApplicationFormFinished", ((object)Web_ApplicationFormFinished ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ReceiveFurtherInformationTicked", ((object)ReceiveFurtherInformationTicked ?? 0)));
				cmd.Parameters.Add(new SqlParameter("AgreedToTermsAndConditions", ((object)AgreedToTermsAndConditions ?? 0)));
				cmd.Parameters.Add(new SqlParameter("AgreedToTermsAndConditionsDate", ((object)AgreedToTermsAndConditionsDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("IPAddress", ((object)IPAddress ?? DBNull.Value)));

				conn.Open();
				cmd.ExecuteNonQuery();

			}
		}


	}
}

