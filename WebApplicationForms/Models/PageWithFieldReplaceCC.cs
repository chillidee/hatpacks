﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;

namespace WebApplicationForms.Models
{
	public class PageWithFieldReplaceCC:Page
	{
		protected override void Render(HtmlTextWriter writer)
		{
      if (Session["CCApplicant"] != null)
      {
        CCApplicant cc = (CCApplicant)Session["CCApplicant"];

        StringWriter output = new StringWriter();
        base.Render(new HtmlTextWriter(output));
        //This is the rendered HTML of your page. Feel free to manipulate it.

        string outputAsString = output.ToString().Replace("[CompanyName]", cc.Company.Company);
        outputAsString = outputAsString.Replace("[ApplicationFee]", String.Format("{0:C}",cc.ApplicationFee));
        outputAsString = outputAsString.Replace("[ApplicationFeeWeekly]", String.Format("{0:C}", (((Decimal)cc.ApplicationFee/6) * 1.1m)));
        outputAsString = outputAsString.Replace("[ApplicationFeeWeeklyTotal]", String.Format("{0:C}", (((Decimal)cc.ApplicationFee) * 1.1m)));
        outputAsString = outputAsString.Replace("[SurchargeValue1]", String.Format("{0:C}", cc.ApplicationFee * 0.022m));
        outputAsString = outputAsString.Replace("[SurchargeValue2]", String.Format("{0:C}", cc.ApplicationFee * 0.033m));
        writer.Write(outputAsString);
      }
			else
			{ Response.Write("Session expired."); }
		}
	}
}