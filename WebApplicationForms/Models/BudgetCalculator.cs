﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace WebApplicationForms.Models
{
    public class BudgetCalculator
    {
        public int BudgetCalculatorID;
        public int CallID;
        public int ApplicantID;
        public List<BudgetCalculatorItem> Items;
        public Decimal TotalIncome;
        public Decimal TotalExpenses;
        public Decimal TotalSurplusDeficit;
        public DateTime DateCreated;
        public DateTime DateModified;

        public void Create()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO BudgetCalculator (");
            sql.Append("CallID, ");
            sql.Append("ApplicantID, ");
            sql.Append("TotalIncome, ");
            sql.Append("TotalExpenses, ");
            sql.Append("TotalSurplusDeficit, ");
            sql.Append("DateCreated ");

            sql.Append(") VALUES (");

            sql.Append("@CallID, ");
            sql.Append("@ApplicantID, ");
            sql.Append("@TotalIncome, ");
            sql.Append("@TotalExpenses, ");
            sql.Append("@TotalSurplusDeficit, ");
            sql.Append("GETDATE()");

            sql.Append("); SELECT @@IDENTITY");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

                cmd.Parameters.Add(new SqlParameter("CallID", ((object)CallID ?? DBNull.Value)));
                cmd.Parameters.Add(new SqlParameter("ApplicantID", ((object)ApplicantID ?? DBNull.Value)));
                cmd.Parameters.Add(new SqlParameter("TotalIncome", ((object)TotalIncome ?? DBNull.Value)));
                cmd.Parameters.Add(new SqlParameter("TotalExpenses", ((object)TotalExpenses ?? DBNull.Value)));
                cmd.Parameters.Add(new SqlParameter("TotalSurplusDeficit", ((object)TotalSurplusDeficit ?? DBNull.Value)));

                conn.Open();
                try
                {
                    BudgetCalculatorID = Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch { throw new Exception(); }

                if (Items != null)
                {
                    foreach (BudgetCalculatorItem item in Items)
                    {
                        item.BudgetCalculatorID = BudgetCalculatorID;
                        item.Create();
                    }
                }
            }
        }

        public void Save()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("UPDATE BudgetCalculator SET ");
            sql.Append("TotalIncome = @TotalIncome, ");
            sql.Append("TotalExpenses = @TotalExpenses, ");
            sql.Append("TotalSurplusDeficit = @TotalSurplusDeficit, ");
            sql.Append("DateModified = GETDATE() ");

            sql.Append("WHERE BudgetCalculatorID = @BudgetCalculatorID");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

                cmd.Parameters.Add(new SqlParameter("TotalIncome", ((object)TotalIncome ?? DBNull.Value)));
                cmd.Parameters.Add(new SqlParameter("TotalExpenses", ((object)TotalExpenses ?? DBNull.Value)));
                cmd.Parameters.Add(new SqlParameter("TotalSurplusDeficit", ((object)TotalSurplusDeficit ?? DBNull.Value)));
                cmd.Parameters.Add(new SqlParameter("BudgetCalculatorID", ((object)BudgetCalculatorID ?? DBNull.Value)));

                conn.Open();
                cmd.ExecuteNonQuery();

                BudgetCalculatorItem.Delete(BudgetCalculatorID);

                if (Items != null)
                {
                    foreach (BudgetCalculatorItem item in Items)
                    {
                        item.BudgetCalculatorID = BudgetCalculatorID;
                        item.Create();
                    }
                }
            }
        }

        public void Get(string guid, bool app)       
        {
            SyncApp();
            SyncLead();

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT * FROM BudgetCalculator WHERE ");

            if (app)
            {
                GetApplicantID(guid);
                sql.AppendFormat("ApplicantID = {0}", ApplicantID);
            }
            else
            {
                GetCallID(guid);
                sql.AppendFormat("CallID = {0}", CallID);
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    BudgetCalculatorID = Convert.ToInt32(dr["BudgetCalculatorID"]);
                    CallID = (dr["CallID"] as Int32?) ?? 0;
                    ApplicantID = (dr["ApplicantID"] as Int32?) ?? 0;
                    TotalIncome = (dr["TotalIncome"] as Decimal?) ?? 0;
                    TotalExpenses = (dr["TotalExpenses"] as Decimal?) ?? 0;
                    TotalSurplusDeficit = (dr["TotalSurplusDeficit"] as Decimal?) ?? 0;
                    DateCreated = (dr["DateCreated"] as DateTime?) ?? DateTime.MinValue;
                    DateModified = (dr["DateModified"] as DateTime?) ?? DateTime.MinValue;
                }
            }

            if (BudgetCalculatorID != 0)
                Items = BudgetCalculatorItem.Get(BudgetCalculatorID);
        }

        private void SyncApp()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("UPDATE BudgetCalculator SET ");
            sql.Append("ApplicantID = (SELECT TOP 1 ApplicantID FROM BMApplicant WHERE BMApplicant.CallID = BudgetCalculator.CallID) ");
            sql.Append("WHERE (ApplicantID IS NULL OR ApplicantID = 0) AND (CallID IS NOT NULL AND CallID > 0) ");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        private void SyncLead()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("UPDATE BudgetCalculator SET ");
            sql.Append("CallID = (SELECT TOP 1 CallID FROM BMApplicant WHERE BMApplicant.ApplicantID = BudgetCalculator.ApplicantID) ");
            sql.Append("WHERE (CallID IS NULL OR CallID = 0) AND (ApplicantID IS NOT NULL AND ApplicantID > 0) ");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        private void GetCallID(string guid)
        {
            string sql = string.Format("SELECT TOP 1 CallID FROM [Call] WHERE GUID = '{0}'", guid);

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    CallID = Convert.ToInt32(dr["CallID"]);
                }
            }
        }

        private void GetApplicantID(string guid)
        {
            string sql = string.Format("SELECT TOP 1 ApplicantID FROM BMApplicant WHERE GUID = '{0}'", guid);

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ApplicantID = Convert.ToInt32(dr["ApplicantID"]);
                }
            }
        }
    }
}


