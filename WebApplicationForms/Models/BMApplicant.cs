﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WebApplicationForms.Models;

namespace WebApplicationForms.Models
{
    public class BMApplicant
    {
        public int ApplicantID { get; set; }
        public string SequenceID { get; set; }
        public Nullable<int> CallID { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public int BMStatusID { get; set; }
        public Nullable<System.DateTime> StatusStartDate { get; set; }
        public Nullable<System.DateTime> StatusExpiryDate { get; set; }
        public string StatusEnteredBy { get; set; }
        public Nullable<System.DateTime> StatusDate { get; set; }
        public Nullable<int> CompanyID { get; set; }
        public string Referral { get; set; }
        public string Title { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Nickname { get; set; }
        public string TelephoneHome { get; set; }
        public string TelephoneWork { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string PersonalRefName { get; set; }
        public string PersonalRefAddress { get; set; }
        public string PersonalRefRelationship { get; set; }
        public string PersonalRefPhone { get; set; }
        public string Sex { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string HomeUnitNumber { get; set; }
        public string HomeStreetNumber { get; set; }
        public string HomeStreetName { get; set; }
        public string HomeStreetType { get; set; }
        public Nullable<int> HomeSuburb { get; set; }
        public string HomeState { get; set; }
        public string PostalUnitNumber { get; set; }
        public string PostalStreetNumber { get; set; }
        public string PostalStreetName { get; set; }
        public string PostalStreetType { get; set; }
        public Nullable<int> PostalSuburb { get; set; }
        public string PostalState { get; set; }
        public string PrevUnitNumber { get; set; }
        public string PrevStreetNumber { get; set; }
        public string PrevStreetName { get; set; }
        public string PrevStreetType { get; set; }
        public Nullable<int> PrevSuburb { get; set; }
        public string PrevState { get; set; }
        public Nullable<bool> HavingDriverLicense { get; set; }
        public string DriversLicenceNo { get; set; }
        public string LicenseStateOfIssue { get; set; }
        public Nullable<bool> HavingSpouse { get; set; }
        public Nullable<int> NoOfDependents { get; set; }
        public string ApplicationManager { get; set; }
        public string Notes { get; set; }
        public string AccountName { get; set; }
        public string AccountBSB { get; set; }
        public string AccountNo { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string CCNumber { get; set; }
        public string CCProvider { get; set; }
        public string CCExpiryMonth { get; set; }
        public string CCExpiryYear { get; set; }
        public string CCNameOnCard { get; set; }
        public string CCBank { get; set; }
        public string CCV { get; set; }
        public string GUID { get; set; }

        public MMCompany Company { get; set; }

        public BMApplicant() { }

        public BMApplicant(string ID)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM BMApplicant WHERE GUID = '" + ID.ToString() + "'", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    GUID = ID;

                    ApplicantID = Convert.ToInt32(dr["ApplicantID"]);
                    SequenceID = Convert.ToString(dr["Title"]);
                    CallID = (dr["CallID"] as Int32?) ?? null;
                    CreateDate = (dr["CreateDate"] as DateTime?) ?? null;
                    LastModifiedDate = (dr["LastModifiedDate"] as DateTime?) ?? null;
                    BMStatusID = Convert.ToInt32(dr["BMStatusID"]);
                    StatusStartDate = (dr["StatusStartDate"] as DateTime?) ?? null;
                    StatusExpiryDate = (dr["StatusExpiryDate"] as DateTime?) ?? null;
                    StatusEnteredBy = Convert.ToString(dr["StatusEnteredBy"]);
                    StatusDate = (dr["StatusDate"] as DateTime?) ?? null;
                    CompanyID = (dr["CompanyID"] as Int32?) ?? null;
                    Referral = Convert.ToString(dr["Referral"]);
                    Title = Convert.ToString(dr["Title"]);
                    Surname = Convert.ToString(dr["Surname"]);
                    FirstName = Convert.ToString(dr["FirstName"]);
                    MiddleName = Convert.ToString(dr["MiddleName"]);
                    TelephoneHome = Convert.ToString(dr["TelephoneHome"]);
                    TelephoneWork = Convert.ToString(dr["TelephoneWork"]);
                    Mobile = Convert.ToString(dr["Mobile"]);
                    Fax = Convert.ToString(dr["Fax"]);
                    Email = Convert.ToString(dr["Email"]);
                    PersonalRefName = Convert.ToString(dr["PersonalRefName"]);
                    PersonalRefAddress = Convert.ToString(dr["PersonalRefAddress"]);
                    PersonalRefRelationship = Convert.ToString(dr["PersonalRefRelationship"]);
                    PersonalRefPhone = Convert.ToString(dr["PersonalRefPhone"]);
                    Sex = Convert.ToString(dr["Sex"]);
                    DateOfBirth = (dr["DateOfBirth"] as DateTime?) ?? null; 
                    HomeUnitNumber = Convert.ToString(dr["HomeUnitNumber"]);
                    HomeStreetNumber = Convert.ToString(dr["HomeStreetNumber"]);
                    HomeStreetName = Convert.ToString(dr["HomeStreetName"]);
                    HomeStreetType = Convert.ToString(dr["HomeStreetType"]);
                    HomeSuburb = (dr["HomeSuburb"] as Int32?) ?? null;
                    HomeState = Convert.ToString(dr["HomeState"]);
                    PostalUnitNumber = Convert.ToString(dr["PostalUnitNumber"]);
                    PostalStreetNumber = Convert.ToString(dr["PostalStreetNumber"]);
                    PostalStreetName = Convert.ToString(dr["PostalStreetName"]);
                    PostalStreetType = Convert.ToString(dr["PostalStreetType"]);
                    PostalSuburb = (dr["PostalSuburb"] as Int32?) ?? null;
                    PostalState = Convert.ToString(dr["PostalState"]);
                    PrevUnitNumber = Convert.ToString(dr["PrevUnitNumber"]);
                    PrevStreetNumber = Convert.ToString(dr["PrevStreetNumber"]);
                    PrevStreetName = Convert.ToString(dr["PrevStreetName"]);
                    PrevStreetType = Convert.ToString(dr["PrevStreetType"]);
                    PrevSuburb = (dr["PrevSuburb"] as Int32?) ?? null;
                    PrevState = Convert.ToString(dr["PrevState"]);
                    HavingDriverLicense = (dr["HavingDriverLicense"] as Boolean?) ?? null;
                    DriversLicenceNo = Convert.ToString(dr["DriversLicenceNo"]);
                    LicenseStateOfIssue = Convert.ToString(dr["LicenseStateOfIssue"]);
                    HavingSpouse = (dr["HavingSpouse"] as Boolean?) ?? null;
                    NoOfDependents = (dr["NoOfDependents"] as Int32?) ?? null;                  
                    ApplicationManager = Convert.ToString(dr["ApplicationManager"]);
                    Notes = Convert.ToString(dr["Notes"]);
                    AccountName = Convert.ToString(dr["AccountName"]);
                    AccountBSB = Convert.ToString(dr["AccountBSB"]);
                    AccountNo = Convert.ToString(dr["AccountNo"]);
                    Bank = Convert.ToString(dr["Bank"]);
                    BankBranch = Convert.ToString(dr["BankBranch"]);
                    CCNumber = Convert.ToString(dr["CCNumber"]);
                    CCProvider = Convert.ToString(dr["CCProvider"]);
                    CCExpiryMonth = Convert.ToString(dr["CCExpiryMonth"]);
                    CCExpiryYear = Convert.ToString(dr["CCExpiryYear"]);
                    CCNameOnCard = Convert.ToString(dr["CCNameOnCard"]);
                    CCBank = Convert.ToString(dr["CCBank"]);
                    CCV = Convert.ToString(dr["CCV"]);
                }
            }
        }
        public void LoadCompany()
        {
            Company = new MMCompany(Convert.ToInt32(CompanyID));
        }

        private string GetString(object data, string defaultValue = "")
        {
            if (data == DBNull.Value)
                return defaultValue;
            else
                return data.ToString();
        }
        private decimal GetDecimal(object data, decimal defaultValue = 0)
        {
            if (data == DBNull.Value)
                return defaultValue;
            else
            {
                decimal result = 0;
                decimal.TryParse(data.ToString(), out result);
                return result;
            }
        }
    }
}