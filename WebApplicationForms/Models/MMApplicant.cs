﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplicationForms.Models
{
	public class MMApplicant
	{
		public int ApplicantID { get; set; }
		public string Title { get; set; }
		public string Surname { get; set; }
		public string FirstName { get; set; }
		public string Nickname { get; set; }
		public string OtherNames { get; set; }
		public Nullable<System.DateTime> DateOfBirth { get; set; }
		public string Sex { get; set; }
		public string MaritalStatus { get; set; }
		public string ResidentialStatus { get; set; }
		public string HomeAddressLine1 { get; set; }
		public string HomeAddressLine2 { get; set; }
		
		//public Nullable<int> HomeSuburb { get; set; }
		public Nullable<int> HomeSuburb { get { return _HomeSuburb; } set { _HomeSuburb = value; HomeSuburbText = new MMPCode(Convert.ToInt32(value)).SuburbStatePostCode; } }

		public Nullable<int> DurationYears { get; set; }
		public Nullable<int> DurationMonths { get; set; }
		public string PrevAddressLine1 { get; set; }
		public string PrevAddressLine2 { get; set; }
		//public Nullable<int> PrevSuburb { get; set; }
		public Nullable<int> PrevSuburb { get { return _PrevSuburb; } set { _PrevSuburb = value; PrevSuburbText = new MMPCode(Convert.ToInt32(value)).SuburbStatePostCode; } }
		public string PrevCountry { get; set; }
		public Nullable<int> PrevDurationYears { get; set; }
		public Nullable<int> PrevDurationMonths { get; set; }
		public string TelephoneHome { get; set; }
		public string TelephoneWork { get; set; }
		public string Mobile { get; set; }
		public string DriversLicenceNo { get; set; }
		public string SpousesName { get; set; }
		public Nullable<int> NoOfDependents { get; set; }
		public string AgesOfDependents { get; set; }
		public string MothersMaidenName { get; set; }
		public string CurrentEmployer { get; set; }
		public string Occupation { get; set; }
		public string EmpABN { get; set; }
		public string EmplAddressLine1 { get; set; }
		public string EmplAddressLine2 { get; set; }
		public Nullable<int> EmplSuburb { get { return _EmplSuburb; } set { _EmplSuburb = value; EmplSuburbText = new MMPCode(Convert.ToInt32(value)).SuburbStatePostCode; } }
		public Nullable<int> EmplDurationYears { get; set; }
		public Nullable<int> EmplDurationMonths { get; set; }
		public string PreviousEmployer { get; set; }
		public string PreviousOccupation { get; set; }
		public string PrevEmplAddLine1 { get; set; }
		public string PrevEmplAddLine2 { get; set; }
		public Nullable<int> PrevEmplSuburb { get { return _PrevEmplSuburb; } set { _PrevEmplSuburb = value; PrevEmplSuburbText = new MMPCode(Convert.ToInt32(value)).SuburbStatePostCode; } }
		public string PrevEmplCountry { get; set; }
		public Nullable<int> PrevEmplDurationYears { get; set; }
		public Nullable<int> PrevEmplDurationMnths { get; set; }
		public string OtherEmployer { get; set; }
		public string OtherOccupation { get; set; }
		public string OtherEmplAddLine1 { get; set; }
		public string OtherEmplAddLine2 { get; set; }
		public Nullable<int> OtherEmplSuburb { get { return _OtherEmplSuburb; } set { _OtherEmplSuburb = value; OtherEmplSuburbText = new MMPCode(Convert.ToInt32(value)).SuburbStatePostCode; } }
		public Nullable<int> OtherEmplDurationYears { get; set; }
		public Nullable<int> OtherEmplDurationMnths { get; set; }
		public Nullable<decimal> GrossIncome { get; set; }
		public Nullable<decimal> NettIncome { get; set; }
		public string IncomePeriod { get; set; }
		public string Publication { get; set; }
		public string Bank { get; set; }
		public string BankBranch { get; set; }
		public Nullable<int> BankDurationYears { get; set; }
		public Nullable<int> BankDurationMonths { get; set; }
		public string PersonalRefTitle { get; set; }
		public string PersonalRefName { get; set; }
		public string PersonalRefSurname { get; set; }
		public string PersonalRefRelationship { get; set; }
		public string PersonalRefHomePhone { get; set; }
		public string PersonalRefAddLine1 { get; set; }
		public string PersonalRefAddLine2 { get; set; }
		public Nullable<int> PersonalRefSuburb { get { return _PersonalRefSuburb; } set { _PersonalRefSuburb = value; PersonalRefSuburbText = new MMPCode(Convert.ToInt32(value)).SuburbStatePostCode; } }
		public string Details { get; set; }
		public string Fax { get; set; }
		public string Email { get; set; }
		public string EmailSecond { get; set; }
		public Nullable<int> PreviousApplicantID { get; set; }
		public string PostalAddressLine1 { get; set; }
		public string PostalAddressLine2 { get; set; }
		public Nullable<int> PostalHomeSuburb { get { return _PostalHomeSuburb; } set { _PostalHomeSuburb = value; PostalHomeSuburbText = new MMPCode(Convert.ToInt32(value)).SuburbStatePostCode; } }
		public string PersonalRefWorkPhone { get; set; }
		public Nullable<System.DateTime> DriversLicenceExpiry { get; set; }
		public string CurrentEmploymentBasis { get; set; }
		public string PreviousEmploymentBasis { get; set; }
		public string OtherEmploymentBasis { get; set; }
		public string PreviousEmployerPhone { get; set; }
		public string OtherEmployerPhone { get; set; }
		public string DriversLicenceState { get; set; }
		public string ResidencyStatus { get; set; }
		public Nullable<System.DateTime> HereSince { get; set; }
		public Nullable<System.DateTime> PrevSince { get; set; }
		public Nullable<System.DateTime> EmplSince { get; set; }
		public Nullable<System.DateTime> PrevEmplSince { get; set; }
		public Nullable<System.DateTime> OtherEmplSince { get; set; }
		public string CurrentEmployerPhone { get; set; }
		public string HomeUnitNumber { get; set; }
		public string HomeStreetNumber { get; set; }
		public string HomeStreetName { get; set; }
		public string HomeStreetType { get; set; }
		public string EmplUnitNumber { get; set; }
		public string EmplStreetNumber { get; set; }
		public string EmplStreetName { get; set; }
		public string EmplStreetType { get; set; }
		public string PrevUnitNumber { get; set; }
		public string PrevStreetNumber { get; set; }
		public string PrevStreetName { get; set; }
		public string PrevStreetType { get; set; }
		public string PrevResidentialStatus { get; set; }
		public string NameOfFriend { get; set; }
		public string FriendPhone { get; set; }
		public Nullable<decimal> OtherAnnualGrossIncome { get; set; }
		public Nullable<decimal> TotalMonthlyNetIncome { get; set; }
		public Nullable<decimal> TotalMonthlyMorgageRentExpense { get; set; }
		public Nullable<decimal> OtherlMonthlyPersonalExpense { get; set; }
		public string OccupationCategory { get; set; }
		public string OccupationIndustry { get; set; }
		public string ResidentialStatusPL { get; set; }
		public string LivedInCurrentAddress { get; set; }
		public string OtherName { get; set; }
		public Nullable<decimal> IncomeOfPartner { get; set; }
		public string NameOfMortgagor { get; set; }
		public string NumberOfMortgagor { get; set; }
		public string SelfBusinessName { get; set; }
		public string SelfContactPerson { get; set; }
		public string SelfPhone { get; set; }
		public string Job2BusinessName { get; set; }
		public string Job2Phone { get; set; }
		public string AccountantCompanyName { get; set; }
		public string AccountantContactPerson { get; set; }
		public string AccountantContactNumber { get; set; }
		public string LivedInPreviousAddress { get; set; }
		public string MiddleName { get; set; }
		public string GivenNameOfMortgagor { get; set; }
		public string SurNameOfMortgagor { get; set; }
		public Nullable<decimal> TotalMonthlyMortgageExpense { get; set; }
		public Nullable<decimal> TotalMonthlyRentExpense { get; set; }
		public string BorrowerCompanyName { get; set; }
		public string BorrowerCompanyABN { get; set; }
		public string BorrowerCompanyPosition { get; set; }
		public string AccountName { get; set; }
		public string AcountNameLong { get; set; }
		public string AccountBSB { get; set; }
		public string AccountNo { get; set; }
		public Nullable<bool> TelephoneHomePrefer { get; set; }
		public Nullable<bool> TelephoneWorkPrefer { get; set; }
		public Nullable<bool> MobilePrefer { get; set; }
		public Nullable<bool> SMSPrefer { get; set; }
		public Nullable<bool> FaxPrefer { get; set; }
		public Nullable<bool> EmailPrefer { get; set; }
		public Nullable<bool> EmailSecondPrefer { get; set; }
		public Nullable<System.DateTime> BDCardDate { get; set; }
		public string TrustName { get; set; }
		public string TrustABN { get; set; }
		public string TrustNatureOfBus { get; set; }
		public string TrustAddressLine1 { get; set; }
		public string TrustAddressLine2 { get; set; }
		public Nullable<int> TrustSuburb { get { return _TrustSuburb; } set { _TrustSuburb = value; TrustSuburbText = new MMPCode(Convert.ToInt32(value)).SuburbStatePostCode; } }
		public string TrustBusAddressLine1 { get; set; }
		public string TrustBusAddressLine2 { get; set; }
		public Nullable<int> TrustBusSuburb { get { return _TrustBusSuburb; } set { _TrustBusSuburb = value; TrustBusSuburbText = new MMPCode(Convert.ToInt32(value)).SuburbStatePostCode; } }
		public string Trustee1 { get; set; }
		public string Trustee2 { get; set; }
		public string Trustee3 { get; set; }
		public string Trustee4 { get; set; }
		public string Beneficiary1 { get; set; }
		public string Beneficiary2 { get; set; }
		public string Beneficiary3 { get; set; }
		public string Beneficiary4 { get; set; }
		public string TypeOfTrust { get; set; }
		public string GSTRegistered { get; set; }
		public string PropertyUnderTrustName { get; set; }
		public string TrustOrCompany { get; set; }
		public string CCNumber { get; set; }
		public string CCProvider { get; set; }
		public string CCExpiryMonth { get; set; }
		public string CCExpiryYear { get; set; }
		public string CCNameOnCard { get; set; }
		public Nullable<bool> AMLCTFIDCompleted { get; set; }
		public Nullable<System.DateTime> AMLCTFIDDate { get; set; }
		public string AMLCTFIDNote { get; set; }
		public string AMLCTFIDCompletedBy { get; set; }
		public string Initials { get; set; }

		//Addition Derived Properties...read only
		public string HomeSuburbText { get; set; }
		public string PostalHomeSuburbText { get; set; }
		public string PrevSuburbText { get; set; }
		public string EmplSuburbText { get; set; }
		public string PrevEmplSuburbText { get; set; }
		public string OtherEmplSuburbText { get; set; }
		public string PersonalRefSuburbText { get; set; }
		public string TrustSuburbText { get; set; }
		public string TrustBusSuburbText { get; set; }

		private Nullable<int> _HomeSuburb;
		private Nullable<int> _PostalHomeSuburb;
		private Nullable<int> _PrevSuburb;
		private Nullable<int> _EmplSuburb;
		private Nullable<int> _PrevEmplSuburb;
		private Nullable<int> _OtherEmplSuburb;
		private Nullable<int> _PersonalRefSuburb;
		private Nullable<int> _TrustSuburb;
		private Nullable<int> _TrustBusSuburb;

		public int ApplicationNo { get; set; } //related Application (despited it being a many-to-many relationship!)

		public MMApplicant() { }

		public MMApplicant(int ID)
		{
			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT * FROM Applicant WHERE ApplicantID = " + ID.ToString(), conn);
				conn.Open();
				SqlDataReader dr = cmd.ExecuteReader();
				while (dr.Read())
				{
					ApplicantID = ID;

					Title = Convert.ToString(dr["Title"]);
					Surname = Convert.ToString(dr["Surname"]);
					FirstName = Convert.ToString(dr["FirstName"]);
					Nickname = Convert.ToString(dr["Nickname"]);
					OtherNames = Convert.ToString(dr["OtherNames"]);
					DateOfBirth = (dr["DateOfBirth"] as DateTime?) ?? null;
					Sex = Convert.ToString(dr["Sex"]);
					MaritalStatus = Convert.ToString(dr["MaritalStatus"]);
					ResidentialStatus = Convert.ToString(dr["ResidentialStatus"]);
					HomeAddressLine1 = Convert.ToString(dr["HomeAddressLine1"]);
					HomeAddressLine2 = Convert.ToString(dr["HomeAddressLine2"]);
					HomeSuburb = (dr["HomeSuburb"] as Int32?) ?? null;
					DurationYears = (dr["DurationYears"] as Int32?) ?? null;
					DurationMonths = (dr["DurationMonths"] as Int32?) ?? null;
					PrevAddressLine1 = Convert.ToString(dr["PrevAddressLine1"]);
					PrevAddressLine2 = Convert.ToString(dr["PrevAddressLine2"]);
					PrevSuburb = (dr["PrevSuburb"] as Int32?) ?? null;
					PrevCountry = Convert.ToString(dr["PrevCountry"]);
					PrevDurationYears = (dr["PrevDurationYears"] as Int32?) ?? null;
					PrevDurationMonths = (dr["PrevDurationMonths"] as Int32?) ?? null;
					TelephoneHome = Convert.ToString(dr["TelephoneHome"]);
					TelephoneWork = Convert.ToString(dr["TelephoneWork"]);
					Mobile = Convert.ToString(dr["Mobile"]);
					DriversLicenceNo = Convert.ToString(dr["DriversLicenceNo"]);
					SpousesName = Convert.ToString(dr["SpousesName"]);
					NoOfDependents = (dr["NoOfDependents"] as Int32?) ?? null;
					AgesOfDependents = Convert.ToString(dr["AgesOfDependents"]);
					MothersMaidenName = Convert.ToString(dr["MothersMaidenName"]);
					CurrentEmployer = Convert.ToString(dr["CurrentEmployer"]);
					Occupation = Convert.ToString(dr["Occupation"]);
					EmpABN = Convert.ToString(dr["EmpABN"]);
					EmplAddressLine1 = Convert.ToString(dr["EmplAddressLine1"]);
					EmplAddressLine2 = Convert.ToString(dr["EmplAddressLine2"]);
					EmplSuburb = (dr["EmplSuburb"] as Int32?) ?? null;
					EmplDurationYears = (dr["EmplDurationYears"] as Int32?) ?? null;
					EmplDurationMonths = (dr["EmplDurationMonths"] as Int32?) ?? null;
					PreviousEmployer = Convert.ToString(dr["PreviousEmployer"]);
					PreviousOccupation = Convert.ToString(dr["PreviousOccupation"]);
					PrevEmplAddLine1 = Convert.ToString(dr["PrevEmplAddLine1"]);
					PrevEmplAddLine2 = Convert.ToString(dr["PrevEmplAddLine2"]);
					PrevEmplSuburb = (dr["PrevEmplSuburb"] as Int32?) ?? null;
					PrevEmplCountry = Convert.ToString(dr["PrevEmplCountry"]);
					PrevEmplDurationYears = (dr["PrevEmplDurationYears"] as Int32?) ?? null;
					PrevEmplDurationMnths = (dr["PrevEmplDurationMnths"] as Int32?) ?? null;
					OtherEmployer = Convert.ToString(dr["OtherEmployer"]);
					OtherOccupation = Convert.ToString(dr["OtherOccupation"]);
					OtherEmplAddLine1 = Convert.ToString(dr["OtherEmplAddLine1"]);
					OtherEmplAddLine2 = Convert.ToString(dr["OtherEmplAddLine2"]);
					OtherEmplSuburb = (dr["OtherEmplSuburb"] as Int32?) ?? null;
					OtherEmplDurationYears = (dr["OtherEmplDurationYears"] as Int32?) ?? null;
					OtherEmplDurationMnths = (dr["OtherEmplDurationMnths"] as Int32?) ?? null;
					GrossIncome = (dr["GrossIncome"] as Decimal?) ?? null;
					NettIncome = (dr["NettIncome"] as Decimal?) ?? null;
					IncomePeriod = Convert.ToString(dr["IncomePeriod"]);
					Publication = Convert.ToString(dr["Publication"]);
					Bank = Convert.ToString(dr["Bank"]);
					BankBranch = Convert.ToString(dr["BankBranch"]);
					BankDurationYears = (dr["BankDurationYears"] as Int32?) ?? null;
					BankDurationMonths = (dr["BankDurationMonths"] as Int32?) ?? null;
					PersonalRefTitle = Convert.ToString(dr["PersonalRefTitle"]);
					PersonalRefName = Convert.ToString(dr["PersonalRefName"]);
					PersonalRefSurname = Convert.ToString(dr["PersonalRefSurname"]);
					PersonalRefRelationship = Convert.ToString(dr["PersonalRefRelationship"]);
					PersonalRefHomePhone = Convert.ToString(dr["PersonalRefHomePhone"]);
					PersonalRefAddLine1 = Convert.ToString(dr["PersonalRefAddLine1"]);
					PersonalRefAddLine2 = Convert.ToString(dr["PersonalRefAddLine2"]);
					PersonalRefSuburb = (dr["PersonalRefSuburb"] as Int32?) ?? null;
					Details = Convert.ToString(dr["Details"]);
					Fax = Convert.ToString(dr["Fax"]);
					Email = Convert.ToString(dr["Email"]);
					EmailSecond = Convert.ToString(dr["EmailSecond"]);
					PreviousApplicantID = (dr["PreviousApplicantID"] as Int32?) ?? null;
					PostalAddressLine1 = Convert.ToString(dr["PostalAddressLine1"]);
					PostalAddressLine2 = Convert.ToString(dr["PostalAddressLine2"]);
					PostalHomeSuburb = (dr["PostalHomeSuburb"] as Int32?) ?? null;
					PersonalRefWorkPhone = Convert.ToString(dr["PersonalRefWorkPhone"]);
					DriversLicenceExpiry = (dr["DriversLicenceExpiry"] as DateTime?) ?? null;
					CurrentEmploymentBasis = Convert.ToString(dr["CurrentEmploymentBasis"]);
					PreviousEmploymentBasis = Convert.ToString(dr["PreviousEmploymentBasis"]);
					OtherEmploymentBasis = Convert.ToString(dr["OtherEmploymentBasis"]);
					PreviousEmployerPhone = Convert.ToString(dr["PreviousEmployerPhone"]);
					OtherEmployerPhone = Convert.ToString(dr["OtherEmployerPhone"]);
					DriversLicenceState = Convert.ToString(dr["DriversLicenceState"]);
					ResidencyStatus = Convert.ToString(dr["ResidencyStatus"]);
					HereSince = (dr["HereSince"] as DateTime?) ?? null;
					PrevSince = (dr["PrevSince"] as DateTime?) ?? null;
					EmplSince = (dr["EmplSince"] as DateTime?) ?? null;
					PrevEmplSince = (dr["PrevEmplSince"] as DateTime?) ?? null;
					OtherEmplSince = (dr["OtherEmplSince"] as DateTime?) ?? null;
					CurrentEmployerPhone = Convert.ToString(dr["CurrentEmployerPhone"]);
					HomeUnitNumber = Convert.ToString(dr["HomeUnitNumber"]);
					HomeStreetNumber = Convert.ToString(dr["HomeStreetNumber"]);
					HomeStreetName = Convert.ToString(dr["HomeStreetName"]);
					HomeStreetType = Convert.ToString(dr["HomeStreetType"]);
					EmplUnitNumber = Convert.ToString(dr["EmplUnitNumber"]);
					EmplStreetNumber = Convert.ToString(dr["EmplStreetNumber"]);
					EmplStreetName = Convert.ToString(dr["EmplStreetName"]);
					EmplStreetType = Convert.ToString(dr["EmplStreetType"]);
					PrevUnitNumber = Convert.ToString(dr["PrevUnitNumber"]);
					PrevStreetNumber = Convert.ToString(dr["PrevStreetNumber"]);
					PrevStreetName = Convert.ToString(dr["PrevStreetName"]);
					PrevStreetType = Convert.ToString(dr["PrevStreetType"]);
					PrevResidentialStatus = Convert.ToString(dr["PrevResidentialStatus"]);
					NameOfFriend = Convert.ToString(dr["NameOfFriend"]);
					FriendPhone = Convert.ToString(dr["FriendPhone"]);
					OtherAnnualGrossIncome = (dr["OtherAnnualGrossIncome"] as Decimal?) ?? null;
					TotalMonthlyNetIncome = (dr["TotalMonthlyNetIncome"] as Decimal?) ?? null;
					TotalMonthlyMorgageRentExpense = (dr["TotalMonthlyMorgageRentExpense"] as Decimal?) ?? null;
					OtherlMonthlyPersonalExpense = (dr["OtherlMonthlyPersonalExpense"] as Decimal?) ?? null;
					OccupationCategory = Convert.ToString(dr["OccupationCategory"]);
					OccupationIndustry = Convert.ToString(dr["OccupationIndustry"]);
					ResidentialStatusPL = Convert.ToString(dr["ResidentialStatusPL"]);
					LivedInCurrentAddress = Convert.ToString(dr["LivedInCurrentAddress"]);
					OtherName = Convert.ToString(dr["OtherName"]);
					IncomeOfPartner = (dr["IncomeOfPartner"] as Decimal?) ?? null;
					NameOfMortgagor = Convert.ToString(dr["NameOfMortgagor"]);
					NumberOfMortgagor = Convert.ToString(dr["NumberOfMortgagor"]);
					SelfBusinessName = Convert.ToString(dr["SelfBusinessName"]);
					SelfContactPerson = Convert.ToString(dr["SelfContactPerson"]);
					SelfPhone = Convert.ToString(dr["SelfPhone"]);
					Job2BusinessName = Convert.ToString(dr["Job2BusinessName"]);
					Job2Phone = Convert.ToString(dr["Job2Phone"]);
					AccountantCompanyName = Convert.ToString(dr["AccountantCompanyName"]);
					AccountantContactPerson = Convert.ToString(dr["AccountantContactPerson"]);
					AccountantContactNumber = Convert.ToString(dr["AccountantContactNumber"]);
					LivedInPreviousAddress = Convert.ToString(dr["LivedInPreviousAddress"]);
					MiddleName = Convert.ToString(dr["MiddleName"]);
					GivenNameOfMortgagor = Convert.ToString(dr["GivenNameOfMortgagor"]);
					SurNameOfMortgagor = Convert.ToString(dr["SurNameOfMortgagor"]);
					TotalMonthlyMortgageExpense = (dr["TotalMonthlyMortgageExpense"] as Decimal?) ?? null;
					TotalMonthlyRentExpense = (dr["TotalMonthlyRentExpense"] as Decimal?) ?? null;
					BorrowerCompanyName = Convert.ToString(dr["BorrowerCompanyName"]);
					BorrowerCompanyABN = Convert.ToString(dr["BorrowerCompanyABN"]);
					BorrowerCompanyPosition = Convert.ToString(dr["BorrowerCompanyPosition"]);
					AccountName = Convert.ToString(dr["AccountName"]);
					AcountNameLong = Convert.ToString(dr["AcountNameLong"]);
					AccountBSB = Convert.ToString(dr["AccountBSB"]);
					AccountNo = Convert.ToString(dr["AccountNo"]);
					TelephoneHomePrefer = (dr["TelephoneHomePrefer"] as Boolean?) ?? null;
					TelephoneWorkPrefer = (dr["TelephoneWorkPrefer"] as Boolean?) ?? null;
					MobilePrefer = (dr["MobilePrefer"] as Boolean?) ?? null;
					SMSPrefer = (dr["SMSPrefer"] as Boolean?) ?? null;
					FaxPrefer = (dr["FaxPrefer"] as Boolean?) ?? null;
					EmailPrefer = (dr["EmailPrefer"] as Boolean?) ?? null;
					EmailSecondPrefer = (dr["EmailSecondPrefer"] as Boolean?) ?? null;
					BDCardDate = (dr["BDCardDate"] as DateTime?) ?? null;
					TrustName = Convert.ToString(dr["TrustName"]);
					TrustABN = Convert.ToString(dr["TrustABN"]);
					TrustNatureOfBus = Convert.ToString(dr["TrustNatureOfBus"]);
					TrustAddressLine1 = Convert.ToString(dr["TrustAddressLine1"]);
					TrustAddressLine2 = Convert.ToString(dr["TrustAddressLine2"]);
					TrustSuburb = (dr["TrustSuburb"] as Int32?) ?? null;
					TrustBusAddressLine1 = Convert.ToString(dr["TrustBusAddressLine1"]);
					TrustBusAddressLine2 = Convert.ToString(dr["TrustBusAddressLine2"]);
					TrustBusSuburb = (dr["TrustBusSuburb"] as Int32?) ?? null;
					Trustee1 = Convert.ToString(dr["Trustee1"]);
					Trustee2 = Convert.ToString(dr["Trustee2"]);
					Trustee3 = Convert.ToString(dr["Trustee3"]);
					Trustee4 = Convert.ToString(dr["Trustee4"]);
					Beneficiary1 = Convert.ToString(dr["Beneficiary1"]);
					Beneficiary2 = Convert.ToString(dr["Beneficiary2"]);
					Beneficiary3 = Convert.ToString(dr["Beneficiary3"]);
					Beneficiary4 = Convert.ToString(dr["Beneficiary4"]);
					TypeOfTrust = Convert.ToString(dr["TypeOfTrust"]);
					GSTRegistered = Convert.ToString(dr["GSTRegistered"]);
					PropertyUnderTrustName = Convert.ToString(dr["PropertyUnderTrustName"]);
					TrustOrCompany = Convert.ToString(dr["TrustOrCompany"]);
					CCNumber = Convert.ToString(dr["CCNumber"]);
					CCProvider = Convert.ToString(dr["CCProvider"]);
					CCExpiryMonth = Convert.ToString(dr["CCExpiryMonth"]);
					CCExpiryYear = Convert.ToString(dr["CCExpiryYear"]);
					CCNameOnCard = Convert.ToString(dr["CCNameOnCard"]);
					AMLCTFIDCompleted = (dr["AMLCTFIDCompleted"] as Boolean?) ?? null;
					AMLCTFIDDate = (dr["AMLCTFIDDate"] as DateTime?) ?? null;
					AMLCTFIDNote = Convert.ToString(dr["AMLCTFIDNote"]);
					AMLCTFIDCompletedBy = Convert.ToString(dr["AMLCTFIDCompletedBy"]);
					Initials = Convert.ToString(dr["Initials"]);

					//Loading derived Suburb Texts (including state and postcode)

					/*HomeSuburbText = new MMPCode(Convert.ToInt32(HomeSuburb)).SuburbStatePostCode;
					PostalHomeSuburbText = new MMPCode(Convert.ToInt32(HomeSuburb)).SuburbStatePostCode;
					PrevSuburbText = new MMPCode(Convert.ToInt32(PrevSuburb)).SuburbStatePostCode;
					EmplSuburbText = new MMPCode(Convert.ToInt32(EmplSuburb)).SuburbStatePostCode;
					PrevEmplSuburbText = new MMPCode(Convert.ToInt32(PrevEmplSuburb)).SuburbStatePostCode;
					OtherEmplSuburbText = new MMPCode(Convert.ToInt32(OtherEmplSuburb)).SuburbStatePostCode;
					PersonalRefSuburbText = new MMPCode(Convert.ToInt32(PersonalRefSuburb)).SuburbStatePostCode;
					PostalHomeSuburbText = new MMPCode(Convert.ToInt32(PostalHomeSuburb)).SuburbStatePostCode;
					TrustSuburbText = new MMPCode(Convert.ToInt32(TrustSuburb)).SuburbStatePostCode;
					TrustBusSuburbText = new MMPCode(Convert.ToInt32(TrustBusSuburb)).SuburbStatePostCode;*/

				}
			}
		}

		public void Save()
		{
			//building the UPDATE string...
			StringBuilder sql = new StringBuilder();
			sql.Append("UPDATE Applicant SET ");
			sql.Append("Title = @Title, ");
			sql.Append("Surname = @Surname, ");
			sql.Append("FirstName = @FirstName, ");
			sql.Append("Nickname = @Nickname, ");
			sql.Append("OtherNames = @OtherNames, ");
			sql.Append("DateOfBirth = @DateOfBirth, ");
			sql.Append("Sex = @Sex, ");
			sql.Append("MaritalStatus = @MaritalStatus, ");
			sql.Append("ResidentialStatus = @ResidentialStatus, ");
			sql.Append("HomeAddressLine1 = @HomeAddressLine1, ");
			sql.Append("HomeAddressLine2 = @HomeAddressLine2, ");
			sql.Append("HomeSuburb = @HomeSuburb, ");
			sql.Append("DurationYears = @DurationYears, ");
			sql.Append("DurationMonths = @DurationMonths, ");
			sql.Append("PrevAddressLine1 = @PrevAddressLine1, ");
			sql.Append("PrevAddressLine2 = @PrevAddressLine2, ");
			sql.Append("PrevSuburb = @PrevSuburb, ");
			sql.Append("PrevCountry = @PrevCountry, ");
			sql.Append("PrevDurationYears = @PrevDurationYears, ");
			sql.Append("PrevDurationMonths = @PrevDurationMonths, ");
			sql.Append("TelephoneHome = @TelephoneHome, ");
			sql.Append("TelephoneWork = @TelephoneWork, ");
			sql.Append("Mobile = @Mobile, ");
			sql.Append("DriversLicenceNo = @DriversLicenceNo, ");
			sql.Append("SpousesName = @SpousesName, ");
			sql.Append("NoOfDependents = @NoOfDependents, ");
			sql.Append("AgesOfDependents = @AgesOfDependents, ");
			sql.Append("MothersMaidenName = @MothersMaidenName, ");
			sql.Append("CurrentEmployer = @CurrentEmployer, ");
			sql.Append("Occupation = @Occupation, ");
			sql.Append("EmpABN = @EmpABN, ");
			sql.Append("EmplAddressLine1 = @EmplAddressLine1, ");
			sql.Append("EmplAddressLine2 = @EmplAddressLine2, ");
			sql.Append("EmplSuburb = @EmplSuburb, ");
			sql.Append("EmplDurationYears = @EmplDurationYears, ");
			sql.Append("EmplDurationMonths = @EmplDurationMonths, ");
			sql.Append("PreviousEmployer = @PreviousEmployer, ");
			sql.Append("PreviousOccupation = @PreviousOccupation, ");
			sql.Append("PrevEmplAddLine1 = @PrevEmplAddLine1, ");
			sql.Append("PrevEmplAddLine2 = @PrevEmplAddLine2, ");
			sql.Append("PrevEmplSuburb = @PrevEmplSuburb, ");
			sql.Append("PrevEmplCountry = @PrevEmplCountry, ");
			sql.Append("PrevEmplDurationYears = @PrevEmplDurationYears, ");
			sql.Append("PrevEmplDurationMnths = @PrevEmplDurationMnths, ");
			sql.Append("OtherEmployer = @OtherEmployer, ");
			sql.Append("OtherOccupation = @OtherOccupation, ");
			sql.Append("OtherEmplAddLine1 = @OtherEmplAddLine1, ");
			sql.Append("OtherEmplAddLine2 = @OtherEmplAddLine2, ");
			sql.Append("OtherEmplSuburb = @OtherEmplSuburb, ");
			sql.Append("OtherEmplDurationYears = @OtherEmplDurationYears, ");
			sql.Append("OtherEmplDurationMnths = @OtherEmplDurationMnths, ");
			sql.Append("GrossIncome = @GrossIncome, ");
			sql.Append("NettIncome = @NettIncome, ");
			sql.Append("IncomePeriod = @IncomePeriod, ");
			sql.Append("Publication = @Publication, ");
			sql.Append("Bank = @Bank, ");
			sql.Append("BankBranch = @BankBranch, ");
			sql.Append("BankDurationYears = @BankDurationYears, ");
			sql.Append("BankDurationMonths = @BankDurationMonths, ");
			sql.Append("PersonalRefTitle = @PersonalRefTitle, ");
			sql.Append("PersonalRefName = @PersonalRefName, ");
			sql.Append("PersonalRefSurname = @PersonalRefSurname, ");
			sql.Append("PersonalRefRelationship = @PersonalRefRelationship, ");
			sql.Append("PersonalRefHomePhone = @PersonalRefHomePhone, ");
			sql.Append("PersonalRefAddLine1 = @PersonalRefAddLine1, ");
			sql.Append("PersonalRefAddLine2 = @PersonalRefAddLine2, ");
			sql.Append("PersonalRefSuburb = @PersonalRefSuburb, ");
			sql.Append("Details = @Details, ");
			sql.Append("Fax = @Fax, ");
			sql.Append("Email = @Email, ");
			sql.Append("EmailSecond = @EmailSecond, ");
			sql.Append("PreviousApplicantID = @PreviousApplicantID, ");
			sql.Append("PostalAddressLine1 = @PostalAddressLine1, ");
			sql.Append("PostalAddressLine2 = @PostalAddressLine2, ");
			sql.Append("PostalHomeSuburb = @PostalHomeSuburb, ");
			sql.Append("PersonalRefWorkPhone = @PersonalRefWorkPhone, ");
			sql.Append("DriversLicenceExpiry = @DriversLicenceExpiry, ");
			sql.Append("CurrentEmploymentBasis = @CurrentEmploymentBasis, ");
			sql.Append("PreviousEmploymentBasis = @PreviousEmploymentBasis, ");
			sql.Append("OtherEmploymentBasis = @OtherEmploymentBasis, ");
			sql.Append("PreviousEmployerPhone = @PreviousEmployerPhone, ");
			sql.Append("OtherEmployerPhone = @OtherEmployerPhone, ");
			sql.Append("DriversLicenceState = @DriversLicenceState, ");
			sql.Append("ResidencyStatus = @ResidencyStatus, ");
			sql.Append("HereSince = @HereSince, ");
			sql.Append("PrevSince = @PrevSince, ");
			sql.Append("EmplSince = @EmplSince, ");
			sql.Append("PrevEmplSince = @PrevEmplSince, ");
			sql.Append("OtherEmplSince = @OtherEmplSince, ");
			sql.Append("CurrentEmployerPhone = @CurrentEmployerPhone, ");
			sql.Append("HomeUnitNumber = @HomeUnitNumber, ");
			sql.Append("HomeStreetNumber = @HomeStreetNumber, ");
			sql.Append("HomeStreetName = @HomeStreetName, ");
			sql.Append("HomeStreetType = @HomeStreetType, ");
			sql.Append("EmplUnitNumber = @EmplUnitNumber, ");
			sql.Append("EmplStreetNumber = @EmplStreetNumber, ");
			sql.Append("EmplStreetName = @EmplStreetName, ");
			sql.Append("EmplStreetType = @EmplStreetType, ");
			sql.Append("PrevUnitNumber = @PrevUnitNumber, ");
			sql.Append("PrevStreetNumber = @PrevStreetNumber, ");
			sql.Append("PrevStreetName = @PrevStreetName, ");
			sql.Append("PrevStreetType = @PrevStreetType, ");
			sql.Append("PrevResidentialStatus = @PrevResidentialStatus, ");
			sql.Append("NameOfFriend = @NameOfFriend, ");
			sql.Append("FriendPhone = @FriendPhone, ");
			sql.Append("OtherAnnualGrossIncome = @OtherAnnualGrossIncome, ");
			sql.Append("TotalMonthlyNetIncome = @TotalMonthlyNetIncome, ");
			sql.Append("TotalMonthlyMorgageRentExpense = @TotalMonthlyMorgageRentExpense, ");
			sql.Append("OtherlMonthlyPersonalExpense = @OtherlMonthlyPersonalExpense, ");
			sql.Append("OccupationCategory = @OccupationCategory, ");
			sql.Append("OccupationIndustry = @OccupationIndustry, ");
			sql.Append("ResidentialStatusPL = @ResidentialStatusPL, ");
			sql.Append("LivedInCurrentAddress = @LivedInCurrentAddress, ");
			sql.Append("OtherName = @OtherName, ");
			sql.Append("IncomeOfPartner = @IncomeOfPartner, ");
			sql.Append("NameOfMortgagor = @NameOfMortgagor, ");
			sql.Append("NumberOfMortgagor = @NumberOfMortgagor, ");
			sql.Append("SelfBusinessName = @SelfBusinessName, ");
			sql.Append("SelfContactPerson = @SelfContactPerson, ");
			sql.Append("SelfPhone = @SelfPhone, ");
			sql.Append("Job2BusinessName = @Job2BusinessName, ");
			sql.Append("Job2Phone = @Job2Phone, ");
			sql.Append("AccountantCompanyName = @AccountantCompanyName, ");
			sql.Append("AccountantContactPerson = @AccountantContactPerson, ");
			sql.Append("AccountantContactNumber = @AccountantContactNumber, ");
			sql.Append("LivedInPreviousAddress = @LivedInPreviousAddress, ");
			sql.Append("MiddleName = @MiddleName, ");
			sql.Append("GivenNameOfMortgagor = @GivenNameOfMortgagor, ");
			sql.Append("SurNameOfMortgagor = @SurNameOfMortgagor, ");
			sql.Append("TotalMonthlyMortgageExpense = @TotalMonthlyMortgageExpense, ");
			sql.Append("TotalMonthlyRentExpense = @TotalMonthlyRentExpense, ");
			sql.Append("BorrowerCompanyName = @BorrowerCompanyName, ");
			sql.Append("BorrowerCompanyABN = @BorrowerCompanyABN, ");
			sql.Append("BorrowerCompanyPosition = @BorrowerCompanyPosition, ");
			sql.Append("AccountName = @AccountName, ");
			sql.Append("AcountNameLong = @AcountNameLong, ");
			sql.Append("AccountBSB = @AccountBSB, ");
			sql.Append("AccountNo = @AccountNo, ");
			sql.Append("TelephoneHomePrefer = @TelephoneHomePrefer, ");
			sql.Append("TelephoneWorkPrefer = @TelephoneWorkPrefer, ");
			sql.Append("MobilePrefer = @MobilePrefer, ");
			sql.Append("SMSPrefer = @SMSPrefer, ");
			sql.Append("FaxPrefer = @FaxPrefer, ");
			sql.Append("EmailPrefer = @EmailPrefer, ");
			sql.Append("EmailSecondPrefer = @EmailSecondPrefer, ");
			sql.Append("BDCardDate = @BDCardDate, ");
			sql.Append("TrustName = @TrustName, ");
			sql.Append("TrustABN = @TrustABN, ");
			sql.Append("TrustNatureOfBus = @TrustNatureOfBus, ");
			sql.Append("TrustAddressLine1 = @TrustAddressLine1, ");
			sql.Append("TrustAddressLine2 = @TrustAddressLine2, ");
			sql.Append("TrustSuburb = @TrustSuburb, ");
			sql.Append("TrustBusAddressLine1 = @TrustBusAddressLine1, ");
			sql.Append("TrustBusAddressLine2 = @TrustBusAddressLine2, ");
			sql.Append("TrustBusSuburb = @TrustBusSuburb, ");
			sql.Append("Trustee1 = @Trustee1, ");
			sql.Append("Trustee2 = @Trustee2, ");
			sql.Append("Trustee3 = @Trustee3, ");
			sql.Append("Trustee4 = @Trustee4, ");
			sql.Append("Beneficiary1 = @Beneficiary1, ");
			sql.Append("Beneficiary2 = @Beneficiary2, ");
			sql.Append("Beneficiary3 = @Beneficiary3, ");
			sql.Append("Beneficiary4 = @Beneficiary4, ");
			sql.Append("TypeOfTrust = @TypeOfTrust, ");
			sql.Append("GSTRegistered = @GSTRegistered, ");
			sql.Append("PropertyUnderTrustName = @PropertyUnderTrustName, ");
			sql.Append("TrustOrCompany = @TrustOrCompany, ");
			sql.Append("CCNumber = @CCNumber, ");
			sql.Append("CCProvider = @CCProvider, ");
			sql.Append("CCExpiryMonth = @CCExpiryMonth, ");
			sql.Append("CCExpiryYear = @CCExpiryYear, ");
			sql.Append("CCNameOnCard = @CCNameOnCard, ");
			sql.Append("AMLCTFIDCompleted = @AMLCTFIDCompleted, ");
			sql.Append("AMLCTFIDDate = @AMLCTFIDDate, ");
			sql.Append("AMLCTFIDNote = @AMLCTFIDNote, ");
			sql.Append("AMLCTFIDCompletedBy = @AMLCTFIDCompletedBy, ");
			sql.Append("Initials = @Initials ");

			sql.Append("WHERE ApplicantID = @ApplicantID");

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
			{
				SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

				cmd.Parameters.Add(new SqlParameter("ApplicantID", ApplicantID));
				cmd.Parameters.Add(new SqlParameter("Title", ((object)Title ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Surname", ((object)Surname ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FirstName", ((object)FirstName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Nickname", ((object)Nickname ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherNames", ((object)OtherNames ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DateOfBirth", ((object)DateOfBirth ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Sex", ((object)Sex ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("MaritalStatus", ((object)MaritalStatus ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ResidentialStatus", ((object)ResidentialStatus ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("HomeAddressLine1", ((object)HomeAddressLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("HomeAddressLine2", ((object)HomeAddressLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("HomeSuburb", ((object)HomeSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DurationYears", ((object)DurationYears ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DurationMonths", ((object)DurationMonths ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevAddressLine1", ((object)PrevAddressLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevAddressLine2", ((object)PrevAddressLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevSuburb", ((object)PrevSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevCountry", ((object)PrevCountry ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevDurationYears", ((object)PrevDurationYears ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevDurationMonths", ((object)PrevDurationMonths ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TelephoneHome", ((object)TelephoneHome ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TelephoneWork", ((object)TelephoneWork ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Mobile", ((object)Mobile ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DriversLicenceNo", ((object)DriversLicenceNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SpousesName", ((object)SpousesName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("NoOfDependents", ((object)NoOfDependents ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AgesOfDependents", ((object)AgesOfDependents ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("MothersMaidenName", ((object)MothersMaidenName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CurrentEmployer", ((object)CurrentEmployer ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Occupation", ((object)Occupation ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmpABN", ((object)EmpABN ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmplAddressLine1", ((object)EmplAddressLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmplAddressLine2", ((object)EmplAddressLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmplSuburb", ((object)EmplSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmplDurationYears", ((object)EmplDurationYears ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmplDurationMonths", ((object)EmplDurationMonths ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PreviousEmployer", ((object)PreviousEmployer ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PreviousOccupation", ((object)PreviousOccupation ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevEmplAddLine1", ((object)PrevEmplAddLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevEmplAddLine2", ((object)PrevEmplAddLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevEmplSuburb", ((object)PrevEmplSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevEmplCountry", ((object)PrevEmplCountry ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevEmplDurationYears", ((object)PrevEmplDurationYears ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevEmplDurationMnths", ((object)PrevEmplDurationMnths ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherEmployer", ((object)OtherEmployer ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherOccupation", ((object)OtherOccupation ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherEmplAddLine1", ((object)OtherEmplAddLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherEmplAddLine2", ((object)OtherEmplAddLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherEmplSuburb", ((object)OtherEmplSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherEmplDurationYears", ((object)OtherEmplDurationYears ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherEmplDurationMnths", ((object)OtherEmplDurationMnths ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("GrossIncome", ((object)GrossIncome ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("NettIncome", ((object)NettIncome ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("IncomePeriod", ((object)IncomePeriod ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Publication", ((object)Publication ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Bank", ((object)Bank ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("BankBranch", ((object)BankBranch ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("BankDurationYears", ((object)BankDurationYears ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("BankDurationMonths", ((object)BankDurationMonths ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PersonalRefTitle", ((object)PersonalRefTitle ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PersonalRefName", ((object)PersonalRefName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PersonalRefSurname", ((object)PersonalRefSurname ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PersonalRefRelationship", ((object)PersonalRefRelationship ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PersonalRefHomePhone", ((object)PersonalRefHomePhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PersonalRefAddLine1", ((object)PersonalRefAddLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PersonalRefAddLine2", ((object)PersonalRefAddLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PersonalRefSuburb", ((object)PersonalRefSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Details", ((object)Details ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Fax", ((object)Fax ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Email", ((object)Email ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmailSecond", ((object)EmailSecond ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PreviousApplicantID", ((object)PreviousApplicantID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PostalAddressLine1", ((object)PostalAddressLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PostalAddressLine2", ((object)PostalAddressLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PostalHomeSuburb", ((object)PostalHomeSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PersonalRefWorkPhone", ((object)PersonalRefWorkPhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DriversLicenceExpiry", ((object)DriversLicenceExpiry ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CurrentEmploymentBasis", ((object)CurrentEmploymentBasis ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PreviousEmploymentBasis", ((object)PreviousEmploymentBasis ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherEmploymentBasis", ((object)OtherEmploymentBasis ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PreviousEmployerPhone", ((object)PreviousEmployerPhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherEmployerPhone", ((object)OtherEmployerPhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("DriversLicenceState", ((object)DriversLicenceState ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ResidencyStatus", ((object)ResidencyStatus ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("HereSince", ((object)HereSince ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevSince", ((object)PrevSince ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmplSince", ((object)EmplSince ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevEmplSince", ((object)PrevEmplSince ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherEmplSince", ((object)OtherEmplSince ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CurrentEmployerPhone", ((object)CurrentEmployerPhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("HomeUnitNumber", ((object)HomeUnitNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("HomeStreetNumber", ((object)HomeStreetNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("HomeStreetName", ((object)HomeStreetName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("HomeStreetType", ((object)HomeStreetType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmplUnitNumber", ((object)EmplUnitNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmplStreetNumber", ((object)EmplStreetNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmplStreetName", ((object)EmplStreetName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmplStreetType", ((object)EmplStreetType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevUnitNumber", ((object)PrevUnitNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevStreetNumber", ((object)PrevStreetNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevStreetName", ((object)PrevStreetName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevStreetType", ((object)PrevStreetType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PrevResidentialStatus", ((object)PrevResidentialStatus ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("NameOfFriend", ((object)NameOfFriend ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FriendPhone", ((object)FriendPhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherAnnualGrossIncome", ((object)OtherAnnualGrossIncome ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TotalMonthlyNetIncome", ((object)TotalMonthlyNetIncome ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TotalMonthlyMorgageRentExpense", ((object)TotalMonthlyMorgageRentExpense ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherlMonthlyPersonalExpense", ((object)OtherlMonthlyPersonalExpense ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OccupationCategory", ((object)OccupationCategory ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OccupationIndustry", ((object)OccupationIndustry ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ResidentialStatusPL", ((object)ResidentialStatusPL ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LivedInCurrentAddress", ((object)LivedInCurrentAddress ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("OtherName", ((object)OtherName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("IncomeOfPartner", ((object)IncomeOfPartner ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("NameOfMortgagor", ((object)NameOfMortgagor ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("NumberOfMortgagor", ((object)NumberOfMortgagor ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SelfBusinessName", ((object)SelfBusinessName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SelfContactPerson", ((object)SelfContactPerson ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SelfPhone", ((object)SelfPhone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Job2BusinessName", ((object)Job2BusinessName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Job2Phone", ((object)Job2Phone ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantCompanyName", ((object)AccountantCompanyName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantContactPerson", ((object)AccountantContactPerson ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountantContactNumber", ((object)AccountantContactNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LivedInPreviousAddress", ((object)LivedInPreviousAddress ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("MiddleName", ((object)MiddleName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("GivenNameOfMortgagor", ((object)GivenNameOfMortgagor ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SurNameOfMortgagor", ((object)SurNameOfMortgagor ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TotalMonthlyMortgageExpense", ((object)TotalMonthlyMortgageExpense ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TotalMonthlyRentExpense", ((object)TotalMonthlyRentExpense ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("BorrowerCompanyName", ((object)BorrowerCompanyName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("BorrowerCompanyABN", ((object)BorrowerCompanyABN ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("BorrowerCompanyPosition", ((object)BorrowerCompanyPosition ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountName", ((object)AccountName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AcountNameLong", ((object)AcountNameLong ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountBSB", ((object)AccountBSB ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AccountNo", ((object)AccountNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TelephoneHomePrefer", ((object)TelephoneHomePrefer ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TelephoneWorkPrefer", ((object)TelephoneWorkPrefer ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("MobilePrefer", ((object)MobilePrefer ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SMSPrefer", ((object)SMSPrefer ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FaxPrefer", ((object)FaxPrefer ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmailPrefer", ((object)EmailPrefer ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EmailSecondPrefer", ((object)EmailSecondPrefer ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("BDCardDate", ((object)BDCardDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TrustName", ((object)TrustName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TrustABN", ((object)TrustABN ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TrustNatureOfBus", ((object)TrustNatureOfBus ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TrustAddressLine1", ((object)TrustAddressLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TrustAddressLine2", ((object)TrustAddressLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TrustSuburb", ((object)TrustSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TrustBusAddressLine1", ((object)TrustBusAddressLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TrustBusAddressLine2", ((object)TrustBusAddressLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TrustBusSuburb", ((object)TrustBusSuburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Trustee1", ((object)Trustee1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Trustee2", ((object)Trustee2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Trustee3", ((object)Trustee3 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Trustee4", ((object)Trustee4 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Beneficiary1", ((object)Beneficiary1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Beneficiary2", ((object)Beneficiary2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Beneficiary3", ((object)Beneficiary3 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Beneficiary4", ((object)Beneficiary4 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TypeOfTrust", ((object)TypeOfTrust ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("GSTRegistered", ((object)GSTRegistered ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PropertyUnderTrustName", ((object)PropertyUnderTrustName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TrustOrCompany", ((object)TrustOrCompany ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CCNumber", ((object)CCNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CCProvider", ((object)CCProvider ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CCExpiryMonth", ((object)CCExpiryMonth ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CCExpiryYear", ((object)CCExpiryYear ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CCNameOnCard", ((object)CCNameOnCard ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AMLCTFIDCompleted", ((object)AMLCTFIDCompleted ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AMLCTFIDDate", ((object)AMLCTFIDDate ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AMLCTFIDNote", ((object)AMLCTFIDNote ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AMLCTFIDCompletedBy", ((object)AMLCTFIDCompletedBy ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Initials", ((object)Initials ?? DBNull.Value)));

				conn.Open();
				cmd.ExecuteNonQuery();

			}
		}

		public void Create()
		{
			//checking that we have an ApplicationNo
			if ((ApplicationNo != null) && (ApplicationNo > 0))
			{


				//building the INSERT string...
				StringBuilder sql = new StringBuilder();
				sql.Append("INSERT INTO Applicant (");
				sql.Append("Title, ");
				sql.Append("Surname, ");
				sql.Append("FirstName, ");
				sql.Append("Nickname, ");
				sql.Append("OtherNames, ");
				sql.Append("DateOfBirth, ");
				sql.Append("Sex, ");
				sql.Append("MaritalStatus, ");
				sql.Append("ResidentialStatus, ");
				sql.Append("HomeAddressLine1, ");
				sql.Append("HomeAddressLine2, ");
				sql.Append("HomeSuburb, ");
				sql.Append("DurationYears, ");
				sql.Append("DurationMonths, ");
				sql.Append("PrevAddressLine1, ");
				sql.Append("PrevAddressLine2, ");
				sql.Append("PrevSuburb, ");
				sql.Append("PrevCountry, ");
				sql.Append("PrevDurationYears, ");
				sql.Append("PrevDurationMonths, ");
				sql.Append("TelephoneHome, ");
				sql.Append("TelephoneWork, ");
				sql.Append("Mobile, ");
				sql.Append("DriversLicenceNo, ");
				sql.Append("SpousesName, ");
				sql.Append("NoOfDependents, ");
				sql.Append("AgesOfDependents, ");
				sql.Append("MothersMaidenName, ");
				sql.Append("CurrentEmployer, ");
				sql.Append("Occupation, ");
				sql.Append("EmpABN, ");
				sql.Append("EmplAddressLine1, ");
				sql.Append("EmplAddressLine2, ");
				sql.Append("EmplSuburb, ");
				sql.Append("EmplDurationYears, ");
				sql.Append("EmplDurationMonths, ");
				sql.Append("PreviousEmployer, ");
				sql.Append("PreviousOccupation, ");
				sql.Append("PrevEmplAddLine1, ");
				sql.Append("PrevEmplAddLine2, ");
				sql.Append("PrevEmplSuburb, ");
				sql.Append("PrevEmplCountry, ");
				sql.Append("PrevEmplDurationYears, ");
				sql.Append("PrevEmplDurationMnths, ");
				sql.Append("OtherEmployer, ");
				sql.Append("OtherOccupation, ");
				sql.Append("OtherEmplAddLine1, ");
				sql.Append("OtherEmplAddLine2, ");
				sql.Append("OtherEmplSuburb, ");
				sql.Append("OtherEmplDurationYears, ");
				sql.Append("OtherEmplDurationMnths, ");
				sql.Append("GrossIncome, ");
				sql.Append("NettIncome, ");
				sql.Append("IncomePeriod, ");
				sql.Append("Publication, ");
				sql.Append("Bank, ");
				sql.Append("BankBranch, ");
				sql.Append("BankDurationYears, ");
				sql.Append("BankDurationMonths, ");
				sql.Append("PersonalRefTitle, ");
				sql.Append("PersonalRefName, ");
				sql.Append("PersonalRefSurname, ");
				sql.Append("PersonalRefRelationship, ");
				sql.Append("PersonalRefHomePhone, ");
				sql.Append("PersonalRefAddLine1, ");
				sql.Append("PersonalRefAddLine2, ");
				sql.Append("PersonalRefSuburb, ");
				sql.Append("Details, ");
				sql.Append("Fax, ");
				sql.Append("Email, ");
				sql.Append("EmailSecond, ");
				sql.Append("PreviousApplicantID, ");
				sql.Append("PostalAddressLine1, ");
				sql.Append("PostalAddressLine2, ");
				sql.Append("PostalHomeSuburb, ");
				sql.Append("PersonalRefWorkPhone, ");
				sql.Append("DriversLicenceExpiry, ");
				sql.Append("CurrentEmploymentBasis, ");
				sql.Append("PreviousEmploymentBasis, ");
				sql.Append("OtherEmploymentBasis, ");
				sql.Append("PreviousEmployerPhone, ");
				sql.Append("OtherEmployerPhone, ");
				sql.Append("DriversLicenceState, ");
				sql.Append("ResidencyStatus, ");
				sql.Append("HereSince, ");
				sql.Append("PrevSince, ");
				sql.Append("EmplSince, ");
				sql.Append("PrevEmplSince, ");
				sql.Append("OtherEmplSince, ");
				sql.Append("CurrentEmployerPhone, ");
				sql.Append("HomeUnitNumber, ");
				sql.Append("HomeStreetNumber, ");
				sql.Append("HomeStreetName, ");
				sql.Append("HomeStreetType, ");
				sql.Append("EmplUnitNumber, ");
				sql.Append("EmplStreetNumber, ");
				sql.Append("EmplStreetName, ");
				sql.Append("EmplStreetType, ");
				sql.Append("PrevUnitNumber, ");
				sql.Append("PrevStreetNumber, ");
				sql.Append("PrevStreetName, ");
				sql.Append("PrevStreetType, ");
				sql.Append("PrevResidentialStatus, ");
				sql.Append("NameOfFriend, ");
				sql.Append("FriendPhone, ");
				sql.Append("OtherAnnualGrossIncome, ");
				sql.Append("TotalMonthlyNetIncome, ");
				sql.Append("TotalMonthlyMorgageRentExpense, ");
				sql.Append("OtherlMonthlyPersonalExpense, ");
				sql.Append("OccupationCategory, ");
				sql.Append("OccupationIndustry, ");
				sql.Append("ResidentialStatusPL, ");
				sql.Append("LivedInCurrentAddress, ");
				sql.Append("OtherName, ");
				sql.Append("IncomeOfPartner, ");
				sql.Append("NameOfMortgagor, ");
				sql.Append("NumberOfMortgagor, ");
				sql.Append("SelfBusinessName, ");
				sql.Append("SelfContactPerson, ");
				sql.Append("SelfPhone, ");
				sql.Append("Job2BusinessName, ");
				sql.Append("Job2Phone, ");
				sql.Append("AccountantCompanyName, ");
				sql.Append("AccountantContactPerson, ");
				sql.Append("AccountantContactNumber, ");
				sql.Append("LivedInPreviousAddress, ");
				sql.Append("MiddleName, ");
				sql.Append("GivenNameOfMortgagor, ");
				sql.Append("SurNameOfMortgagor, ");
				sql.Append("TotalMonthlyMortgageExpense, ");
				sql.Append("TotalMonthlyRentExpense, ");
				sql.Append("BorrowerCompanyName, ");
				sql.Append("BorrowerCompanyABN, ");
				sql.Append("BorrowerCompanyPosition, ");
				sql.Append("AccountName, ");
				sql.Append("AcountNameLong, ");
				sql.Append("AccountBSB, ");
				sql.Append("AccountNo, ");
				sql.Append("TelephoneHomePrefer, ");
				sql.Append("TelephoneWorkPrefer, ");
				sql.Append("MobilePrefer, ");
				sql.Append("SMSPrefer, ");
				sql.Append("FaxPrefer, ");
				sql.Append("EmailPrefer, ");
				sql.Append("EmailSecondPrefer, ");
				sql.Append("BDCardDate, ");
				sql.Append("TrustName, ");
				sql.Append("TrustABN, ");
				sql.Append("TrustNatureOfBus, ");
				sql.Append("TrustAddressLine1, ");
				sql.Append("TrustAddressLine2, ");
				sql.Append("TrustSuburb, ");
				sql.Append("TrustBusAddressLine1, ");
				sql.Append("TrustBusAddressLine2, ");
				sql.Append("TrustBusSuburb, ");
				sql.Append("Trustee1, ");
				sql.Append("Trustee2, ");
				sql.Append("Trustee3, ");
				sql.Append("Trustee4, ");
				sql.Append("Beneficiary1, ");
				sql.Append("Beneficiary2, ");
				sql.Append("Beneficiary3, ");
				sql.Append("Beneficiary4, ");
				sql.Append("TypeOfTrust, ");
				sql.Append("GSTRegistered, ");
				sql.Append("PropertyUnderTrustName, ");
				sql.Append("TrustOrCompany, ");
				sql.Append("CCNumber, ");
				sql.Append("CCProvider, ");
				sql.Append("CCExpiryMonth, ");
				sql.Append("CCExpiryYear, ");
				sql.Append("CCNameOnCard, ");
				sql.Append("AMLCTFIDCompleted, ");
				sql.Append("AMLCTFIDDate, ");
				sql.Append("AMLCTFIDNote, ");
				sql.Append("AMLCTFIDCompletedBy, ");
				sql.Append("Initials ");

				sql.Append(") VALUES (");

				sql.Append("@Title, ");
				sql.Append("@Surname, ");
				sql.Append("@FirstName, ");
				sql.Append("@Nickname, ");
				sql.Append("@OtherNames, ");
				sql.Append("@DateOfBirth, ");
				sql.Append("@Sex, ");
				sql.Append("@MaritalStatus, ");
				sql.Append("@ResidentialStatus, ");
				sql.Append("@HomeAddressLine1, ");
				sql.Append("@HomeAddressLine2, ");
				sql.Append("@HomeSuburb, ");
				sql.Append("@DurationYears, ");
				sql.Append("@DurationMonths, ");
				sql.Append("@PrevAddressLine1, ");
				sql.Append("@PrevAddressLine2, ");
				sql.Append("@PrevSuburb, ");
				sql.Append("@PrevCountry, ");
				sql.Append("@PrevDurationYears, ");
				sql.Append("@PrevDurationMonths, ");
				sql.Append("@TelephoneHome, ");
				sql.Append("@TelephoneWork, ");
				sql.Append("@Mobile, ");
				sql.Append("@DriversLicenceNo, ");
				sql.Append("@SpousesName, ");
				sql.Append("@NoOfDependents, ");
				sql.Append("@AgesOfDependents, ");
				sql.Append("@MothersMaidenName, ");
				sql.Append("@CurrentEmployer, ");
				sql.Append("@Occupation, ");
				sql.Append("@EmpABN, ");
				sql.Append("@EmplAddressLine1, ");
				sql.Append("@EmplAddressLine2, ");
				sql.Append("@EmplSuburb, ");
				sql.Append("@EmplDurationYears, ");
				sql.Append("@EmplDurationMonths, ");
				sql.Append("@PreviousEmployer, ");
				sql.Append("@PreviousOccupation, ");
				sql.Append("@PrevEmplAddLine1, ");
				sql.Append("@PrevEmplAddLine2, ");
				sql.Append("@PrevEmplSuburb, ");
				sql.Append("@PrevEmplCountry, ");
				sql.Append("@PrevEmplDurationYears, ");
				sql.Append("@PrevEmplDurationMnths, ");
				sql.Append("@OtherEmployer, ");
				sql.Append("@OtherOccupation, ");
				sql.Append("@OtherEmplAddLine1, ");
				sql.Append("@OtherEmplAddLine2, ");
				sql.Append("@OtherEmplSuburb, ");
				sql.Append("@OtherEmplDurationYears, ");
				sql.Append("@OtherEmplDurationMnths, ");
				sql.Append("@GrossIncome, ");
				sql.Append("@NettIncome, ");
				sql.Append("@IncomePeriod, ");
				sql.Append("@Publication, ");
				sql.Append("@Bank, ");
				sql.Append("@BankBranch, ");
				sql.Append("@BankDurationYears, ");
				sql.Append("@BankDurationMonths, ");
				sql.Append("@PersonalRefTitle, ");
				sql.Append("@PersonalRefName, ");
				sql.Append("@PersonalRefSurname, ");
				sql.Append("@PersonalRefRelationship, ");
				sql.Append("@PersonalRefHomePhone, ");
				sql.Append("@PersonalRefAddLine1, ");
				sql.Append("@PersonalRefAddLine2, ");
				sql.Append("@PersonalRefSuburb, ");
				sql.Append("@Details, ");
				sql.Append("@Fax, ");
				sql.Append("@Email, ");
				sql.Append("@EmailSecond, ");
				sql.Append("@PreviousApplicantID, ");
				sql.Append("@PostalAddressLine1, ");
				sql.Append("@PostalAddressLine2, ");
				sql.Append("@PostalHomeSuburb, ");
				sql.Append("@PersonalRefWorkPhone, ");
				sql.Append("@DriversLicenceExpiry, ");
				sql.Append("@CurrentEmploymentBasis, ");
				sql.Append("@PreviousEmploymentBasis, ");
				sql.Append("@OtherEmploymentBasis, ");
				sql.Append("@PreviousEmployerPhone, ");
				sql.Append("@OtherEmployerPhone, ");
				sql.Append("@DriversLicenceState, ");
				sql.Append("@ResidencyStatus, ");
				sql.Append("@HereSince, ");
				sql.Append("@PrevSince, ");
				sql.Append("@EmplSince, ");
				sql.Append("@PrevEmplSince, ");
				sql.Append("@OtherEmplSince, ");
				sql.Append("@CurrentEmployerPhone, ");
				sql.Append("@HomeUnitNumber, ");
				sql.Append("@HomeStreetNumber, ");
				sql.Append("@HomeStreetName, ");
				sql.Append("@HomeStreetType, ");
				sql.Append("@EmplUnitNumber, ");
				sql.Append("@EmplStreetNumber, ");
				sql.Append("@EmplStreetName, ");
				sql.Append("@EmplStreetType, ");
				sql.Append("@PrevUnitNumber, ");
				sql.Append("@PrevStreetNumber, ");
				sql.Append("@PrevStreetName, ");
				sql.Append("@PrevStreetType, ");
				sql.Append("@PrevResidentialStatus, ");
				sql.Append("@NameOfFriend, ");
				sql.Append("@FriendPhone, ");
				sql.Append("@OtherAnnualGrossIncome, ");
				sql.Append("@TotalMonthlyNetIncome, ");
				sql.Append("@TotalMonthlyMorgageRentExpense, ");
				sql.Append("@OtherlMonthlyPersonalExpense, ");
				sql.Append("@OccupationCategory, ");
				sql.Append("@OccupationIndustry, ");
				sql.Append("@ResidentialStatusPL, ");
				sql.Append("@LivedInCurrentAddress, ");
				sql.Append("@OtherName, ");
				sql.Append("@IncomeOfPartner, ");
				sql.Append("@NameOfMortgagor, ");
				sql.Append("@NumberOfMortgagor, ");
				sql.Append("@SelfBusinessName, ");
				sql.Append("@SelfContactPerson, ");
				sql.Append("@SelfPhone, ");
				sql.Append("@Job2BusinessName, ");
				sql.Append("@Job2Phone, ");
				sql.Append("@AccountantCompanyName, ");
				sql.Append("@AccountantContactPerson, ");
				sql.Append("@AccountantContactNumber, ");
				sql.Append("@LivedInPreviousAddress, ");
				sql.Append("@MiddleName, ");
				sql.Append("@GivenNameOfMortgagor, ");
				sql.Append("@SurNameOfMortgagor, ");
				sql.Append("@TotalMonthlyMortgageExpense, ");
				sql.Append("@TotalMonthlyRentExpense, ");
				sql.Append("@BorrowerCompanyName, ");
				sql.Append("@BorrowerCompanyABN, ");
				sql.Append("@BorrowerCompanyPosition, ");
				sql.Append("@AccountName, ");
				sql.Append("@AcountNameLong, ");
				sql.Append("@AccountBSB, ");
				sql.Append("@AccountNo, ");
				sql.Append("@TelephoneHomePrefer, ");
				sql.Append("@TelephoneWorkPrefer, ");
				sql.Append("@MobilePrefer, ");
				sql.Append("@SMSPrefer, ");
				sql.Append("@FaxPrefer, ");
				sql.Append("@EmailPrefer, ");
				sql.Append("@EmailSecondPrefer, ");
				sql.Append("@BDCardDate, ");
				sql.Append("@TrustName, ");
				sql.Append("@TrustABN, ");
				sql.Append("@TrustNatureOfBus, ");
				sql.Append("@TrustAddressLine1, ");
				sql.Append("@TrustAddressLine2, ");
				sql.Append("@TrustSuburb, ");
				sql.Append("@TrustBusAddressLine1, ");
				sql.Append("@TrustBusAddressLine2, ");
				sql.Append("@TrustBusSuburb, ");
				sql.Append("@Trustee1, ");
				sql.Append("@Trustee2, ");
				sql.Append("@Trustee3, ");
				sql.Append("@Trustee4, ");
				sql.Append("@Beneficiary1, ");
				sql.Append("@Beneficiary2, ");
				sql.Append("@Beneficiary3, ");
				sql.Append("@Beneficiary4, ");
				sql.Append("@TypeOfTrust, ");
				sql.Append("@GSTRegistered, ");
				sql.Append("@PropertyUnderTrustName, ");
				sql.Append("@TrustOrCompany, ");
				sql.Append("@CCNumber, ");
				sql.Append("@CCProvider, ");
				sql.Append("@CCExpiryMonth, ");
				sql.Append("@CCExpiryYear, ");
				sql.Append("@CCNameOnCard, ");
				sql.Append("@AMLCTFIDCompleted, ");
				sql.Append("@AMLCTFIDDate, ");
				sql.Append("@AMLCTFIDNote, ");
				sql.Append("@AMLCTFIDCompletedBy, ");
				sql.Append("@Initials");

				sql.Append("); SELECT @@IDENTITY");

				using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
				{
					SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

					cmd.Parameters.Add(new SqlParameter("Title", ((object)Title ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Surname", ((object)Surname ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FirstName", ((object)FirstName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Nickname", ((object)Nickname ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherNames", ((object)OtherNames ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("DateOfBirth", ((object)DateOfBirth ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Sex", ((object)Sex ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("MaritalStatus", ((object)MaritalStatus ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ResidentialStatus", ((object)ResidentialStatus ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("HomeAddressLine1", ((object)HomeAddressLine1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("HomeAddressLine2", ((object)HomeAddressLine2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("HomeSuburb", ((object)HomeSuburb ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("DurationYears", ((object)DurationYears ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("DurationMonths", ((object)DurationMonths ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevAddressLine1", ((object)PrevAddressLine1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevAddressLine2", ((object)PrevAddressLine2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevSuburb", ((object)PrevSuburb ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevCountry", ((object)PrevCountry ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevDurationYears", ((object)PrevDurationYears ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevDurationMonths", ((object)PrevDurationMonths ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TelephoneHome", ((object)TelephoneHome ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TelephoneWork", ((object)TelephoneWork ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Mobile", ((object)Mobile ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("DriversLicenceNo", ((object)DriversLicenceNo ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SpousesName", ((object)SpousesName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("NoOfDependents", ((object)NoOfDependents ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AgesOfDependents", ((object)AgesOfDependents ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("MothersMaidenName", ((object)MothersMaidenName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("CurrentEmployer", ((object)CurrentEmployer ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Occupation", ((object)Occupation ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmpABN", ((object)EmpABN ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmplAddressLine1", ((object)EmplAddressLine1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmplAddressLine2", ((object)EmplAddressLine2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmplSuburb", ((object)EmplSuburb ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmplDurationYears", ((object)EmplDurationYears ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmplDurationMonths", ((object)EmplDurationMonths ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PreviousEmployer", ((object)PreviousEmployer ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PreviousOccupation", ((object)PreviousOccupation ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevEmplAddLine1", ((object)PrevEmplAddLine1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevEmplAddLine2", ((object)PrevEmplAddLine2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevEmplSuburb", ((object)PrevEmplSuburb ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevEmplCountry", ((object)PrevEmplCountry ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevEmplDurationYears", ((object)PrevEmplDurationYears ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevEmplDurationMnths", ((object)PrevEmplDurationMnths ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherEmployer", ((object)OtherEmployer ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherOccupation", ((object)OtherOccupation ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherEmplAddLine1", ((object)OtherEmplAddLine1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherEmplAddLine2", ((object)OtherEmplAddLine2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherEmplSuburb", ((object)OtherEmplSuburb ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherEmplDurationYears", ((object)OtherEmplDurationYears ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherEmplDurationMnths", ((object)OtherEmplDurationMnths ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("GrossIncome", ((object)GrossIncome ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("NettIncome", ((object)NettIncome ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("IncomePeriod", ((object)IncomePeriod ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Publication", ((object)Publication ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Bank", ((object)Bank ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("BankBranch", ((object)BankBranch ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("BankDurationYears", ((object)BankDurationYears ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("BankDurationMonths", ((object)BankDurationMonths ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PersonalRefTitle", ((object)PersonalRefTitle ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PersonalRefName", ((object)PersonalRefName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PersonalRefSurname", ((object)PersonalRefSurname ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PersonalRefRelationship", ((object)PersonalRefRelationship ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PersonalRefHomePhone", ((object)PersonalRefHomePhone ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PersonalRefAddLine1", ((object)PersonalRefAddLine1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PersonalRefAddLine2", ((object)PersonalRefAddLine2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PersonalRefSuburb", ((object)PersonalRefSuburb ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Details", ((object)Details ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Fax", ((object)Fax ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Email", ((object)Email ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmailSecond", ((object)EmailSecond ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PreviousApplicantID", ((object)PreviousApplicantID ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PostalAddressLine1", ((object)PostalAddressLine1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PostalAddressLine2", ((object)PostalAddressLine2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PostalHomeSuburb", ((object)PostalHomeSuburb ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PersonalRefWorkPhone", ((object)PersonalRefWorkPhone ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("DriversLicenceExpiry", ((object)DriversLicenceExpiry ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("CurrentEmploymentBasis", ((object)CurrentEmploymentBasis ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PreviousEmploymentBasis", ((object)PreviousEmploymentBasis ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherEmploymentBasis", ((object)OtherEmploymentBasis ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PreviousEmployerPhone", ((object)PreviousEmployerPhone ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherEmployerPhone", ((object)OtherEmployerPhone ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("DriversLicenceState", ((object)DriversLicenceState ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ResidencyStatus", ((object)ResidencyStatus ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("HereSince", ((object)HereSince ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevSince", ((object)PrevSince ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmplSince", ((object)EmplSince ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevEmplSince", ((object)PrevEmplSince ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherEmplSince", ((object)OtherEmplSince ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("CurrentEmployerPhone", ((object)CurrentEmployerPhone ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("HomeUnitNumber", ((object)HomeUnitNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("HomeStreetNumber", ((object)HomeStreetNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("HomeStreetName", ((object)HomeStreetName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("HomeStreetType", ((object)HomeStreetType ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmplUnitNumber", ((object)EmplUnitNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmplStreetNumber", ((object)EmplStreetNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmplStreetName", ((object)EmplStreetName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmplStreetType", ((object)EmplStreetType ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevUnitNumber", ((object)PrevUnitNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevStreetNumber", ((object)PrevStreetNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevStreetName", ((object)PrevStreetName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevStreetType", ((object)PrevStreetType ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PrevResidentialStatus", ((object)PrevResidentialStatus ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("NameOfFriend", ((object)NameOfFriend ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FriendPhone", ((object)FriendPhone ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherAnnualGrossIncome", ((object)OtherAnnualGrossIncome ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TotalMonthlyNetIncome", ((object)TotalMonthlyNetIncome ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TotalMonthlyMorgageRentExpense", ((object)TotalMonthlyMorgageRentExpense ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherlMonthlyPersonalExpense", ((object)OtherlMonthlyPersonalExpense ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OccupationCategory", ((object)OccupationCategory ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OccupationIndustry", ((object)OccupationIndustry ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ResidentialStatusPL", ((object)ResidentialStatusPL ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("LivedInCurrentAddress", ((object)LivedInCurrentAddress ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("OtherName", ((object)OtherName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("IncomeOfPartner", ((object)IncomeOfPartner ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("NameOfMortgagor", ((object)NameOfMortgagor ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("NumberOfMortgagor", ((object)NumberOfMortgagor ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SelfBusinessName", ((object)SelfBusinessName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SelfContactPerson", ((object)SelfContactPerson ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SelfPhone", ((object)SelfPhone ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Job2BusinessName", ((object)Job2BusinessName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Job2Phone", ((object)Job2Phone ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AccountantCompanyName", ((object)AccountantCompanyName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AccountantContactPerson", ((object)AccountantContactPerson ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AccountantContactNumber", ((object)AccountantContactNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("LivedInPreviousAddress", ((object)LivedInPreviousAddress ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("MiddleName", ((object)MiddleName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("GivenNameOfMortgagor", ((object)GivenNameOfMortgagor ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SurNameOfMortgagor", ((object)SurNameOfMortgagor ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TotalMonthlyMortgageExpense", ((object)TotalMonthlyMortgageExpense ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TotalMonthlyRentExpense", ((object)TotalMonthlyRentExpense ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("BorrowerCompanyName", ((object)BorrowerCompanyName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("BorrowerCompanyABN", ((object)BorrowerCompanyABN ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("BorrowerCompanyPosition", ((object)BorrowerCompanyPosition ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AccountName", ((object)AccountName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AcountNameLong", ((object)AcountNameLong ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AccountBSB", ((object)AccountBSB ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AccountNo", ((object)AccountNo ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TelephoneHomePrefer", ((object)TelephoneHomePrefer ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TelephoneWorkPrefer", ((object)TelephoneWorkPrefer ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("MobilePrefer", ((object)MobilePrefer ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SMSPrefer", ((object)SMSPrefer ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FaxPrefer", ((object)FaxPrefer ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmailPrefer", ((object)EmailPrefer ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EmailSecondPrefer", ((object)EmailSecondPrefer ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("BDCardDate", ((object)BDCardDate ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TrustName", ((object)TrustName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TrustABN", ((object)TrustABN ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TrustNatureOfBus", ((object)TrustNatureOfBus ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TrustAddressLine1", ((object)TrustAddressLine1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TrustAddressLine2", ((object)TrustAddressLine2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TrustSuburb", ((object)TrustSuburb ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TrustBusAddressLine1", ((object)TrustBusAddressLine1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TrustBusAddressLine2", ((object)TrustBusAddressLine2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TrustBusSuburb", ((object)TrustBusSuburb ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Trustee1", ((object)Trustee1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Trustee2", ((object)Trustee2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Trustee3", ((object)Trustee3 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Trustee4", ((object)Trustee4 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Beneficiary1", ((object)Beneficiary1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Beneficiary2", ((object)Beneficiary2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Beneficiary3", ((object)Beneficiary3 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Beneficiary4", ((object)Beneficiary4 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TypeOfTrust", ((object)TypeOfTrust ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("GSTRegistered", ((object)GSTRegistered ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PropertyUnderTrustName", ((object)PropertyUnderTrustName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TrustOrCompany", ((object)TrustOrCompany ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("CCNumber", ((object)CCNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("CCProvider", ((object)CCProvider ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("CCExpiryMonth", ((object)CCExpiryMonth ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("CCExpiryYear", ((object)CCExpiryYear ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("CCNameOnCard", ((object)CCNameOnCard ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AMLCTFIDCompleted", ((object)AMLCTFIDCompleted ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AMLCTFIDDate", ((object)AMLCTFIDDate ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AMLCTFIDNote", ((object)AMLCTFIDNote ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AMLCTFIDCompletedBy", ((object)AMLCTFIDCompletedBy ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Initials", ((object)Initials ?? DBNull.Value)));

					conn.Open();
					try
					{
						ApplicantID = Convert.ToInt32(cmd.ExecuteScalar());
					}
					catch { throw new Exception(); }

					//creating the ApplicantApplication link record (despite being a many-to-many relationship!)
					string sql2 = "INSERT INTO ApplicantApplication (ApplicantID, ApplicationNo) VALUES (" + ApplicantID.ToString() + ", " + ApplicationNo.ToString() + ")";
					SqlCommand cmd2 = new SqlCommand(sql2, conn);
					cmd2.ExecuteNonQuery();
				}
			}
			else { throw new Exception("ApplicationNo is required to create."); }
		}

    public void InitialiseAssetLiabilityItems()
    {
      //find out if there are any Asset Liability items.
			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT * FROM AssetLiability WHERE ApplicantID = " + ApplicantID.ToString(), conn);
				conn.Open();
				SqlDataReader dr = cmd.ExecuteReader();
        SqlCommand cmd22; SqlCommand cmd33;
        SqlDataReader dr22;
        if (!dr.Read()) //if there are no items, insert the default items.
        {                    
          cmd22 = new SqlCommand("SELECT * FROM AssetLiabilityType WHERE DisplayOnWeb = 1 AND NumberOfDefaultCreates > 0 ORDER BY SortOrder", conn);          
          dr22 = cmd22.ExecuteReader();
          while (dr22.Read())
          {
            for (int i = 1; i <= Convert.ToInt32(dr22["NumberOfDefaultCreates"]); i++)
            {
              cmd33 = new SqlCommand("INSERT INTO AssetLiability (ApplicantID, AssetLiabilityID) VALUES (" + ApplicantID.ToString() + ", " + dr22["AssetLiabilityID"].ToString() + ")", conn);
              cmd33.ExecuteNonQuery();              
            }
          }
        }
      }
      
    }

	}
}
