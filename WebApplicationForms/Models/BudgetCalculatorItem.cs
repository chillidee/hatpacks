﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace WebApplicationForms.Models
{
    public class BudgetCalculatorItem
    {
        public int ItemID;
        public int BudgetCalculatorID;
        public int ItemTypeID;
        public Decimal ItemValue;
        public String ItemFrequency;
        public String ItemComments;

        public void Create()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO BudgetCalculatorItem (");
            sql.Append("BudgetCalculatorID, ");
            sql.Append("ItemTypeID, ");
            sql.Append("ItemValue, ");
            sql.Append("ItemFrequency, ");
            sql.Append("ItemComments ");

            sql.Append(") VALUES (");

            sql.Append("@BudgetCalculatorID, ");
            sql.Append("@ItemTypeID, ");
            sql.Append("@ItemValue, ");
            sql.Append("@ItemFrequency, ");
            sql.Append("@ItemComments");

            sql.Append("); SELECT @@IDENTITY");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

                cmd.Parameters.Add(new SqlParameter("BudgetCalculatorID", ((object)BudgetCalculatorID ?? DBNull.Value)));
                cmd.Parameters.Add(new SqlParameter("ItemTypeID", ((object)ItemTypeID ?? DBNull.Value)));
                cmd.Parameters.Add(new SqlParameter("ItemValue", ((object)ItemValue ?? DBNull.Value)));
                cmd.Parameters.Add(new SqlParameter("ItemFrequency", ((object)ItemFrequency ?? DBNull.Value)));
                cmd.Parameters.Add(new SqlParameter("ItemComments", ((object)ItemComments ?? DBNull.Value)));

                conn.Open();
                try
                {
                    ItemID = Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch { throw new Exception(); }
            }
        }

        public static List<BudgetCalculatorItem> Get(int budgetCalculatorID)
        {
            List<BudgetCalculatorItem> items = new List<BudgetCalculatorItem>();

            string sql = "SELECT * FROM BudgetCalculatorItem WHERE BudgetCalculatorID = " + budgetCalculatorID.ToString();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    items.Add(new BudgetCalculatorItem()
                    {
                        ItemID = Convert.ToInt32(dr["ItemID"]),
                        BudgetCalculatorID = budgetCalculatorID,
                        ItemTypeID = (dr["ItemTypeID"] as Int32?) ?? 0,
                        ItemValue = (dr["ItemValue"] as Decimal?) ?? 0,
                        ItemFrequency = dr["ItemFrequency"] as String,
                        ItemComments = dr["ItemComments"] as String
                    });
                }
            }

            return items;
        }

        public static void Delete(int budgetCalculatorID)
        {
            string sql = "DELETE BudgetCalculatorItem WHERE BudgetCalculatorID =" + budgetCalculatorID.ToString();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}