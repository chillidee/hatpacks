﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplicationForms.Models
{
    public class EnquiryForm
    {
        private Boolean _isValid = false;
        private List<string> _errorMessage = new List<string>();

        private Decimal _loanAmount;
        private Int32 _typeOfLoan;
        private Boolean _hasProperty;
        private Boolean _haveDeposit;
        private Decimal _realEstateValue;
        private Decimal _balanceOwing;

        public Boolean _UserHasDefaults;
        public Boolean _UserLoanOver7K;

        public String cid { get; set; }
        public String title { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public String loanAmount { get; set; }
        public String typeOfLoan { get; set; }
        public String hasProperty { get; set; }
        public String haveDeposit { get; set; }
        public String realEstateValue { get; set; }
        public String balanceOwing { get; set; }
        public String mobileNumber { get; set; }
        public String landLineNumber { get; set; }
        public String emailAddress { get; set; }
        public String suburb { get; set; }
        public String state { get; set; }
        public String postCode { get; set; }
        public String referral { get; set; }
        public String comments { get; set; }
        public String referrer { get; set; }
        public String t { get; set; }
        public String k { get; set; }
        public String a { get; set; }
        public String platform { get; set; }

        public String UserHasDefaults { get; set; }
        public String UserLoanOver7K { get; set; }
        public String LenderID { get; set; }
        public String CallStatusId { get; set; }

        public Boolean isValid
        {
            get { return _isValid; }
        }

        public string[] errorMessage
        {
            get { return _errorMessage.ToArray(); }
        }

        public string CallID { get; set; }

        public void Save()
        {
            Validate();
            if (_isValid)
            {
                if (String.IsNullOrEmpty(title)) title = "";
                if (String.IsNullOrEmpty(firstName)) firstName = "";
                if (String.IsNullOrEmpty(lastName)) lastName = "[UNKNOWN]";
                if (String.IsNullOrEmpty(landLineNumber)) landLineNumber = "";
                if (String.IsNullOrEmpty(mobileNumber)) mobileNumber = "";
                if (String.IsNullOrEmpty(emailAddress)) emailAddress = "";
                if (String.IsNullOrEmpty(comments)) comments = "";
                if (String.IsNullOrEmpty(postCode)) postCode = "";
                if (String.IsNullOrEmpty(suburb)) suburb = "";
                if (String.IsNullOrEmpty(state)) state = "";
                if (String.IsNullOrEmpty(referrer)) referrer = "";
                if (String.IsNullOrEmpty(loanAmount)) loanAmount = "";
                if (String.IsNullOrEmpty(t)) t = "";
                if (String.IsNullOrEmpty(k)) k = "";
                if (String.IsNullOrEmpty(a)) a = "";

                //Added by ADW
                if (String.IsNullOrEmpty(UserHasDefaults)) UserHasDefaults = string.Empty;
                if (String.IsNullOrEmpty(UserLoanOver7K)) UserLoanOver7K = string.Empty;
                if (String.IsNullOrEmpty(LenderID)) LenderID = string.Empty;



                MMCompany c = new MMCompany(cid);
                if (_typeOfLoan == 0 && (c.DefaultLoanPurposeID != null || c.DefaultLoanPurposeID == 0)) { _typeOfLoan = (int)c.DefaultLoanPurposeID; }


                int? callStatusId = c.DefaultCallStatusID;

                if (!string.IsNullOrEmpty(LenderID) && LenderID.Length > 0)
                {
                    MMLender lender = new MMLender(LenderID);
                    if (!string.IsNullOrEmpty(lender.Code))
                    {
                        if (lender.DefaultCallStatusID > 0)
                        {
                            callStatusId = lender.DefaultCallStatusID;
                        }
                    }
                }
                
                if (!string.IsNullOrEmpty(comments))
                {
                    var newComments = comments;
                    while (newComments.Contains("<br>"))
                    {
                        var postion = newComments.IndexOf("<br>");
                        var left = newComments.Substring(0, postion);
                        var right = newComments.Substring(postion + 4, newComments.Length - (postion + 4));
                        newComments = left + Environment.NewLine + right;

                    }
                    comments = newComments;
                }

                //lets create!
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("spCreateLeadFromWebAPI", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("CompanyID", c.CompanyID));
                    cmd.Parameters.Add(new SqlParameter("Website", c.Acronym));
                    cmd.Parameters.Add(new SqlParameter("Title", title));
                    cmd.Parameters.Add(new SqlParameter("Surname", lastName));
                    cmd.Parameters.Add(new SqlParameter("Firstname", firstName));
                    cmd.Parameters.Add(new SqlParameter("TelephoneHome", ""));
                    cmd.Parameters.Add(new SqlParameter("TelephoneWork", landLineNumber));
                    cmd.Parameters.Add(new SqlParameter("Mobile", mobileNumber));
                    cmd.Parameters.Add(new SqlParameter("Email", emailAddress));
                    cmd.Parameters.Add(new SqlParameter("Notes", comments));
                    cmd.Parameters.Add(new SqlParameter("PostCode", postCode));
                    cmd.Parameters.Add(new SqlParameter("Suburb", suburb));
                    cmd.Parameters.Add(new SqlParameter("State", state));
                    cmd.Parameters.Add(new SqlParameter("ReferralURL", referrer));
                    cmd.Parameters.Add(new SqlParameter("LoanPurposeID", (_typeOfLoan == 0 ? (object)DBNull.Value : _typeOfLoan)));
                    cmd.Parameters.Add(new SqlParameter("HasProperty", _hasProperty));
                    cmd.Parameters.Add(new SqlParameter("LoanAmount", loanAmount));
                    cmd.Parameters.Add(new SqlParameter("TotalPropertyValue", _realEstateValue));
                    cmd.Parameters.Add(new SqlParameter("TotalPropertyOwing", _balanceOwing));
                    cmd.Parameters.Add(new SqlParameter("CallStatusID", callStatusId));
                    cmd.Parameters.Add(new SqlParameter("CFIssue01", ""));
                    cmd.Parameters.Add(new SqlParameter("CFIssue02", ""));
                    cmd.Parameters.Add(new SqlParameter("CFIssue03", ""));
                    cmd.Parameters.Add(new SqlParameter("CFIssue04", ""));
                    cmd.Parameters.Add(new SqlParameter("ProductID", ""));
                    cmd.Parameters.Add(new SqlParameter("SearchTerm", ""));
                    cmd.Parameters.Add(new SqlParameter("ReferralType", t));

                    cmd.Parameters.Add(new SqlParameter("UserHasDefaults", UserHasDefaults));
                    cmd.Parameters.Add(new SqlParameter("UserLoanOverXK", UserLoanOver7K));
                    cmd.Parameters.Add(new SqlParameter("LenderID", LenderID));
                    cmd.Parameters.Add(new SqlParameter("UpdateCallStatusIDTo", CallStatusId));                    
                    //cmd.Parameters.Add(new SqlParameter("AdwordsKeyword", AdwordsKeyword));

                    SqlParameter sp = new SqlParameter("AdwordsKeyword", System.Data.SqlDbType.VarChar, 50);
                    sp.Value = k;
                    cmd.Parameters.Add(sp);

                    cmd.Parameters.Add(new SqlParameter("Adgroup", a));
                    cmd.Parameters.Add(new SqlParameter("Referral", referral));

                    SqlParameter p_rtn = cmd.Parameters.Add("@ReturnValue", System.Data.SqlDbType.Int);
                    p_rtn.Direction = System.Data.ParameterDirection.ReturnValue;

                    conn.Open();
                    try
                    {
                        cmd.ExecuteNonQuery();
                        if (p_rtn.Value.ToString() != "0")
                        {
                            CallID = p_rtn.Value.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        _errorMessage.Add(ex.Message);
                        _isValid = false;
                        CallID = "";
                    }

                }
            }

        }

        public void Validate()
        {
            _isValid = true;

            if (string.IsNullOrEmpty(platform))
            {
                _errorMessage.Add("<platform> is required. Must be either 'mobile' or 'desktop'.");
                _isValid = false;
            }
            else
            {
                if ((platform == "mobile") || (platform == "desktop")) { }//all ok!
                else
                {
                    _errorMessage.Add("<platform> must be either 'mobile' or 'desktop'.");
                    _isValid = false;
                }
            }


            MMCompany c = new MMCompany(cid);
            if (c.CompanyID == 0)
            {
                _errorMessage.Add("<cid> is invalid or not found.");
                _isValid = false;
            }
            if ((platform == "mobile" && c.WebEnquiryFormShowTitle_M) || (platform == "desktop" && c.WebEnquiryFormShowTitle))
            {
                if ((title == "Mr") || (title == "Mrs") || (title == "Ms") || (title == "Dr"))
                {
                    //all ok!
                }
                else
                {
                    _errorMessage.Add("<title> is invalid, only Mr, Mrs, Ms or Dr accepted."); _isValid = false;
                }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowFirstname_M) || (platform == "desktop" && c.WebEnquiryFormShowFirstname))
            {
                if (string.IsNullOrEmpty(firstName))
                {
                    _errorMessage.Add("<firstName> is required.");
                    _isValid = false;
                }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowLastname_M) || (platform == "desktop" && c.WebEnquiryFormShowLastname))
            {
                if (string.IsNullOrEmpty(lastName))
                {
                    _errorMessage.Add("<lastName> is required.");
                    _isValid = false;
                }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowLoanAmount_M) || (platform == "desktop" && c.WebEnquiryFormShowLoanAmount))
            {
                if (string.IsNullOrEmpty(loanAmount))
                {
                    _errorMessage.Add("<loanAmount> is required.");
                    _isValid = false;
                }
                else
                {
                    if (!Decimal.TryParse(loanAmount, out _loanAmount))
                    {
                        _errorMessage.Add("<loanAmount> is not valid.");
                        _isValid = false;
                    }

                }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowLoanType_M) || (platform == "desktop" && c.WebEnquiryFormShowLoanType))
            {
                if (string.IsNullOrEmpty(typeOfLoan))
                {
                    _errorMessage.Add("<typeOfLoan> is required.");
                    _isValid = false;
                }
                else
                {
                    if (!Int32.TryParse(typeOfLoan, out _typeOfLoan))
                    {
                        _errorMessage.Add("<typeOfLoan> is not valid, only integers are accepted.");
                        _isValid = false;
                    }
                }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowHasProperty_M) || (platform == "desktop" && c.WebEnquiryFormShowHasProperty))
            {
                if (string.IsNullOrEmpty(hasProperty))
                {
                    _errorMessage.Add("<hasProperty> is required.");
                    _isValid = false;
                }
                else
                {
                    if (!Boolean.TryParse(hasProperty, out _hasProperty))
                    {
                        _errorMessage.Add("<hasProperty> is not valid, only \"true\" or \"false\" accepted.");
                        _isValid = false;
                    }

                }
            }


            if ((platform == "mobile" && c.WebEnquiryFormShowHasProperty_M) || (platform == "desktop" && c.WebEnquiryFormShowHasProperty))
            {
                if (!Boolean.TryParse(haveDeposit, out _haveDeposit))
                {
                    _errorMessage.Add("<haveDeposit> is not valid, only \"true\" or \"false\" or null accepted.");
                    _isValid = false;
                }
            }


            //if ((platform == "mobile" && c.WebEnquiryFormShowHasProperty_M) || (platform == "desktop" && c.WebEnquiryFormShowHasProperty))
            //{
            //    if (!Boolean.TryParse(UserHasDefaults, out _UserHasDefaults))
            //    {
            //        _errorMessage.Add("<UserHasDefaults> is not valid, only \"true\" or \"false\" or null accepted.");
            //        _isValid = false;
            //    }
            //}

            //if ((platform == "mobile" && c.WebEnquiryFormShowHasProperty_M) || (platform == "desktop" && c.WebEnquiryFormShowHasProperty))
            //{
            //    if (!Boolean.TryParse(UserLoanOver7K, out _UserLoanOver7K))
            //    {
            //        _errorMessage.Add("<UserLoanOver7K> is not valid, only \"true\" or \"false\" or null accepted.");
            //        _isValid = false;
            //    }
            //}




            if ((platform == "mobile" && c.WebEnquiryFormShowHasProperty_M) || (platform == "desktop" && c.WebEnquiryFormShowHasProperty))
            {
                if (string.IsNullOrEmpty(realEstateValue))
                {
                    _errorMessage.Add("<realEstateValue> is required.");
                    _isValid = false;
                }
                else
                {
                    if (!Decimal.TryParse(realEstateValue, out _realEstateValue))
                    {
                        _errorMessage.Add("<realEstateValue> is not valid.");
                        _isValid = false;
                    }
                }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowHasProperty_M) || (platform == "desktop" && c.WebEnquiryFormShowHasProperty))
            {
                if (string.IsNullOrEmpty(balanceOwing))
                {
                    _errorMessage.Add("<balanceOwing> is required.");
                    _isValid = false;
                }
                else
                {
                    if (!Decimal.TryParse(balanceOwing, out _balanceOwing))
                    {
                        _errorMessage.Add("<balanceOwing> is not valid.");
                        _isValid = false;
                    }
                }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowMobile_M) || (platform == "desktop" && c.WebEnquiryFormShowMobile))
            {
                if (string.IsNullOrEmpty(mobileNumber))
                {
                    _errorMessage.Add("<mobileNumber> is required.");
                    _isValid = false;
                }
                else
                {
                    //cleaning up...
                    mobileNumber = mobileNumber.Replace(" ", string.Empty);
                    mobileNumber = mobileNumber.Replace("-", string.Empty);

                    //checking length
                    if (!(mobileNumber.Length == 10))
                    {
                        _errorMessage.Add("<mobileNumber> must have 10 digits.");
                        _isValid = false;
                    }

                    if (!Utils.IsDigitsOnly(mobileNumber))
                    {
                        _errorMessage.Add("<mobileNumber> contains invalid charaters. Only digits (numbers) are accepted.");
                        _isValid = false;
                    }

                    if (!(Utils.Left(mobileNumber, 2) == "04"))
                    {
                        _errorMessage.Add("<mobileNumber> must start with an '04'.");
                        _isValid = false;
                    }

                }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowEmail_M) || (platform == "desktop" && c.WebEnquiryFormShowEmail))
            {
                if (string.IsNullOrEmpty(emailAddress))
                {
                    _errorMessage.Add("<emailAddress> is required.");
                    _isValid = false;
                }
                else
                    if (!Utils.IsValidEmail(emailAddress))
                    {
                        _errorMessage.Add("<emailAddress> is invalid. ");
                        _isValid = false;
                    }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowSuburb_M) || (platform == "desktop" && c.WebEnquiryFormShowSuburb))
            {
                if (string.IsNullOrEmpty(suburb))
                {
                    _errorMessage.Add("<suburb> is required.");
                    _isValid = false;
                }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowStatePostCode_M) || (platform == "desktop" && c.WebEnquiryFormShowStatePostCode))
            {
                if (string.IsNullOrEmpty(state))
                {
                    _errorMessage.Add("<state> is required.");
                    _isValid = false;
                }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowStatePostCode_M) || (platform == "desktop" && c.WebEnquiryFormShowStatePostCode))
            {
                if (string.IsNullOrEmpty(postCode))
                {
                    _errorMessage.Add("<postCode> is required.");
                    _isValid = false;
                }
            }

            if ((platform == "mobile" && c.WebEnquiryFormShowWhereDidYouHear_M) || (platform == "desktop" && c.WebEnquiryFormShowWhereDidYouHear))
            {
                if (string.IsNullOrEmpty(referral))
                {
                    _errorMessage.Add("<referral> is required.");
                    _isValid = false;
                }
            }
        }

        public void AddComment()
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("spAddCommentToLeadFromWebAPI", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("CallID", CallID));
                cmd.Parameters.Add(new SqlParameter("Notes", comments));
                conn.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    _errorMessage.Add(ex.Message);
                }
            }
        }

        public void AnswerQuestionnaire(int questionID, int answerID, string reference = null, string additionalInfo = null)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("spAnswerQuestionnaireFromWebAPI", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("CallID", CallID));
                cmd.Parameters.Add(new SqlParameter("Reference", reference));
                cmd.Parameters.Add(new SqlParameter("QuestionID", questionID));
                cmd.Parameters.Add(new SqlParameter("AnswerID", answerID));
                cmd.Parameters.Add(new SqlParameter("AdditionalInfo", additionalInfo));
                conn.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    _errorMessage.Add(ex.Message);
                }
            }
        }
    }
}