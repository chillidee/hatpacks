﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationForms.Models
{
    public class Questionnaire
    {
        public String callID { get; set; }
        public String reference { get; set; }
        public String questionID { get; set; }
        public String answerID { get; set; }
        public String additionalInfo { get; set; }
    }
}