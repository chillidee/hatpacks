﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationForms.Models
{
  static class CCCreditCard
  {
    public static object[,] CreditCardType
    {
      get
      {
        return new object[,] { { "Visa", 0.0125 }, { "Mastercard", 0.0125 }, { "AMEX", 0.0125 }, { "Diners", 0.0125 } };
      }
    }
    public static decimal Surcharge(string CCType)
    {
      decimal s;
      switch (CCType)
      {
        case "Visa":
        case "Mastercard":
          s = 0.0125m;
          break;
        case "Diners":
        case "AMEX":
          s = 0.0125m;
          break;
        default:
          s = 0;
          break;
      }
      return s;
    }
  }
}
