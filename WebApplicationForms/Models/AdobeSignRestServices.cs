﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace WebApplicationForms.Models
{
  public class AdobeSignRestServices
  {
   const string abodeSignAppId = "CBJCHBCAABAABpBoGa4xsfXcIGN3hVQB3obXguWlLtWC";
   const string abodeSignClientSerect = "NMKPNBF998c9rt2U2pbyqRLl9fxYD29a";
   const string redirectUri = "https://application-build.hatpacks.com.au/";
   const string authToken = "3AAABLblqZhB2JD_I0Bz0QOy7xcjFZ6h08lkou2JVmSS2HO_x5e4TQacuQsHgWwl27w0hh_-dQmhLV8pf0TYbQ-aFytxFOrpF";
  private string html = string.Empty;

   private string getAbodeSignAppId()
   {
     return abodeSignAppId;
   }

   private string getAbodeSignClientSerect()
   {
     return abodeSignClientSerect;
   }
   private string getRedirectUri()
   {
     return redirectUri;
   }

   private string makeURLString()
   {
     return "https://secure.na1.echosign.com/public/oauth?redirect_uri=" + getRedirectUri() + "&response_type=code&client_id=" + getAbodeSignAppId() + "&scope=widget_write:account";
   }

   public void refreshToken()
   {

     var client = new RestClient("https://api.na1.echosign.com");
     var request = new RestRequest("/oauth/refresh", Method.POST);
     request.AddHeader("grant_type", "refresh_token");
     request.AddHeader("client_id", abodeSignAppId);
     request.AddHeader("client_secret", abodeSignClientSerect);
     request.AddHeader("refresh_token", authToken);
     request.AddParameter("grant_type", "refresh_token");
     request.AddParameter("client_id", abodeSignAppId);
     request.AddParameter("client_secret", abodeSignClientSerect);
     request.AddParameter("refresh_token", authToken);

     IRestResponse response = client.Execute(request);
     setHtmlString( response.Content);
   }

   public void getAuthCode() {
     HttpWebRequest request = (HttpWebRequest)WebRequest.Create(makeURLString());
     request.AutomaticDecompression = DecompressionMethods.GZip;

     using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
     using (Stream stream = response.GetResponseStream())
     using (StreamReader reader = new StreamReader(stream))
     {
       html = reader.ReadToEnd();
     }

   }

   public void transientDocuments()
   {
     var client = new RestClient("https://api.na1.echosign.com");
     var request = new RestRequest("/oauth/refresh", Method.POST);
     request.AddParameter("Access-Token", authToken);
     IRestResponse response = client.Execute(request);
     setHtmlString(response.Content);
   }

   public string getHtmlString()
   {
     return html;
   }

   public void setHtmlString(string text)
   {
      html = text;
   }

  }
}