﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplicationForms.Models
{
  public class EmailAgent
  {
    public string Body { get; set; }
    public Boolean Sent = false;
    public string CCEmailAddressSent;

    public void SendWebApplicationFormEmail(MMApplication a)
    {
      a.LoadApplicants();
      a.LoadCompany();
     
      try
      {
          FurnishBody(a.Company.Acronym + "_" + "StartWebApplicationEmail", a);
      }
      catch (Exception error)
      {
          FurnishBody("StartWebApplicationEmail", a);
      }

      MMUser SalesManager = new MMUser(a.SalesManager);
      MailMessage m = new MailMessage();
      string emailDomain = a.Company.WebsiteURL.Replace("www.", "");
      MailAddress emailFrom;

      if (string.IsNullOrEmpty(emailDomain)) { emailDomain = "hatpacks.com.au"; }
      
      //m.From = new MailAddress("do-not-reply@" + emailDomain);
      if (!(string.IsNullOrEmpty(SalesManager.Firstname)) && !(string.IsNullOrEmpty(SalesManager.Lastname)))
      {
        emailFrom = new MailAddress(SalesManager.Firstname.ToLower() + "@" + emailDomain, SalesManager.Firstname + " " + SalesManager.Lastname);
        m.From = emailFrom;
        m.CC.Add(emailFrom);
        CCEmailAddressSent = emailFrom.Address.ToString();
      }
      else throw new System.Exception("Firstname/Surname of Sales Manager not specified in Money Maker.");

      //m.To.Add(new MailAddress("kelvin@australianlendingcentre.com.au"));
      m.To.Add(new MailAddress(a.MMApplicant1.Email, a.MMApplicant1.FirstName + " " + a.MMApplicant1.Surname));
      //if (!string.IsNullOrEmpty(SalesManager.Email)) m.CC.Add(new MailAddress(SalesManager.Email, a.SalesManager));
      //m.Bcc.Add(new MailAddress("kelvin@australianlendingcentre.com.au"));
      m.Subject = a.MMApplicant1.FirstName + ", start your Loan Application here";
      m.IsBodyHtml = true;
      m.Body = Body;

      SmtpClient s = new SmtpClient("localhost");
      try
      {
        s.Send(m);
        Sent = true;
        a.Web_InvitationEmailSent = DateTime.Now;
        a.Save();
      }
      catch (Exception ex)
      {
        Sent = false;
        throw ex;
      }
    }
    public void SendThankYouEmail(MMApplication a)
    {
      a.LoadApplicants();
      a.LoadCompany();

      FurnishBody("WebApplicationFinishThankYouEmail", a);

      MMUser SalesManager = new MMUser(a.SalesManager);
      MailMessage m = new MailMessage();
      MailAddress emailFrom;

      string emailDomain = a.Company.WebsiteURL.Replace("www.", "");
      if (string.IsNullOrEmpty(emailDomain)) { emailDomain = "hatpacks.com.au"; }
      //m.From = new MailAddress("do-not-reply@" + emailDomain);
      if (!(string.IsNullOrEmpty(SalesManager.Firstname)) && !(string.IsNullOrEmpty(SalesManager.Lastname)))
      {
        emailFrom = new MailAddress(SalesManager.Firstname.ToLower() + "@" + emailDomain, SalesManager.Firstname + " " + SalesManager.Lastname);
        m.From = emailFrom;
        m.CC.Add(emailFrom);
      }
      else throw new System.Exception("Firstname/Surname of Sales Manager not specified in Money Maker.");
      m.To.Add(new MailAddress(a.MMApplicant1.Email, a.MMApplicant1.FirstName + " " + a.MMApplicant1.Surname));
      if (!string.IsNullOrEmpty(SalesManager.Email)) m.CC.Add(new MailAddress(SalesManager.Email, a.SalesManager));
      //m.Bcc.Add(new MailAddress("kelvin@australianlendingcentre.com.au"));
      m.Subject = "Thank you";
      m.IsBodyHtml = true;
      m.Body = Body;

      SmtpClient s = new SmtpClient("localhost");
      try
      {
        s.Send(m);
        Sent = true;        
        //a.Web_InvitationEmailSent = DateTime.Now;
        a.Save();
      }
      catch (Exception ex)
      {
        Sent = false;
        throw ex;
      }
    }
    private void FurnishBody(string htmlFileLocation, MMApplication a)
    {
      string path = System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"];
      if (path == "")
      {
        path = HttpContext.Current.Request.PhysicalApplicationPath + "/" + htmlFileLocation;
      }
      else
      {
        path = path + "/" + htmlFileLocation;
      }

      StreamReader reader = new StreamReader(path + "/index.html");
      Body = reader.ReadToEnd();
      reader.Dispose();
      string host = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host
        + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);
      Body = Body.Replace("[Firstname]", a.MMApplicant1.FirstName);
      Body = Body.Replace("[Date]", DateTime.Now.ToString("dd-MMM-yyyy"));
      Body = Body.Replace("[Year]", DateTime.Now.ToString("yyyy"));
      Body = Body.Replace("[CompanyName]", a.Company.Company);
      Body = Body.Replace("[CompanyABN]", a.Company.ABN);
      Body = Body.Replace("[CompanyPhone]", a.Company.Phone);
      Body = Body.Replace("[CompanyFax]", a.Company.Fax);
      Body = Body.Replace("[CompanyEmail]", a.Company.Email);
      Body = Body.Replace("[CompanyAccountName]", a.Company.AccountName);
      Body = Body.Replace("[CompanyAccountBSB]", a.Company.AccountBSB);
      Body = Body.Replace("[CompanyAccountNo]", a.Company.AccountNo);
      Body = Body.Replace("[CompanyCreditLicense]", a.Company.CreditLicNo);
      Body = Body.Replace("[WebApplicationLink]", host + "/default.aspx?ID=" + a.GUID);
      Body = Body.Replace("[FaceBookURL]", "http://" + a.Company.FacebookURL.Replace("http://", ""));
      Body = Body.Replace("[TwitterURL]", "http://" + a.Company.TwitterURL.Replace("http://", ""));
      Body = Body.Replace("[YouTubeURL]", "http://" + a.Company.YouTubeURL.Replace("http://", ""));
      Body = Body.Replace("[WebsiteURL]", "http://" + a.Company.WebsiteURL.Replace("http://", ""));
      Body = Body.Replace("[AboutUsURL]", "http://" + a.Company.AboutUsURL.Replace("http://", ""));
      Body = Body.Replace("[ContactUsURL]", "http://" + a.Company.ContactUsURL.Replace("http://", ""));
      Body = Body.Replace("[LinkedInURL]", "http://" + a.Company.LinkedInURL.Replace("http://", ""));
      Body = Body.Replace("[Logo]", host + "/image_logos/" + a.Company.WebApplicationLogoImageFilename);
      Body = Body.Replace("[SalesManager]", a.SalesManager);
      Body = Body.Replace("src=\"images", "src=\"" + host + "/Emails/" + htmlFileLocation + "/images");
      Body = Body.Replace("src=\"Images", "src=\"" + host + "/Emails/" + htmlFileLocation + "/images");

    }

    public void SendWebApplicationFormEmailCC(CCApplicant cc)
    {      
      cc.LoadCompany();

      FurnishBodyCC("StartWebApplicationEmailCC", cc);

      MMUser ApplicationManager = new MMUser(cc.ApplicationManager);
      MailMessage m = new MailMessage();
      MailAddress emailFrom;

      string emailDomain = cc.Company.WebsiteURL.Replace("www.", "");
      if (string.IsNullOrEmpty(emailDomain)) { emailDomain = "hatpacks.com.au"; }


      //m.From = new MailAddress("do-not-reply@" + emailDomain);
      if (!(string.IsNullOrEmpty(ApplicationManager.Firstname)) && !(string.IsNullOrEmpty(ApplicationManager.Lastname)))
      {
        emailFrom = new MailAddress(ApplicationManager.Firstname.ToLower() + "@" + emailDomain, "Clean Credit");
        m.From = emailFrom;
        m.CC.Add(emailFrom);
      }

      //m.To.Add(new MailAddress("kelvin@australianlendingcentre.com.au"));
      m.To.Add(new MailAddress(cc.Email, cc.FirstName + " " + cc.Surname));
      //if (!string.IsNullOrEmpty(ApplicationManager.Email)) m.CC.Add(new MailAddress(ApplicationManager.Email, cc.ApplicationManager));

      //m.Bcc.Add(new MailAddress("kelvin@australianlendingcentre.com.au"));
      m.Subject = "Credit Repair in 3 Easy Steps";
      m.IsBodyHtml = true;
      m.Body = Body;

      SmtpClient s = new SmtpClient("localhost");
      try
      {
        s.Send(m);
        Sent = true;
        cc.WebApplicationSent = DateTime.Now;        
        cc.Save();
      }
      catch (Exception ex)
      {
        Sent = false;
        throw ex;
      }
    }
    private void FurnishBodyCC(string htmlFileLocation, CCApplicant cc)
    {
      string path = System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"];
      if (path == "")
      {
        path = HttpContext.Current.Request.PhysicalApplicationPath + "/" + htmlFileLocation;
      }
      else
      {
        path = path + "/" + htmlFileLocation;
      }

      StreamReader reader = new StreamReader(path + "/index.html");
      Body = reader.ReadToEnd();
      reader.Dispose();
      string host = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host
        + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);
      Body = Body.Replace("[Firstname]", cc.FirstName);
      Body = Body.Replace("[Date]", DateTime.Now.ToString("dd-MMM-yyyy"));
      Body = Body.Replace("[Year]", DateTime.Now.ToString("yyyy"));
      Body = Body.Replace("[CompanyName]", cc.Company.Company);
      Body = Body.Replace("[CompanyABN]", cc.Company.ABN);
      Body = Body.Replace("[CompanyPhone]", cc.Company.Phone);
      Body = Body.Replace("[CompanyFax]", cc.Company.Fax);
      Body = Body.Replace("[CompanyEmail]", cc.Company.Email);
      Body = Body.Replace("[CompanyAccountName]", cc.Company.AccountName);
      Body = Body.Replace("[CompanyAccountBSB]", cc.Company.AccountBSB);
      Body = Body.Replace("[CompanyAccountNo]", cc.Company.AccountNo);
      Body = Body.Replace("[CompanyCreditLicense]", cc.Company.CreditLicNo);
      Body = Body.Replace("[WebApplicationLink]", host + "/CleanCredit/CleanCreditDefault.aspx?ID=" + cc.GUID);
      Body = Body.Replace("[FaceBookURL]", "http://" + cc.Company.FacebookURL.Replace("http://", ""));
      Body = Body.Replace("[TwitterURL]", "http://" + cc.Company.TwitterURL.Replace("http://", ""));
      Body = Body.Replace("[YouTubeURL]", "http://" + cc.Company.YouTubeURL.Replace("http://", ""));
      Body = Body.Replace("[WebsiteURL]", "http://" + cc.Company.WebsiteURL.Replace("http://", ""));
      Body = Body.Replace("[AboutUsURL]", "http://" + cc.Company.AboutUsURL.Replace("http://", ""));
      Body = Body.Replace("[ContactUsURL]", "http://" + cc.Company.ContactUsURL.Replace("http://", ""));
      Body = Body.Replace("[LinkedInURL]", "http://" + cc.Company.LinkedInURL.Replace("http://", ""));
      Body = Body.Replace("[Logo]", host + "/image_logos/" + cc.Company.WebApplicationLogoImageFilename);
      Body = Body.Replace("[SalesManager]", cc.ApplicationManager);
      Body = Body.Replace("src=\"images", "src=\"" + host + "/Emails/" + htmlFileLocation + "/images");
      Body = Body.Replace("src=\"Images", "src=\"" + host + "/Emails/" + htmlFileLocation + "/images");
      Body = Body.Replace("[ReferenceNo]", cc.ApplicantID.ToString());

    }
    public void SendThankYouEmailCC(CCApplicant cc)
    {     
      cc.LoadCompany();

      FurnishBodyCC("WebApplicationFinishThankYouEmailCC", cc);

      MMUser ApplicationManager = new MMUser(cc.ApplicationManager);
      MailMessage m = new MailMessage();
      MailAddress emailFrom;

      string emailDomain = cc.Company.WebsiteURL.Replace("www.", "");
      if (string.IsNullOrEmpty(emailDomain)) { emailDomain = "cleancredit.com.au"; }
      
      //m.From = new MailAddress("do-not-reply@" + emailDomain);
      if (!(string.IsNullOrEmpty(ApplicationManager.Firstname)) && !(string.IsNullOrEmpty(ApplicationManager.Lastname)))
      {
        emailFrom = new MailAddress(ApplicationManager.Firstname.ToLower() + "@" + emailDomain, ApplicationManager.Firstname + " " + ApplicationManager.Lastname);
        m.From = emailFrom;
        m.CC.Add(emailFrom);
      }
      m.To.Add(new MailAddress(cc.Email, cc.FirstName + " " + cc.Surname));
      //if (!string.IsNullOrEmpty(ApplicationManager.Email)) m.CC.Add(new MailAddress(ApplicationManager.Email, cc.ApplicationManager));
      //m.Bcc.Add(new MailAddress("kelvin@australianlendingcentre.com.au"));
      m.Subject = "Thank you";
      m.IsBodyHtml = true;
      m.Body = Body;

      SmtpClient s = new SmtpClient("localhost");
      try
      {
        s.Send(m);
        Sent = true;        
        cc.Save();
      }
      catch (Exception ex)
      {
        Sent = false;
        throw ex;
      }
    }

    public void SendThankYouEmail_WebEnquiry(Int32 CallId)
    {

      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand("SELECT * FROM [Call] WHERE CallID = '" + CallId.ToString() + "'", conn);
        conn.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {
          Int32 CompanyID = (dr["CompanyID"] as Int32?) ?? 0;
          MMCompany c;
          if (CompanyID == 0)
            c = new MMCompany("ALC");
          else
            c = new MMCompany(CompanyID);
          string Surname = Convert.ToString(dr["Surname"]);
          string FirstName = Convert.ToString(dr["FirstName"]);
          string EmailAddress = Convert.ToString(dr["Email"]);

          FurnishBodyCall(c.Acronym + "_WebEnquiryForm_ThankYou", CallId);

          MailMessage m = new MailMessage();
          MailAddress emailFrom;

          string emailDomain = c.WebsiteURL.Replace("www.", "");
          if (string.IsNullOrEmpty(emailDomain)) { emailDomain = "hatpacks.com.au"; }
          //m.From = new MailAddress("do-not-reply@" + emailDomain);

          emailFrom = new MailAddress(c.Email);
          m.From = emailFrom;
          //m.CC.Add(emailFrom);

          m.To.Add(new MailAddress(EmailAddress, FirstName + " " + Surname));
          //m.Bcc.Add(new MailAddress("kelvin@australianlendingcentre.com.au"));
          m.Subject = "Thank you";
          m.IsBodyHtml = true;
          m.Body = Body;

          SmtpClient s = new SmtpClient("localhost");
          try
          {
            s.Send(m);
            Sent = true;
            //a.Web_InvitationEmailSent = DateTime.Now;

          }
          catch (Exception ex)
          {
            Sent = false;
            throw ex;
          }
        }
      }
    }


    private void FurnishBodyCall(string htmlFileLocation, Int32 CallID)
    {
      string path = System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"];
      if (path == "")
      {
        path = HttpContext.Current.Request.PhysicalApplicationPath + "/" + htmlFileLocation;
      }
      else
      {
        path = path + "/" + htmlFileLocation;
      }

      StreamReader reader = new StreamReader(path + "/index.html");
      Body = reader.ReadToEnd();
      reader.Dispose();
      string host = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host
        + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);


      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand("SELECT * FROM [Call] WHERE CallID = '" + CallID.ToString() + "'", conn);
        conn.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {
          Int32 CompanyID = (dr["CompanyID"] as Int32?) ?? 0;
          MMCompany c;
          if (CompanyID == 0) 
            c = new MMCompany("ALC");
          else
            c = new MMCompany(CompanyID);
          
          Body = Body.Replace("[Firstname]", dr["Firstname"].ToString());
          Body = Body.Replace("[Date]", DateTime.Now.ToString("dd-MMM-yyyy"));
          Body = Body.Replace("[Year]", DateTime.Now.ToString("yyyy"));
          Body = Body.Replace("[CompanyName]", c.Company);
          Body = Body.Replace("[CompanyABN]", c.ABN);
          Body = Body.Replace("[CompanyPhone]", c.Phone);
          Body = Body.Replace("[CompanyFax]", c.Fax);
          Body = Body.Replace("[CompanyEmail]", c.Email);
          Body = Body.Replace("[CompanyAccountName]", c.AccountName);
          Body = Body.Replace("[CompanyAccountBSB]", c.AccountBSB);
          Body = Body.Replace("[CompanyAccountNo]", c.AccountNo);
          Body = Body.Replace("[CompanyCreditLicense]", c.CreditLicNo);          
          Body = Body.Replace("[FaceBookURL]", "http://" + c.FacebookURL.Replace("http://", ""));
          Body = Body.Replace("[TwitterURL]", "http://" + c.TwitterURL.Replace("http://", ""));
          Body = Body.Replace("[YouTubeURL]", "http://" + c.YouTubeURL.Replace("http://", ""));
          Body = Body.Replace("[WebsiteURL]", "http://" + c.WebsiteURL.Replace("http://", ""));
          Body = Body.Replace("[AboutUsURL]", "http://" + c.AboutUsURL.Replace("http://", ""));
          Body = Body.Replace("[ContactUsURL]", "http://" + c.ContactUsURL.Replace("http://", ""));
          Body = Body.Replace("[LinkedInURL]", "http://" + c.LinkedInURL.Replace("http://", ""));
          Body = Body.Replace("[Logo]", host + "/image_logos/" + c.WebApplicationLogoImageFilename);          
          Body = Body.Replace("src=\"images", "src=\"" + host + "/Emails/" + htmlFileLocation + "/images");
          Body = Body.Replace("src=\"Images", "src=\"" + host + "/Emails/" + htmlFileLocation + "/images");
          Body = Body.Replace("[is-app]", "true");
          Body = Body.Replace("[guid]", dr["GUID"].ToString());
        }
      }
    }
  }
}