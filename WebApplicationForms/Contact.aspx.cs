﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms
{
	public partial class Contact : Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
        if (Session["Application"] != null)
        {
          MMApplication a = (MMApplication)Session["Application"];
          a.LoadCompany();
          lblSalesManager.Text = a.SalesManager;
          lblPhoneNumber.Text = a.Company.Phone;
          hlEmail.NavigateUrl = "mailto:" + a.Company.Email;
          hlEmail.Text = a.Company.Email;
          lblAddressLine1.Text = a.Company.AddressLine1;
          lblSuburb.Text = a.Company.Suburb;
          lblPostCode.Text = a.Company.PostCode;
          lblState.Text = a.Company.State;
        }
        else if (Session["CCApplicant"] != null)
        {
          CCApplicant cc = (CCApplicant)Session["CCApplicant"];
          cc.LoadCompany();
          lblSalesManager.Text = cc.ApplicationManager;
          lblPhoneNumber.Text = cc.Company.Phone;
          hlEmail.NavigateUrl = "mailto:" + cc.Company.Email;
          hlEmail.Text = cc.Company.Email;
          lblAddressLine1.Text = cc.Company.AddressLine1;
          lblSuburb.Text = cc.Company.Suburb;
          lblPostCode.Text = cc.Company.PostCode;
          lblState.Text = cc.Company.State;
        }
        else if (Session["Company"] != null)
        {
          MMCompany c = (MMCompany)Session["Company"];
          lblSalesManager.Text = "n/a";
          lblPhoneNumber.Text = c.Phone;
          hlEmail.NavigateUrl = "mailto:" + c.Email;
          hlEmail.Text = c.Email;
          lblAddressLine1.Text = c.AddressLine1;
          lblSuburb.Text = c.Suburb;
          lblPostCode.Text = c.PostCode;
          lblState.Text = c.State;
        }
			}
		}
	}
}