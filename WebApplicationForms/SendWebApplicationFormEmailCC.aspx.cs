﻿using EchosignRESTClient.Models;
using EchosignRESTClient.Parameters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Web;
using WebApplicationForms.Models;
using System.Linq;

namespace WebApplicationForms
{
    public partial class SendWebApplicationFormEmailCC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AdobeEchoSign();
        }

        private void AdobeEchoSign()
        {
            string GUID = Request.QueryString["GUID"];
            CCApplicant cc = new CCApplicant(GUID);

            Session["CCApplicant"] = cc;
            //checking client's email...
            if (string.IsNullOrEmpty(cc.Email))
            {
                literal.Text = "Recipient's email is empty. Unable to send.";
            }
            else
            {
                /*EmailAgent ea = new EmailAgent();
                ea.SendWebApplicationFormEmailCC(cc);
                //Response.Write(ea.Body);
                if (ea.Sent)*/

                MMUser ApplicationManager = new MMUser(cc.ApplicationManager);

                // ClientApp and JsonData location
                String clientAppPath = Path.Combine(HttpRuntime.AppDomainAppPath, "AdobeEchoSignClient");
                String jsonDataPath = Path.Combine(clientAppPath, "JsonDataParameter");

                // Data for Removal Fees
                string removalFee = "";
                string removalFeeNo = "";
                List<RemovalFees> removalfee = cc.GetRemovalFees(cc.GUID);

                if (removalfee != null && removalfee.Count > 0)
                {
                    removalFeeNo = removalfee.Count.ToString();
                    removalFee = String.Format("{0:C}", removalfee.Sum(item => item.RemovalFee));
                }

                //Prepare data
                JsonData jsonData = new JsonData();
                jsonData.companyID = cc.CompanyID.Value;
                jsonData.ccs = new List<string> { ApplicationManager.Email };
                jsonData.memberInfos = new List<ParticipantInfo>() 
                { 
                    new ParticipantInfo() { email = cc.Email },                    
                };
                jsonData.mergeFieldInfo = new List<MergefieldInfo>()
                {
                    new MergefieldInfo() { defaultValue = cc.FirstName + " " + cc.Surname, fieldName = "customerName" },
                    new MergefieldInfo() { defaultValue = cc.ApplicantID.ToString(), fieldName = "customerReference" },
                    new MergefieldInfo() { defaultValue = cc.Surname, fieldName = "customerSurname" },
                    new MergefieldInfo() { defaultValue = cc.FirstName, fieldName = "customerGivenName" },
                    new MergefieldInfo() { defaultValue = cc.Mobile, fieldName = "customerMobile" },
                    new MergefieldInfo() { defaultValue = cc.FirstName + " " + cc.Surname, fieldName = "accountHolderName" },
                    new MergefieldInfo() { defaultValue = String.Format("{0:C}", cc.ApplicationFee + cc.AdditionalFee), fieldName = "applicationFee" },
                    new MergefieldInfo() { defaultValue = removalFee, fieldName = "removalFee" },
                    new MergefieldInfo() { defaultValue = removalFeeNo, fieldName = "removalFeeNo" },
                    new MergefieldInfo() { defaultValue = cc.JudgementFee > 0 ? String.Format("{0:C}", cc.JudgementFee) : "", fieldName = "judgementFee" }
                };

                //Just in case of not existing the json param repository
                if (!Directory.Exists(jsonDataPath))
                    Directory.CreateDirectory(jsonDataPath);

                //Create file named in this format "JsonDataParameter/yyyyMMdd_HHmmss_ApplicantID_GUID.json"
                String jsonDataFilePath = Path.Combine(jsonDataPath, string.Format("{0}_{1}_{2}.json", DateTime.Now.ToString("yyyyMMdd_HHmmss"), cc.ApplicantID, GUID));

                using (StreamWriter sw = new StreamWriter(jsonDataFilePath))
                {
                    sw.Write(JsonConvert.SerializeObject(jsonData));
                }

                //Call the ClientApp that consumes Adobe Echo Sign API - necessary to inform action & json param file
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = Path.Combine(clientAppPath, "ClientApp.exe"); //The source code of this app is in Bitbucket with the others websites
                startInfo.Arguments = string.Format("action={0} json=\"{1}\"", "SendAgreement", jsonDataFilePath);
                Process.Start(startInfo);

                if (true) //The ClientApp has to return something to confirm that those emails were sent
                {
                    literal.Text = "Email sent to " + cc.FirstName + " " + cc.Surname + ".<br/>Email address: " + cc.Email + "<br/><br/>" +
                                    "A copy (by CC) was also sent to the Application Manager " + cc.ApplicationManager + " (" + ApplicationManager.Email + ").";
                }
            }
        }
    }
}