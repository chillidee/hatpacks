﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms.DebtQuestions
{
  public partial class UnableToAssist : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Master.DisplayGoogleAdwordsConversionScript = false;

      MMCompany c = (MMCompany)(Session["Company"] ?? new MMCompany("ALC"));
      if (!String.IsNullOrEmpty(c.WebEnquiryFormFailureURL))
      {
        Session["redirect"] = "http://" + c.WebEnquiryFormFailureURL;
        Response.Redirect("/EnquiryForm/redirect.aspx");
      }
    }
  }
}