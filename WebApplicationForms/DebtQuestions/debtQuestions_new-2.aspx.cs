﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Telerik.Web.UI;
using System.Configuration;

namespace WebApplicationForms.DebtQuestions
{
  public partial class debtQuestions_new_2 : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      //testing only!!!
      //Session["bid"] = (Int32)206659;
      //Session["callID"] = (Int32)206659;
      
      Master.DisplayGoogleAdwordsConversionScript = false;

      if (!IsPostBack && Session["callID"] != null)
      {
        rgQualifierDADebt.MasterTableView.FilterExpression = "CallID = " + Session["callID"].ToString();
        GridBoundColumn gbc2 = (GridBoundColumn)rgQualifierDADebt.MasterTableView.GetColumn("CallID");
        gbc2.DefaultInsertValue = Session["callID"].ToString();
      }
    }
    protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
      e.Command.Parameters["@CallID"].Value = Session["callID"].ToString();
    }

    protected void rgQualifierDADebt_ItemCommand(object sender, GridCommandEventArgs e)
    {
      
      if (e.CommandName == RadGrid.CancelCommandName && e.Item.IsInEditMode)
        Session["editMode"] = null;
      if (e.CommandName == RadGrid.UpdateCommandName) Session["editmode"] = null;
      if ((e.CommandName == RadGrid.EditCommandName) || (e.CommandName == RadGrid.InitInsertCommandName))
      {
        if (Session["editMode"] != null)
          e.Canceled = true;
        else
          Session["editMode"] = "edit";
      }
    }

    protected void rgQualifierDADebt_PreRender(object sender, EventArgs e)
    {
      rgQualifierDADebt.MasterTableView.IsItemInserted = true; // Check for the !IsPostBack also for initial page load
      //Session["editMode"] = "edit";
      rgQualifierDADebt.Rebind();
    }

    protected void SqlDataSource1_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      if (IsValid)
      {
        if (Session["editMode"] != null)
        {
          // RadWindowManager1.RadAlert("Please \"save\" or \"cancel\" first.", 330, 180, "Save or Cancel", "");         
          foreach (GridItem i in rgQualifierDADebt.Items)
          {
            if (i.IsInEditMode)
            {                            
               i.FireCommandEvent("Update", ""); 
            }            
          }
        }
        try
        {
          GridItem ii = (GridItem)rgQualifierDADebt.MasterTableView.GetInsertItem();
          RadNumericTextBox rtbCreditLimit = (RadNumericTextBox)ii.FindControl("rtbCreditLimit");
          RadNumericTextBox rtbTotalAmountOwing = (RadNumericTextBox)ii.FindControl("rtbTotalAmountOwing");
          if ((!(rtbCreditLimit.Text == "0")) || (!(rtbTotalAmountOwing.Text == "0")))
          {
            rgQualifierDADebt.MasterTableView.GetInsertItem().FireCommandEvent("PerformInsert", "");
          }
        }
        catch { }

        //need to check, if the total amount of unsecured debt is >0, then atleast 1 item is required.
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
        {
          string sql = "SELECT QualifierID FROM QualifierDADebt WHERE CallID = " + Session["callID"].ToString();
          SqlCommand cmd = new SqlCommand(sql, conn);
          conn.Open();
          SqlDataReader dr = cmd.ExecuteReader();
          if (!dr.Read())
          {
            lblMsg.Visible = true;
            lblMsg.Text = "Please fill in at least 1 debt item.";
          }
          else
          {
            Response.Redirect("debtQuestions_new-3.aspx");
          }
        }
      }      
    }
  }
}
