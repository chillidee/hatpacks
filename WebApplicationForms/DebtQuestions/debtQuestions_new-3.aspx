﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="debtQuestions_new-3.aspx.cs" Inherits="WebApplicationForms.DebtQuestions.debtQuestions_new_3" %>
<%@ MasterType virtualpath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
  <section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
        <h1 runat="server" id="h1">Home stretch...the last page.</h1>        
      </hgroup>
    </div>
  </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  
  <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
  <style>
  td.data {
    padding-top:40px;
    vertical-align:top;    
  }
</style>
<table style="width:100%">
  <tr>
    <td>
      <h5>Are you 21 years old and over?</h5>
    </td>
    <td class="data">
      <asp:RadioButtonList ID="grpOver21" runat="server" RepeatDirection="Vertical">
        <asp:ListItem Value="1">Yes</asp:ListItem>
        <asp:ListItem Value="0">No</asp:ListItem>
      </asp:RadioButtonList>
      <asp:RequiredFieldValidator ID="rfvgrpOver21" ControlToValidate="grpOver21" runat="Server" ErrorMessage="Required." EnableClientScript="true"></asp:RequiredFieldValidator>                                      
    </td>
  </tr>
  <tr><td colspan="2"><hr /></td></tr>

    <tr>
    <td>
      <h5>Do you own a motor vehicle that is registered in your name?</h5>      
    </td>    
    <td class="data">
      <asp:RadioButtonList ID="grpOwnVehicle" runat="server" RepeatDirection="Vertical" AutoPostBack="true" OnSelectedIndexChanged="grpOwnVehicle_SelectedIndexChanged">
        <asp:ListItem Value="1">Yes</asp:ListItem>
        <asp:ListItem Value="0">No</asp:ListItem>
      </asp:RadioButtonList>
      <asp:RequiredFieldValidator ID="rfvgrpOwnVehicle" ControlToValidate="grpOwnVehicle" runat="Server" ErrorMessage="Required." EnableClientScript="true"></asp:RequiredFieldValidator>                                      
    </td>
  </tr>
  <tr><td colspan="2"><hr /></td></tr>

  <tr>
    <td style="padding-left:50px">
      <div runat="server" id="divVehicle" visible="false">
        <table style="width:100%">
          <tr>
            <td>
              <h5>Is your vehicle less than 10 years old?</h5>
            </td>
            <td class="data">
              <asp:RadioButtonList ID="grpVehicleLessThan10YearsOld" runat="server" RepeatDirection="Vertical">
                <asp:ListItem Value="1">Yes</asp:ListItem>
                <asp:ListItem Value="0">No</asp:ListItem>
              </asp:RadioButtonList>
              <asp:RequiredFieldValidator ID="rfvgrpVehicleLessThan10YearsOld" ControlToValidate="grpVehicleLessThan10YearsOld" runat="Server" ErrorMessage="Required." EnableClientScript="true"></asp:RequiredFieldValidator>                                      
            </td>
          </tr>
          <tr><td colspan="2"><hr /></td></tr>

          <tr>
            <td>
              <h5>Is your vehicle financed?</h5>
            </td>
            <td class="data">
              <asp:RadioButtonList ID="grpVehicleFinanced" runat="server" RepeatDirection="Vertical">
                <asp:ListItem Value="1">Yes</asp:ListItem>
                <asp:ListItem Value="0">No</asp:ListItem>
              </asp:RadioButtonList>
              <asp:RequiredFieldValidator ID="rfvgrpVehicleFinanced" ControlToValidate="grpVehicleFinanced" runat="Server" ErrorMessage="Required." EnableClientScript="true"></asp:RequiredFieldValidator>                                      
            </td>
          </tr>
          <tr><td colspan="2"><hr /></td></tr>
        </table>
      </div>
    </td>
  </tr>

</table>

		<div style="text-align: right">
      <a href="javascript:window.history.back()">back</a>
			<asp:Button ID="btnSubmit" Text="finish" CssClass="main" runat="server" onclick="btnSubmit_Click" />
		</div>
  <asp:Label runat="server" ID="lblDebtQuestionID" Visible="false" />
  <asp:Label runat="server" ID="lblLID" Visible="false" />

</telerik:RadAjaxPanel>
</asp:Content>

