﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using WebApplicationForms.DebtQuestions;

namespace WebApplicationForms.DebtQuestions
{
  public partial class debtQuestions_new_3 : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Master.DisplayGoogleAdwordsConversionScript = false;

      if (!IsPostBack)
      {
        try
        {
          Int32 iCallID = Convert.ToInt32(Session["callID"]);
          lblDebtQuestionID.Text = iCallID.ToString();          
        }
        catch
        {
          Session["ErrorMessage"] = "Session has timed out.";
          Response.Redirect("error.aspx");
        }
      }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      Boolean blnOver21 = (grpOver21.SelectedValue == "1");
      Boolean blnOwnVehicle = (grpOwnVehicle.SelectedValue == "1");
      Boolean blnVehicleLessThan10YearsOld = (grpVehicleLessThan10YearsOld.SelectedValue == "1");
      Boolean blnVehicleFinanced = (grpVehicleFinanced.SelectedValue == "1");

      string nl = System.Environment.NewLine;
      string notes = "";

      //buiding the notes...        
      notes += "Are you 21 years old and over? " + Utils.YN(blnOver21) + nl + nl;

      notes += "Do you own a motor vehicle that is registered in your name? " + Utils.YN(blnOwnVehicle) + nl + nl;

      notes += "Is your vehicle less than 10 years old? " + Utils.YN(blnVehicleLessThan10YearsOld) + nl + nl;

      notes += "Is your vehicle financed? " + Utils.YN(blnVehicleFinanced) + nl + nl;
      
      notes = notes.Replace("'", "''");
      string sql;

      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        sql = "update DebtQuestion SET " +
          "Over21 = '" + Convert.ToInt32(blnOver21).ToString() + "', " +
          "OwnVehicle = '" + Convert.ToInt32(blnOwnVehicle).ToString() + "', " +
          "VehicleLessThan10YearsOld = '" + Convert.ToInt32(blnVehicleLessThan10YearsOld).ToString() + "', " +
          "VehicleFinanced = '" + Convert.ToInt32(blnVehicleFinanced).ToString() + "' " +
          "WHERE CallID = " + lblDebtQuestionID.Text;
        SqlCommand cmd_u = new SqlCommand(sql, conn);
        conn.Open();
        cmd_u.ExecuteNonQuery();

        //updating Notes in Call table...
        sql = "UPDATE Call SET Notes = Cast(notes as nvarchar(max)) + '" + notes + "' WHERE CallID = " + lblDebtQuestionID.Text;
        SqlCommand cmd2 = new SqlCommand(sql, conn);
        cmd2.ExecuteNonQuery();
      }

      //WebApplicationForms.DebtQuestions.Utils.UpdateCallRecord(Convert.ToInt32(lblDebtQuestionID.Text),"CallID");


      if (CheckPass(Convert.ToInt32(lblDebtQuestionID.Text)))
      {
        Response.Redirect("complete.aspx");
      }
      else
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
        {

          //sql = "update LoanApplication set NoProductAvailable =1 where loanApplicationID ='" + lblLID.Text + "'";
          sql = "UPDATE Call SET CallStatusID = 77, Operator = 'Website', StatusStartDate = GETDATE(), StatusEnteredBy = 'Website' WHERE CallStatusID <> 77 AND CallID = " + Session["callID"].ToString();
          SqlCommand cmd_u2 = new SqlCommand(sql, conn);
          conn.Open();
          cmd_u2.ExecuteNonQuery();
          Response.Redirect("UnableToAssist.aspx");
        }
      }


    }

    protected void grpOwnVehicle_SelectedIndexChanged(object sender, EventArgs e)
    {
      divVehicle.Visible = (grpOwnVehicle.SelectedValue == "1");
    }

    private Boolean CheckPass(Int32 CallID)
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        string sql = "SELECT * FROM DebtQuestion WHERE CallID = " + CallID.ToString();
        SqlCommand cmd = new SqlCommand(sql, conn);
        conn.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.Read())
        {
          if (Convert.ToBoolean(dr["UnsecuredDebt"])) return true;
          else if (Convert.ToString(dr["EmploymentStatus"]) == "GOV" && Convert.ToBoolean(dr["Over21"]) && (Convert.ToString(dr["EmploymentDuration"]) == "3mplus")) return true;
          else if (Convert.ToString(dr["EmploymentStatus"]) == "CAS" || Convert.ToString(dr["EmploymentStatus"]) == "NW") return false;
          else if (
            Convert.ToBoolean(dr["Over21"]) &&
            (Convert.ToString(dr["EmploymentDuration"]) == "3mplus") &&
            Convert.ToBoolean(dr["OwnVehicle"]) &&
            Convert.ToBoolean(dr["VehicleLessThan10YearsOld"]) &&
            !Convert.ToBoolean(dr["VehicleFinanced"])
            ) return true;
          else return false;         
        }
        else return false;
      }
    }
  }
}