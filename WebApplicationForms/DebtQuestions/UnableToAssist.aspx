﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UnableToAssist.aspx.cs" Inherits="WebApplicationForms.DebtQuestions.UnableToAssist" %>
<%@ MasterType virtualpath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
      <section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
        <h1 runat="server" id="h1">We are sorry...we are unable to assist :-(</h1>
        <p>Unfortunately, our assessment has not resulted in a suitable solution for your current situation.</p>
      </hgroup>
    </div>
  </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>
