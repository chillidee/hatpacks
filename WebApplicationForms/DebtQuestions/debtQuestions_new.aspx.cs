﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;
using WebApplicationForms.Models;
using System.Data.SqlClient;
using WebApplicationForms.DebtQuestions;

namespace WebApplicationForms.DebtQuestions
{
  public partial class debtQuestions_new : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)   
    {
      Master.DisplayGoogleAdwordsConversionScript = false;
      if (!IsPostBack)
      {
        //checking required "callID"        
        string sCallID = Request.QueryString["callID"];
        if (sCallID == null)
        {
          Session["ErrorMessage"] = "Required IDs not provided.";
          Response.Redirect("error.aspx");
        }
        else
        {
          try
          {
            Int32 iCallID = Convert.ToInt32(sCallID);            
            Session["callID"] = iCallID;            
          }
          catch
          {
            Session["ErrorMessage"] = "Required IDs not in appropriate format.";
            Response.Redirect("error.aspx");
          }
          //we now have a valid CallID, lets load from database if it exist.
          using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
          {
            //check to see if it already exists... if so, then we are updating, else we are inserting.
            string sql = "SELECT * FROM DebtQuestion WHERE CallID = " + Request.QueryString["callID"];
            SqlCommand cmd = new SqlCommand(sql, conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
              //priming controls...
              //rtbWeekNet1st.Value = (dr["WeekNet1st"] as Double?) ?? 0;
              rtbWeekNet1st.Text = dr["WeekNet1st"].ToString();
              rbcPayFrequency.SelectedValue = dr["PayFrequency"].ToString();
              rbcEmploymentStatus.SelectedValue = dr["EmploymentStatus"].ToString();
              grpEmploymentDuration.SelectedValue = dr["EmploymentDuration"].ToString();
              grpBeenBankrupt.SelectedValue = (Convert.ToBoolean((dr["BeenBankrupt"])) ? "1" : "0");
              grpStruggling.SelectedValue = (Convert.ToBoolean((dr["Struggling"]))?"1":"0");
              grpIsHardForMinPayment.SelectedValue = (Convert.ToBoolean((dr["IsHardForMinPayment"])) ? "1" : "0");
              grpUnsecuredDebt.SelectedValue = (Convert.ToBoolean((dr["UnsecuredDebt"])) ? "1" : "0");
              rtbDebtAmount.Text = dr["OwingAmt"].ToString();

            }
          }
        }

      }


    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      if (IsValid && Session["Company"] != null)
      {
        //getting data from the form...
        Decimal decWeekNet1st = Convert.ToDecimal(rtbWeekNet1st.Value);
        String strPayFrequency = rbcPayFrequency.SelectedValue;
        String strEmploymentStatus = rbcEmploymentStatus.SelectedValue;
        String strEmploymentDuration = (grpEmploymentDuration.SelectedValue);
        Boolean blnUnsecuredDebt = (grpUnsecuredDebt.SelectedValue == "1");
        Boolean blnStruggling = (grpStruggling.SelectedValue == "1");
        Boolean blnIsHardForMinPayment = (grpIsHardForMinPayment.SelectedValue == "1");
        Boolean blnBeenBankrupt = (grpBeenBankrupt.SelectedValue == "1");
        Decimal decDebtAmount = Convert.ToDecimal(rtbDebtAmount.Value);
        String strCompanyCode = ((MMCompany)Session["Company"]).Acronym;


        //buiding the notes...        
        string nl = System.Environment.NewLine;
        string notes = nl + nl;

        notes += "What is your take home income each pay, including Government Benefits and or Pensions? $" + Convert.ToString(decWeekNet1st) + ", " + strPayFrequency + nl + nl;
        
        notes += "What is your employment status? " + strEmploymentStatus + nl + nl;

        notes += "How long have your worked at your current form of employment? " + strEmploymentDuration + nl + nl;

        notes += "Have you been bankrupt during the last 10 years? " + Utils.YN(blnBeenBankrupt) + nl + nl;

        notes += "Have you had any late or missed payments on any of your debts in the last 6 months? " + Utils.YN(blnStruggling) + nl + nl;

        notes += "Are you finding it difficult to afford the minimum payments on your debts as well as living expenses? " + Utils.YN(blnIsHardForMinPayment) + nl + nl;

        notes += "Do you have unsecured debts over $8,000? " + Utils.YN(blnUnsecuredDebt) + nl + nl;
        
        notes = notes.Replace("'", "''");

        if (lblDebtQuestionID.Text == "")
        {
          using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
          {
            //check to see if it already exists... if so, then we are updating, else we are inserting.
            string sql = "SELECT DebtQuestionID FROM DebtQuestion WHERE CallID = " + Request.QueryString["callID"];
            SqlCommand cmd = new SqlCommand(sql, conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
              //updating
              sql = "update DebtQuestion SET " +
                "UnsecuredDebt = " + Convert.ToInt32(blnUnsecuredDebt).ToString() + ", " +
                "BeenBankrupt = " + Convert.ToInt32(blnBeenBankrupt).ToString() + ", " +
                "Struggling = " + Convert.ToInt32(blnStruggling).ToString() + ", " +
                "WeekNet1st = " + decWeekNet1st.ToString() + ", " +
                "PayFrequency = '" + strPayFrequency + "', " +
                "IsHardForMinPayment = " + Convert.ToInt32(blnIsHardForMinPayment).ToString() + ", " +
                "CompanyCode = '" + strCompanyCode + "', " +
                "OwingAmt = " + decDebtAmount.ToString() + ", " +
                "EmploymentStatus = '" + strEmploymentStatus + "', " +
                "EmploymentDuration = '" + strEmploymentDuration + "' " +
                "WHERE CallID = " + Request.QueryString["callID"];
              SqlCommand cmd_u = new SqlCommand(sql, conn);
              cmd_u.ExecuteNonQuery();
            }
            else
            {
              //inserting.
              sql = "insert into DebtQuestion(CallID, UnsecuredDebt, BeenBankrupt, Struggling, WeekNet1st, PayFrequency, IsHardForMinPayment, CompanyCode, EmploymentStatus, EmploymentDuration, OwingAmt) values("
              + Request.QueryString["callID"] + "," + Convert.ToInt32(blnUnsecuredDebt).ToString() + "," + Convert.ToInt32(blnBeenBankrupt).ToString() + "," + Convert.ToInt32(blnStruggling).ToString() + ","
              + decWeekNet1st.ToString() + ",'" + strPayFrequency + "'," + Convert.ToInt32(blnIsHardForMinPayment).ToString() + ",'" + strCompanyCode + "', '" + strEmploymentStatus + "', '" + strEmploymentDuration + "', " + decDebtAmount.ToString() + ")";
              SqlCommand cmd_i = new SqlCommand(sql, conn);
              cmd_i.ExecuteNonQuery();
            }
            //updating Notes in Call table...
            sql = "UPDATE Call SET Notes = Cast(notes as nvarchar(max)) + '" + notes + "' WHERE CallID = " + Request.QueryString["callID"];
            SqlCommand cmd2 = new SqlCommand(sql, conn);
            cmd2.ExecuteNonQuery();
          }

          //if (Convert.ToDecimal(rtbDebtAmount.Text) > 0)
          if (blnUnsecuredDebt)
          {
            Response.Redirect("debtQuestions_new-2.aspx");
          }
          else
          {
            Response.Redirect("debtQuestions_new-3.aspx");
          }

        }
        else { }
      }
    }

    protected void grpUnsecuredDebt_SelectedIndexChanged(object sender, EventArgs e)
    {
      //trDebtAmount.Visible = (grpUnsecuredDebt.SelectedValue == "1");
      //trDebtAmountHr.Visible = trDebtAmount.Visible;
      //rtbDebtAmount.Text = "0";
    }

  }
}