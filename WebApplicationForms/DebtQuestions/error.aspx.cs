﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplicationForms.DebtQuestions
{
  public partial class error : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Master.DisplayGoogleAdwordsConversionScript = true;
      lblErrorMessage.Text = Session["ErrorMessage"].ToString();
    }
  }
}