﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplicationForms.DebtQuestions
{
  public partial class complete : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Master.DisplayGoogleAdwordsConversionScript = true;
      if (!IsPostBack)
      {
        /*19 Aug 2013 instructed by Chris to bypass
        Call_Processing();
        */
        MMCompany c = (MMCompany)Session["Company"];
        
       //this is hardcoded 2 July 2013 - VERY QUICKLY!!!!
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
        {
          string sql = "SELECT * FROM [Call] WHERE CallID = " + Session["callID"];
          SqlCommand cmd = new SqlCommand(sql, conn);
          conn.Open();
          SqlDataReader dr = cmd.ExecuteReader();
          while (dr.Read())
          {
            if (dr["CompanyID"].ToString() == "6" && !Convert.ToBoolean(dr["HasProperty"]) && Convert.ToInt32(dr["LoanPurposeID"]) != 92)
            {
              //Session["redirect"] = "http://www.australianlendingcentre.com.au/complete-np/";
              Response.Redirect("/EnquiryForm/redirect.aspx?u=" + Server.UrlEncode("http://www.australianlendingcentre.com.au/complete-np/"));
            }
          }
        }



        if (!String.IsNullOrEmpty(c.WebEnquiryFormSuccessURL))
        {
          //Session["redirect"] = "http://" + c.WebEnquiryFormSuccessURL;
          Response.Redirect("/EnquiryForm/redirect.aspx?u=" + Server.UrlEncode("http://" + c.WebEnquiryFormSuccessURL));
        }
        litCompanyPhone.Text = c.Phone;
        hlWebsite.NavigateUrl = "http://" + c.WebsiteURL;
        hlWebsite.Text = c.WebsiteURL;
      }
    }
    private void Call_Processing()
    {
      if (!IsPostBack && Session["callID"] != null)
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
        {
          //Quick N Easy Export
          string sql = "UPDATE [Call] SET CallStatusID = 56, Operator = 'Website', StatusEnteredBy = 'Website' " +
                        "FROM [Call] c INNER JOIN Qualifier q ON c.CallID = q.CallID INNER JOIN DebtQuestion dq ON c.CallID = dq.CallID " +
                        "WHERE CallStatusID <> 56 AND CallStatusID <> 55 AND (c.DebtAmount IS NULL OR c.DebtAmount < 8000) AND HasProperty = 0 " +
                        "AND q.DesiredLoanAmount < 8000 " +
                        "AND dq.UnsecuredDebt = 0 " +
                        "AND c.LoanPurposeID <> 32 " +
                        "AND c.CallID = " + Session["callID"].ToString();
                        //LoanPurposeID 32 = Business
          SqlCommand cmd = new SqlCommand(sql, conn);
          conn.Open();
          cmd.ExecuteNonQuery();

        }
      }





    }
  }
}