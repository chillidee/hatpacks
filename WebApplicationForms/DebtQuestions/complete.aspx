﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="complete.aspx.cs" Inherits="WebApplicationForms.DebtQuestions.complete" %>
<%@ MasterType virtualpath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
        <h1 runat="server" id="h1">All done! You're awesome! :-)</h1>        
      </hgroup>
    </div>
  </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <p>Thank you for taking the time to fill out an enquiry form.  One of our consultants will contact you as soon as possible.</p>
  <p>If you need to speak with a consultant urgently, please wait 5 minutes for us to receive your enquiry then call our following office line:
  <b><asp:Literal runat="server" ID="litCompanyPhone" /></b></p>
  <p>
    You may return to our main website here: <asp:HyperLink runat="server" ID="hlWebsite" />
  </p>
</asp:Content>
