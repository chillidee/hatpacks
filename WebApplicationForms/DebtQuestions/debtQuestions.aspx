﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="debtQuestions.aspx.cs" Inherits="WebApplicationForms.DebtQuestions.DebtQuestionsUtils" %>
<%@ MasterType virtualpath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
  <section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
        <h1 runat="server" id="h1">Your enquiry is almost Complete...</h1>
        <p>In order for us to assess your situation as quickly as possible, please complete the questions below and we will contact you to further discuss your enquiry.</p>
      </hgroup>
    </div>
  </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  
  <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
  <style>
  td.data {
    padding-top:40px;
    vertical-align:top;
    
  }
</style>
<telerik:RadAjaxPanel runat="server">
<table style="width:100%">
  <tr>
    <td>
      <h5>What is your take home income each pay, including Government Benefits and or Pensions?</h5>
      <div class="NoteHelp">Include your total income if you have several sources. Select the appropriate frequency.</div>
    </td>
    <td class="data">
      <telerik:RadNumericTextBox ID="rtbWeekNet1st" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Type="Currency" Width="140px" EmptyMessage="(enter income here)" />
      <asp:RequiredFieldValidator ID="rfvWeekNet1st" ControlToValidate="rtbWeekNet1st" runat="server" EnableClientScript="True" ErrorMessage="We need to know your income details." Text="Required." Display="Dynamic"></asp:RequiredFieldValidator>
      <br />
      <telerik:RadComboBox ID="rbcPayFrequency" runat="server" Width="140px" OffsetY="10" AllowCustomText="False">
        <Items>
          <telerik:RadComboBoxItem Selected="true" Text="Weekly" Value="Weekly" />
          <telerik:RadComboBoxItem Text="Fortnightly" Value="Fortnightly" />
          <telerik:RadComboBoxItem Text="Monthly" Value="Monthly"/>
          <telerik:RadComboBoxItem Text="Yearly" Value="Yearly" />
        </Items>
      </telerik:RadComboBox>
    </td>
  </tr>
  <tr><td colspan="2"><hr /></td></tr>

  <tr>
    <td>
      <h5>Have you been bankrupt during the last 10 years?</h5>
      <div class="NoteHelp">Please note that we are unable to offer a Debt Management Solution to applicants that have been bankrupt during the past 10 years.</div>
    </td>    
    <td class="data">
      <asp:RadioButtonList ID="grpBeenBankrupt" runat="server" RepeatDirection="Vertical">
        <asp:ListItem Value="1">Yes</asp:ListItem>
        <asp:ListItem Value="0">No</asp:ListItem>
      </asp:RadioButtonList>
      <asp:RequiredFieldValidator ID="rfvgrpBeenBankrupt" ControlToValidate="grpBeenBankrupt" runat="Server" ErrorMessage="We need to know if you've been bankrupt during the last 10 years." text="Required." EnableClientScript="true" Display="Dynamic"></asp:RequiredFieldValidator>
    </td>
  </tr>
  <tr><td colspan="2"><hr /></td></tr>

    <tr>
    <td>
      <h5>Have you had any late or missed payments on any of your debts in the last 6 months?</h5>
      <div class="NoteHelp"></div>
    </td>    
    <td class="data">
      <asp:RadioButtonList ID="grpStruggling" runat="server" RepeatDirection="Vertical">
        <asp:ListItem Value="1">Yes</asp:ListItem>
        <asp:ListItem Value="0">No</asp:ListItem>
      </asp:RadioButtonList>
      <asp:RequiredFieldValidator ID="rfvgrpStruggling" ControlToValidate="grpStruggling" runat="Server" ErrorMessage="We need to know if you have any late of missed payments of any of your debts in the last 6 months." Text="Required." EnableClientScript="true" Display="Dynamic"></asp:RequiredFieldValidator>
    </td>
  </tr>
  <tr><td colspan="2"><hr /></td></tr>

    <tr>
    <td>
      <h5>Are you finding it difficult to afford the minimum payments on your debts as well as living expenses?</h5>
      <div class="NoteHelp"></div>
    </td>    
    <td class="data">
      <asp:RadioButtonList ID="grpIsHardForMinPayment" runat="server" RepeatDirection="Vertical">
        <asp:ListItem Value="1">Yes</asp:ListItem>
        <asp:ListItem Value="0">No</asp:ListItem>
      </asp:RadioButtonList>
      <asp:RequiredFieldValidator ID="rfvgrpIsHardForMinPayment" ControlToValidate="grpIsHardForMinPayment" runat="Server" InitialValue="" ErrorMessage="We need to know if you are finding it difficult to afford the minimum payments." Text="Required." EnableClientScript="true" Display="Dynamic"></asp:RequiredFieldValidator>
    </td>
  </tr>
  <tr><td colspan="2"><hr /></td></tr>
  <tr>
    <td>
      <h5>Do you have unsecured debts over $8,000?</h5>
      <div class="NoteHelp"><strong>Unsecured debts</strong> include credit cards, store cards, personal loans and interest-free loans. <strong>Secured</strong> debts are those secured against an asset, such as real estate property or a motor vehicle.</div>
    </td>    
    <td class="data">
      <asp:RadioButtonList ID="grpUnsecuredDebt" runat="server" RepeatDirection="Vertical" AutoPostBack="true" OnSelectedIndexChanged="grpUnsecuredDebt_SelectedIndexChanged">
        <asp:ListItem Value="1">Yes</asp:ListItem>
        <asp:ListItem Value="0">No</asp:ListItem>
      </asp:RadioButtonList>
      <asp:RequiredFieldValidator ID="rfvgrpUnsecuredDebt" ControlToValidate="grpUnsecuredDebt" runat="Server" ErrorMessage="We need to know if you have any unsecured debts over $8,000." Text="Required." EnableClientScript="true" Display="Dynamic"></asp:RequiredFieldValidator>
    </td>
  </tr>
  <tr><td colspan="2"><hr /></td></tr>
  <tr runat="server" id="trDebtAmount" visible="false">
    <td>
      <h5>How much unsecured debt do you have in total?</h5>
      <div class="NoteHelp">Please give us an approximate value of your unsecured debt you have in total.</div>
    </td>    
    <td class="data">
      <telerik:RadNumericTextBox ID="rtbDebtAmount" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Type="Currency" Width="140px" EmptyMessage="(enter here)" Value="0" />
      <asp:RequiredFieldValidator runat="server" ControlToValidate="rtbDebtAmount" InitialValue="" ErrorMessage="We need to know how much unsecured debt you have in total." Text="Required." EnableClientScript="true" Display="Dynamic"></asp:RequiredFieldValidator>
    </td>
  </tr>
  <tr runat="server" id="trDebtAmountHr" visible="false"><td colspan="2"><hr /></td></tr>

</table>
</telerik:RadAjaxPanel>

    <asp:ValidationSummary runat="server" ID="vs" HeaderText="<b>Oops, you've missed something...see above in red.</b>" DisplayMode="BulletList" EnableClientScript="true" ForeColor="Red" ShowValidationErrors="true" BackColor="#ffeaea"/>
		<div style="text-align: right">
      
			<asp:Button ID="btnSubmit" Text="next" CssClass="main" runat="server" onclick="btnSubmit_Click" />
		</div>
  <asp:Label runat="server" ID="lblDebtQuestionID" Visible="false" />

</telerik:RadAjaxPanel>
</asp:Content>
