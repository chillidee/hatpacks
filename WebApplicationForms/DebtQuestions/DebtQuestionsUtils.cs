﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplicationForms.DebtQuestions
{
  static class Utils
  {
    public static void UpdateCallRecord(int ID, string type)
    {
      string notes = "";      
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        //check to see if it already exists... if so, then we are updating, else we are inserting.
        string sql = "SELECT * FROM DebtQuestion WHERE ";
        if (type == "CallID")
        {
          sql += "CallID =  " + Convert.ToString(ID);
        }
        else
        {
          sql += "DebtQuestionID =  " + Convert.ToString(ID);
        }

        SqlCommand cmd = new SqlCommand(sql, conn);
        conn.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.Read())
        {

          string nl = System.Environment.NewLine;

          //buiding the notes...
          notes = nl + nl;

          notes += "What is your take home income each pay, including Government Benefits and or Pensions? $" + Convert.ToString(dr["WeekNet1st"]) + ", " + Convert.ToString(dr["PayFrequency"]) + nl + nl;

          notes += "What is your employment status? " + Convert.ToString(dr["EmploymentStatus"]) + nl + nl;

          notes += "How long have your worked at your current form of employment? " + Convert.ToString(dr["EmploymentDuration"]) + nl + nl;

          notes += "Have you been bankrupt during the last 10 years? " + YN(dr["UnsecuredDebt"]) + nl + nl;

          notes += "Have you had any late or missed payments on any of your debts in the last 6 months? " + YN(dr["Struggling"]) + nl + nl;

          notes += "Are you finding it difficult to afford the minimum payments on your debts as well as living expenses? " + YN(dr["IsHardForMinPayment"]) + nl + nl;

          notes += "Do you have unsecured debts over $8,000? " + YN(dr["UnsecuredDebt"]) + nl + nl;

          notes += "Are you 21 years old and over? " + YN(dr["Over21"]) + nl + nl;

          notes += "Do you own a motor vehicle that is registered in your name? " + YN(dr["OwnVehicle"]) + nl + nl;

          notes += "Is your vehicle less than 10 years old? " + YN(dr["VehicleLessThan10YearsOld"]) + nl + nl;

          notes += "Is your vehicle financed? " + YN(dr["VehicleFinanced"]) + nl + nl;

          notes = notes.Replace("'","''");

          sql = "UPDATE Call SET Notes = Cast(notes as nvarchar(max)) + '" + notes + "' WHERE CallID = " + Convert.ToString(dr["CallID"]);

          SqlCommand cmd2 = new SqlCommand(sql, conn);          
          cmd2.ExecuteNonQuery();
        }

      }

    }
    public static string YN(object x)
    {
      return (Convert.ToBoolean(x) ? "Yes" : "No");
    }
  }


}