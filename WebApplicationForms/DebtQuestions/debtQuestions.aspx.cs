﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;
using WebApplicationForms.Models;
using System.Data.SqlClient;

namespace WebApplicationForms.DebtQuestions
{
  public partial class DebtQuestionsUtils : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Master.DisplayGoogleAdwordsConversionScript = false;
      if (!IsPostBack)
      {
        //checking required "bid" and "lid" querystrings...
        string bid = Request.QueryString["bid"];
        string lid = Request.QueryString["lid"];
        string callID = Request.QueryString["callID"];
        if (bid == null || lid == null)
        {
          if (callID == null)
          {
            Session["ErrorMessage"] = "Required IDs not provided.";
            Response.Redirect("error.aspx");
          }
          else
          {
            try
            {
              Int32 icallID = Convert.ToInt32(callID);
              Session["callID"] = icallID;
            }
            catch
            {
              Session["ErrorMessage"] = "Required IDs not in appropriate format.";
              Response.Redirect("error.aspx");
            }
          }
          //we now have a valid CallID, lets load from database if it exist.
          using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
          {
            //check to see if it already exists... if so, then we are updating, else we are inserting.
            string sql = "SELECT * FROM DebtQuestion WHERE CallID = " + Request.QueryString["callID"];
            SqlCommand cmd = new SqlCommand(sql, conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
              //priming controls...
              //rtbWeekNet1st.Value = (dr["WeekNet1st"] as Double?) ?? 0;
              rtbWeekNet1st.Text = dr["WeekNet1st"].ToString();
              rbcPayFrequency.SelectedValue = dr["PayFrequency"].ToString();
              grpBeenBankrupt.SelectedValue = (Convert.ToBoolean((dr["BeenBankrupt"])) ? "1" : "0");
              grpStruggling.SelectedValue = (Convert.ToBoolean((dr["Struggling"])) ? "1" : "0");
              grpIsHardForMinPayment.SelectedValue = (Convert.ToBoolean((dr["IsHardForMinPayment"])) ? "1" : "0");
              grpUnsecuredDebt.SelectedValue = (Convert.ToBoolean((dr["UnsecuredDebt"])) ? "1" : "0");
              rtbDebtAmount.Text = dr["OwingAmt"].ToString();
            }
          }
        }
        else
        {
          try
          {
            Int32 ibid = Convert.ToInt32(bid);
            Int32 ilid = Convert.ToInt32(lid);
            Session["bid"] = ibid;
            Session["lid"] = ilid;
          }
          catch
          {
            Session["ErrorMessage"] = "Required IDs not in appropriate format.";
            Response.Redirect("error.aspx");
          }
        }

      }


    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      if (IsValid && Session["Company"] != null)
      {
        //getting data from the form...
        Decimal decWeekNet1st = Convert.ToDecimal(rtbWeekNet1st.Value);
        String strPayFrequency = rbcPayFrequency.SelectedValue;
        Boolean blnUnsecuredDebt = (grpUnsecuredDebt.SelectedValue == "1");
        Boolean blnStruggling = (grpStruggling.SelectedValue == "1");
        Boolean blnIsHardForMinPayment = (grpIsHardForMinPayment.SelectedValue == "1");
        Boolean blnBeenBankrupt = (grpBeenBankrupt.SelectedValue == "1");
        Decimal decDebtAmount = Convert.ToDecimal(rtbDebtAmount.Value);
        String strCompanyCode = ((MMCompany)Session["Company"]).Acronym;

        //buiding the notes...        
        string nl = System.Environment.NewLine;
        string notes = nl + nl;

        notes += "What is your take home income each pay, including Government Benefits and or Pensions? $" + Convert.ToString(decWeekNet1st) + ", " + strPayFrequency + nl + nl;               

        notes += "Have you been bankrupt during the last 10 years? " + Utils.YN(blnBeenBankrupt) + nl + nl;

        notes += "Have you had any late or missed payments on any of your debts in the last 6 months? " + Utils.YN(blnStruggling) + nl + nl;

        notes += "Are you finding it difficult to afford the minimum payments on your debts as well as living expenses? " + Utils.YN(blnIsHardForMinPayment) + nl + nl;

        notes += "Do you have unsecured debts over $8,000? " + Utils.YN(blnUnsecuredDebt) + nl + nl;

        notes = notes.Replace("'", "''");

        if (lblDebtQuestionID.Text == "")
          if (Session["callID"] != null)
          {
            string sCallID = Session["callID"].ToString();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
              //check to see if it already exists... if so, then we are updating, else we are inserting.
              string sql = "SELECT DebtQuestionID FROM DebtQuestion WHERE CallID = " + sCallID ;
              SqlCommand cmd = new SqlCommand(sql, conn);
              conn.Open();
              SqlDataReader dr = cmd.ExecuteReader();
              if (dr.Read())
              {
                //updating
                sql = "update DebtQuestion SET " +
                  "UnsecuredDebt = " + Convert.ToInt32(blnUnsecuredDebt).ToString() + ", " +
                  "BeenBankrupt = " + Convert.ToInt32(blnBeenBankrupt).ToString() + ", " +
                  "Struggling = " + Convert.ToInt32(blnStruggling).ToString() + ", " +
                  "WeekNet1st = " + decWeekNet1st.ToString() + ", " +
                  "PayFrequency = '" + strPayFrequency + "', " +
                  "IsHardForMinPayment = " + Convert.ToInt32(blnIsHardForMinPayment).ToString() + ", " +
                  "CompanyCode = '" + strCompanyCode + "', " +
                  "OwingAmt = " + decDebtAmount.ToString() + " " +
                  "WHERE CallID = " + sCallID;
                SqlCommand cmd_u = new SqlCommand(sql, conn);
                cmd_u.ExecuteNonQuery();
              }
              else
              {
                //inserting.
                sql = "insert into DebtQuestion(CallID, UnsecuredDebt, BeenBankrupt, Struggling, WeekNet1st, PayFrequency, IsHardForMinPayment, CompanyCode, OwingAmt) values("
                + sCallID + "," + Convert.ToInt32(blnUnsecuredDebt).ToString() + "," + Convert.ToInt32(blnBeenBankrupt).ToString() + "," + Convert.ToInt32(blnStruggling).ToString() + ","
                + decWeekNet1st.ToString() + ",'" + strPayFrequency + "'," + Convert.ToInt32(blnIsHardForMinPayment).ToString() + ",'" + strCompanyCode + "', " + decDebtAmount.ToString() + ")";
                SqlCommand cmd_i = new SqlCommand(sql, conn);
                cmd_i.ExecuteNonQuery();
              }
              //updating Notes in Call table...
              sql = "UPDATE Call SET Notes = Cast(notes as nvarchar(max)) + '" + notes + "' WHERE CallID = " + Session["callID"].ToString();
              SqlCommand cmd3 = new SqlCommand(sql, conn);
              cmd3.ExecuteNonQuery();
            }

            //WebApplicationForms.DebtQuestions.Utils.UpdateCallRecord(Convert.ToInt32(Session["callID"]), "CallID");

            if (Convert.ToDecimal(rtbDebtAmount.Text) > 0)
            {
              Response.Redirect("debtQuestions-2.aspx");
            }
            else
            {
              Response.Redirect("complete.aspx");
            }
          
          
          }
          else
          {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WEBDBConnectionString"].ConnectionString))
            {
              //check to see if it already exists... if so, then we are updating, else we are inserting.
              string sql = "SELECT DebtQuestionID FROM DebtQuestion WHERE BorrowerID = " + Request.QueryString["bid"];
              SqlCommand cmd = new SqlCommand(sql, conn);
              conn.Open();
              SqlDataReader dr = cmd.ExecuteReader();
              if (dr.Read())
              {
                //updating
                sql = "update DebtQuestion SET " +
                  "UnsecuredDebt = " + Convert.ToInt32(blnUnsecuredDebt).ToString() + ", " +
                  "BeenBankrupt = " + Convert.ToInt32(blnBeenBankrupt).ToString() + ", " +
                  "Struggling = " + Convert.ToInt32(blnStruggling).ToString() + ", " +
                  "WeekNet1st = " + decWeekNet1st.ToString() + ", " +
                  "PayFrequency = '" + strPayFrequency + "', " +
                  "IsHardForMinPayment = " + Convert.ToInt32(blnIsHardForMinPayment).ToString() + ", " +
                  "CompanyCode = '" + strCompanyCode + "', " +
                  "OwingAmt = " + decDebtAmount.ToString() + " " +
                  "WHERE BorrowerID = " + Request.QueryString["bid"];
                SqlCommand cmd_u = new SqlCommand(sql, conn);
                cmd_u.ExecuteNonQuery();
              }
              else
              {
                //inserting.
                sql = "insert into DebtQuestion(BorrowerID,UnsecuredDebt,BeenBankrupt, Struggling, WeekNet1st, PayFrequency, IsHardForMinPayment, CompanyCode, OwingAmt) values("
                + Request.QueryString["bid"] + "," + Convert.ToInt32(blnUnsecuredDebt).ToString() + "," + Convert.ToInt32(blnBeenBankrupt).ToString() + "," + Convert.ToInt32(blnStruggling).ToString() + ","
                + decWeekNet1st.ToString() + ",'" + strPayFrequency + "'," + Convert.ToInt32(blnIsHardForMinPayment).ToString() + ",'" + strCompanyCode + "', " + decDebtAmount.ToString() + ")";
                SqlCommand cmd_i = new SqlCommand(sql, conn);
                cmd_i.ExecuteNonQuery();
              }
              string sql2 = "update loanapplication set DebtAmount = " + decDebtAmount.ToString() + " where loanapplicationID = " + Request.QueryString["lid"];
              SqlCommand cmd2 = new SqlCommand(sql2, conn);
              cmd2.ExecuteNonQuery();

              //updating Notes in Call table...
              sql = "UPDATE Call SET Notes = Cast(notes as nvarchar(max)) + '" + notes + "' WHERE CallID = " + Session["callID"].ToString();
              SqlCommand cmd3 = new SqlCommand(sql, conn);
              cmd3.ExecuteNonQuery();

            }
            if (Convert.ToDecimal(rtbDebtAmount.Text) > 0)
            {
              Response.Redirect("debtQuestions-2.aspx");
            }
            else
            {
              Response.Redirect("complete.aspx");
            }
          }

        else { }
      }
    }
    protected void grpUnsecuredDebt_SelectedIndexChanged(object sender, EventArgs e)
    {
      //trDebtAmount.Visible = (grpUnsecuredDebt.SelectedValue == "1");
      //trDebtAmountHr.Visible = trDebtAmount.Visible;
      //rtbDebtAmount.Text = "0";
    }
  }
}
