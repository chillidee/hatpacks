﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;

namespace WebApplicationForms.UserControls
{
	public partial class MonthlyIncomeGrid : System.Web.UI.UserControl
	{
		public int ApplicantID { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				lblApplicantID.Text = ApplicantID.ToString();
				rgMonthlyIncome.MasterTableView.FilterExpression = "ApplicantID = " + ApplicantID.ToString();
				GridBoundColumn gbc2 = (GridBoundColumn)rgMonthlyIncome.MasterTableView.GetColumn("ApplicantID");
				gbc2.DefaultInsertValue = ApplicantID.ToString();	
			}
		}
		protected void rgMonthlyIncome_ItemCommand(object sender, GridCommandEventArgs e)
		{
			if ((e.Item is GridDataItem) && ((e.CommandName == RadGrid.UpdateCommandName) || (e.CommandName == RadGrid.PerformInsertCommandName)))
			{
				GridDataItem editItem = (GridDataItem)e.Item;

				String term = ((DropDownList)editItem.FindControl("DDL_TermDisplay")).SelectedValue;
				Decimal AmountDisplay = Convert.ToDecimal(((RadNumericTextBox)editItem.FindControl("rtbAmountDisplay")).Text);
				Decimal Amount = 0;
				switch (term)
				{
					case "W":
						Amount = AmountDisplay * (Decimal)4.345238;
						break;
					case "F":
						Amount = AmountDisplay * (Decimal)2.172619;
						break;
					case "M":
						Amount = AmountDisplay;
						break;
					case "Y":
						Amount = AmountDisplay / 12;
						break;
					default:
						Amount = AmountDisplay;
						break;
				}
				RadNumericTextBox rtbAmount = (RadNumericTextBox)editItem.FindControl("rtbAmount");
				rtbAmount.Text = Amount.ToString();
			}
      //added 26 Mar 2013
      if (e.CommandName == RadGrid.CancelCommandName && e.Item.IsInEditMode)
        Session["editMode"] = null;
      if (e.CommandName == RadGrid.UpdateCommandName) Session["editmode"] = null;
      if ((e.CommandName == RadGrid.EditCommandName) || (e.CommandName == RadGrid.InitInsertCommandName))
      {
        if (Session["editMode"] != null)
          e.Canceled = true;
        else
          Session["editMode"] = "edit";
      }
    }

		protected void sqlMonthlyIncome_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
		{
			e.Command.Parameters["@ApplicantID"].Value = lblApplicantID.Text;
		}

    protected void rgMonthlyIncome_PreRender(object sender, EventArgs e)
    {
      
        rgMonthlyIncome.MasterTableView.IsItemInserted = (Session["editMode"] == null); // Check for the !IsPostBack also for initial page load      
        rgMonthlyIncome.Rebind();
      
    }

    protected void rgMonthlyIncome_Unload(object sender, EventArgs e)
    { 
    }
    public void WrapUp()
    {
      if (Session["editMode"] != null)
      {
        // RadWindowManager1.RadAlert("Please \"save\" or \"cancel\" first.", 330, 180, "Save or Cancel", "");         
        foreach (GridItem i in rgMonthlyIncome.Items)
        {
          if (i.IsInEditMode)
          {
            i.FireCommandEvent("Update", "");
          }
        }
      }
      try
      {
        GridItem ii = (GridItem)rgMonthlyIncome.MasterTableView.GetInsertItem();
        RadNumericTextBox rtbAmountDisplay = (RadNumericTextBox)ii.FindControl("rtbAmountDisplay");

        if (!(rtbAmountDisplay.Text == "0"))
        {
          rgMonthlyIncome.MasterTableView.GetInsertItem().FireCommandEvent("PerformInsert", "");
        }
      }
      catch { }    
    }
	}
}