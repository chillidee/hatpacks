﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using WebApplicationForms.Models;
using System.Web.UI.HtmlControls;

namespace WebApplicationForms.UserControls
{
  [Bindable(true, BindingDirection.TwoWay), Category("Data")]
  public partial class AssetLiabilityEdit : System.Web.UI.UserControl
  {
    private int _ApplicantID;
    public int ApplicantID
    {
      get
      {
        return _ApplicantID;
      }
      set
      {
        _ApplicantID = value;
        lblApplicantID.Text = _ApplicantID.ToString();
      }
    }
    private object _dataItem = null;
    public object DataItem
    {
      get
      {
        return this._dataItem;
      }
      set
      {
        this._dataItem = value;
        //_FinanceDetailID = Convert.ToInt32(DataBinder.Eval(_dataItem, "FinanceDetailID"));								
        try
        {
          _FinanceDetailID = (Convert.ToInt32(((System.Data.DataRowView)((GridEditFormItem)_dataItem).DataItem).Row[0]));
          theForm.ChangeMode(FormViewMode.Edit);
        }
        catch
        { theForm.ChangeMode(FormViewMode.Insert); }
      }
    }

    private int _FinanceDetailID;

    protected bool IsUCPostBack
    {
      get
      {
        bool b = this.ViewState["IsUserControlPostBack"] != null;
        this.ViewState["IsUserControlPostBack"] = true;
        return b;
      }
      set { }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsUCPostBack)
      {
        theForm.InsertItemTemplate = theForm.EditItemTemplate;
      }
    }

    protected void sqlAssetLiability_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
      e.Command.Parameters["@FinanceDetailID"].Value = _FinanceDetailID;
    }

    protected void rcbAssetLiabilityID_SelectedIndexChanged(object sender, EventArgs e)
    {
      UpdateControls();
    }

    protected void rtbAmountOwing_TextChanged(object sender, EventArgs e)
    {
      RadNumericTextBox r = (RadNumericTextBox)sender;
      HtmlTableRow tr = (HtmlTableRow)theForm.FindControl("trMonthly");
      RadNumericTextBox rtbMonthly = (RadNumericTextBox)theForm.FindControl("rtbMonthly");

      if ((r.Value) > 0)
      {
        tr.Visible = true;
        DropDownList rcbAssetLiabilityID = (DropDownList)theForm.FindControl("rcbAssetLiabilityID");
        MMAssetLiabilityType alt = new MMAssetLiabilityType(Convert.ToInt32(rcbAssetLiabilityID.SelectedValue));
        if (alt.IsCreditCard) rtbMonthly.Value = r.Value * 0.03;
      }
      else
      {
        tr.Visible = false;
        rtbMonthly.Text = null;
      }
      rtbMonthly.Focus();
    }

    protected void cbUseForSecurity_CheckedChanged(object sender, EventArgs e)
    {
      CheckBox cbUseForSecurity = (CheckBox)sender;
      SecurityEdit ucSecurityEdit = (SecurityEdit)theForm.FindControl("ucSecurityEdit");
      Label lblSecurityID = (Label)theForm.FindControl("lblSecurityID");
      if (cbUseForSecurity.Checked)
      {
        if (lblSecurityID.Text == "")
        {
          CreateNewSecurity();
          ucSecurityEdit.SecurityID = Convert.ToInt32(lblSecurityID.Text);
          //theForm.UpdateItem(true);
        }
        else
        {
          //security already exists, so, lets just open up...					
          ucSecurityEdit.SecurityID = Convert.ToInt32(lblSecurityID.Text);
        }
      }
      ucSecurityEdit.Visible = true;
      ucSecurityEdit.Start();

    }

    protected void theForm_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
      if (e.CommandName == "Cancel") RefreshParent();
      else
      {
        if (theForm.CurrentMode == FormViewMode.Edit || theForm.CurrentMode == FormViewMode.Insert)
        {
          Label lblApplicantID = (Label)theForm.FindControl("lblApplicantID");
          Decimal Value = Utils.EmptyToZero(((RadNumericTextBox)theForm.FindControl("rtbValue")).Text);
          Decimal Owing = Utils.EmptyToZero(((RadNumericTextBox)theForm.FindControl("rtbAmountOwing")).Text);
          RadNumericTextBox rtbNettValue = (RadNumericTextBox)theForm.FindControl("rtbNettValue");
          rtbNettValue.Text = (Value - Owing).ToString();

          if (lblApplicantID.Text == "") lblApplicantID.Text = this.lblApplicantID.Text;

          SecurityEdit ucSecurityEdit = (SecurityEdit)theForm.FindControl("ucSecurityEdit");
          if (ucSecurityEdit.Visible)
          {
            Address S_ucAddress = (Address)(ucSecurityEdit.FindControl("theForm")).FindControl("ucAddress");
            RadTextBox S_rtbSecurityDescription = (RadTextBox)(ucSecurityEdit.FindControl("theForm")).FindControl("rtbSecurityDescription");
            RadNumericTextBox S_rtbEstimatedMarketValue = (RadNumericTextBox)(ucSecurityEdit.FindControl("theForm")).FindControl("rtbEstimatedMarketValue");
            RadNumericTextBox S_rtbFirstMortgageOwing = (RadNumericTextBox)(ucSecurityEdit.FindControl("theForm")).FindControl("rtbFirstMortgageOwing");

            Address ucAddress = (Address)theForm.FindControl("ucAddress");
            RadTextBox rtbDetails = (RadTextBox)theForm.FindControl("rtbDetails");
            RadNumericTextBox rtbValue = (RadNumericTextBox)theForm.FindControl("rtbValue");
            RadNumericTextBox rtbAmountOwing = (RadNumericTextBox)theForm.FindControl("rtbAmountOwing");

            S_rtbEstimatedMarketValue.Text = rtbValue.Text;
            S_rtbFirstMortgageOwing.Text = rtbAmountOwing.Text;

            S_ucAddress.UnitNumber = ucAddress.UnitNumber;
            S_ucAddress.StreetNumber = ucAddress.StreetNumber;
            S_ucAddress.StreetName = ucAddress.StreetName;
            S_ucAddress.StreetType = ucAddress.StreetType;
            S_ucAddress.SuburbID = ucAddress.SuburbID;
            S_rtbSecurityDescription.Text = rtbDetails.Text;

            ucSecurityEdit.UpdateForm();
          }

        }
      }
    }

    protected void theForm_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
    {
      RefreshParent();
    }

    protected void theForm_ItemInserted(object sender, FormViewInsertedEventArgs e)
    {
      RefreshParent();
    }

    protected void theForm_DataBound(object sender, EventArgs e)
    {
      UpdateControls();
    }

    private void UpdateControls()
    {
      FormView f = theForm;
      DropDownList rcbAssetLiabilityID = (DropDownList)f.FindControl("rcbAssetLiabilityID");
      //GridEditFormItem f = (GridEditFormItem)rcbAssetLiabilityID.NamingContainer;
      CheckBox cbIsRealEstate = (CheckBox)f.FindControl("cbIsRealEstate");
      CheckBox cbUseForSecurity = (CheckBox)f.FindControl("cbUseForSecurity");
      HtmlTableRow trFullAddress = (HtmlTableRow)f.FindControl("trFullAddress");
      HtmlTableRow trUseForSecurity = (HtmlTableRow)f.FindControl("trUseForSecurity");
      HtmlTableRow trCreditCardLimit = (HtmlTableRow)f.FindControl("trCreditCardLimit");
      HtmlTableRow trValue = (HtmlTableRow)f.FindControl("trValue");
      HtmlTableRow trOwing = (HtmlTableRow)f.FindControl("trOwing");
      RadNumericTextBox rtbValue = (RadNumericTextBox)f.FindControl("rtbValue");
      RadNumericTextBox rtbCreditCardLimit = (RadNumericTextBox)f.FindControl("rtbCreditCardLimit");
      Address ucAddress = (Address)f.FindControl("ucAddress");
      SecurityEdit ucSecurityEdit = (SecurityEdit)f.FindControl("ucSecurityEdit");
      RadTextBox rtbDetails = (RadTextBox)f.FindControl("rtbDetails");
      RadNumericTextBox rtbAmountOwing = (RadNumericTextBox)f.FindControl("rtbAmountOwing");
      RadTextBox rtbLender = (RadTextBox)f.FindControl("rtbLender");
      HtmlGenericControl tBodyLeft = (HtmlGenericControl)f.FindControl("tBodyLeft");
      HtmlTable tblRight = (HtmlTable)f.FindControl("tblRight");
      Button btnInsert = (Button)f.FindControl("btnInsert");
      Button btnUpdate = (Button)f.FindControl("btnUpdate");


      tBodyLeft.Visible = (rcbAssetLiabilityID.SelectedValue != "");
      tblRight.Visible = (rcbAssetLiabilityID.SelectedValue != "");
      btnInsert.Enabled = (rcbAssetLiabilityID.SelectedValue != "");
      btnInsert.Visible = ((rcbAssetLiabilityID.SelectedValue != "") && (!btnUpdate.Visible));
      
      //only specifically during the time of adding a new record.
      //if (rcbAssetLiabilityID.SelectedValue == "") btnInsert.Visible = false;

      if (rcbAssetLiabilityID.SelectedValue != "")
      {
        MMAssetLiabilityType alt = new MMAssetLiabilityType(Convert.ToInt32(rcbAssetLiabilityID.SelectedValue));
        cbIsRealEstate.Checked = alt.IsRealEstate;
        trUseForSecurity.Visible = alt.IsRealEstate;
        trFullAddress.Visible = alt.IsRealEstate;
        trValue.Visible = alt.ShowValue;
        trCreditCardLimit.Visible = alt.IsCreditCard;
        trOwing.Visible = alt.ShowOwing;

        if (alt.IsCreditCard)
        {
          rtbValue.Text = null;
        }
        if (alt.IsRealEstate)
        {
          rtbCreditCardLimit.Text = null;
        }
      }
      else
      {        
      }

      if (cbUseForSecurity.Checked)
      {
        //check to see if Eval("SecurityID") is null...
        if (Eval("SecurityID") == DBNull.Value)
        {
          CreateNewSecurity();
          Label lblSecurityID = (Label)theForm.FindControl("lblSecurityID");
          ucSecurityEdit.SecurityID = lblSecurityID.Text;
          theForm.UpdateItem(false);
        }
        else
        {
          ucSecurityEdit.SecurityID = Convert.ToInt32(Eval("SecurityID"));
        }
        ucSecurityEdit.Start();
        ucSecurityEdit.Visible = true;
      }
      else
      { }
    }

    protected void RefreshParent()
    {
      GridEditFormItem editItem = (GridEditFormItem)this.Parent.NamingContainer;
      editItem.FireCommandEvent("Cancel", "");
    }

    protected void CreateNewSecurity()
    {

      //check Session["Application"]
      MMApplication a = (MMApplication)Session["Application"];

      Label lblSecurityID = (Label)theForm.FindControl("lblSecurityID");
      MMSecurity s = new MMSecurity();
      Address add = (Address)((UserControl)theForm.FindControl("ucAddress"));
      s.UnitNumber = add.UnitNumber.ToString();
      s.StreetNumber = add.StreetNumber.ToString();
      s.StreetName = add.StreetName.ToString();
      s.StreetType = add.StreetType.ToString();
      s.Suburb = Convert.ToInt32(add.SuburbID);
      s.SecurityDescription = ((RadTextBox)theForm.FindControl("rtbDetails")).Text;
      s.EstimatedMarketValue = Convert.ToInt32(Utils.EmptyToZero(((RadNumericTextBox)theForm.FindControl("rtbValue")).Text));
      s.FirstMortgageOwing = Convert.ToInt32(Utils.EmptyToZero(((RadNumericTextBox)theForm.FindControl("rtbAmountOwing")).Text));
      s.TenderPrice = s.FirstMortgageOwing; //this is actually the total value at the bottom, wrongly named from MoneyMaker.
      s.FirstMortgageLender = ((RadTextBox)theForm.FindControl("rtbLender")).Text;
      s.ApplicationNo = a.ApplicationNo;
      s.Create();

      lblSecurityID.Text = s.SecurityID.ToString();

    }

    protected void rtbValue_TextChanged(object sender, EventArgs e)
    {
      SecurityEdit ucSecurityEdit = (SecurityEdit)theForm.FindControl("ucSecurityEdit");
      if (ucSecurityEdit.Visible)
      {
        ((RadNumericTextBox)ucSecurityEdit.FindControl("theForm").FindControl("rtbEstimatedMarketValue")).Value = ((RadNumericTextBox)sender).Value;
      }
      if (theForm.FindControl("rtbAmountOwing").Visible) theForm.FindControl("rtbAmountOwing").Focus();
    }

  }
}