﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;
using Telerik.Web.UI;
using System.ComponentModel;

namespace WebApplicationForms.UserControls
{
	[Bindable(true,BindingDirection.TwoWay), Category("Data") ]
	public partial class Address_Simple : System.Web.UI.UserControl
	{
		private string _AddressLine1;
		private string _AddressLine2;

		private Nullable<int> _SuburbID;
		private MMPCode _pc;
		private string _Combo;
		private string _Suburb;
		private string _State;
		private string _PostCode;
		
		public object AddressLine1 {
			get { _AddressLine1 = rtbAddressLine1.Text; return _AddressLine1; }
			set { _AddressLine1 = value.ToString(); rtbAddressLine1.Text = _AddressLine1; }
		}
		public object AddressLine2
		{
			get { _AddressLine2 = rtbAddressLine2.Text; return _AddressLine2; }
			set { _AddressLine2 = value.ToString(); rtbAddressLine2.Text = _AddressLine2; }
		}

		public object SuburbID
		{
			get { _SuburbID = (Utils.EmptyToNull(rcbSuburb.SelectedValue) as Int32?) ?? null; return _SuburbID; }
			set {
				int tempInt;
				if (Int32.TryParse(Convert.ToString(value),out tempInt))
				{
					_SuburbID = tempInt;
				}
				else
				{
					_SuburbID = (value as Int32?) ?? null;
				}
				_pc = new MMPCode(_SuburbID);
				_Combo = _pc.SuburbStatePostCode;
				_Suburb = _pc.SUBURB;
				_State = _pc.STATE;
				_PostCode = _pc.PCODE;
				rcbSuburb.SelectedValue = _SuburbID.ToString();
				rcbSuburb.Text = _Combo;

			}
		}
		public string Combo { get { return _Combo; } }
		public string Suburb { get { return _Suburb; } }
		public string State { get { return _State; } }
		public string PostCode { get { return _PostCode; } }

		public string FieldCssClass { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{

			}
		}
	}
}