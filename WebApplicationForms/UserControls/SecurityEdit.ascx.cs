﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;
using WebApplicationForms.UserControls;
using Telerik.Web.UI;

namespace WebApplicationForms.UserControls
{
	public partial class SecurityEdit : System.Web.UI.UserControl
	{
		protected bool IsUCPostBack
		{
			get
			{
				bool b = this.ViewState["IsUserControlPostBack"] != null;
				this.ViewState["IsUserControlPostBack"] = true;
				return b;
			}
			set { }
		}

		protected Nullable<int> _SecurityID;

		public object SecurityID
		{
			get
			{
				return (object)_SecurityID;
			}
			set 
			{
				if (value is int)	{_SecurityID = Convert.ToInt32(value);}
				if (value == DBNull.Value) { _SecurityID = null; }
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{			
			//theForm.ChangeMode(FormViewMode.Edit);
		}

		public void Start()
		{

			//for testing only. remove for production.
			//Session["SecurityID"] = 27733;

			//checking session...
			if (SecurityID != null)
			{
				//this means we need to load from database. Go into edit mode.
				lblSecurityID.Text = SecurityID.ToString();
				lblApplicationNo.Text = ((MMApplication)Session["Application"]).ApplicationNo.ToString();
				theForm.ChangeMode(FormViewMode.Edit);
			}
			else if (Session["Security"] != null)
			{
				//this means we need to go into Insert mode, ready to insert into database.
				MMApplication a = (MMApplication)Session["Application"];
				lblApplicationNo.Text = a.ApplicationNo.ToString();
				theForm.InsertItemTemplate = theForm.EditItemTemplate;
				theForm.ChangeMode(FormViewMode.Insert);
				//populating control values...
				Address ad = (Address)theForm.FindControl("ucAddress");
				RadTextBox rtbSecurityDescription = (RadTextBox)theForm.FindControl("rtbSecurityDescription");
				RadNumericTextBox rtbEstimatedMarketValue = (RadNumericTextBox)theForm.FindControl("rtbEstimatedMarketValue");
				RadNumericTextBox rtbFirstMortgageOwing = (RadNumericTextBox)theForm.FindControl("rtbFirstMortgageOwing");
				TextBox tbApplicationNo = (TextBox)theForm.FindControl("tbApplicationNo");

				RadComboBox rb1 = (RadComboBox)theForm.FindControl("rcbFirstMortgageType");
				RadComboBox rb2 = (RadComboBox)theForm.FindControl("rcbSecondMortgageType");
				RadComboBox rb3 = (RadComboBox)theForm.FindControl("rcbThirdMortgageType");
				RadComboBox rb4 = (RadComboBox)theForm.FindControl("rcbFourthMortgageType");
				RadComboBox rb5 = (RadComboBox)theForm.FindControl("rcbFifthMortgageType");
				RadComboBox rb6 = (RadComboBox)theForm.FindControl("rcbSixthMortgageType");
				RadComboBox rb7 = (RadComboBox)theForm.FindControl("rcbSeventhMortgageType");

				rb1.Text = "1st Mortgage"; rb1.SelectedValue = "1st Mortgage";
				rb2.Text = "2nd Mortgage"; rb2.SelectedValue = "2nd Mortgage";
				rb3.Text = "3rd Mortgage"; rb3.SelectedValue = "3rd Mortgage";
				rb4.Text = "Overdraft"; rb4.SelectedValue = "Overdraft";
				rb5.Text = "Business Loan"; rb5.SelectedValue = "Business Loan";
				rb6.Text = "Caveat 1"; rb6.SelectedValue = "Caveat 1";
				rb7.Text = "Caveat 2"; rb7.SelectedValue = "Caveat 2";

				MMSecurity s = (MMSecurity)Session["Security"];

				tbApplicationNo.Text = a.ApplicationNo.ToString();
				rtbSecurityDescription.Text = s.SecurityDescription;
				rtbEstimatedMarketValue.Text = s.EstimatedMarketValue.ToString();
				rtbFirstMortgageOwing.Text = s.FirstMortgageOwing.ToString();
				ad.UnitNumber = s.UnitNumber;
				ad.StreetNumber = s.StreetNumber;
				ad.StreetName = s.StreetName;
				ad.StreetType = s.StreetType;
				ad.SuburbID = s.Suburb;

			}

		}

		protected void sqlSecurity_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
		{
			if (lblSecurityID.Text != "")
			{
				e.Command.Parameters["@SecurityID"].Value = Convert.ToInt32(lblSecurityID.Text);
			}
			else
			{
				e.Command.Parameters["@SecurityID"].Value = 27733; //test security ID
			}

		}

		protected void Owing_TextChanged(object sender, EventArgs e)
		{
			RadNumericTextBox r1 = (RadNumericTextBox)theForm.FindControl("rtbFirstMortgageOwing");
			RadNumericTextBox r2 = (RadNumericTextBox)theForm.FindControl("rtbSecondMortgageOwing");
			RadNumericTextBox r3 = (RadNumericTextBox)theForm.FindControl("rtbThirdMortgageOwing");
			RadNumericTextBox r4 = (RadNumericTextBox)theForm.FindControl("rtbFourthMortgageOwing");
			RadNumericTextBox r5 = (RadNumericTextBox)theForm.FindControl("rtbFifthMortgageOwing");
			RadNumericTextBox r6 = (RadNumericTextBox)theForm.FindControl("rtbSixthMortgageOwing");
			RadNumericTextBox r7 = (RadNumericTextBox)theForm.FindControl("rtbSeventhMortgageOwing");
			RadNumericTextBox rTotal = (RadNumericTextBox)theForm.FindControl("rtbTenderPrice");

			decimal total =
				Convert.ToDecimal(Utils.EmptyToZero(r1.Text)) +
				Convert.ToDecimal(Utils.EmptyToZero(r2.Text)) +
				Convert.ToDecimal(Utils.EmptyToZero(r3.Text)) +
				Convert.ToDecimal(Utils.EmptyToZero(r4.Text)) +
				Convert.ToDecimal(Utils.EmptyToZero(r5.Text)) +
				Convert.ToDecimal(Utils.EmptyToZero(r6.Text)) +
				Convert.ToDecimal(Utils.EmptyToZero(r7.Text));

			rTotal.Text = total.ToString();
			RadNumericTextBox rdTotalOwing = (RadNumericTextBox)(this.Parent.NamingContainer).FindControl("rtbAmountOwing");
			if (rdTotalOwing != null) { rdTotalOwing.Text = total.ToString();}

			//change focus to the next control (rtbXXMortgageACNo)
			if (sender is RadNumericTextBox)
			{
				string controlID = (((RadNumericTextBox)sender).ID).Replace("MortgageOwing","");
				theForm.FindControl(controlID + "MortgageACNo").Focus();
			}
		}

		protected void theForm_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
		{			
		}

		protected void theForm_ItemInserted(object sender, FormViewInsertedEventArgs e)
		{
		}

		protected void theForm_ItemCommand(object sender, FormViewCommandEventArgs e)
		{
			if (e.CommandName != "Cancel")
			{
				//re-building MMSecurity object to put into session variable...
				MMSecurity s = new MMSecurity();

				s.TenderPrice = Convert.ToInt32(Utils.EmptyToZero(((RadNumericTextBox)theForm.FindControl("rtbTenderPrice")).Text));

			}

		}

		protected void sqlSecurity_Inserted(object sender, SqlDataSourceStatusEventArgs e)
		{
			int NewID = Convert.ToInt32(e.Command.Parameters["@NewId"].Value);
			lblSecurityID.Text = NewID.ToString();
		}

		protected void rtbPurchasePrice_TextChanged(object sender, EventArgs e)
		{
			UpdateForm();
			theForm.FindControl("rtbPurchasePrice").Focus();
		}

		public void UpdateForm()
		{
			theForm.UpdateItem(true);
			//theForm.ChangeMode(FormViewMode.Edit);
		}
	}
}