﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.HtmlControls;

namespace WebApplicationForms.UserControls
{
	public partial class ApplicantAssetLiabilityGrid : System.Web.UI.UserControl
	{
		public int ApplicantID { get; set; }
		protected void Page_Load(object sender, EventArgs e)
		{
			
			if (!IsPostBack)
			{
				lblApplicantID.Text = ApplicantID.ToString();				
				//sqlAssetLiability.FilterExpression = "ApplicantID = " + ApplicantID.ToString();
				//sqlAssetLiability.DataBind();
				//sqlAssetLiability.SelectCommand = "SELECT [FinanceDetailID], [ApplicantID], [AssetLiabilityID], [Value], [Monthly], [AmountOwing], [NettValue], [Details], [CreditCardLimit], [IsRealEstate], [Lender], [FullAddress] FROM [AssetLiability] WHERE ApplicantID = " + ApplicantID.ToString();
				rgAssetLiability.MasterTableView.FilterExpression = "ApplicantID = " + ApplicantID.ToString();
				GridBoundColumn gbc = (GridBoundColumn)rgAssetLiability.MasterTableView.GetColumn("ApplicantID");
				gbc.DefaultInsertValue = ApplicantID.ToString();

			}
			litTest.Text = sqlAssetLiability.SelectCommand.ToString() + "<br/>ApplicantID: " + ApplicantID.ToString();
		}

		protected void rgAssetLiability_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			if ((e.Item is GridEditFormItem) && ((e.CommandName == RadGrid.UpdateCommandName) || (e.CommandName == RadGrid.PerformInsertCommandName)))
			{
				GridEditFormItem editItem = (GridEditFormItem)e.Item;

				//getting data from the Address user control...



			}
			if((e.Item is GridCommandItem) && ((e.CommandName == RadGrid.InitInsertCommandName)))
			{

			}
			if ((e.Item is GridDataItem) && ((e.CommandName == RadGrid.EditCommandName)))
			{

			}
			
		}
		protected void rgAssetLiability_ItemInserted(object sender, GridInsertedEventArgs e)
		{
			{
				if (e.Exception != null)
				{
					e.ExceptionHandled = true;
					e.KeepInInsertMode = true;
					throw new Exception("Product cannot be inserted. Reason: " + e.Exception.Message);
				}
				else
				{
					rgAssetLiability.Rebind();
				}
			}
		}

		protected void rgAssetLiability_UpdateCommand(object sender, GridCommandEventArgs e)
		{

		}

		protected void rgAssetLiability_ItemUpdated(object sender, GridUpdatedEventArgs e)
		{
			if (e.Exception != null)
			{
				e.ExceptionHandled = true;
				e.KeepInEditMode = true;
				throw new Exception("Product cannot be updated. Reason: " + e.Exception.Message);
			}
			else
			{
				rgAssetLiability.Rebind();
			}
		}

		protected void rgAssetLiability_EditCommand(object sender, GridCommandEventArgs e)
		{
			//UpdateControls((GridDataItem)e.Item);
			/*
			GridDataItem editItem = (GridDataItem)e.Item;
			Address ucAddress = (Address)editItem.FindControl("ucAddress");

			ucAddress.UnitNumber = ((RadTextBox)editItem.FindControl("rtbUnitNumber")).Text;
			ucAddress.StreetNumber = ((RadTextBox)editItem.FindControl("rtbStreetNumber")).Text;
			ucAddress.StreetName = ((RadTextBox)editItem.FindControl("rtbStreetName")).Text;
			ucAddress.StreetType = ((RadTextBox)editItem.FindControl("rtbStreetType")).Text;
			ucAddress.SuburbID = (Utils.EmptyToNull((((RadTextBox)editItem.FindControl("rtbSuburb")).Text).ToString()) as Int32?) ?? null;
			*/
		}

		protected void sqlAssetLiability_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
		{			
			e.Command.Parameters["@ApplicantID"].Value = lblApplicantID.Text;
		}

		protected void rgAssetLiability_PreRender(object sender, EventArgs e)
		{
			/*foreach (GridItem item in rgAssetLiability.MasterTableView.Items)
			{
				if (item is GridDataItem && item.Edit)
				{
					item.Visible = false;
				}
			}*/
		}

		protected void rtbAmountOwing_TextChanged(object sender, EventArgs e)
		{
		}

		protected void cbIsRealEstate_CheckedChanged(object sender, EventArgs e)
		{
			/*CheckBox cbIsRealEstate = (CheckBox)sender;
			GridEditFormItem editItem = (GridEditFormItem)cbIsRealEstate.NamingContainer;
			System.Web.UI.HtmlControls.HtmlTableRow trFullAddress = (System.Web.UI.HtmlControls.HtmlTableRow)editItem.FindControl("trFullAddress");
			System.Web.UI.HtmlControls.HtmlTableRow trUseForSecurity = (System.Web.UI.HtmlControls.HtmlTableRow)editItem.FindControl("trUseForSecurity");
			//RadTextBox rtbFullAddress = (RadTextBox)editItem.FindControl("rtbFullAddress");
			CheckBox cbUseForSecurity = (CheckBox)editItem.FindControl("cbUseForSecurity");
			Address ucAddress = (Address)editItem.FindControl("ucAddress");
			

			trFullAddress.Visible = cbIsRealEstate.Checked;
			trUseForSecurity.Visible = cbIsRealEstate.Checked;
			
			if (cbIsRealEstate.Checked)
			{								
				//ucAddress.SuburbID = Convert.ToInt32(editItem.SavedOldValues["Suburb"]);
				ucAddress.SuburbID = (Utils.EmptyToNull((editItem.SavedOldValues["Suburb"]).ToString()) as Int32?) ?? null;
				ucAddress.UnitNumber = ((RadTextBox)editItem.FindControl("rtbUnitNumber")).Text;
				ucAddress.StreetNumber = ((RadTextBox)editItem.FindControl("rtbStreetNumber")).Text;
				ucAddress.StreetName = ((RadTextBox)editItem.FindControl("rtbStreetName")).Text;
				ucAddress.StreetType = ((RadTextBox)editItem.FindControl("rtbStreetType")).Text;
			}
			else
			{
				//rtbFullAddress.Text = null;
				cbUseForSecurity.Checked = false;
				ucAddress.SuburbID = null;
				ucAddress.UnitNumber = null;
				ucAddress.StreetNumber = null;
				ucAddress.StreetName = null;
				ucAddress.StreetType = null;
			}*/
		}



		private void UpdateMonthRepayment()
		{
			//asdf
		}

		protected void rgAssetLiability_ItemDataBound(object sender, GridItemEventArgs e)
		{
			
		}
	}
}