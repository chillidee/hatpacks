﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;

namespace WebApplicationForms.UserControls
{
	
	public partial class SelfEmployedIncomeGrid : System.Web.UI.UserControl
	{
		public int ApplicantID { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				rgSelfEmployed.MasterTableView.FilterExpression = "ApplicantID = " + ApplicantID.ToString();
				GridBoundColumn gbc1 = (GridBoundColumn)rgSelfEmployed.MasterTableView.GetColumn("ApplicantID");
				gbc1.DefaultInsertValue = ApplicantID.ToString();
			}
		}
		protected void rgSelfEmployed_ItemInserted(object sender, GridInsertedEventArgs e)
		{
			{
				if (e.Exception != null)
				{
					e.ExceptionHandled = true;
					e.KeepInInsertMode = true;
					throw new Exception("Product cannot be inserted. Reason: " + e.Exception.Message);
				}
				else
				{
					rgSelfEmployed.Rebind();
				}
			}
		}
	}
}