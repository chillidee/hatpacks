﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssetLiabilityEdit.ascx.cs" Inherits="WebApplicationForms.UserControls.AssetLiabilityEdit" %>
<%@ Register Src="~/UserControls/Address.ascx" TagPrefix="uc1" TagName="Address" %>
<%@ Register Src="~/UserControls/SecurityEdit.ascx" TagPrefix="uc1" TagName="SecurityEdit" %>

<asp:Label runat="server" ID="lblApplicantID" Visible="false" Text=""></asp:Label>

<asp:FormView runat="server" ID="theForm" DataSourceID="sqlAssetLiability" DataKeyNames="FinanceDetailID" OnItemCommand="theForm_ItemCommand" OnItemUpdated="theForm_ItemUpdated" OnItemInserted="theForm_ItemInserted" OnDataBound="theForm_DataBound">
	<EditItemTemplate>
		<div>
			<h3 style="margin-top:20px;margin-left:20px">Details about your Asset/Liability</h3>
			<asp:Label runat="server" ID="lblApplicantID" Visible="false" Text='<%# Bind("ApplicantID") %>'></asp:Label>
			<asp:Label runat="server" ID="lblSecurityID" Visible="false" Text='<%# Bind("SecurityID") %>'></asp:Label>

			<table style="border-collapse: separate; border-spacing: 0px; margin-left: 20px">
				<tr>
					<td valign="top">
						<table style="border-collapse: separate; border-spacing: 0px; margin-left: 5px">
							<tr>
								<td style="text-align: right">Type</td>
								<td>
									<asp:DropDownList ID="rcbAssetLiabilityID" runat="server" DataSourceID="sqlAssetLiabilityType" SelectedValue='<%# Bind("AssetLiabilityID") %>' DataValueField="AssetLiabilityID" DataTextField="AssetLiability" AppendDataBoundItems="true" AutoPostBack="True" OnSelectedIndexChanged="rcbAssetLiabilityID_SelectedIndexChanged">										
									</asp:DropDownList></td>
							</tr>
							<tbody runat="server" id="tBodyLeft">
							<tr>
								<td style="text-align: right; vertical-align: top">Details</td>
								<td>
									<telerik:RadTextBox runat="server" ID="rtbDetails" Text='<%# Bind("Details") %>' TextMode="MultiLine" Height="50px" Width="250px"></telerik:RadTextBox>
									<div class="Note" runat="server" visible="false">eg: House at Sydney, Honda jetski, Holden Commodore 2003, Superannuation with MLC</div>
								</td>
							</tr>
							<tr runat="server" id="trIsRealEstate" visible="false">
								<td style="text-align: right; vertical-align: top" nowrap="nowrap">Real Estate?</td>
								<td>									
									<asp:CheckBox runat="server" Text="Is this entry a real estate item?" ID="cbIsRealEstate" Checked='<%# Bind("IsRealEstate") %>' ForeColor="Green" AutoPostBack="true" />
								</td>
							</tr>
							<tr runat="server" id="trFullAddress" visible='<%# (Eval("IsRealEstate") ?? false)%>'>
								<%--<td style="text-align:right;vertical-align:top" nowrap="nowrap">Full Address</td>--%>
								<td colspan="2">
									<telerik:RadTextBox runat="server" ID="rtbFullAddress" Text='<%# Bind("FullAddress") %>' Width="250px" Visible="false"></telerik:RadTextBox>
									<uc1:address runat="server" id="ucAddress"
										unitnumber='<%# Bind("UnitNumber") %>'
										streetnumber='<%# Bind("StreetNumber") %>'
										streetname='<%# Bind("StreetName") %>'
										streettype='<%# Bind("StreetType") %>'
										suburbid='<%# Bind("Suburb") %>' />
								</td>
							</tr>

							<tr runat="server" id="trValue" visible='<%# ( (Eval("AssetLiabilityID") == DBNull.Value) || !(new WebApplicationForms.Models.MMAssetLiabilityType(Convert.ToInt32(Eval("AssetLiabilityID"))).ShowValue))%>'>
								<td style="text-align: right; vertical-align: top">Value</td>
								<td>									
									<telerik:RadNumericTextBox ID="rtbValue" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("Value") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right" AutoPostBack="true" OnTextChanged="rtbValue_TextChanged"></telerik:RadNumericTextBox>
									<div class="Note">What is the estimated value of this item?</div>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
					<td valign="top">
						<table style="border-collapse: separate; border-spacing: 0px; margin-left: 5px" runat="server" id="tblRight">
							<tr runat="server" id="trCreditCardLimit">
								<td style="text-align: right; vertical-align: top" nowrap="nowrap">Credit Limit</td>
								<td>
									<telerik:RadNumericTextBox ID="rtbCreditCardLimit" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("CreditCardLimit") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right"></telerik:RadNumericTextBox>
									<div class="Note">If applicable, is there a credit limit on this item?</div>
								</td>
							</tr>
							<tr runat="server" id="trOwing" visible='<%# ( (Eval("AssetLiabilityID") == DBNull.Value) || !(new WebApplicationForms.Models.MMAssetLiabilityType(Convert.ToInt32(Eval("AssetLiabilityID"))).ShowOwing))%>'>
								<td style="text-align: right; vertical-align: top" nowrap="nowrap">Owing</td>
								<td>
									<telerik:RadNumericTextBox ID="rtbAmountOwing" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("AmountOwing") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right" OnTextChanged="rtbAmountOwing_TextChanged" AutoPostBack="true"></telerik:RadNumericTextBox>
									<div class="Note">If applicable, are there any amounts owing on this item?</div>
								</td>
							</tr>	
							<tr runat="server" visible='<%# !((Eval("AmountOwing") == DBNull.Value) || (Convert.ToString(Eval("AmountOwing")) == ""))%>' id="trMonthly">
									<td style="text-align: right; vertical-align: top">Monthly Repayments</td>
									<td>
										<telerik:RadNumericTextBox ID="rtbMonthly" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("Monthly") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right"></telerik:RadNumericTextBox>
										<div class="Note">If there are amounts owing, how much are you repaying each month?</div>
									</td>
							</tr>
							
							<tr>
								<td style="text-align: right; vertical-align: top" nowrap="nowrap">Credit Provider</td>
								<td valign="top">
									<telerik:RadTextBox runat="server" ID="rtbLender" Text='<%# Bind("Lender") %>'></telerik:RadTextBox>
									<div class="Note" runat="server" visible="true">(if applicable)</div>
								</td>
							</tr>


				<tr runat="server" id="trUseForSecurity" visible='<%# (Eval("IsRealEstate") ?? false)%>'>
					<td style="text-align: right; vertical-align: top" nowrap="nowrap">Use for Security?</td>
					<td>
						<asp:CheckBox runat="server" Text="Is this the property that you would like to use as security or collateral for your loan?" ID="cbUseForSecurity" Checked='<%# Bind("UseForSecurity") %>' ForeColor="Green" OnCheckedChanged="cbUseForSecurity_CheckedChanged" AutoPostBack="true" />
					</td>
				</tr>
				<tr id="Tr1" runat="server" visible="false">
					<td style="text-align: right; vertical-align: top" nowrap="nowrap">Nett Value</td>
					<td>
						<telerik:RadNumericTextBox ID="rtbNettValue" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("NettValue") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right"></telerik:RadNumericTextBox>
					</td>
				</tr>
			</table>
			</td>
					</tr>
				</table>

			<div style="padding: 0 0 0 20px">
			<uc1:SecurityEdit runat="server" id="ucSecurityEdit" visible="false" SecurityID='<%# Eval("SecurityID") %>'/>
			</div>


					<div style="text-align: right; padding: 10px">
						<asp:LinkButton ID="btnCancel" Text="cancel" runat="server" CausesValidation="False" CommandName="Cancel"></asp:LinkButton>
						<asp:Button ID="btnUpdate" Text="update" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridEditFormInsertItem) %>'></asp:Button>
						<asp:Button ID="btnInsert" Text="add" runat="server" CommandName="Insert" Visible='<%# ((DataItem is Telerik.Web.UI.GridEditFormInsertItem)) %>'></asp:Button>
					</div>
		</div>
	</EditItemTemplate>
</asp:FormView>

<asp:SqlDataSource ID="sqlAssetLiability" runat="server" ConnectionString="<%$ ConnectionStrings:CRISMARConnectionString %>"
	OnSelecting="sqlAssetLiability_Selecting"	
	SelectCommand="SELECT [FinanceDetailID], [ApplicantID], [AssetLiabilityID], [Value], [Monthly], [AmountOwing], [NettValue], [Details], [CreditCardLimit], [IsRealEstate], [Lender], [FullAddress], [UseForSecurity], [UnitNumber], [StreetNumber], [StreetName], [StreetType], [Suburb], [SecurityID] FROM [AssetLiability] WHERE FinanceDetailID = @FinanceDetailID"	
	InsertCommand="INSERT INTO [AssetLiability] ([ApplicantID], [AssetLiabilityID], [Value], [Monthly], [AmountOwing], [NettValue], [Details], [CreditCardLimit], [IsRealEstate], [Lender], [FullAddress], [UseForSecurity], [UnitNumber], [StreetNumber], [StreetName], [StreetType], [Suburb], [SecurityID]) VALUES (@ApplicantID, @AssetLiabilityID, @Value, @Monthly, @AmountOwing, @NettValue, @Details, @CreditCardLimit, @IsRealEstate, @Lender, @FullAddress, @UseForSecurity, @UnitNumber, @StreetNumber, @StreetName, @StreetType, @Suburb, @SecurityID)"
	UpdateCommand="UPDATE [AssetLiability] SET [ApplicantID] = @ApplicantID, [AssetLiabilityID] = @AssetLiabilityID, [Value] = @Value, [Monthly] = @Monthly, [AmountOwing] = @AmountOwing, [NettValue] = @NettValue, [Details] = @Details, [CreditCardLimit] = @CreditCardLimit, [IsRealEstate] = @IsRealEstate, [Lender] = @Lender, [FullAddress] = @FullAddress, [UseForSecurity] = @UseForSecurity, [UnitNumber] = @UnitNumber, [StreetNumber] = @StreetNumber, [StreetName] = @StreetName, [StreetType] = @StreetType, [Suburb] = @Suburb, [SecurityID] = @SecurityID WHERE [FinanceDetailID] = @FinanceDetailID">
	<SelectParameters>
		<asp:Parameter Name="FinanceDetailID" Type="Int32"></asp:Parameter>
	</SelectParameters>	
	<InsertParameters>
		<asp:Parameter Name="ApplicantID" Type="Int32"></asp:Parameter>
		<asp:Parameter Name="AssetLiabilityID" Type="Int32"></asp:Parameter>
		<asp:Parameter Name="Value" Type="Decimal"></asp:Parameter>
		<asp:Parameter Name="Monthly" Type="Decimal"></asp:Parameter>
		<asp:Parameter Name="AmountOwing" Type="Decimal"></asp:Parameter>
		<asp:Parameter Name="NettValue" Type="Decimal"></asp:Parameter>
		<asp:Parameter Name="Details" Type="String"></asp:Parameter>
		<asp:Parameter Name="CreditCardLimit" Type="Decimal"></asp:Parameter>
		<asp:Parameter Name="IsRealEstate" Type="Boolean"></asp:Parameter>
		<asp:Parameter Name="Lender" Type="String"></asp:Parameter>
		<asp:Parameter Name="FullAddress" Type="String"></asp:Parameter>
		<asp:Parameter Name="UseForSecurity" Type="Boolean"></asp:Parameter>
		<asp:Parameter Name="UnitNumber" Type="String"></asp:Parameter>
		<asp:Parameter Name="StreetNumber" Type="String"></asp:Parameter>
		<asp:Parameter Name="StreetName" Type="String"></asp:Parameter>
		<asp:Parameter Name="StreetType" Type="String"></asp:Parameter>
		<asp:Parameter Name="Suburb" Type="Int32"></asp:Parameter>
		<asp:Parameter Name="SecurityID" Type="Int32" />
	</InsertParameters>
	<UpdateParameters>
		<asp:Parameter Name="ApplicantID" Type="Int32"></asp:Parameter>
		<asp:Parameter Name="AssetLiabilityID" Type="Int32"></asp:Parameter>
		<asp:Parameter Name="Value" Type="Decimal"></asp:Parameter>
		<asp:Parameter Name="Monthly" Type="Decimal"></asp:Parameter>
		<asp:Parameter Name="AmountOwing" Type="Decimal"></asp:Parameter>
		<asp:Parameter Name="NettValue" Type="Decimal"></asp:Parameter>
		<asp:Parameter Name="Details" Type="String"></asp:Parameter>
		<asp:Parameter Name="CreditCardLimit" Type="Decimal"></asp:Parameter>
		<asp:Parameter Name="IsRealEstate" Type="Boolean"></asp:Parameter>
		<asp:Parameter Name="Lender" Type="String"></asp:Parameter>
		<asp:Parameter Name="FullAddress" Type="String"></asp:Parameter>
		<asp:Parameter Name="UseForSecurity" Type="Boolean"></asp:Parameter>
		<asp:Parameter Name="UnitNumber" Type="String"></asp:Parameter>
		<asp:Parameter Name="StreetNumber" Type="String"></asp:Parameter>
		<asp:Parameter Name="StreetName" Type="String"></asp:Parameter>
		<asp:Parameter Name="StreetType" Type="String"></asp:Parameter>
		<asp:Parameter Name="Suburb" Type="Int32"></asp:Parameter>
		<asp:Parameter Name="SecurityID" Type="Int32" />
		<asp:Parameter Name="FinanceDetailID" Type="Int32"></asp:Parameter>
	</UpdateParameters>
</asp:SqlDataSource>