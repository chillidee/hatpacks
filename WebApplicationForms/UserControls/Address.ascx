﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Address.ascx.cs" Inherits="WebApplicationForms.UserControls.Address" %>

<table style="width: 100%;">
	<tr>
		<td class="half-block"  style="padding: 0;">
            <p class="label-block">Unit No.</p>
			<telerik:RadTextBox ID="rtbUnitNumber" runat="server" Width="100%"></telerik:RadTextBox>
		</td>
		<td class="half-block-2"  style="padding: 0;">
            <p class="label-block">Street No.</p>
			<telerik:RadTextBox ID="rtbStreetNumber" runat="server" Width="100%"></telerik:RadTextBox>
		</td>
	</tr>
	<tr runat="server" id="trStreetName">
		<td colspan="3" style="padding: 0;">
            <p class="label-block">Street Name</p>
			<telerik:RadTextBox ID="rtbStreetName" runat="server" Width="100%"></telerik:RadTextBox>
		</td>
	</tr>
	<tr>
		<td colspan="3" style="padding: 0;">
            <p class="label-block">Street Type</p>
			<telerik:RadComboBox ID="rcbStreetType" runat="server" DataSourceID="sqlStreetType" Width="100%" DataTextField="StreetType" DataValueField="StreetType" OffsetY="10" AllowCustomText="True" MarkFirstMatch="True"></telerik:RadComboBox>
		</td>
	</tr>
	<tr runat="server" id="trSuburb">
		<td colspan="3" style="padding: 0;">
            <p class="label-block">Suburb</p>
			<script>
				function rcbSuburb_OnClientItemsRequestingHandler(sender, eventArgs) {
					if (sender.get_text().length < 2) {
						eventArgs.set_cancel(true);
					}
				}
			</script>
			<telerik:RadComboBox ID="rcbSuburb" runat="server"
				DataSourceID="sqlPCode" DataTextField="Combo" DataValueField="PCodeID"
				ShowMoreResultsBox="True" EnableVirtualScrolling="True"
				EnableAutomaticLoadOnDemand="True" ItemsPerRequest="20" Height="200px" Width="100%"
				DropDownWidth="300px" MarkFirstMatch="True" OffsetY="10" EmptyMessage=""         
				>
			</telerik:RadComboBox>
      
		</td>
	</tr>
  <tr><td></td><td colspan="3"><asp:CustomValidator runat="server" ID="cvSuburb" OnServerValidate="cvSuburb_ServerValidate" Text="Suburb and Street Name is required."/></td></tr>
</table>
<asp:SqlDataSource ID="sqlPCode" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT [PCodeID], [Combo] FROM [vwPCode] ORDER BY [SUBURB], [PCODE] DESC"></asp:SqlDataSource>
<asp:SqlDataSource ID="sqlStreetType" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT * FROM [StreetType]"></asp:SqlDataSource>