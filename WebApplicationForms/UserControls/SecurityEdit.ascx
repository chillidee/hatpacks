﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecurityEdit.ascx.cs" Inherits="WebApplicationForms.UserControls.SecurityEdit" %>
<%@ Register Src="~/UserControls/Address.ascx" TagPrefix="uc1" TagName="Address" %>
	<asp:Label runat="server" ID="lblSecurityID" Visible="false" />
	<asp:Label runat="server" ID="lblApplicationNo" Text="" Visible="false" />
	<asp:FormView ID="theForm" runat="server" DataSourceID="sqlSecurity" DataKeyNames="SecurityID" OnItemCommand="theForm_ItemCommand" OnItemUpdated="theForm_ItemUpdated" OnItemInserted="theForm_ItemInserted" DefaultMode="Insert" Width="100%">
		<EditItemTemplate>
			<h3>Additional details about the property...</h3>
			<div align="center">
					<asp:TextBox runat="server" ID="tbApplicationNo" Visible="false" Text='<%# Bind("ApplicationNo") %>'/>
							<table style="border-collapse: separate; border-spacing: 0px; margin-left:20px">
								<tr runat="server" visible="false">
									<td style="text-align: right; vertical-align: top">Description</td>
									<td>
										<telerik:RadTextBox runat="server" ID="rtbSecurityDescription" Text='<%# Bind("SecurityDescription") %>' TextMode="MultiLine" Height="50px" Width="275px"></telerik:RadTextBox>
										<div class="Note">eg: House at Sydney, Land at Bathurst.</div>
									</td>
								</tr>
								<tr runat="server" visible="false" id="trPurchase">
									<td style="text-align: right; vertical-align: top" nowrap="nowrap">Purchase price</td>
									<td style="vertical-align:top">
										<telerik:RadNumericTextBox ID="rtbPurchasePrice" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("PurchasePrice") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right"></telerik:RadNumericTextBox>
									</td>
									<td style="width: 100%"><div class="Note">What is/was the estimated purchase price of this property?</div></td>
								</tr>
								<tr runat="server" id="trEstMarketValue" visible="false">
									<td style="text-align: right; vertical-align: top" nowrap="nowrap">Est market value</td>
									<td style="vertical-align:top">
										<telerik:RadNumericTextBox ID="rtbEstimatedMarketValue" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("EstimatedMarketValue") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right"></telerik:RadNumericTextBox>
									</td>
									<td style="width: 100%"><div class="Note">What is the estimated market value of this property?</div></td>
								</tr>
								<tr runat="server" visible="false">
									<td colspan="2">
												<uc1:address runat="server" id="ucAddress"
													unitnumber='<%# Bind("UnitNumber") %>'
													streetnumber='<%# Bind("StreetNumber") %>'
													streetname='<%# Bind("StreetName") %>'
													streettype='<%# Bind("StreetType") %>'
													suburbid='<%# Bind("Suburb") %>' />
									</td>
								</tr>
							</table>

							<table class="tight">
								<tr>
									<th>Type</th>
									<th>Provider</th>
									<th>Owing</th>
									<th>Account Number (optional)</th>
								</tr>
									<!--First-->
								<tr>
									<td><telerik:RadComboBox runat="server" ID="rcbFirstMortgageType" DataSourceID="sqlMortgageType" DataValueField="MortgageTypeDesc" DataTextField="MortgageTypeDesc" SelectedValue='<%# Bind("FirstMortgageType") %>' AppendDataBoundItems="true" OffsetY="10" MarkFirstMatch="True" EmptyMessage="(none)" Width="120px" /></td>
									<td><telerik:RadTextBox ID="rtbFirstMortgageLender" runat="server" Text='<%# Bind("FirstMortgageLender") %>'></telerik:RadTextBox></td>
									<td><telerik:RadNumericTextBox ID="rtbFirstMortgageOwing" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("FirstMortgageOwing") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right" AutoPostBack="True" OnTextChanged="Owing_TextChanged"></telerik:RadNumericTextBox></td>
									<td><telerik:RadTextBox ID="rtbFirstMortgageACNo" runat="server" Text='<%# Bind("FirstMortgageACNo") %>' Width="120px"></telerik:RadTextBox></td>
								</tr>

								<!--Second-->
								<tr>
									<td><telerik:RadComboBox runat="server" ID="rcbSecondMortgageType" DataSourceID="sqlMortgageType" DataValueField="MortgageTypeDesc" DataTextField="MortgageTypeDesc" SelectedValue='<%# Bind("SecondMortgageType") %>' AppendDataBoundItems="true" OffsetY="10" MarkFirstMatch="True" EmptyMessage="(none)" Width="120px" /></td>
									<td><telerik:RadTextBox ID="rtbSecondMortgageLender" runat="server" Text='<%# Bind("SecondMortgageLender") %>'></telerik:RadTextBox></td>
									<td><telerik:RadNumericTextBox ID="rtbSecondMortgageOwing" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("SecondMortgageOwing") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right" AutoPostBack="True" OnTextChanged="Owing_TextChanged"></telerik:RadNumericTextBox></td>
									<td><telerik:RadTextBox ID="rtbSecondMortgageACNo" runat="server" Text='<%# Bind("SecondMortgageACNo") %>' Width="120px"></telerik:RadTextBox></td>
								</tr>

								<!--Third-->
								<tr>
									<td><telerik:RadComboBox runat="server" ID="rcbThirdMortgageType" DataSourceID="sqlMortgageType" DataValueField="MortgageTypeDesc" DataTextField="MortgageTypeDesc" SelectedValue='<%# Bind("ThirdMortgageType") %>' AppendDataBoundItems="true" OffsetY="10" MarkFirstMatch="True" EmptyMessage="(none)" Width="120px" /></td>
									<td><telerik:RadTextBox ID="rtbThridMortgageLender" runat="server" Text='<%# Bind("ThridMortgageLender") %>'></telerik:RadTextBox></td>
									<td><telerik:RadNumericTextBox ID="rtbThirdMortgageOwing" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("ThridMortgageOwing") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right" AutoPostBack="True" OnTextChanged="Owing_TextChanged"></telerik:RadNumericTextBox></td>
									<td><telerik:RadTextBox ID="rtbThirdMortgageACNo" runat="server" Text='<%# Bind("ThirdMortgageACNo") %>' Width="120px"></telerik:RadTextBox></td>
								</tr>

								<!--Forth-->
								<tr>
									<td><telerik:RadComboBox runat="server" ID="rcbFourthMortgageType" DataSourceID="sqlMortgageType" DataValueField="MortgageTypeDesc" DataTextField="MortgageTypeDesc" SelectedValue='<%# Bind("FourthMortgageType") %>' AppendDataBoundItems="true" OffsetY="10" MarkFirstMatch="True" EmptyMessage="(none)" Width="120px" /></td>
									<td><telerik:RadTextBox ID="rtbFourthMortgageLender" runat="server" Text='<%# Bind("FourthMortgageLender") %>'></telerik:RadTextBox></td>
									<td><telerik:RadNumericTextBox ID="rtbFourthMortgageOwing" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("FourthMortgageOwing") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right" AutoPostBack="True" OnTextChanged="Owing_TextChanged"></telerik:RadNumericTextBox></td>
									<td><telerik:RadTextBox ID="rtbFourthMortgageACNo" runat="server" Text='<%# Bind("FourthMortgageACNo") %>' Width="120px"></telerik:RadTextBox></td>
								</tr>

								<!--Fifth-->
								<tr>
									<td><telerik:RadComboBox runat="server" ID="rcbFifthMortgageType" DataSourceID="sqlMortgageType" DataValueField="MortgageTypeDesc" DataTextField="MortgageTypeDesc" SelectedValue='<%# Bind("FifthMortgageType") %>' AppendDataBoundItems="true" OffsetY="10" MarkFirstMatch="True" EmptyMessage="(none)" Width="120px" /></td>
									<td><telerik:RadTextBox ID="rtbFifthMortgageLender" runat="server" Text='<%# Bind("FifthMortgageLender") %>'></telerik:RadTextBox></td>
									<td><telerik:RadNumericTextBox ID="rtbFifthMortgageOwing" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("FifthMortgageOwing") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right" AutoPostBack="True" OnTextChanged="Owing_TextChanged"></telerik:RadNumericTextBox></td>
									<td><telerik:RadTextBox ID="rtbFifthMortgageACNo" runat="server" Text='<%# Bind("FifthMortgageACNo") %>' Width="120px"></telerik:RadTextBox></td>
								</tr>

								<!--Sixth-->
								<tr>
									<td><telerik:RadComboBox runat="server" ID="rcbSixthMortgageType" DataSourceID="sqlMortgageType" DataValueField="MortgageTypeDesc" DataTextField="MortgageTypeDesc" SelectedValue='<%# Bind("SixthMortgageType") %>' AppendDataBoundItems="true" OffsetY="10" MarkFirstMatch="True" EmptyMessage="(none)" Width="120px" /></td>									
									<td><telerik:RadTextBox ID="rtbSixthMortgageLender" runat="server" Text='<%# Bind("SixthMortgageLender") %>'></telerik:RadTextBox></td>
									<td><telerik:RadNumericTextBox ID="rtbSixthMortgageOwing" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("SixthMortgageOwing") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right" AutoPostBack="True" OnTextChanged="Owing_TextChanged"></telerik:RadNumericTextBox></td>
									<td><telerik:RadTextBox ID="rtbSixthMortgageACNo" runat="server" Text='<%# Bind("SixthMortgageACNo") %>' Width="120px"></telerik:RadTextBox></td>
								</tr>

								<!--Seventh-->
								<tr>
									<td><telerik:RadComboBox runat="server" ID="rcbSeventhMortgageType" DataSourceID="sqlMortgageType" DataValueField="MortgageTypeDesc" DataTextField="MortgageTypeDesc" SelectedValue='<%# Bind("SeventhMortgageType") %>' AppendDataBoundItems="true" OffsetY="10" MarkFirstMatch="True" EmptyMessage="(none)" Width="120px" /></td>
									<td><telerik:RadTextBox ID="rtbSeventhMortgageLender" runat="server" Text='<%# Bind("SeventhMortgageLender") %>'></telerik:RadTextBox></td>
									<td><telerik:RadNumericTextBox ID="rtbSeventhMortgageOwing" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("SeventhMortgageOwing") %>' Type="Currency" Width="70px" EnabledStyle-HorizontalAlign="Right" AutoPostBack="True" OnTextChanged="Owing_TextChanged"></telerik:RadNumericTextBox></td>
									<td><telerik:RadTextBox ID="rtbSeventhMortgageACNo" runat="server" Text='<%# Bind("SeventhMortgageACNo") %>' Width="120px"></telerik:RadTextBox></td>
								</tr>

								<!--TOTAL-->
								<tr>
									<td></td>
									<td style="text-align:right"><b>Total Owing</b></td>
									<td style="text-align:right">
										<telerik:RadNumericTextBox ID="rtbTenderPrice" runat="server" DataType="System.Decimal" NumberFormat-DecimalDigits="0" Text='<%# Bind("TenderPrice") %>' Type="Currency" Width="70px" DisabledStyle-HorizontalAlign="Right" Enabled="false" Font-Bold="True" DisabledStyle-ForeColor="Black"></telerik:RadNumericTextBox>
								</tr>
								<tr>
									<td colspan="4" align="right">
<%--					<asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" OnClientClick="closeWindow();" Visible="false"></asp:LinkButton>
					<asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" Visible='<%# (theForm.CurrentMode == FormViewMode.Edit) %>' Width="80px"></asp:Button>
					<asp:Button ID="btnInsert" Text="Add" runat="server" CommandName="Insert" Visible='<%# (theForm.CurrentMode == FormViewMode.Insert) %>' Width="80px"></asp:Button>--%>
									</td>

								</tr>
							</table>

				
				</div>

		</EditItemTemplate>		
	</asp:FormView>
	<asp:Label ID="Label1" runat="server"></asp:Label>
	<asp:SqlDataSource ID="sqlSecurity" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>'
		DeleteCommand="DELETE FROM [Securities] WHERE [SecurityID] = @SecurityID"
		InsertCommand="INSERT INTO Securities(ApplicationNo, SecurityDescription, Suburb, EstimatedMarketValue, FirstMortgageLender, SecondMortgageLender, ThridMortgageLender, FirstMortgageOwing, SecondMortgageOwing, ThridMortgageOwing, FourthMortgageLender, FifthMortgageLender, FourthMortgageOwing, FifthMortgageOwing, UnitNumber, StreetNumber, StreetName, StreetType, FirstMortgageType, SecondMortgageType, ThirdMortgageType, FourthMortgageType, FifthMortgageType, SixthMortgageType, SeventhMortgageType, SixthMortgageOwing, SeventhMortgageOwing, SixthMortgageLender, SeventhMortgageLender, TenderPrice, PurchasePrice, FirstMortgageACNo, SecondMortgageACNo, ThirdMortgageACNo, FourthMortgageACNo, FifthMortgageACNo, SixthMortgageACNo, SeventhMortgageACNo) VALUES (@ApplicationNo, @SecurityDescription, @Suburb, @EstimatedMarketValue, @FirstMortgageLender, @SecondMortgageLender, @ThridMortgageLender, @FirstMortgageOwing, @SecondMortgageOwing, @ThridMortgageOwing, @FourthMortgageLender, @FifthMortgageLender, @FourthMortgageOwing, @FifthMortgageOwing, @UnitNumber, @StreetNumber, @StreetName, @StreetType, @FirstMortgageType, @SecondMortgageType, @ThirdMortgageType, @FourthMortgageType, @FifthMortgageType, @SixthMortgageType, @SeventhMortgageType, @SixthMortgageOwing, @SeventhMortgageOwing, @SixthMortgageLender, @SeventhMortgageLender, @TenderPrice, @PurchasePrice, @FirstMortgageACNo, @SecondMortgageACNo, @ThirdMortgageACNo, @FourthMortgageACNo, @FifthMortgageACNo, @SixthMortgageACNo, @SeventhMortgageACNo); SELECT @NewId = SCOPE_IDENTITY()"
		SelectCommand="SELECT SecurityID, ApplicationNo, SecurityDescription, Suburb, EstimatedMarketValue, FirstMortgageLender, SecondMortgageLender, ThridMortgageLender, FirstMortgageOwing, SecondMortgageOwing, ThridMortgageOwing, FourthMortgageLender, FifthMortgageLender, FourthMortgageOwing, FifthMortgageOwing, UnitNumber, StreetNumber, StreetName, StreetType, FirstMortgageType, SecondMortgageType, ThirdMortgageType, FourthMortgageType, FifthMortgageType, SixthMortgageType, SeventhMortgageType, SixthMortgageOwing, SeventhMortgageOwing, SixthMortgageLender, SeventhMortgageLender, TenderPrice, PurchasePrice, FirstMortgageACNo, SecondMortgageACNo, ThirdMortgageACNo, FourthMortgageACNo, FifthMortgageACNo, SixthMortgageACNo, SeventhMortgageACNo FROM Securities WHERE (SecurityID = @SecurityID)"
		UpdateCommand="UPDATE Securities SET SecurityDescription = @SecurityDescription, Suburb = @Suburb, EstimatedMarketValue = @EstimatedMarketValue, FirstMortgageLender = @FirstMortgageLender, SecondMortgageLender = @SecondMortgageLender, ThridMortgageLender = @ThridMortgageLender, FirstMortgageOwing = @FirstMortgageOwing, SecondMortgageOwing = @SecondMortgageOwing, ThridMortgageOwing = @ThridMortgageOwing, FourthMortgageLender = @FourthMortgageLender, FifthMortgageLender = @FifthMortgageLender, FourthMortgageOwing = @FourthMortgageOwing, FifthMortgageOwing = @FifthMortgageOwing, UnitNumber = @UnitNumber, StreetNumber = @StreetNumber, StreetName = @StreetName, StreetType = @StreetType, FirstMortgageType = @FirstMortgageType, SecondMortgageType = @SecondMortgageType, ThirdMortgageType = @ThirdMortgageType, FourthMortgageType = @FourthMortgageType, FifthMortgageType = @FifthMortgageType, SixthMortgageType = @SixthMortgageType, SeventhMortgageType = @SeventhMortgageType, SixthMortgageOwing = @SixthMortgageOwing, SeventhMortgageOwing = @SeventhMortgageOwing, SixthMortgageLender = @SixthMortgageLender, SeventhMortgageLender = @SeventhMortgageLender, TenderPrice = @TenderPrice, PurchasePrice = @PurchasePrice, FirstMortgageACNo = @FirstMortgageACNo, SecondMortgageACNo = @SecondMortgageACNo, ThirdMortgageACNo = @ThirdMortgageACNo, FourthMortgageACNo = @FourthMortgageACNo, FifthMortgageACNo = @FifthMortgageACNo, SixthMortgageACNo = @SixthMortgageACNo, SeventhMortgageACNo = @SeventhMortgageACNo  WHERE (SecurityID = @SecurityID)"
		OnSelecting="sqlSecurity_Selecting" OnInserted="sqlSecurity_Inserted">
		<DeleteParameters>
			<asp:Parameter Name="SecurityID" Type="Int32"></asp:Parameter>
		</DeleteParameters>
		<InsertParameters>
			<asp:Parameter Name="ApplicationNo" Type="Int32"></asp:Parameter>
			<asp:Parameter Name="SecurityDescription" Type="String"></asp:Parameter>
			<asp:Parameter Name="Suburb" Type="Int32"></asp:Parameter>
			<asp:Parameter Name="EstimatedMarketValue" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="FirstMortgageLender" Type="String"></asp:Parameter>
			<asp:Parameter Name="SecondMortgageLender" Type="String"></asp:Parameter>
			<asp:Parameter Name="ThridMortgageLender" Type="String"></asp:Parameter>
			<asp:Parameter Name="FirstMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="SecondMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="ThridMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="FourthMortgageLender" Type="String"></asp:Parameter>
			<asp:Parameter Name="FifthMortgageLender" Type="String"></asp:Parameter>
			<asp:Parameter Name="FourthMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="FifthMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="UnitNumber" Type="String"></asp:Parameter>
			<asp:Parameter Name="StreetNumber" Type="String"></asp:Parameter>
			<asp:Parameter Name="StreetName" Type="String"></asp:Parameter>
			<asp:Parameter Name="StreetType" Type="String"></asp:Parameter>
			<asp:Parameter Name="FirstMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="SecondMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="ThirdMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="FourthMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="FifthMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="SixthMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="SeventhMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="SixthMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="SeventhMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="SixthMortgageLender" Type="String"></asp:Parameter>			
			<asp:Parameter Name="SeventhMortgageLender" Type="String"></asp:Parameter>			
			<asp:Parameter Name="TenderPrice" Type="Decimal"></asp:Parameter>			
			<asp:Parameter Name="PurchasePrice" Type="Decimal"></asp:Parameter>			
			<asp:Parameter Name="FirstMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="SecondMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="ThirdMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="FourthMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="FifthMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="SixthMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="SeventhMortgageACNo" Type="String"></asp:Parameter>			
			<asp:Parameter Direction="Output" Name="NewId" Type="Int32" />
		</InsertParameters>
		<UpdateParameters>
			<asp:Parameter Name="SecurityDescription" Type="String"></asp:Parameter>
			<asp:Parameter Name="Suburb" Type="Int32"></asp:Parameter>
			<asp:Parameter Name="EstimatedMarketValue" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="FirstMortgageLender" Type="String"></asp:Parameter>
			<asp:Parameter Name="SecondMortgageLender" Type="String"></asp:Parameter>
			<asp:Parameter Name="ThridMortgageLender" Type="String"></asp:Parameter>
			<asp:Parameter Name="FirstMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="SecondMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="ThridMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="FourthMortgageLender" Type="String"></asp:Parameter>
			<asp:Parameter Name="FifthMortgageLender" Type="String"></asp:Parameter>
			<asp:Parameter Name="FourthMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="FifthMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="UnitNumber" Type="String"></asp:Parameter>
			<asp:Parameter Name="StreetNumber" Type="String"></asp:Parameter>
			<asp:Parameter Name="StreetName" Type="String"></asp:Parameter>
			<asp:Parameter Name="StreetType" Type="String"></asp:Parameter>
			<asp:Parameter Name="FirstMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="SecondMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="ThirdMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="FourthMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="FifthMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="SixthMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="SeventhMortgageType" Type="String"></asp:Parameter>
			<asp:Parameter Name="SixthMortgageOwing" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="SeventhMortgageOwing" Type="Decimal"></asp:Parameter>			
			<asp:Parameter Name="SixthMortgageLender" Type="String"></asp:Parameter>			
			<asp:Parameter Name="SeventhMortgageLender" Type="String"></asp:Parameter>
			<asp:Parameter Name="TenderPrice" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="PurchasePrice" Type="Decimal"></asp:Parameter>
			<asp:Parameter Name="FirstMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="SecondMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="ThirdMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="FourthMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="FifthMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="SixthMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="SeventhMortgageACNo" Type="String"></asp:Parameter>
			<asp:Parameter Name="SecurityID" Type="Int32"></asp:Parameter>
		</UpdateParameters>
		<SelectParameters>
			<asp:Parameter Name="SecurityID" Type="Int32" />
		</SelectParameters>
	</asp:SqlDataSource>
		<asp:SqlDataSource runat="server" ID="sqlMortgageType" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT [MortgageTypeDesc] FROM [MortgageType]"></asp:SqlDataSource>
	