﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;
using Telerik.Web.UI;
using System.ComponentModel;

namespace WebApplicationForms.UserControls
{
	[Bindable(true,BindingDirection.TwoWay), Category("Data") ]
	public partial class Address : System.Web.UI.UserControl
	{
		private string _UnitNumber;
		private string _StreetNumber;
		private string _StreetName;
		private string _StreetType;

		private Nullable<int> _SuburbID;
		private MMPCode _pc;
		private string _Combo;
		private string _Suburb;
		private string _State;
		private string _PostCode;
		
		public object UnitNumber {
			get {	_UnitNumber = rtbUnitNumber.Text; return _UnitNumber; }
			set { _UnitNumber = value.ToString(); rtbUnitNumber.Text = _UnitNumber; }
		}
		public object StreetNumber
		{
			get { _StreetNumber = rtbStreetNumber.Text; return _StreetNumber; }
			set { _StreetNumber = value.ToString(); rtbStreetNumber.Text = _StreetNumber; }
		}
		public object StreetName
		{
			get { _StreetName = rtbStreetName.Text; return _StreetName; }
			set { _StreetName = value.ToString(); rtbStreetName.Text = _StreetName; }
		}
		public object StreetType
		{
			get { _StreetType = rcbStreetType.SelectedValue; return _StreetType; }
			set { _StreetType = value.ToString(); rcbStreetType.SelectedValue = _StreetType; }
		}
		public object SuburbID
		{
			get { _SuburbID = (Utils.EmptyToNull(rcbSuburb.SelectedValue) as Int32?) ?? null; return _SuburbID; }
			set {
				_SuburbID = (value as Int32?) ?? null;
				_pc = new MMPCode(_SuburbID);
				_Combo = _pc.SuburbStatePostCode;
				_Suburb = _pc.SUBURB;
				_State = _pc.STATE;
				_PostCode = _pc.PCODE;
				rcbSuburb.SelectedValue = _SuburbID.ToString();
				rcbSuburb.Text = _Combo;

			}
		}
		public string Combo { get { return _Combo; } }
		public string Suburb { get { return _Suburb; } }
		public string State { get { return _State; } }
		public string PostCode { get { return _PostCode; } }

		public string FieldCssClass { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				//populating controls...
				/*rtbUnitNumber.Text = UnitNumber;
				rtbStreetNumber.Text = StreetNumber;
				rtbStreetName.Text = StreetName;
				rcbStreetType.SelectedValue = StreetType;
				rcbStreetType.Text = StreetType;
				rcbSuburb.SelectedValue = SuburbID.ToString();
				rcbSuburb.Text = Combo;*/
			}
		}

    protected void cvSuburb_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = ((!string.IsNullOrEmpty(rtbStreetName.Text) && !string.IsNullOrEmpty(rcbSuburb.SelectedValue)) ||
        (string.IsNullOrEmpty(rtbStreetName.Text) && string.IsNullOrEmpty(rcbSuburb.SelectedValue)));
      if (args.IsValid)
      {
        trStreetName.BgColor = "";
        trSuburb.BgColor = "";
      }
      else 
      {
        trStreetName.BgColor = "#FFD7D7";
        trSuburb.BgColor = "#FFD7D7";
      }
    }
	}
}