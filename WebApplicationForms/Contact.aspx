﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WebApplicationForms.Contact" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
        <h2>Ways you can reach us.</h2>
    </hgroup>

	<section class="contact">
        <header>
            <h3>Your account manager:</h3>
        </header>
        <p>            
          <span><asp:Label runat="server" ID="lblSalesManager" /></span>
        </p>
    </section>

    <section class="contact">
        <header>
            <h3>Phone:</h3>
        </header>
        <p>            
            <span><asp:Label runat="server" ID="lblPhoneNumber" /></span>
        </p>
    </section>

    <section class="contact">
        <header>
            <h3>Email:</h3>
        </header>
        <p>            
          <span><asp:HyperLink runat="server" ID="hlEmail" /></span>
        </p>
    </section>

    <section class="contact">
        <header>
            <h3>Postal Address:</h3>
        </header>
        <p>
					<asp:Label runat="server" ID="lblAddressLine1"/><br />
					<asp:Label runat="server" ID="lblSuburb"/> <asp:Label runat="server" ID="lblPostCode"/> <asp:Label runat="server" ID="lblState"/>
        </p>
    </section>
</asp:Content>