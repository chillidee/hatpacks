﻿using EchosignRESTClient.Models;
using EchosignRESTClient.Parameters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms
{
    public partial class TestADW : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                // Methods to be tested
                //AddComment();
                //AnswerQuestionnaire();
                //PostAnswerQuestionnaire();
                //Post();
                //AdobeEchoSign();
                //PostBudgetCalculator();
                //GetBudgetCalculator();
            }
            catch (Exception ex)
            {

                lblerror.Text = ex.ToString();
            }
        }

        private void PostBudgetCalculator()
        {
            var budget = new WebApplicationForms.Models.BudgetCalculator()
            {
                BudgetCalculatorID = 1,
                ApplicantID = 2317,
                Items = new List<BudgetCalculatorItem>() { 
                    new BudgetCalculatorItem() { 
                        ItemTypeID = 1, ItemFrequency = "Monthly", ItemValue = 1600 
                    },
                    new BudgetCalculatorItem() { 
                        ItemTypeID = 10, ItemFrequency = "Weekly", ItemValue = 1000 
                    }                    
                },
                TotalIncome = 1600,
                TotalExpenses = 4000,
                TotalSurplusDeficit = 2400
            };

            if (budget != null)
            {
                if (budget.BudgetCalculatorID == 0)
                    budget.Create();
                else
                    budget.Save();
            }
        }

        private void GetBudgetCalculator()
        {
            // Existing App
            string guid = "18ED8EB7F1114083B3ED2C00A7FB1991"; 
            bool app = true;

            // Existing Lead
            //string guid = "5B69E533-FF10-4455-A168-C82F244A6EAE";
            //bool app = false;

            var budget = new WebApplicationForms.Models.BudgetCalculator();
            budget.Get(guid, app);
            budget.Save();
        }

        private void AdobeEchoSign()
        {
            string GUID = "EB4C6F3088064B6B8F9641B5590037B9";
            CCApplicant cc = new CCApplicant(GUID);

            Session["CCApplicant"] = cc;
            //checking client's email...
            if (string.IsNullOrEmpty(cc.Email))
            {
                lblerror.Text = "Recipient's email is empty. Unable to send.";
            }
            else
            {
                /*EmailAgent ea = new EmailAgent();
                ea.SendWebApplicationFormEmailCC(cc);
                //Response.Write(ea.Body);
                if (ea.Sent)*/

                MMUser ApplicationManager = new MMUser(cc.ApplicationManager);

                // ClientApp and JsonData location
                String clientAppPath = Path.Combine(HttpRuntime.AppDomainAppPath, "AdobeEchoSignClient");
                String jsonDataPath = Path.Combine(clientAppPath, "JsonDataParameter");

                //Prepare data
                JsonData jsonData = new JsonData();
                jsonData.ccs = new List<string> { ApplicationManager.Email };
                jsonData.memberInfos = new List<ParticipantInfo>() 
                { 
                    new ParticipantInfo() { email = cc.Email }                    
                };
                jsonData.mergeFieldInfo = new List<MergefieldInfo>()
                {
                    new MergefieldInfo() { defaultValue = cc.FirstName + " " + cc.Surname, fieldName = "customerName" },
                    new MergefieldInfo() { defaultValue = cc.ApplicantID.ToString(), fieldName = "customerReference" },
                    new MergefieldInfo() { defaultValue = cc.Surname, fieldName = "customerSurname" },
                    new MergefieldInfo() { defaultValue = cc.FirstName, fieldName = "customerGivenName" },
                    new MergefieldInfo() { defaultValue = cc.Mobile, fieldName = "customerMobile" },
                    new MergefieldInfo() { defaultValue = cc.FirstName + " " + cc.Surname, fieldName = "accountHolderName" },
                    new MergefieldInfo() { defaultValue = cc.FirstName + " " + cc.Surname, fieldName = "customerName" }
                };

                //Just in case of not existing the json param repository
                if (!Directory.Exists(jsonDataPath))
                    Directory.CreateDirectory(jsonDataPath);

                //Create file named in this format "JsonDataParameter/yyyyMMdd_HHmmss_ApplicantID_GUID.json"
                String jsonDataFilePath = Path.Combine(jsonDataPath, string.Format("{0}_{1}_{2}.json", DateTime.Now.ToString("yyyyMMdd_HHmmss"), cc.ApplicantID, GUID));

                using (StreamWriter sw = new StreamWriter(jsonDataFilePath))
                {
                    sw.Write(JsonConvert.SerializeObject(jsonData));
                }

                //Call the ClientApp that consumes Adobe Echo Sign API - necessary to inform action & json param file
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = Path.Combine(clientAppPath, "ClientApp.exe");
                startInfo.Arguments = string.Format("action={0} json=\"{1}\"", "SendAgreement", jsonDataFilePath);
                Process.Start(startInfo);

                if (true)
                {
                    lblerror.Text = "Email sent to " + cc.FirstName + " " + cc.Surname + ".<br/>Email address: " + cc.Email + "<br/><br/>" +
                                    "A copy (by CC) was also sent to the Application Manager " + cc.ApplicationManager + " (" + ApplicationManager.Email + ").";
                }
            }
        }

        private void Post()
        {
            var enquiry = new WebApplicationForms.Models.EnquiryForm()
            {

                loanAmount = "500",
                typeOfLoan = "126",
                hasProperty = "False",
                realEstateValue = "1000000",
                balanceOwing = "10",
                firstName = "Enquiry Form - Test",
                mobileNumber = "0411880121",
                emailAddress = "test@alc.com.au",
                state = "NSW",
                postCode = "2077",
                comments = "This is a test, <br>Please ignore<br>Line3",
                platform = "desktop",
                cid = "DEV",
                UserHasDefaults = "No",
                UserLoanOver7K = "Yes",
                LenderID = "",
                haveDeposit = "false",
                suburb = "Asquith",
                referral = "abc"/*,
                CallStatusId = "14"*/
            };
            enquiry.Validate();
            if (IsValid)
                enquiry.Save();
        }

        private void AddComment()
        {
            var enquiry = new WebApplicationForms.Models.EnquiryForm()
            {
                CallID = "937557",
                comments = "Congratulations, you've just added a comment! Keep up the good work."
            };
            enquiry.AddComment();
        }

        private void AnswerQuestionnaire()
        {
            var enquiry = new WebApplicationForms.Models.EnquiryForm()
            {
                CallID = ""
            };
            enquiry.AnswerQuestionnaire(2, 3, "boni@chillidee.com.au", "Test");
        }

        private async void PostAnswerQuestionnaire()
        {

            HttpClient client = new HttpClient();

            var values = new Dictionary<string, string>
            {
               { "callID", null },
               { "reference", "boni@chillidee.com.au" },
               { "questionID", "2" },
               { "answerID", "3" },
               { "additionalInfo", "Test" },
            };

            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync("http://localhost:64756/api/enquiryform/postAnswerQuestionnaire", content);
            var responseString = await response.Content.ReadAsStringAsync();
        }


    };
}