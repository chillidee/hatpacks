﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using WebApplicationForms.Models;

namespace WebApplicationForms
{
	public partial class _Default : PageWithFieldReplace
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				//getting the querystring...
				string strGUID = Request.QueryString["ID"];
				if (string.IsNullOrEmpty(strGUID))
				{
					Response.Redirect("/Errors/IDRequired.aspx");
					return;
				}
				//we have a guid, lets load data!...
				try
				{
					MMApplication a = new MMApplication(strGUID);
					a.LoadApplicants();
					a.LoadCompany();
					Session["Application"] = a;
					Session["Company"] = a.Company;

					litFirstname.Text = a.MMApplicant1.FirstName;
					litFirstname2.Text = a.MMApplicant1.FirstName;
					lblCompanyName.Text = a.Company.Company;
					lblPhone.Text = a.Company.Phone;
					lblSalesManager.Text = a.SalesManager;
					lblSalesManager2.Text = a.SalesManager;
					cb1.Checked = a.AgreedToTermsAndConditions;
					if (!a.AgreedToTermsAndConditions)
					{ cb2.Checked = true; }
					else{ cb2.Checked = a.ReceiveFurtherInformationTicked; }

					//find out if this application can be loaded, depending on [Status].[WebApplicationEnabled] = 1
					Boolean WebApplicationEnabled = false;
					using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
					{
						SqlCommand cmd = new SqlCommand("SELECT * FROM Status WHERE StatusID = " + a.StatusID.ToString(), conn);
						conn.Open();
						SqlDataReader dr = cmd.ExecuteReader();
						if (dr.Read())
						{
							 WebApplicationEnabled = (Boolean)dr["WebApplicationEnabled"];
						}
						if (!WebApplicationEnabled)
						{
							Response.Redirect("~/ApplicationClosed.aspx");
						}
					}


				}
				catch
				{
					Response.Write("ID invalid.");
					Response.End();
					return;
				}
			}
		}


		protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
		{
			args.IsValid = (cb1.Checked);
			if (!cb1.Checked)
			{
				tdAccept1.BgColor = "#FFD7D7";
			}
			else
			{
				tdAccept1.BgColor = "";
			}
		}

		protected void btnGetStartedNow_Click(object sender, ImageClickEventArgs e)
		{
			if (Page.IsValid)
			{
				if (Session["Application"] != null)
				{
					if (cb2.Checked)
					{
						MMApplication a = (MMApplication)Session["Application"];						
						if (!a.AgreedToTermsAndConditions)
						{
							a.AgreedToTermsAndConditions = cb1.Checked;
							a.AgreedToTermsAndConditionsDate = DateTime.Now;
							a.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
						}
						a.ReceiveFurtherInformationTicked = cb2.Checked;
						a.Save();
					}
					Response.Redirect("ApplicationForm-01.aspx");
				}
				else
				{ }
			}			
		}

	}
}
