﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplicationForms.Models;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json.Linq;

namespace WebApplicationForms.Controllers
{
    //[RoutePrefix("api/enquiryform")]
    public class EnquiryFormController : ApiController
    {
        // GET api/company
        public string GetAll()
        {
            return "";
        }
        // GET api/EnquiryForm/mobile/alc

        [HttpGet]
        [ActionName("mobile")]
        public EnquiryFormInit GetMobile(string cid)
        {
            //return new string[] { cid, "mobile!" };

            EnquiryFormInit init = new EnquiryFormInit();
            MMCompany c = new MMCompany(cid);

            if (c.WebEnquiryFormShowTitle_M) init.fields.Add("title");
            if (c.WebEnquiryFormShowFirstname_M) init.fields.Add("firstName");
            if (c.WebEnquiryFormShowLastname_M) init.fields.Add("lirstName");
            if (c.WebEnquiryFormShowLoanAmount_M) init.fields.Add("loanAmount");
            if (c.WebEnquiryFormShowLoanType_M) init.fields.Add("typeOfLoan");
            if (c.WebEnquiryFormShowHasProperty_M) { init.fields.Add("hasProperty"); init.fields.Add("haveDeposit"); init.fields.Add("realEstateValue"); init.fields.Add("balanceOwing"); }
            if (c.WebEnquiryFormShowMobile_M) init.fields.Add("mobileNumber");
            if (c.WebEnquiryFormShowLandline_M) init.fields.Add("landLineNumber");
            if (c.WebEnquiryFormShowEmail_M) init.fields.Add("emailAddress");
            if (c.WebEnquiryFormShowSuburb_M) init.fields.Add("suburb");
            if (c.WebEnquiryFormShowStatePostCode_M) { init.fields.Add("state"); init.fields.Add("postCode"); }
            if (c.WebEnquiryFormShowWhereDidYouHear_M) init.fields.Add("referral");
            if (c.WebEnquiryFormShowComments_M) init.fields.Add("comments");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT clp.LoanPurposeID AS LoanPurposeID, COALESCE(AlternativeLoanPurpose, lp.LoanPurpose) AS TypeOfLoan FROM CompanyLoanPurpose clp INNER JOIN LoanPurpose lp ON clp.LoanPurposeID = lp.LoanPurposeID WHERE clp.CompanyID = " + c.CompanyID + " AND AppearOnWebEnquiryForm = 1 ORDER BY clp.SortOrder", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    init.typeOfLoan.Add(
                        (int)(dr["LoanPurposeID"]),
                        dr["TypeOfLoan"].ToString()
                    );
                }
            }
            init.sucessURL = (c.WebEnquiryFormSuccessURL == string.Empty) ? string.Empty : c.WebEnquiryFormSuccessURL;
            init.failureURL = (c.WebEnquiryFormFailureURL == string.Empty) ? string.Empty : c.WebEnquiryFormFailureURL;

            return init;

        }

        // GET api/company/desktop/alc
        [HttpGet]
        [ActionName("desktop")]
        public EnquiryFormInit GetDesktop(string cid)
        {
            //return new string[] { cid, "desktop!" };            

            EnquiryFormInit init = new EnquiryFormInit();
            MMCompany c = new MMCompany(cid);

            if (c.WebEnquiryFormShowTitle) init.fields.Add("title");
            if (c.WebEnquiryFormShowFirstname) init.fields.Add("firstName");
            if (c.WebEnquiryFormShowLastname) init.fields.Add("lastName");
            if (c.WebEnquiryFormShowLoanAmount) init.fields.Add("loanAmount");
            if (c.WebEnquiryFormShowLoanType) init.fields.Add("typeOfLoan");
            if (c.WebEnquiryFormShowHasProperty) { init.fields.Add("hasProperty"); init.fields.Add("haveDeposit"); init.fields.Add("realEstateValue"); init.fields.Add("balanceOwing"); }
            if (c.WebEnquiryFormShowMobile) init.fields.Add("mobileNumber");
            if (c.WebEnquiryFormShowLandline) init.fields.Add("landLineNumber");
            if (c.WebEnquiryFormShowEmail) init.fields.Add("emailAddress");
            if (c.WebEnquiryFormShowSuburb) init.fields.Add("suburb");
            if (c.WebEnquiryFormShowStatePostCode) { init.fields.Add("state"); init.fields.Add("postCode"); }
            if (c.WebEnquiryFormShowWhereDidYouHear) init.fields.Add("referral");
            if (c.WebEnquiryFormShowComments) init.fields.Add("comments");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT clp.LoanPurposeID AS LoanPurposeID, COALESCE(AlternativeLoanPurpose, lp.LoanPurpose) AS TypeOfLoan FROM CompanyLoanPurpose clp INNER JOIN LoanPurpose lp ON clp.LoanPurposeID = lp.LoanPurposeID WHERE clp.CompanyID = " + c.CompanyID + " AND AppearOnWebEnquiryForm = 1 ORDER BY clp.SortOrder", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    init.typeOfLoan.Add(
                        (int)(dr["LoanPurposeID"]),
                        dr["TypeOfLoan"].ToString()
                    );
                }
                init.sucessURL = (c.WebEnquiryFormSuccessURL == string.Empty) ? string.Empty : c.WebEnquiryFormSuccessURL;
                init.failureURL = (c.WebEnquiryFormFailureURL == string.Empty) ? string.Empty : c.WebEnquiryFormFailureURL;
                return init;
            }
        }

        [HttpPost]
        [ActionName("post")]
        public HttpResponseMessage Post([FromBody]Models.EnquiryForm e)
        {
            EnquiryFormReturn er = new EnquiryFormReturn();

            if (e == null)
            {
                er.status = "FAILED";
                er.errorMessage = new string[] { "The JSON message is NOT well formed. Unable to translate the received JSON message." };
                return Request.CreateResponse(er);
            }
            else
            {

                e.Validate();
                if (e.isValid)
                {
                    e.Save();
                    if (e.isValid)
                    {
                        er.status = "SUCCESS";
                        er.ID = e.CallID;
                    }
                    else
                    {
                        er.status = "FAILED";
                        er.ID = null;
                    }
                }
                else
                {
                    er.status = "FAILED";
                    er.ID = null;
                }
                er.errorMessage = e.errorMessage;
                return Request.CreateResponse(er);
            }
        }

        [HttpPost]
        [ActionName("addComment")]
        public HttpResponseMessage AddComment([FromBody]Models.EnquiryForm e)
        {
            EnquiryFormReturn er = new EnquiryFormReturn();

            if (e == null)
            {
                er.status = "FAILED";
                er.errorMessage = new string[] { "The JSON message is NOT well formed. Unable to translate the received JSON message." };
                return Request.CreateResponse(er);
            }
            else
            {
                e.AddComment();
                er.status = "SUCCESS";
                return Request.CreateResponse(er);
            }
        }

        [HttpPost]
        [ActionName("postBudgetCalculator")]
        public HttpResponseMessage PostBudgetCalculator(HttpRequestMessage request)
        {
            Models.BudgetCalculator b = null;

            try
            {
                var jsonContent = request.Content.ReadAsStringAsync().Result;
                b = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.BudgetCalculator>(jsonContent);
            }
            catch (Exception) { }
            
            EnquiryFormReturn er = new EnquiryFormReturn();

            if (b == null || (b.CallID == 0 && b.ApplicantID == 0))
            {
                er.status = "FAILED";
                er.errorMessage = new string[] { "The JSON message is NOT well formed. Unable to translate the received JSON message." };
                return Request.CreateResponse(er);
            }
            else
            {
                if (b.BudgetCalculatorID == 0)
                    b.Create();
                else
                    b.Save();

                er.ID = b.BudgetCalculatorID.ToString();
                er.status = "SUCCESS";
                return Request.CreateResponse(er);
            }
        }

        [HttpGet]
        [ActionName("getBudgetCalculator")]
        public BudgetCalculator GetBudgetCalculator(string guid, bool app)
        {
            BudgetCalculator b = new BudgetCalculator();

            if (!string.IsNullOrEmpty(guid))
                b.Get(guid, app);

            return b;
        }

        // This method is used to answers emails surveys and add comments into their leads.        
        // Generally Javascript is not supported in emails, so let's keep it simple the way of receiving parameters.
        // Usage: api/enquiryform/answerQuestionnaire?p={CallID}-{QuestionID}-{AnswerID}

        private const int Param_CallID = 0;
        private const int Param_QuestionID = 1;
        private const int Param_AnswerID = 2;
        private const int Param_Reference = 3;
        private const int Param_AdditionalInfo = 4;

        [HttpGet]
        [ActionName("answerQuestionnaire")]
        public string AnswerQuestionnaire(string p)
        {
            string[] parameters = p.Split('-');

            var e = new WebApplicationForms.Models.EnquiryForm()
            {
                CallID = parameters[Param_CallID]
            };

            if (parameters.Length > 3)
            {
                e.AnswerQuestionnaire(int.Parse(parameters[Param_QuestionID]), int.Parse(parameters[Param_AnswerID]), parameters[Param_Reference], parameters[Param_AdditionalInfo]);
            }
            else
            {
                e.AnswerQuestionnaire(int.Parse(parameters[Param_QuestionID]), int.Parse(parameters[Param_AnswerID]));
            }

            return "SUCCESS";
        }

        [HttpPost]
        [ActionName("postAnswerQuestionnaire")]
        public HttpResponseMessage AnswerQuestionnaire([FromBody]Models.Questionnaire q)
        {
            try
            {
                var e = new WebApplicationForms.Models.EnquiryForm()
                {
                    CallID = q.callID
                };

                e.AnswerQuestionnaire(int.Parse(q.questionID), int.Parse(q.answerID), q.reference, q.additionalInfo);

                return Request.CreateResponse("SUCCESS");
            }
            catch (Exception error)
            {
                return Request.CreateResponse(error.Message);
            }
        }
    }
}
