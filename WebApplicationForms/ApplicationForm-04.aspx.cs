﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;


namespace WebApplicationForms
{
	public partial class WebForm_A04 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				//initialising controls...
				RadDatePickerDateOfBirth.MaxDate = DateTime.Today.AddYears(-18);
				RadDatePickerDateOfBirth.MinDate = DateTime.Today.AddYears(-100);
				RadDatePickerDriversLicenceExpiry.MaxDate = DateTime.Today.AddYears(6);
				RadDatePickerDriversLicenceExpiry.MinDate = DateTime.Today.AddYears(-10);

				RadDatePickerDateOfBirth2.MaxDate = DateTime.Today.AddYears(-18);
				RadDatePickerDateOfBirth2.MinDate = DateTime.Today.AddYears(-100);
				RadDatePickerDriversLicenceExpiry2.MaxDate = DateTime.Today.AddYears(6);
				RadDatePickerDriversLicenceExpiry2.MinDate = DateTime.Today.AddYears(-10);				

				//getting data...
				MMApplication a;
				if ((Session["Application"] == null) && HttpContext.Current.Request.IsLocal)
				{
					a = new MMApplication("CFE209842ECB48F4AD1E47BB40529E1F");
					a.LoadApplicants();
					Session["Application"] = a;
				}
				else
				{ a = (MMApplication)Session["Application"]; }
				
				MMApplicant ap1 = a.MMApplicant1;

				//Populating controls...				
				litAp1Firstname1.Text = ap1.FirstName;
				litAp1Firstname2.Text = ap1.FirstName;
				RadDatePickerDateOfBirth.SelectedDate = ap1.DateOfBirth;
				RadComboBoxMaritalStatus.SelectedValue = ap1.MaritalStatus;
				RadNumericNoOfDependents.Value = ap1.NoOfDependents;
				RadTextBoxAgesOfDependents.Text = ap1.AgesOfDependents;
				RadComboBoxResidencyStatus.SelectedValue = ap1.ResidencyStatus;
				RadTextBoxMothersMaidenName.Text = ap1.MothersMaidenName;
				RadTextBoxDriversLicenceNo.Text = ap1.DriversLicenceNo;
				RadDatePickerDriversLicenceExpiry.SelectedDate = ap1.DriversLicenceExpiry;
				rcbDriversLicenceState.SelectedValue = Convert.ToString(ap1.DriversLicenceState);

				rtbPersonalRefRelationship.Text = ap1.PersonalRefRelationship;
				rcbPersonalRefTitle.SelectedValue = ap1.PersonalRefTitle;
				rtbPersonalRefName.Text = ap1.PersonalRefName;
				rtbPersonalRefSurname.Text = ap1.PersonalRefSurname;
				rtbPersonalRefAddLine1.Text = ap1.PersonalRefAddLine1;
				rtbPersonalRefAddLine2.Text = ap1.PersonalRefAddLine2;
				rcbPersonalRefSuburb.SelectedValue = ap1.PersonalRefSuburb.ToString();
				rcbPersonalRefSuburb.Text = ap1.PersonalRefSuburbText;
				rtbPersonalRefWorkPhone.Text = ap1.PersonalRefWorkPhone;
				rtbPersonalRefHomePhone.Text = ap1.PersonalRefHomePhone;



				try
				{
					if (a.MMApplicant2 != null)
					{
						MMApplicant ap2 = a.MMApplicant2;
						//populating controls...
						litAp2Firstname1.Text = ap2.FirstName;
						litAp2Firstname2.Text = ap2.FirstName;
						RadDatePickerDateOfBirth2.SelectedDate = ap2.DateOfBirth;
						RadComboBoxMaritalStatus2.SelectedValue = ap2.MaritalStatus;
						RadNumericNoOfDependents2.Value = ap2.NoOfDependents;
						RadTextBoxAgesOfDependents2.Text = ap2.AgesOfDependents;
						RadComboBoxResidencyStatus2.SelectedValue = ap2.ResidencyStatus;
						RadTextBoxMothersMaidenName2.Text = ap2.MothersMaidenName;
						RadTextBoxDriversLicenceNo2.Text = ap2.DriversLicenceNo;
						RadDatePickerDriversLicenceExpiry2.SelectedDate = ap2.DriversLicenceExpiry;
						rcbDriversLicenceState2.SelectedValue = Convert.ToString(ap2.DriversLicenceState);

						rtbPersonalRefRelationship2.Text = ap2.PersonalRefRelationship;
						rcbPersonalRefTitle2.SelectedValue = ap2.PersonalRefTitle;
						rtbPersonalRefName2.Text = ap2.PersonalRefName;
						rtbPersonalRefSurname2.Text = ap2.PersonalRefSurname;
						rtbPersonalRefAddLine12.Text = ap2.PersonalRefAddLine1;
						rtbPersonalRefAddLine22.Text = ap2.PersonalRefAddLine2;
						rcbPersonalRefSuburb2.SelectedValue = ap2.PersonalRefSuburb.ToString();
						rcbPersonalRefSuburb2.Text = ap2.PersonalRefSuburbText;
						rtbPersonalRefWorkPhone2.Text = ap2.PersonalRefWorkPhone;
						rtbPersonalRefHomePhone2.Text = ap2.PersonalRefHomePhone;
					}
					else
					{						
						tdP2_1.Visible = false;
						tdP2_2.Visible = false;
						NameLabelsVisible(false);
					}
				}
				catch { }
			}
		}


		

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				Save();
				Response.Redirect("ApplicationForm-05.aspx");
			}
		}

		protected void cbSameAs1_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (cb.Checked)
			{
				RadComboBoxMaritalStatus2.SelectedValue = RadComboBoxMaritalStatus.SelectedValue;
				RadNumericNoOfDependents2.Text = RadNumericNoOfDependents.Text;
				RadTextBoxAgesOfDependents2.Text = RadTextBoxAgesOfDependents.Text;
				RadComboBoxResidencyStatus2.SelectedValue = RadComboBoxResidencyStatus.SelectedValue;
			}
			else
			{
				RadComboBoxMaritalStatus2.ClearSelection();
				RadNumericNoOfDependents2.Text = String.Empty;
				RadTextBoxAgesOfDependents2.Text = String.Empty;
				RadComboBoxResidencyStatus2.ClearSelection();
			}
			
		}

		protected void Save()
		{
			MMApplication a = new MMApplication();
			if (Session["Application"] != null)
			{
				a = (MMApplication)Session["Application"];
			}
			else
			{
				throw new Exception("No Application Session, or timed out");
			}

			if (a.MMApplicant1 != null)
			{

			}
			else
			{
				//most likely, the session variable has expired. Lets re-load from the Database...
				throw new Exception("Unable to load Applicant object");
			}
			//Collecting ap1 data from the form...

			a.MMApplicant1.DateOfBirth = RadDatePickerDateOfBirth.SelectedDate;
			a.MMApplicant1.MaritalStatus = RadComboBoxMaritalStatus.SelectedValue;
			a.MMApplicant1.NoOfDependents = Convert.ToInt32(RadNumericNoOfDependents.Value);
			a.MMApplicant1.AgesOfDependents = RadTextBoxAgesOfDependents.Text;
			a.MMApplicant1.ResidencyStatus = RadComboBoxResidencyStatus.SelectedValue;
			a.MMApplicant1.MothersMaidenName = RadTextBoxMothersMaidenName.Text;
			a.MMApplicant1.DriversLicenceNo = RadTextBoxDriversLicenceNo.Text;
			a.MMApplicant1.DriversLicenceExpiry = RadDatePickerDriversLicenceExpiry.SelectedDate;
			a.MMApplicant1.DriversLicenceState = rcbDriversLicenceState.SelectedValue;

			a.MMApplicant1.PersonalRefRelationship = rtbPersonalRefRelationship.Text;
			a.MMApplicant1.PersonalRefTitle = rcbPersonalRefTitle.SelectedValue;
			a.MMApplicant1.PersonalRefName = rtbPersonalRefName.Text;
			a.MMApplicant1.PersonalRefSurname = rtbPersonalRefSurname.Text;
			a.MMApplicant1.PersonalRefAddLine1 = rtbPersonalRefAddLine1.Text;
			a.MMApplicant1.PersonalRefAddLine2 = rtbPersonalRefAddLine2.Text;
			a.MMApplicant1.PersonalRefSuburb = Utils.EmptyToNull(rcbPersonalRefSuburb.SelectedValue);			
			a.MMApplicant1.PersonalRefWorkPhone = rtbPersonalRefWorkPhone.Text;
			a.MMApplicant1.PersonalRefHomePhone = rtbPersonalRefHomePhone.Text;

			a.MMApplicant1.Save();

			if (tdP2_1.Visible)
			{
				if (a.MMApplicant2 != null)
				{
				}
				else
				{
				}

				//Collecting ap2 data from form...
				a.MMApplicant2.DateOfBirth = RadDatePickerDateOfBirth2.SelectedDate;
				a.MMApplicant2.MaritalStatus = RadComboBoxMaritalStatus2.SelectedValue;
				a.MMApplicant2.NoOfDependents = Convert.ToInt32(RadNumericNoOfDependents2.Value);
				a.MMApplicant2.AgesOfDependents = RadTextBoxAgesOfDependents2.Text;
				a.MMApplicant2.ResidencyStatus = RadComboBoxResidencyStatus2.SelectedValue;
				a.MMApplicant2.MothersMaidenName = RadTextBoxMothersMaidenName2.Text;
				a.MMApplicant2.DriversLicenceNo = RadTextBoxDriversLicenceNo2.Text;
				a.MMApplicant2.DriversLicenceExpiry = RadDatePickerDriversLicenceExpiry2.SelectedDate;
				a.MMApplicant2.DriversLicenceState = rcbDriversLicenceState2.SelectedValue;

				a.MMApplicant2.PersonalRefRelationship = rtbPersonalRefRelationship2.Text;
				a.MMApplicant2.PersonalRefTitle = rcbPersonalRefTitle2.SelectedValue;
				a.MMApplicant2.PersonalRefName = rtbPersonalRefName2.Text;
				a.MMApplicant2.PersonalRefSurname = rtbPersonalRefSurname2.Text;
				a.MMApplicant2.PersonalRefAddLine1 = rtbPersonalRefAddLine12.Text;
				a.MMApplicant2.PersonalRefAddLine2 = rtbPersonalRefAddLine22.Text;
				a.MMApplicant2.PersonalRefSuburb = Utils.EmptyToNull(rcbPersonalRefSuburb2.SelectedValue);
				a.MMApplicant2.PersonalRefWorkPhone = rtbPersonalRefWorkPhone2.Text;
				a.MMApplicant2.PersonalRefHomePhone = rtbPersonalRefHomePhone2.Text;

				a.MMApplicant2.Save();
			}

			Session["Application"] = a;
		}
		protected void cbSameAs12_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (cb.Checked)
			{
				rtbPersonalRefRelationship2.Text = rtbPersonalRefRelationship.Text;
				rcbPersonalRefTitle2.SelectedValue = rcbPersonalRefTitle.SelectedValue;
				rtbPersonalRefName2.Text = rtbPersonalRefName.Text;
				rtbPersonalRefSurname2.Text = rtbPersonalRefSurname.Text;
				rtbPersonalRefAddLine12.Text = rtbPersonalRefAddLine1.Text;
				rtbPersonalRefAddLine22.Text = rtbPersonalRefAddLine2.Text;
				rcbPersonalRefSuburb2.SelectedValue = rcbPersonalRefSuburb.SelectedValue;
				rcbPersonalRefSuburb2.Text = rcbPersonalRefSuburb.Text;
				rtbPersonalRefWorkPhone2.Text = rtbPersonalRefWorkPhone.Text;
				rtbPersonalRefHomePhone2.Text = rtbPersonalRefHomePhone.Text;				
			}
			else
			{
				rtbPersonalRefRelationship2.Text = String.Empty;
				rcbPersonalRefTitle2.ClearSelection();
				rtbPersonalRefName2.Text = String.Empty;
				rtbPersonalRefSurname2.Text = String.Empty;
				rtbPersonalRefAddLine12.Text = String.Empty;
				rtbPersonalRefAddLine22.Text = String.Empty;
				rcbPersonalRefSuburb2.ClearSelection();
				rcbPersonalRefSuburb2.Text = String.Empty;
				rtbPersonalRefWorkPhone2.Text = String.Empty;
				rtbPersonalRefHomePhone2.Text = String.Empty;
			}
			
		}
		protected void btnBack_Click(object sender, EventArgs e)
		{
			Save();
			Response.Redirect("ApplicationForm-03.aspx");
		}
		protected void NameLabelsVisible(bool visible)
		{
			litAp1Firstname1.Visible = visible;
			litAp1Firstname2.Visible = visible;
			litAp2Firstname1.Visible = visible;
			litAp2Firstname2.Visible = visible;
		}

        protected void btnSameAs12_Click(object sender, EventArgs e)
        {
            rtbPersonalRefRelationship2.Text = rtbPersonalRefRelationship.Text;
            rcbPersonalRefTitle2.SelectedValue = rcbPersonalRefTitle.SelectedValue;
            rtbPersonalRefName2.Text = rtbPersonalRefName.Text;
            rtbPersonalRefSurname2.Text = rtbPersonalRefSurname.Text;
            rtbPersonalRefAddLine12.Text = rtbPersonalRefAddLine1.Text;
            rtbPersonalRefAddLine22.Text = rtbPersonalRefAddLine2.Text;
            rcbPersonalRefSuburb2.SelectedValue = rcbPersonalRefSuburb.SelectedValue;
            rcbPersonalRefSuburb2.Text = rcbPersonalRefSuburb.Text;
            rtbPersonalRefWorkPhone2.Text = rtbPersonalRefWorkPhone.Text;
            rtbPersonalRefHomePhone2.Text = rtbPersonalRefHomePhone.Text;	
        }
	}
}