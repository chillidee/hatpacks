﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WebApplicationForms.Models;


namespace WebApplicationForms
{
	public partial class WebForm_A03 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				bool Ap1NeedPreviousAddress = false;
				bool Ap2NeedPreviousAddress = false;

				//initialising controls...
				MMApplication a;
				if ((Session["Application"] == null) && HttpContext.Current.Request.IsLocal)
				{
					a = new MMApplication("CFE209842ECB48F4AD1E47BB40529E1F");
					a.LoadApplicants();
					Session["Application"] = a;
				}
				else
				{ a = (MMApplication)Session["Application"]; }
				//getting data...				
				MMApplicant ap1 = a.MMApplicant1;

				//checking to see if the applicant has resided for greater than MinimumMonthsToRequirePreviousAddress
				if (ap1.HereSince != null)
				{
					int months = Convert.ToInt32(Microsoft.VisualBasic.DateAndTime.DateDiff("m", (DateTime)ap1.HereSince, DateTime.Now));
					Ap1NeedPreviousAddress = months <= Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MinimumMonthsToRequirePreviousAddress"]);
				}

				//Populating controls...				
				litAp1Firstname1.Text = ap1.FirstName;
				RadTextBoxPrevUnitNumber.Text = ap1.PrevUnitNumber;
				RadTextBoxPrevStreetNumber.Text = ap1.PrevStreetNumber;
				RadTextBoxPrevStreetName.Text = ap1.PrevStreetName;
				RadComboBoxPrevStreetType.SelectedValue = ap1.PrevStreetType;
				RadComboBoxPrevStreetType.Text = ap1.PrevStreetType;
				RadComboBoxPrevSuburb.SelectedValue = ap1.PrevSuburb.ToString();
				RadComboBoxPrevSuburb.Text = ap1.PrevSuburbText;
				RadMonthYearPickerPrevSince.SelectedDate = ap1.PrevSince;
				RadComboBoxPrevResidentialStatus.SelectedValue = ap1.PrevResidentialStatus;

				try
				{
					if (a.MMApplicant2 != null)
					{
						MMApplicant ap2 = a.MMApplicant2;

						//checking to see if the applicant has resided for greater than MinimumMonthsToRequirePreviousAddress
						if (ap2.HereSince != null)
						{
							int months = Convert.ToInt32(Microsoft.VisualBasic.DateAndTime.DateDiff("m", (DateTime)ap2.HereSince, DateTime.Now));
							Ap2NeedPreviousAddress = months <= Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MinimumMonthsToRequirePreviousAddress"]);
						}

						//populating controls...
						litAp2Firstname1.Text = ap2.FirstName;
						RadTextBoxPrevUnitNumber2.Text = ap2.PrevUnitNumber;
						RadTextBoxPrevStreetNumber2.Text = ap2.PrevStreetNumber;
						RadTextBoxPrevStreetName2.Text = ap2.PrevStreetName;
						RadComboBoxPrevStreetType2.SelectedValue = ap2.PrevStreetType;
						RadComboBoxPrevStreetType2.Text = ap2.PrevStreetType;
						RadComboBoxPrevSuburb2.SelectedValue = ap2.PrevSuburb.ToString();
						RadComboBoxPrevSuburb2.Text = ap2.PrevSuburbText;
						RadMonthYearPickerPrevSince2.SelectedDate = ap2.PrevSince;
						RadComboBoxPrevResidentialStatus2.SelectedValue = ap2.PrevResidentialStatus;
					}
					else
					{
						tdP2_1.Visible = false;
						NameLabelsVisible(false);
					}
				}
				catch { }
				if (!Ap1NeedPreviousAddress && !Ap2NeedPreviousAddress)
				{
					string previousPage = Request.UrlReferrer.AbsolutePath;
					string navigateToPage = "";
					switch (previousPage)
					{
						case "/ApplicationForm-02.aspx": navigateToPage = "/ApplicationForm-04.aspx"; break;
						case "/ApplicationForm-04.aspx": navigateToPage = "/ApplicationForm-02.aspx"; break;
						default: navigateToPage = "/Application-04.aspx"; break;
					}
					Response.Redirect(navigateToPage);
				}
				
				
				lblAp1Msg.Visible = !Ap1NeedPreviousAddress;
				tblAp1.Visible = Ap1NeedPreviousAddress;
				lblAp2Msg.Visible = !Ap2NeedPreviousAddress;
				tblAp2.Visible = Ap2NeedPreviousAddress;
				trSameAsMainContact.Visible = Ap1NeedPreviousAddress;
				
				
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				Save();
				Response.Redirect("ApplicationForm-04.aspx");
			}
		}

		protected void Save()
		{
			MMApplication a = new MMApplication();
			if (Session["Application"] != null)
			{
				a = (MMApplication)Session["Application"];
			}
			else
			{
				throw new Exception("No Application Session, or timed out");
			}

			if (a.MMApplicant1 != null)
			{

			}
			else
			{
				//most likely, the session variable has expired. Lets re-load from the Database...
				throw new Exception("Unable to load Applicant object");
			}
			//Collecting ap1 data from the form...

			a.MMApplicant1.PrevUnitNumber = RadTextBoxPrevUnitNumber.Text;
			a.MMApplicant1.PrevStreetNumber = RadTextBoxPrevStreetNumber.Text;
			a.MMApplicant1.PrevStreetName = RadTextBoxPrevStreetName.Text;
			a.MMApplicant1.PrevStreetType = RadComboBoxPrevStreetType.Text;
			a.MMApplicant1.PrevSuburb = Utils.EmptyToNull(RadComboBoxPrevSuburb.SelectedValue);
			a.MMApplicant1.PrevSince = RadMonthYearPickerPrevSince.SelectedDate;
      if (a.MMApplicant1.PrevSince != null)
      {
        TimeSpan ts = DateTime.Now - (DateTime)a.MMApplicant1.PrevSince;
        a.MMApplicant1.PrevDurationYears = (Int32)ts.Days / 365;
        a.MMApplicant1.PrevDurationMonths = (Int32)((ts.Days % 365) / 31);
      }
			a.MMApplicant1.PrevResidentialStatus = RadComboBoxPrevResidentialStatus.SelectedValue;
			a.MMApplicant1.Save();

			if (tdP2_1.Visible)
			{
				if (a.MMApplicant2 != null)
				{
				}
				else
				{
				}

				//Collecting ap2 data from form...
				a.MMApplicant2.PrevUnitNumber = RadTextBoxPrevUnitNumber2.Text;
				a.MMApplicant2.PrevStreetNumber = RadTextBoxPrevStreetNumber2.Text;
				a.MMApplicant2.PrevStreetName = RadTextBoxPrevStreetName2.Text;
				a.MMApplicant2.PrevStreetType = RadComboBoxPrevStreetType2.Text;
				a.MMApplicant2.PrevSuburb = Utils.EmptyToNull(RadComboBoxPrevSuburb2.SelectedValue);
				a.MMApplicant2.PrevSince = RadMonthYearPickerPrevSince2.SelectedDate;
        if (a.MMApplicant2.PrevSince != null)
        {
          TimeSpan ts = DateTime.Now - (DateTime)a.MMApplicant2.PrevSince;
          a.MMApplicant2.PrevDurationYears = (Int32)ts.Days / 365;
          a.MMApplicant2.PrevDurationMonths = (Int32)((ts.Days % 365) / 31);
        }
				a.MMApplicant2.PrevResidentialStatus = RadComboBoxPrevResidentialStatus2.SelectedValue;
				a.MMApplicant2.Save();
			}

			Session["Application"] = a;
		}

		protected void cbPrevAddressSameAs1_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (cb.Checked)
			{
				RadTextBoxPrevUnitNumber2.Text = RadTextBoxPrevUnitNumber.Text;
				RadTextBoxPrevStreetNumber2.Text = RadTextBoxPrevStreetNumber.Text;
				RadTextBoxPrevStreetName2.Text = RadTextBoxPrevStreetName.Text;
				RadComboBoxPrevStreetType2.SelectedValue = RadComboBoxPrevStreetType.SelectedValue;
				RadComboBoxPrevStreetType2.Text = RadComboBoxPrevStreetType.Text;
				RadComboBoxPrevSuburb2.SelectedValue = RadComboBoxPrevSuburb.SelectedValue;
				RadComboBoxPrevSuburb2.Text = RadComboBoxPrevSuburb.Text;
				RadMonthYearPickerPrevSince2.SelectedDate = RadMonthYearPickerPrevSince.SelectedDate;
			}
			else
			{
				RadTextBoxPrevUnitNumber2.Text = String.Empty;
				RadTextBoxPrevStreetNumber2.Text = String.Empty;
				RadTextBoxPrevStreetName2.Text = String.Empty;
				RadComboBoxPrevSuburb2.ClearSelection();
				RadComboBoxPrevSuburb2.Text = String.Empty;
				RadMonthYearPickerPrevSince2.Clear();
			}
		}

		protected void btnBack_Click(object sender, EventArgs e)
		{
			Save();
			Response.Redirect("ApplicationForm-02.aspx");
		}
		protected void NameLabelsVisible(bool visible)
		{
			litAp1Firstname1.Visible = visible;
			litAp2Firstname1.Visible = visible;
		}

        protected void btnPrevAddressSameAs1_Click(object sender, EventArgs e)
        {
            RadTextBoxPrevUnitNumber2.Text = RadTextBoxPrevUnitNumber.Text;
            RadTextBoxPrevStreetNumber2.Text = RadTextBoxPrevStreetNumber.Text;
            RadTextBoxPrevStreetName2.Text = RadTextBoxPrevStreetName.Text;
            RadComboBoxPrevStreetType2.SelectedValue = RadComboBoxPrevStreetType.SelectedValue;
            RadComboBoxPrevStreetType2.Text = RadComboBoxPrevStreetType.Text;
            RadComboBoxPrevSuburb2.SelectedValue = RadComboBoxPrevSuburb.SelectedValue;
            RadComboBoxPrevSuburb2.Text = RadComboBoxPrevSuburb.Text;
            RadMonthYearPickerPrevSince2.SelectedDate = RadMonthYearPickerPrevSince.SelectedDate;
        }
	}
}