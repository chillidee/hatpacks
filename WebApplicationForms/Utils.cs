﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace WebApplicationForms
{
  static class Utils
  {
    public static Nullable<int> EmptyToNull(string s)
    {
      if (String.IsNullOrEmpty(s))
      {
        return null;
      }
      else
      {
        return Convert.ToInt32(s);
      }
    }
    public static int EmptyToZero(string s)
    {
      if (String.IsNullOrEmpty(s)) return 0;
      else return Convert.ToInt32(s);
    }
    public static string GetKeyword(string strU)
    {
      Regex regKeyword = new Regex("(p|q)=(?<keyword>.*?)&", RegexOptions.IgnoreCase | RegexOptions.Multiline);
      Regex regSnippet = new Regex("forgetcode.com.*%2F(?<id>\\d+)%2F", RegexOptions.IgnoreCase | RegexOptions.Multiline);
      Match match = regKeyword.Match(strU);
      string keyword = match.Groups["keyword"].ToString();
      // Get the decoded URL
      string result = HttpUtility.UrlDecode(keyword);
      // Get the HTML representation
      result = HttpUtility.HtmlEncode(result);

      return result;

    }

    public static Boolean IsDigitsOnly(string str)
    {
      foreach (char c in str)
      {
        if (c < '0' || c > '9')
          return false;
      }

      return true;
    }


    public static bool IsValidEmail(string strIn)

    {
      bool invalid = false;
      
      if (String.IsNullOrEmpty(strIn))
        return false;

      if (invalid)
        return false;

      // Return true if strIn is in valid e-mail format. 
      try
      {
        return Regex.IsMatch(strIn,
              @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
              @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
              RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
      }
      catch (RegexMatchTimeoutException)
      {
        return false;
      }
    }
    public static string Left(string param, int length)
    {
      //we start at 0 since we want to get the characters starting from the
      //left and with the specified lenght and assign it to a variable
      string result = param.Substring(0, length);
      //return the result of the operation
      return result;
    }
    public static string Right(string param, int length)
    {
      //start at the index based on the lenght of the sting minus
      //the specified lenght and assign it a variable
      string result = param.Substring(param.Length - length, length);
      //return the result of the operation
      return result;
    }

    public static string Mid(string param, int startIndex, int length)
    {
      //start at the specified index in the string ang get N number of
      //characters depending on the lenght and assign it to a variable
      string result = param.Substring(startIndex, length);
      //return the result of the operation
      return result;
    }

    public static string Mid(string param, int startIndex)
    {
      //start at the specified index and return all characters after it
      //and assign it to a variable
      string result = param.Substring(startIndex);
      //return the result of the operation
      return result;
    }

  }
  public struct DateTimeSpan
  {
    private readonly int years;
    private readonly int months;
    private readonly int days;
    private readonly int hours;
    private readonly int minutes;
    private readonly int seconds;
    private readonly int milliseconds;

    public DateTimeSpan(int years, int months, int days, int hours, int minutes, int seconds, int milliseconds)
    {
      this.years = years;
      this.months = months;
      this.days = days;
      this.hours = hours;
      this.minutes = minutes;
      this.seconds = seconds;
      this.milliseconds = milliseconds;
    }

    public int Years { get { return years; } }
    public int Months { get { return months; } }
    public int Days { get { return days; } }
    public int Hours { get { return hours; } }
    public int Minutes { get { return minutes; } }
    public int Seconds { get { return seconds; } }
    public int Milliseconds { get { return milliseconds; } }

    enum Phase { Years, Months, Days, Done }

    public static DateTimeSpan CompareDates(DateTime date1, DateTime date2)
    {

      if (date2 < date1)
      {
        var sub = date1;
        date1 = date2;
        date2 = sub;
      }

      DateTime current = date2;
      int years = 0;
      int months = 0;
      int days = 0;

      Phase phase = Phase.Years;
      DateTimeSpan span = new DateTimeSpan();

      while (phase != Phase.Done)
      {
        switch (phase)
        {
          case Phase.Years:
            if (current.Year == 1 || current.AddYears(-1) < date1)
            {
              phase = Phase.Months;
            }
            else
            {
              current = current.AddYears(-1);
              years++;
            }
            break;
          case Phase.Months:
            if (current.AddMonths(-1) < date1)
            {
              phase = Phase.Days;
            }
            else
            {
              current = current.AddMonths(-1);
              months++;
            }
            break;
          case Phase.Days:
            if (current.AddDays(-1) < date1)
            {
              var timespan = current - date1;
              span = new DateTimeSpan(years, months, days, timespan.Hours, timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
              phase = Phase.Done;
            }
            else
            {
              current = current.AddDays(-1);
              days++;
            }
            break;
        }
      }

      return span;

    }

  }
}