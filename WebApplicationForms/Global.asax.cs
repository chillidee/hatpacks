﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using WebApplicationForms;
using WebApplicationForms.Models;
using System.Web.Http;

namespace WebApplicationForms
{
	public class Global : HttpApplication
	{
		void Application_Start(object sender, EventArgs e)
		{
			// Code that runs on application startup
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AuthConfig.RegisterOpenAuth();

            RouteTable.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{cid}",
                defaults: new { cid = System.Web.Http.RouteParameter.Optional }
                );
		}

		void Application_End(object sender, EventArgs e)
		{
			//  Code that runs on application shutdown
		}

		void Session_Start(object sender, EventArgs e)
		{
      //Getting CompanyCode
      string CompanyCode = Convert.ToString(Request.QueryString["cid"]);
      if (CompanyCode == "" || CompanyCode == null)
      {
        //default company is ALC.
        MMCompany c = new MMCompany("ALC");
        Session["Company"] = c;
      }
      else
      {
        MMCompany c = new MMCompany(CompanyCode);
        Session["Company"] = c;
      }
      /*if ((Session["CCApplicant"] == null) && HttpContext.Current.Request.IsLocal)
      {
        CCApplicant cc = new CCApplicant("FF05473E50074F4DB6BED92E982FBBDD");        
        cc.LoadCompany();
        Session["CCApplicant"] = cc;
        Session["Company"] = cc.Company;
      }*/
			/*if ((Session["Application"] == null) && HttpContext.Current.Request.IsLocal)
			{
        MMApplication a = new MMApplication("70B7178FF2334CEA97695077793C79AD");
				a.LoadApplicants();
				a.LoadCompany();
				Session["Application"] = a;
				Session["Company"] = a.Company;
			}
			else
			{}*/

      Session["internal"] = (Request.QueryString["internal"] == "yeahbaby");

		}

		void Application_Error(object sender, EventArgs e)
		{
			// Code that runs when an unhandled error occurs
		}
	}
}
