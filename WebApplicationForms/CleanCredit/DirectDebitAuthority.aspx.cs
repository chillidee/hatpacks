﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms.CleanCredit
{
  public partial class DirectDebitAuthority : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!this.IsPostBack)
      {
        CCApplicant cc;
        if (!string.IsNullOrEmpty(Request.QueryString["admin"]))
        {
          string strGUID = Request.QueryString["ID"];
          cc = new CCApplicant(strGUID);
        }else{
          cc = (CCApplicant)Session["CCApplicant"];
        }
        //getting data...
        
        //initialising controls...

        if (!string.IsNullOrEmpty(Request.QueryString["admin"]))
        {
          adminView.Visible = true;
          cb1.Checked = cc.AgreedToPayment;
          cc.LoadCompany();
          Session["Company"] = cc.Company;
          string date_agreed = cc.AgreedToPaymentDate.ToString();
          string ipaddress = cc.IPAddress;
          agreed.Text = "Agreed on: " + date_agreed + ", from IP Address: " + ipaddress;
        }

        //populating...
        if (cc.PaymentOptionID != null)
        {
          rtbBank.Text = cc.Bank;
          rtbBankBranch.Text = cc.BankBranch;
          rtbAccountName.Text = cc.AccountName;
          rtbAccountNo.Text = cc.AccountNo;
          rmtbAccountBSB.Text = cc.AccountBSB;
          lblAgreementMessage.Text = AgreementMessage();
        }

      }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      if (Page.IsValid)
      {
        Save();
        Response.Redirect("AuthorityToAct.aspx");        
      }
    }


    protected void Save()
    {
      CCApplicant cc = new CCApplicant();
      if (Session["CCApplicant"] != null)
      {
        cc = (CCApplicant)Session["CCApplicant"];
      }
      else
      {
        throw new Exception("No CC Applicant Session, or timed out");
      }

      //Collecting ap1 data from the form...
      cc.Bank = rtbBank.Text;
      cc.BankBranch = rtbBankBranch.Text;
      cc.AccountName= rtbAccountName.Text;      
      cc.AccountNo=rtbAccountNo.Text ;
      cc.AccountBSB=rmtbAccountBSB.Text;
      if (cb1.Checked)
      {
        cc.AgreedToDirectDebitRequest = cb1.Checked;
        cc.AgreedToDirectDebitDate = DateTime.Now;
      }

      cc.AgreedToPaymentType = "DD";
      cc.AgreedToPayment = cb1.Checked;
      cc.AgreedToPaymentDate = DateTime.Now;
      
      cc.Save();
      
      Session["CCApplicant"] = cc;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
      Save();
      Response.Redirect("P02.aspx");
    }

    protected void cvAgree_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = (cb1.Checked);
      if (!cb1.Checked)
      {
        lblAgreementMessage.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFD7D7");
        cb1.Focus();
      }
      else
      {
        lblAgreementMessage.BackColor = System.Drawing.Color.Transparent;
      }
    }
    
    private string AgreementMessage()
    {

      CCApplicant cc;
      if (!string.IsNullOrEmpty(Request.QueryString["admin"]))
      {
        string strGUID = Request.QueryString["ID"];
        cc = new CCApplicant(strGUID);
      }
      else
      {
        cc = (CCApplicant)Session["CCApplicant"];
      }
      string msg;
      switch (cc.PaymentOptionID)
      {
        case 2:
          msg = "I "+cc.FirstName + " " + cc.MiddleName + " " + cc.Surname+" agree to the above terms and conditions, and give authority to Clean Credit Pty Ltd to debit the application fee from my bank account.";// <b>"
            //+ string.Format("{0:C}", cc.ApplicationFee)            
            //+ "</b> inc GST."
            //;
          break;
        case 3:
          msg = "I "+cc.FirstName + " " + cc.MiddleName + " " + cc.Surname+" agree to the above terms and conditions and give authority to Clean Credit Pty Ltd to debit from my bank account."; // <b>over 2 weeks</b>, every <b>"
            //+ cc.DayOfBusinessWeekToDebit
            //+ "</b> for the application fee of <b>"
            //+ String.Format("{0:C}", (((Decimal)cc.ApplicationFee / 2) * 1.1m)) + "</b> inclusive of GST.";
          break;
        default:
          msg = "ERROR. Please go back to re-specify payment options.";
          break;
      }
      return msg;
    }
  }
}