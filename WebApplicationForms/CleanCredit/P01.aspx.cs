﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms.CleanCredit
{
    public partial class P01 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                CCApplicant cc = new CCApplicant();
                if (Session["CCApplicant"] != null)
                {
                    cc = (CCApplicant)Session["CCApplicant"];
                }
                else
                {
                    throw new Exception("No CC Applicant Session, or timed out");
                }

                //getting data...

                //initialising controls...
                rdpDateOfBirth.MaxDate = DateTime.Today;
                rdpDateOfBirth.MinDate = DateTime.Today.AddYears(-100);
                //populating...
                hfApplicantID.Value = cc.ApplicantID.ToString();
                rcbTitle.SelectedValue = cc.Title;
                rtbFirstname.Text = cc.FirstName;
                rtbSurname.Text = cc.Surname;
                rtbMiddleName.Text = cc.MiddleName;
                rdpDateOfBirth.SelectedDate = cc.DateOfBirth;
                rtbDriversLicenceNo.Text = cc.DriversLicenceNo;
                rcbDriversLicenceState.SelectedValue = cc.LicenseStateOfIssue;
                rtbEmail.Text = cc.Email;
                rtbMobile.Text = cc.Mobile;
                rtbTelephoneWork.Text = cc.TelephoneWork;
                rtbTelephoneHome.Text = cc.TelephoneHome;
                ucAddress.UnitNumber = cc.HomeUnitNumber;
                ucAddress.StreetNumber = cc.HomeStreetNumber;
                ucAddress.StreetName = cc.HomeStreetName;
                ucAddress.StreetType = cc.HomeStreetType;
                ucAddress.SuburbID = cc.HomeSuburb;
                ucPostalAddress.UnitNumber = cc.PostalUnitNumber;
                ucPostalAddress.StreetNumber = cc.PostalStreetNumber;
                ucPostalAddress.StreetName = cc.PostalStreetName;
                ucPostalAddress.StreetType = cc.PostalStreetType;
                ucPostalAddress.SuburbID = cc.PostalSuburb;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Save();
                Response.Redirect("P02.aspx");
            }
        }

        protected void btnSameAs1_Click(object sender, EventArgs e)
        {
            ucPostalAddress.UnitNumber = ucAddress.UnitNumber;
            ucPostalAddress.StreetNumber = ucAddress.StreetNumber;
            ucPostalAddress.StreetName = ucAddress.StreetName;
            ucPostalAddress.StreetType = ucAddress.StreetType;
            ucPostalAddress.SuburbID = ucAddress.SuburbID;

        }
        protected void Save()
        {
            CCApplicant cc = new CCApplicant();
            if (Session["CCApplicant"] != null)
            {
                cc = (CCApplicant)Session["CCApplicant"];
            }
            else
            {
                throw new Exception("No CC Applicant Session, or timed out");
            }

            //Collecting ap1 data from the form...

            cc.Title = rcbTitle.SelectedValue;
            cc.FirstName = rtbFirstname.Text;
            cc.Surname = rtbSurname.Text;
            cc.MiddleName = rtbMiddleName.Text;

            cc.DateOfBirth = rdpDateOfBirth.SelectedDate;
            cc.DriversLicenceNo = rtbDriversLicenceNo.Text;
            cc.LicenseStateOfIssue = rcbDriversLicenceState.SelectedValue;

            cc.Email = rtbEmail.Text;
            cc.Mobile = rtbMobile.Text;
            cc.TelephoneHome = rtbTelephoneHome.Text;
            cc.TelephoneWork = rtbTelephoneWork.Text;

            cc.HomeUnitNumber = ucAddress.UnitNumber.ToString();
            cc.HomeStreetNumber = ucAddress.StreetNumber.ToString();
            cc.HomeStreetName = ucAddress.StreetName.ToString();
            cc.HomeStreetType = ucAddress.StreetType.ToString();
            cc.HomeSuburb = Utils.EmptyToNull(Convert.ToString(ucAddress.SuburbID));

            cc.PostalUnitNumber = ucPostalAddress.UnitNumber.ToString();
            cc.PostalStreetNumber = ucPostalAddress.StreetNumber.ToString();
            cc.PostalStreetName = ucPostalAddress.StreetName.ToString();
            cc.PostalStreetType = ucPostalAddress.StreetType.ToString();
            cc.PostalSuburb = Utils.EmptyToNull(Convert.ToString(ucPostalAddress.SuburbID));

            cc.Save();

            Session["CCApplicant"] = cc;
        }
    }
}