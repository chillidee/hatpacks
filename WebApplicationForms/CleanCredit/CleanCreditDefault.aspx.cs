﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using WebApplicationForms.Models;
using System.Net;
using System.Text;

namespace WebApplicationForms
{
  public partial class _CleanCreditDefault : PageWithFieldReplace
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!this.IsPostBack)
      {
        //getting the querystring...
        string strGUID = Request.QueryString["ID"];
        if (string.IsNullOrEmpty(strGUID))
        {
          Response.Redirect("/Errors/IDRequired.aspx");
          return;
        }
        //we have a GUID, lets load data!...
        try
        {
          CCApplicant cc = new CCApplicant(strGUID);
          cc.LoadCompany();           

          Session["CCApplicant"] = cc;
          Session["Company"] = cc.Company;

          //litFirstname.Text = cc.FirstName;
          //litFirstname2.Text = cc.FirstName;


          //lblPhone.Text = cc.Company.Phone;
          //lblSalesManager.Text = cc.ApplicationManager;
          aplicate_name.Text = cc.FirstName + " " + cc.MiddleName + " " + cc.Surname;
          aplicate_name1.Text = cc.FirstName + " " + cc.MiddleName + " " + cc.Surname;
          //lblSalesManager2.Text = cc.ApplicationManager;
          //lblCompanyName.Text = cc.Company.Company;

          //find out if this application can be loaded, c.WebApplicationCompleted
          Boolean WebApplicationEnabled = false;

          WebApplicationEnabled = (cc.ApplicationPackageReceivedDate == null);

          if (!WebApplicationEnabled)
          {
            Response.Redirect("~/ApplicationClosed.aspx");
          }

          if (!string.IsNullOrEmpty(Request.QueryString["admin"]))
          {
            WebClient myClient = new WebClient();
            UTF8Encoding utf8 = new UTF8Encoding();
            adminView.Visible = true;
            cb2.Checked = cc.ReceiveFurtherInformationTicked;
            cb1.Checked = cc.AgreedToTermsAndConditions;
            string date_agreed = cc.AgreedToTermsAndConditionsDate.ToString();
            string ipaddress = cc.IPAddress;
            agreed.Text = "Agreed on: " + date_agreed + ", from IP Address: " + ipaddress;
          }

        }



        catch (Exception ex)
        {
          Response.Write("ID invalid.");
         // Response.Write(ex.ToString());
          Response.End();
          return;
        }
      }
    }


    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = (cb1.Checked);
      if (!cb1.Checked)
      {
        tdAccept1.BgColor = "#FFD7D7";
      }
      else
      {
        tdAccept1.BgColor = "";
      }
    }

    protected void btnGetStartedNow_Click(object sender, EventArgs e)
    {
      if (Page.IsValid)
      {
        if (Session["CCApplicant"] != null)
        {
          if (cb2.Checked || cb1.Checked)
          {
            CCApplicant c = (CCApplicant)Session["CCApplicant"];
            if (!c.AgreedToTermsAndConditions)
            {
              c.AgreedToTermsAndConditions = cb1.Checked;
              c.AgreedToTermsAndConditionsDate = DateTime.Now;
              c.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
            }
            c.ReceiveFurtherInformationTicked = cb2.Checked;
            c.Save();
          }
          //Response.Redirect("P01.aspx");
          Response.Redirect("AuthorityToAct.aspx");
        }
        else
        { }
      }
    }

  }
}
