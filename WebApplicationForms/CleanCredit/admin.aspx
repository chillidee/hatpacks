﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="WebApplicationForms.CleanCredit.admin" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Admin</title>
  <style>
  body {
    margin: 0;
  }

  iframe {
    width: 100%;
    border: none;
  }

  .header {
    position: fixed;
    width: 100%;
    height: 100px;
    background-color: black;
    color: white;
    top: 0;
  }

  .header p {
    float: left;
    font-size: 20px;
    margin: 40px 20px;
  }

  .header #print_pages {
    float: right;
    margin: 20px 20px;
    padding: 10px 20px;
  }

    #MainContent {
      margin-top: 100px;
    }

    @media print {
      .header {
        display: none;
      }
    }
</style>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<body>
  <form id="form1" runat="server">
    <div>
      <div class="header">
        <p id="applicate_details" runat="server"></p>
        <asp:Button ID="print_pages" OnClientClick="aspnetForm.target ='_blank';" Text="Print" CssClass="btn btn-info" runat="server" OnClick="printToPdf" />
      </div>
      <div id="MainContent" runat="server">
      </div>
    </div>
  </form>
  <script>
    function resizeIframe(obj) {
      obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    }
  </script>
</body>
</html>
