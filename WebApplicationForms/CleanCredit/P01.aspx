﻿<%@ Page Title="Clean Credit Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="P01.aspx.cs" Inherits="WebApplicationForms.CleanCredit.P01" %>
<%@ Register Src="~/UserControls/Address.ascx" TagPrefix="uc1" TagName="Address" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
  <section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Your Details</h1>
			<p>
				Please fill in any missing information.
			</p>
      </hgroup>
		</div>
  </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">  
    <table class="splitTable">
      <tr>
        <td class="split" style="padding: 10px 20px 20px 20px;">
      
            <div class="form-block">
							<div class="label-block">
								<asp:HiddenField ID="hfApplicantID" runat="server" />
								Title</div>
							<div class="input-block">
								<telerik:RadComboBox ID="rcbTitle" runat="server" OffsetY="10" Width="100%">
									<Items>
                    <telerik:RadComboBoxItem runat="server" Value="" Text="(none)"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Value="Mr" Text="Mr"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Text="Mrs" Value="Mrs"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Text="Ms" Value="Ms"></telerik:RadComboBoxItem>
										<telerik:RadComboBoxItem runat="server" Text="Dr" Value="Dr"></telerik:RadComboBoxItem>
									</Items>
								</telerik:RadComboBox>
							</div>
						
						
							<div class="label-block">Firstname*</div>
							<div class="input-block">
								<telerik:RadTextBox ID="rtbFirstname" runat="server" Width="100%"></telerik:RadTextBox>
								<asp:RequiredFieldValidator ID="rfvFirstname" runat="server" ErrorMessage="Required." ControlToValidate="rtbFirstname"></asp:RequiredFieldValidator>
							</div>
						
							<div class="label-block">Surname*</div>
							<div class="input-block">
								<telerik:RadTextBox ID="rtbSurname" runat="server" Width="100%"></telerik:RadTextBox>
								<asp:RequiredFieldValidator ID="rfvSurname" runat="server" ErrorMessage="Required." ControlToValidate="rtbSurname"></asp:RequiredFieldValidator>
							</div>
						
							<div class="label-block">Middle Name</div>
							<div class="input-block">
								<telerik:RadTextBox ID="rtbMiddleName" runat="server" Width="100%"></telerik:RadTextBox>								
							</div>
						
				</div>
        </td>
        <td class="split"  style="padding: 10px 20px 20px 20px;">
      
            <div class="form-block">
						
							<div class="label-block">Date of Birth*</div>
                            <div class="input-block">
								<telerik:RadDatePicker ID="rdpDateOfBirth" Runat="server" Culture="en-AU" Width="200px" Calendar-FastNavigationStep="12"  >
									<Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ShowRowHeaders="False" ViewSelectorText="x">
									</Calendar>
									<DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" DisplayText="" LabelWidth="40%" type="text" value="">
									</DateInput>
									<DatePopupButton HoverImageUrl="" ImageUrl="" />
								</telerik:RadDatePicker>
								<asp:RequiredFieldValidator ErrorMessage="Required." ID="RequiredFieldValidator1" runat="server" ControlToValidate="rdpDateOfBirth"></asp:RequiredFieldValidator>
                            </div>
            
						
							<div class="label-block">Driver&#39;s License Number</div>
							<div class="input-block">
								<telerik:RadTextBox ID="rtbDriversLicenceNo" Runat="server" Width="100%">
								</telerik:RadTextBox>
							</div>
						
							<div class="label-block">State of Issue</div>
							<div class="input-block">
								<telerik:RadComboBox ID="rcbDriversLicenceState" runat="server" DataSourceID="sqlState" DataTextField="State" DataValueField="State" AllowCustomText="false" OffsetY="10" Width="100%"></telerik:RadComboBox>
							</div>
					
            </div>
          </td> 
        </tr>
      </table>

    <table class="splitTable">
      <tr>
        <td class="split" style="padding: 10px 20px 20px 20px;">
      
          <div class="form-block">

            <div class="label-block">Email</div>
            <div class="input-block">
                <telerik:RadTextBox ID="rtbEmail" runat="server" Width="100%"></telerik:RadTextBox>
				<asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Invalid email." ControlToValidate="rtbEmail" EnableClientScript="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
				 <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Required." ControlToValidate="rtbEmail"></asp:RequiredFieldValidator>
            </div>

            <div class="label-block">Mobile phone</div>
		    <div class="input-block"><telerik:RadTextBox runat="server" ID="rtbMobile" Width="100%"></telerik:RadTextBox></div>
            
            <div class="label-block">Home phone</div>
            <div class="input-block"><telerik:RadTextBox runat="server" ID="rtbTelephoneHome" Width="100%"></telerik:RadTextBox></div>
           
            <div class="label-block">Work phone</div>
            <div class="input-block"><telerik:RadTextBox runat="server" ID="rtbTelephoneWork" Width="100%"></telerik:RadTextBox></div>
         </div>
       </td>
          <td class="split" style="padding: 10px 20px 20px 20px;">
      
            <div class="form-block">
                 <div class="half-box"><h4 style="color:#fff;border:0;padding: 12px 0;">Your Residential Address</h4>	</div>		
                <uc1:address runat="server" id="ucAddress" FieldCssClass="fieldLabel" />
            </div>
          </td>
        </tr>
      </table>
    <table class="splitTable" style="display:none">
      <tr>
        <td class="split" style="padding: 10px 20px 20px 20px;">
            <div class="form-block">    
                <div class="half-box"><h4 style="color:#fff;border:0;padding: 12px 0;">Your Postal Address</h4></div>
                <div class="half-box" style="text-align: right;"><asp:Button runat="server" ID="btnSameAs1" Text="Same as residential address" CausesValidation="false" OnClick="btnSameAs1_Click" /></div>
                <uc1:address runat="server" id="ucPostalAddress" FieldCssClass="fieldLabel" />                    
            </div>
        </td>
    </table>
  </telerik:RadAjaxPanel>
    <table class="splitTable" style="">
     <tr>
         <td style="padding:0">
			<div class="nextbtn-block" style="text-align:center!important;float:none;width:100%;">
			    <asp:Button ID="btnSubmit" Text="Next" CssClass="main" runat="server" OnClick="btnSubmit_Click" style="margin:10px 0!important"/>
                <p style="margin: 0;padding-top: 5px;border-top: 1px solid #cacaca;"><img src="../Images/noeffects.png" style="width: 13px;vertical-align: middle;margin-right: 5px;margin-top: -3px;">  <strong>Will NOT affect your credit file</strong></p>
		    </div>
        </td>
     </tr>
    </table>
<asp:SqlDataSource ID="sqlState" runat="server" ConnectionString='<%$ ConnectionStrings:CRISMARConnectionString %>' SelectCommand="SELECT '' State UNION SELECT State FROM State"></asp:SqlDataSource>
<script>
  document.getElementsByName("ctl00$MainContent$btnSubmit")[0].onclick = function () {
    window.scrollTo(0, 0);
  }
</script>
</asp:Content>

