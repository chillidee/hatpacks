﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms
{
	public partial class ThankYouCC : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			CCApplicant cc = (CCApplicant)Session["CCApplicant"];
			Session["Company"] = cc.Company;

			litFirstname.Text = cc.FirstName;
			//litFirstname2.Text = cc.FirstName;
			lblCompanyName.Text = cc.Company.Company;
			lblPhone.Text = cc.Company.Phone;
			lblSalesManager.Text = cc.ApplicationManager;
			//lblSalesManager2.Text = a.SalesManager;
		}
	}
}