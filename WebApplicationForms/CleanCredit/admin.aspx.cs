﻿using System;
using System.IO;
using System.Web;
using WebApplicationForms.Models;
using HiQPdf;
using Ionic.Zip;

namespace WebApplicationForms.CleanCredit
{
  public partial class admin : System.Web.UI.Page
  {

    public string GUID { get; set; }
    public CCApplicant cc { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

      if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
      {
        string strGUID = Request.QueryString["ID"];
        GUID = strGUID;
        cc = new CCApplicant(strGUID);
        cc.LoadCompany();
        Session["CCApplicant"] = cc;
        Session["Company"] = cc.Company;
        applicate_details.InnerHtml = "Application form for: " + cc.FirstName + " " + cc.MiddleName + " " + cc.Surname + ", completed on: " + cc.AgreedToFeeStructureDate.ToString();
        MainContent.InnerHtml = getHtmlPages(strGUID, cc);
      }
      else
      {
        throw new HttpException(404, "Page not Found");
      }
    }

    private string[] getApplicationPages()
    {
      string[] pages = new string[3];
      pages[0] = "CleanCreditDefault.aspx?ID=";
      pages[1] = "P02.aspx?ID=";
      if (cc.AgreedToPaymentType.Equals("DD"))
      {
        pages[2] = "DirectDebitAuthority.aspx?ID=";
      }
      else
      {
        pages[2] = "CreditCardPayment.aspx?ID=";
      }
      //pages[3] = "FeeAgreement.aspx?ID=";
      return pages;
    }

    private string getHtmlPages(string GUID, CCApplicant cc)
    {
      string url = "https://applicationform.hatpacks.com.au/CleanCredit/";

      string _pagesHtml = "";

      string[] pages = getApplicationPages();

      foreach (string page in pages)
      {
        _pagesHtml += "<iframe id='" + page + "' src='";
        _pagesHtml += url + page + GUID + "&admin=true";
        _pagesHtml += "' onload='resizeIframe(this)'></iframe>";
      }

      return _pagesHtml;

    }

    protected void printToPdf(object sender, EventArgs e)
    {
      try
      {

        ZipFile zip = new ZipFile();

        MemoryStream ms = new MemoryStream();

        string[] pages = getApplicationPages();
        int index = 1;
        foreach (string page in pages)
        {
          HtmlToPdf htmlToPdfConverter = new HtmlToPdf();

          htmlToPdfConverter.Document.Margins = new PdfMargins(10);

          byte[] pdfBuffer = null;

          string PageUrl = "https://applicationform.hatpacks.com.au/CleanCredit/";

          string currentPageUrl = PageUrl + page + GUID + "&admin=true";

          pdfBuffer = htmlToPdfConverter.ConvertUrlToMemory(currentPageUrl);
          string fileName = "signed-page-" + index + ".pdf";
          zip.AddEntry(fileName,pdfBuffer);

          index++;
        }

        zip.Save(ms);

        HttpContext.Current.Response.AddHeader("Content-Type", "application/zip");

          HttpContext.Current.Response.AddHeader("Content-Disposition",
              String.Format("{0}; filename=signed-documents.zip; size={1}",
              "attachment",
              ms.Length.ToString()));

          HttpContext.Current.Response.BinaryWrite(ms.ToArray());
          
          HttpContext.Current.Response.End();
      }
      catch (Exception ex)
      {
        Response.Write(ex.ToString());
      }
    }

  }
}