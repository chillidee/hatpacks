﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms.CleanCredit
{
  public partial class FeeAgreement : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!this.IsPostBack)
      {
        CCApplicant cc = null;
        //getting data...

        if (string.IsNullOrEmpty(Request.QueryString["ID"]))
        {
          cc = (CCApplicant)Session["CCApplicant"];
          aplicate_name.Text = cc.FirstName + " " + cc.MiddleName + " " + cc.Surname;
        }
        else
        {
          //getting the querystring...
          string strGUID = Request.QueryString["ID"];
          if (string.IsNullOrEmpty(strGUID))
          {
            Response.Redirect("/Errors/IDRequired.aspx");
            return;
          }
          //we have a GUID, lets load data!...
          try
          {
            cc = new CCApplicant(strGUID);
            cc.LoadCompany();
            Session["CCApplicant"] = cc;
            Session["Company"] = cc.Company;
            btnSubmit.Visible = false;
            btnBack.Visible = false;
            cb1.Checked = cc.AgreedToFeeStructure;
            lblStamp.Text = "Agreed on: " + cc.AgreedToFeeStructureDate.ToString() + ", from IP Address: " + cc.IPAddress;
            lblStamp.Visible = true;
            aplicate_name.Text = cc.FirstName + " " + cc.MiddleName + " " + cc.Surname;
          }
          catch
          { }
        }
        LoadRemoveFees(cc);
        //***temporary hack!
        if (!string.IsNullOrEmpty(cc.ReferralDX))
        {
          decimal RemovalFee;
          if (Decimal.TryParse(cc.ReferralDX, out RemovalFee))
          {
            decimal RemovalFee_Installments = (RemovalFee * (10m / 11m) + 100) * 1.1m;
            rblRemovalFeeOption.Items[0].Text = rblRemovalFeeOption.Items[0].Text.Replace("XXX", RemovalFee.ToString("C"));
            rblRemovalFeeOption.Items[1].Text = rblRemovalFeeOption.Items[1].Text.Replace("YYY", RemovalFee_Installments.ToString("C"));
          }
          else
          {
            rblRemovalFeeOption.Items[0].Text = rblRemovalFeeOption.Items[0].Text.Replace("XXX", "$880");
            rblRemovalFeeOption.Items[1].Text = rblRemovalFeeOption.Items[1].Text.Replace("YYY", "$990");
          }
        }
        else
        {
          rblRemovalFeeOption.Items[0].Text = rblRemovalFeeOption.Items[0].Text.Replace("XXX", "$880");
          rblRemovalFeeOption.Items[1].Text = rblRemovalFeeOption.Items[1].Text.Replace("YYY", "$990");
        }
        //***

        rblRemovalFeeOption.SelectedValue = cc.RemovalFeeOption.ToString();
        

      }
    }
    private void LoadRemoveFees(CCApplicant applicant)
    {
        try
        {
            var removalfee = applicant.GetRemovalFees(applicant.GUID);
            grdRemovalFee.DataSource = removalfee;
            grdRemovalFee.DataBind();
        } 
        catch
        {
            //
        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      if (Page.IsValid)
      {
        Save(true);
        Response.Redirect("AuthorityToAct.aspx");
      }
    }


    protected void Save(Boolean IsFinish)
    {
      CCApplicant cc = new CCApplicant();
      if (Session["CCApplicant"] != null)
      {
        cc = (CCApplicant)Session["CCApplicant"];
      }
      else
      {
        throw new Exception("No CC Applicant Session, or timed out");
      }

      //Collecting ap1 data from the form...
      cc.RemovalFeeOption = Convert.ToInt32(rblRemovalFeeOption.SelectedValue);
      if (cb1.Checked && IsFinish)
      {
        cc.AgreedToFeeStructure = cb1.Checked;
        cc.AgreedToFeeStructureDate = DateTime.Now;
        cc.WebApplicationCompleted = DateTime.Now;
       // cc.AgreedToFeePaymentOptionsDate = DateTime.Now;
      }

      cc.Save();

      Session["CCApplicant"] = cc;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
      Save(false);
      CCApplicant cc = (CCApplicant)Session["CCApplicant"];
      switch (cc.PaymentOptionID)
      {
        case 1:
        case 4:
          Response.Redirect("CreditCardPayment.aspx");
          break;
        case 2:
        case 3:
          Response.Redirect("DirectDebitAuthority.aspx");
          break;
        default:
          break;
      }
    }

    protected void cvAgree_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = (cb1.Checked);
      if (!cb1.Checked)
      {
        lblAgreementMessage.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFD7D7");
        cb1.Focus();
      }
      else
      {
        lblAgreementMessage.BackColor = System.Drawing.Color.Transparent;
      }
    }
  //  protected void SendEmails()
   // {
   //   EmailAgent ea = new EmailAgent();
   //   ea.SendThankYouEmailCC((CCApplicant)Session["CCApplicant"]);
  //  }

  }
}