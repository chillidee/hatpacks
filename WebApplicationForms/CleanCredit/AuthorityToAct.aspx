﻿<%@ Page Title="Clean Credit Application Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuthorityToAct.aspx.cs" Inherits="WebApplicationForms.CleanCredit.AuthorityToAct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
  <section class="featured">
		<div class="content-wrapper">
			<hgroup class="title">
				<h1>Authority to Act</h1>			
      </hgroup>
		</div>
  </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
    <div style="padding: 0;display: none;">
      <table><tr><td style="display:none;"><input runat="server" type="checkbox" class="checkbox" ID="cb1"/></td>
        <td><!--<asp:Label runat="server" ID="lblAgreementMessage" Text="I agree to the above conditions."/> -->
        <br /><asp:CustomValidator runat="server" ID="cvAgree" OnServerValidate="cvAgree_ServerValidate" ErrorMessage="Please sign the form." Display="Dynamic"></asp:CustomValidator>
      </td></tr></table>
     </div>
     <!-- <iframe src="https://au1.documents.adobe.com/public/esignWidget?wid=CBFCIBAA3AAABLblqZhA3RJ-5W2Yct7xUZEVLE3aPdkcKn2O8RaGv7Hv5gRUA7VAh3XSRyP91Ee-qPyGc0sE*&hosted=false" width="100%" height="100%" frameborder="0" style="border: 0; overflow: hidden; min-height: 500px; min-width:initial;"></iframe> -->
     <iframe src="https://au1.documents.adobe.com/public/esignWidget?wid=CBFCIBAA3AAABLblqZhAFDL10ua8lso6hmF3X-f7rjoEyHbC7pDsj05Gg26iirOgx0S7KqeikoWDKi9-7ofc*&hosted=false" width="100%" height="100%" frameborder="0" style="border: 0; overflow: hidden; min-height: 500px; min-width: initial;"></iframe>
    <!-- <p>I hereby give Clean Credit Pty Ltd & Universal Enquires the authority to act on my behalf in all or any of the following matters:</p>
    <ul>
      <li>Apply, obtain and review a copy of my credit report from any credit reporting agency.</li>
      <li>Adjust or correct the credit information contained within my credit file with any credit reporting agency.</li>
      <li>Directly correspond with any credit provider, plaintiff or acting person on behalf of any such individual or organisation for which credit has been provided.</li>
      <li>Correspond with any person or organisation that has provided credit to me or a third party for which I have provided a guarantee or indemnity to vary the terms on which credit was granted in order for me to satisfy any default or to otherwise satisfy my obligations.</li>
      <li>Obtain legal advice on my behalf.</li>
    </ul>
    <p>I authorise Clean Credit Pty Ltd, its agents, employees and consultants to correspond directly pursuant to this appointment with any credit reporting agency or credit provider, legal adviser, government agency or industry Ombudsman with regard to my affairs and financial position and to reveal in the course of those discussions any and all information that I have provided to Clean Credit Pty Ltd.</p>
    <p>I authorise any credit reporting agency, credit provider or government agency approached by Clean Credit Pty Ltd with regard to this appointment to fully disclose to Clean Credit Pty Ltd and its officers, employees, agents, Lawyers, consultants appointed by Clean Credit Pty Ltd any and all information and documents with regards to my affairs, financial position and my dealings with the credit reporting agency, credit provider or government agency.</p>

    <p>I acknowledge Clean Credit Pty Ltd is not liable for communicating its client’s instruction. I indemnify and keep indemnified Clean Credit Pty Ltd for its actions with regard to this appointment.</p>

    <p>I acknowledge that Clean Credit Pty Ltd cannot guarantee the removal or improvement of negative listings from my credit file.</p>

    <p>I acknowledge the refund of removal fees will be processed upon the credit provider conforming they are not willing to 
remove or improve the credit listing or after Clean Credit Pty Ltd has been working on the credit listing/s for a period of six 
months with no resolution.</p>
<p>The entire removal fee must be paid prior to refund.  Dishonoured direct debt payments for removal fees will void refund.</p>
    <p>I authorise Clean Credit Pty Ltd to act as my agent to implement my rights to access all of my credit information in possession or control of any credit provider or credit reporting agency.</p>

    <p> This authority applies in relation to:</p>
    <ul>
      <li>An application, or proposed application, by me for credit.</li>
      <li>My having sought advice in relation to credit.</li>
    </ul>
      <p>This authority will remain in force until the matter(s) of the subject appointment are resolved or until I terminate this authority by notice in writing to Clean Credit Pty Ltd.</p> -->
    <!--<p id="formReminderText" style="font-size: 22px;font-weight: bold;">Please scroll up to sign your name before clicking next</p>-->
  </telerik:RadAjaxPanel>
				
            <div class="goback-btn"><asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click">go back</asp:LinkButton></div>
			<div class="nextbtn-block"><asp:Button ID="btnSubmit" Text="Finish!" CssClass="main" runat="server" OnClick="btnSubmit_Click" /> 
                 <p style="margin: 0;padding-top: 5px;border-top: 1px solid #cacaca;"><img src="../Images/noeffects.png" style="width: 13px;vertical-align: middle;margin-right: 5px;margin-top: -3px;">  <strong>Will NOT affect your credit file</strong></p>
			</div>
		 
  <script>
    window.onload = function () {
      if (!window.addEventListener) {
        window.attachEvent('onmessage', eventHandler);
      } else {
        window.addEventListener('message', eventHandler, false);
      }

      function eventHandler(e) {
        var event = JSON.parse(e.data)
        if (event.type === 'ESIGN') {
          document.getElementById("MainContent_cb1").checked = true;
          document.getElementById('MainContent_btnSubmit').disabled = false;
        }
      }
    }
  </script>
</asp:Content>
