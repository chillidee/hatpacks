﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;
using Bluelaser.Utilities;
using System.IO;

namespace WebApplicationForms.CleanCredit
{
  public partial class CreditCardPayment : Page//PageWithFieldReplaceCC
  {

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!this.IsPostBack)
      {
        //getting data...
        CCApplicant cc;
        if (!string.IsNullOrEmpty(Request.QueryString["admin"]))
        {
          string strGUID = Request.QueryString["ID"];
          cc = new CCApplicant(strGUID);
        }
        else
        {
          cc = (CCApplicant)Session["CCApplicant"];
        }
        //initialising controls...
        ChangeMask(cc.CCProvider);
        string mth; string yr;

        ddlCCExpiryMonth.Items.Add(new ListItem(string.Empty)); //default - blank
        for (int i = 1; i <= 12; i++)
        {
          mth = "0" + i.ToString();
          ddlCCExpiryMonth.Items.Add(new ListItem(mth.Substring(mth.ToString().Length - 2))); //right function
        }
        ddlCCExpiryYear.Items.Add(new ListItem(string.Empty)); //default - blank
        for (int y = DateTime.Now.Year; y <= (DateTime.Now.Year + 7); y++)
        {
          yr = y.ToString();
          ddlCCExpiryYear.Items.Add(new ListItem(yr, yr.Substring(yr.Length - 2))); //right function
        }
        //populating...
        rblCreditCardType.SelectedValue = cc.CCProvider;
        rmtbCreditCardNumber.Text = cc.CCNumber;
        ddlCCExpiryMonth.SelectedValue = cc.CCExpiryMonth;
        ddlCCExpiryYear.SelectedValue = cc.CCExpiryYear;
        rtbCCNameOnCard.Text = (string.IsNullOrEmpty(cc.CCNameOnCard)) ? cc.FirstName + " " + cc.Surname : cc.CCNameOnCard;
        lblAgreementMessage.Text = AgreementMessage();

        if (!string.IsNullOrEmpty(Request.QueryString["admin"]))
        {
          adminView.Visible = true;
          cc.LoadCompany();
          Session["Company"] = cc.Company;
          cb1.Checked = cc.AgreedToPayment;
          string date_agreed = cc.AgreedToPaymentDate.ToString();
          string ipaddress = cc.IPAddress;
          agreed.Text = "Agreed on: " + date_agreed + ", from IP Address: " + ipaddress;
        }

      }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      if (Page.IsValid)
      {
        Save();
        Response.Redirect("AuthorityToAct.aspx");
      }
    }


    protected void Save()
    {
      CCApplicant cc = new CCApplicant();
      if (Session["CCApplicant"] != null)
      {
        cc = (CCApplicant)Session["CCApplicant"];
      }
      else
      {
        throw new Exception("No CC Applicant Session, or timed out");
      }

      //Collecting ap1 data from the form...
      cc.CCProvider = rblCreditCardType.SelectedValue;
      cc.CCNumber = rmtbCreditCardNumber.Text;
      cc.CCExpiryMonth = ddlCCExpiryMonth.SelectedValue;
      cc.CCExpiryYear = ddlCCExpiryYear.SelectedValue;
      cc.CCNameOnCard = rtbCCNameOnCard.Text;

      cc.AgreedToPaymentType = "CC";
      cc.AgreedToPayment = cb1.Checked;
      cc.AgreedToPaymentDate = DateTime.Now;

      cc.Save();

      Session["CCApplicant"] = cc;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
      Save();
      Response.Redirect("P02.aspx");
    }

    protected void rblCreditCardType_SelectedIndexChanged(object sender, EventArgs e)
    {
      CCApplicant cc;
      if (!string.IsNullOrEmpty(Request.QueryString["admin"]))
      {
        string strGUID = Request.QueryString["ID"];
        cc = new CCApplicant(strGUID);
      }
      else
      {
        cc = (CCApplicant)Session["CCApplicant"];
      }
      cc.CCProvider = rblCreditCardType.SelectedValue;
      Session["CCApplication"] = cc;
      ChangeMask(rblCreditCardType.SelectedValue);
      lblAgreementMessage.Text = AgreementMessage();
    }

    protected void rmtbCreditCardNumber_TextChanged(object sender, EventArgs e)
    {
      Page.Validate();
    }

    protected void cvCreditCardNumber_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = (CreditCardUtility.IsValidNumber(args.Value));
    }

    protected void ChangeMask(string CreditCardType)
    {
      switch (CreditCardType)
      {
        case "Visa":
        case "Mastercard":
          rmtbCreditCardNumber.Mask = "#### #### #### ####";
          break;
        case "AMEX":
          rmtbCreditCardNumber.Mask = "#### ###### #####";
          break;
        case "Diners":
          rmtbCreditCardNumber.Mask = "##############";
          break;
        default:
          rmtbCreditCardNumber.Mask = "#### #### #### ####";
          break;
      }
    }

    private bool ExpIsValid(string mth, string yr)
    {
      string yrnow = DateTime.Now.Year.ToString();
      string mthnow = "0" + DateTime.Now.Month.ToString();
      string now = yrnow.Substring(yrnow.Length - 2) + mthnow.Substring(mthnow.Length - 2);
      int inow = Convert.ToInt32(now);
      string compare = yr + mth;
      int icompare = Convert.ToInt32(compare);

      return (icompare >= inow);
    }
    private string AgreementMessage()
    {

      CCApplicant cc;
      if (!string.IsNullOrEmpty(Request.QueryString["admin"]))
      {
        string strGUID = Request.QueryString["ID"];
        cc = new CCApplicant(strGUID);
      }
      else
      {
        cc = (CCApplicant)Session["CCApplicant"];
      }
      string msg;
      switch (cc.PaymentOptionID)
      {
        /*case 1:
          msg = "I agree to pay Clean Credit Pty Ltd an application fee of "
            + string.Format("{0:C}", cc.ApplicationFee)
            + " plus a surcharge of "
            + string.Format("{0:C}",cc.ApplicationFee * CCCreditCard.Surcharge(cc.CCProvider))
            + " bringing to a total of <b>"
            + string.Format("{0:C}", cc.ApplicationFee * (1+CCCreditCard.Surcharge(cc.CCProvider)))
            + "</b> inc GST."
            ;
          break;*/
        case 1:
        case 4:
          msg = "I " + cc.FirstName + " " + cc.MiddleName + " " + cc.Surname + " agree to the terms and conditions and give authority to Clean Credit Pty Ltd to debit from my credit card.";
          //msg = "I authorise Clean Credit Pty Ltd to debit from my credit card <b>over 2 weeks</b>, every <b>"
          //+ cc.DayOfBusinessWeekToDebit
          //+ "</b> for the application fee of <b>"
          //+ String.Format("{0:C}", (((Decimal)cc.ApplicationFee / 2) * 1.1m * (1+CCCreditCard.Surcharge(cc.CCProvider)))) + "</b> inclusive of GST and surcharge.";
          break;
        default:
          msg = "ERROR. Please go back to re-specify payment options.";
          break;
      }
      return msg;
    }

    protected void cvExpMthYear_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = ExpIsValid(ddlCCExpiryMonth.SelectedValue, ddlCCExpiryYear.SelectedValue);
    }

    protected void ddlCCExpiryYear_SelectedIndexChanged(object sender, EventArgs e)
    {
      Page.Validate();
    }

    protected void cvAgree_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = (cb1.Checked);
      if (!cb1.Checked)
      {
        lblAgreementMessage.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFD7D7");
      }
      else
      {
        lblAgreementMessage.BackColor = System.Drawing.Color.Transparent;
      }
    }
  }
}