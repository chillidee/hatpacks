﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms.CleanCredit
{
  public partial class AuthorityToAct : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!this.IsPostBack)
      {
        //getting data...
        CCApplicant cc = (CCApplicant)Session["CCApplicant"];
        //AdobeSignRestServices asrs = new AdobeSignRestServices();
        //asrs.getAuthCode();
        //TextBoxChildID.Text =  asrs.getHtmlString();
        //initialising controls...

        //populating...

        btnSubmit.Enabled = false;

      }
    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      if (Page.IsValid)
      {
        Save();
        SendEmails();
        Response.Redirect("ThankYouCC.aspx");        
      }
    }


    protected void Save()
    {
      CCApplicant cc = new CCApplicant();
      if (Session["CCApplicant"] != null)
      {
        cc = (CCApplicant)Session["CCApplicant"];
      }
      else
      {
        throw new Exception("No CC Applicant Session, or timed out");
      }

      //Collecting ap1 data from the form...
      if (cb1.Checked)
      {
        cc.AgreedToAuthorityToAct = cb1.Checked;
        cc.AgreedToAuthorityToActDate = DateTime.Now;
      }
      
      cc.Save();
      
      Session["CCApplicant"] = cc;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
      //Save();      
      CCApplicant cc = (CCApplicant)Session["CCApplicant"];
      Response.Redirect("CleanCreditDefault.aspx?ID=" + cc.GUID);
      /*switch (cc.PaymentOptionID)
      {
        case 1: case 4:
          Response.Redirect("CreditCardPayment.aspx");
          break;
        case 2: case 3:
          Response.Redirect("DirectDebitAuthority.aspx");
          break;
        default:
          break;
      }*/      
    }

    protected void cvAgree_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = (cb1.Checked);
      if (!cb1.Checked)
      {
        lblAgreementMessage.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFD7D7");
        cb1.Focus();
      }
      else
      {
        lblAgreementMessage.BackColor = System.Drawing.Color.Transparent;
      }
    }

    protected void SendEmails()
    {
        EmailAgent ea = new EmailAgent();
        ea.SendThankYouEmailCC((CCApplicant)Session["CCApplicant"]);
    }

  }
}