﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationForms.Models;

namespace WebApplicationForms.CleanCredit
{
  public partial class P02 : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!this.IsPostBack)
      {
        CCApplicant cc = null;
        //getting data...
        if (string.IsNullOrEmpty(Request.QueryString["ID"]))
        {
          cc = (CCApplicant)Session["CCApplicant"];
          aplicate_name.Text = cc.FirstName + " " + cc.MiddleName + " " + cc.Surname;
        }
        else
        {
          //getting the querystring...
          string strGUID = Request.QueryString["ID"];
          if (string.IsNullOrEmpty(strGUID))
          {
            Response.Redirect("/Errors/IDRequired.aspx");
            return;
          }
          //we have a GUID, lets load data!...
          try
          {
            cc = new CCApplicant(strGUID);
            WebClient myClient = new WebClient();
            UTF8Encoding utf8 = new UTF8Encoding();
            string css_url = "https://applicationform.hatpacks.com.au/Content/Site.css";
            string css = utf8.GetString(myClient.DownloadData(css_url));

            AdminCss.Text = "<style>" +  css + "</style>";
            cc.LoadCompany();
            Session["CCApplicant"] = cc;
            Session["Company"] = cc.Company;
            btnBack.Visible = false;
            btnSubmit.Visible = false;
            cb1.Checked = cc.AgreedToFeePaymentOptions;
            lblStamp.Text = "Agreed on: " + cc.AgreedToFeePaymentOptionsDate.ToString() + ", from IP Address: " + cc.IPAddress;
            lblStamp.Visible = true;
              //rblCourtCost.SelectedIndex = 1;
          }
          catch
          { }
        }

        LoadRemoveFees(cc);
        //***temporary hack!
        if (!string.IsNullOrEmpty(cc.ReferralDX))
        {
          decimal RemovalFee;
          if (Decimal.TryParse(cc.ReferralDX, out RemovalFee))
          {
            decimal RemovalFee_Installments = (RemovalFee * (10m / 11m) + 100) * 1.1m;
            rblRemovalFeeOption.Items[0].Text = rblRemovalFeeOption.Items[0].Text.Replace("XXX", RemovalFee.ToString("C"));
            rblRemovalFeeOption.Items[1].Text = rblRemovalFeeOption.Items[1].Text.Replace("YYY", RemovalFee_Installments.ToString("C"));
          }
          else
          {
            rblRemovalFeeOption.Items[0].Text = rblRemovalFeeOption.Items[0].Text.Replace("XXX", "$880");
            rblRemovalFeeOption.Items[1].Text = rblRemovalFeeOption.Items[1].Text.Replace("YYY", "$990");
          }
        }
        else
        {
          rblRemovalFeeOption.Items[0].Text = rblRemovalFeeOption.Items[0].Text.Replace("XXX", "$880");
          rblRemovalFeeOption.Items[1].Text = rblRemovalFeeOption.Items[1].Text.Replace("YYY", "$990");
        }
        //***

        rblRemovalFeeOption.SelectedValue = cc.RemovalFeeOption.ToString();
        


        //initialising controls...
        if ((cc.Notes.Length >=4)  && (cc.Notes.Substring(0, 4) == "NOPP"))
        {
          /*ListItem li1 = rbl.Items[2];
          ListItem li2 = rbl.Items[3];
          li1.Attributes.Add("display", "none");
          li2.Attributes.Add("display", "none");*/
          rbl.Items.RemoveAt(2);
          rbl.Items.RemoveAt(2);

        }
        //populating...
        
        if (cc.PaymentOptionID != null)
        {
          rbl.SelectedValue = cc.PaymentOptionID.ToString();
          ddlDayOfBusinessWeekToDebit.SelectedValue = cc.DayOfBusinessWeekToDebit;
          divDayOfBusinessWeekToDebit.Visible = (cc.PaymentOptionID == 3 || cc.PaymentOptionID == 4 || cc.PaymentOptionID == 2 || cc.PaymentOptionID == 1);
          divFurtherAgreement.Visible = (cc.PaymentOptionID == 3 || cc.PaymentOptionID == 4 || cc.PaymentOptionID == 2 || cc.PaymentOptionID == 1);
          cb1.Visible = true;
        }
        if (cc.JudgementPaymentOptionID != null)
        {
            rblCourtCost.SelectedValue = cc.JudgementPaymentOptionID.ToString();
        }
        string iText;
        foreach (ListItem li in rbl.Items)
        {
          iText = li.Text;
          iText = iText.Replace("[ApplicationFee]", String.Format("{0:C}", cc.ApplicationFee));
          iText = iText.Replace("[ApplicationFeeWeekly]", String.Format("{0:C}", (((Decimal)cc.ApplicationFee / 2))));
          iText = iText.Replace("[ApplicationFeeWeeklyTotal]", String.Format("{0:C}", (((Decimal)cc.ApplicationFee))));
          /*iText = iText.Replace("[ApplicationFeeWeekly]", String.Format("{0:C}", (((Decimal)cc.ApplicationFee / 4) * 1.1m)));
          iText = iText.Replace("[ApplicationFeeWeeklyTotal]", String.Format("{0:C}", (((Decimal)cc.ApplicationFee) * 1.1m)));*/
          iText = iText.Replace("[SurchargeValue1]", String.Format("{0:C}", cc.ApplicationFee * 0.022m));
          iText = iText.Replace("[SurchargeValue2]", String.Format("{0:C}", cc.ApplicationFee * 0.033m));
          li.Text = iText;
        }
        
        if (cc.JudgementFee > 0)
        {
            pnlJudgement.Visible = true;
            foreach (ListItem li in rblCourtCost.Items)
            {
                iText = li.Text;
                iText = iText.Replace("[JudgementFee]", String.Format("{0:C}", cc.JudgementFee));
                li.Text = iText;
            }
        }
        else
            pnlJudgement.Visible = false;
      }
    }
   
    private void LoadRemoveFees(CCApplicant applicant)
    {
        try
        {
            var removalfee = applicant.GetRemovalFees(applicant.GUID);
            grdRemovalFee.DataSource = removalfee;
            grdRemovalFee.DataBind();
        } 
        catch
        {
            //
        }
    }

    protected void Save(Boolean IsFinish)
    {
        CCApplicant cc = new CCApplicant();
        if (Session["CCApplicant"] != null)
        {
            cc = (CCApplicant)Session["CCApplicant"];
        }
        else
        {
            throw new Exception("No CC Applicant Session, or timed out");
        }

        //Collecting ap1 data from the form...
        cc.RemovalFeeOption = Convert.ToInt32(rblRemovalFeeOption.SelectedValue);
        if (cb1.Checked && IsFinish)
        {
            cc.AgreedToFeeStructure = cb1.Checked;
            cc.AgreedToFeeStructureDate = DateTime.Now;
            cc.WebApplicationCompleted = DateTime.Now;
            // cc.AgreedToFeePaymentOptionsDate = DateTime.Now;
        }

        cc.Save();

        Session["CCApplicant"] = cc;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      if (Page.IsValid)
      {
        Save();
        switch (Convert.ToInt32(rbl.SelectedValue))
        { 
          case 1: case 4:
            Response.Redirect("CreditCardPayment.aspx");
            break;
          case 2: case 3:
            Response.Redirect("DirectDebitAuthority.aspx");
            break;
          default:
            break;
        }
        
      }
    }


    protected void Save()
    {
        CCApplicant cc = new CCApplicant();
        if (Session["CCApplicant"] != null)
        {
        cc = (CCApplicant)Session["CCApplicant"];
        }
        else
        {
        throw new Exception("No CC Applicant Session, or timed out");
        }

        //Collecting ap1 data from the form...
        cc.PaymentOptionID = Convert.ToInt32(rbl.SelectedValue);
        cc.DayOfBusinessWeekToDebit = ddlDayOfBusinessWeekToDebit.SelectedValue;
        cc.RemovalFeeOption = Convert.ToInt32(rblRemovalFeeOption.SelectedValue);
        cc.AgreedToFeePaymentOptions = cb1.Checked;
        cc.AgreedToFeePaymentOptionsDate = DateTime.Now;
        if (string.IsNullOrEmpty(rblCourtCost.SelectedValue ))
            cc.JudgementPaymentOptionID = null;
        else
            cc.JudgementPaymentOptionID = Convert.ToInt32(rblCourtCost.SelectedValue);
        cc.Save();
      
        Session["CCApplicant"] = cc;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
      Save();
      Response.Redirect("P01.aspx");
    }

    protected void rbl_SelectedIndexChanged(object sender, EventArgs e)
    {
        divDayOfBusinessWeekToDebit.Visible = (rbl.SelectedValue == "3" || rbl.SelectedValue == "4" || rbl.SelectedValue == "2" || rbl.SelectedValue == "1");
        divFurtherAgreement.Visible = (rbl.SelectedValue == "3" || rbl.SelectedValue == "4" || rbl.SelectedValue == "2" || rbl.SelectedValue == "1");
        cb1.Visible = true;
        bestdaytoprocess.Visible = (rbl.SelectedValue == "3" || rbl.SelectedValue == "2" || rbl.SelectedValue == "4");
        adminifee.Visible = (rbl.SelectedValue == "3" || rbl.SelectedValue == "2");
        if ((rbl.SelectedValue == "4") || (rbl.SelectedValue == "1"))
        {
            rblCourtCost.Items[1].Attributes.Add("style", "display:none");
            rblCourtCost.SelectedIndex = 0;
        }

        if ((rbl.SelectedValue == "2") || (rbl.SelectedValue == "3"))
        {
            rblCourtCost.Items[0].Attributes.Add("style", "display:none");
            rblCourtCost.SelectedIndex = 1;
        }
    }

    protected void cvAgree_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = (cb1.Checked);
      if (!cb1.Checked)
      {
        lblAgreementMessage.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFD7D7");
        cb1.Focus();
      }
      else
      {
        lblAgreementMessage.BackColor = System.Drawing.Color.Transparent;
      }
    }

  }
}